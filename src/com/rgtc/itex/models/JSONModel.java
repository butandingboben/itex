/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

/**
 * <div class="jp">
 * 
 * </div>
 * <p>
 * <div class="en" style="padding:0em 0.5em" >
 * 
 * </div>
 *
 * @author Joven 'Bob' Montero
 * @version 0.01
 * @since 0.01
 */
public class JSONModel {

	/**
	 * List of test case data contained as JSONObjects.
	 */
	private Map<String, JSONObject> jsonDataList;
	/**
	 * List of test case names.
	 */
	private List<String> testCaseList;

	public JSONModel() {
		jsonDataList = new HashMap<String, JSONObject>();
		testCaseList = new ArrayList<String>();
	}

	/**
	 * @return the jsonDataList
	 */
	public Map<String, JSONObject> getJsonDataList() {
		return jsonDataList;
	}
	/**
	 * @param jsonDataList the jsonDataList to set
	 */
	public void setJsonDataList(Map<String, JSONObject> jsonDataList) {
		this.jsonDataList = jsonDataList;
	}
	/**
	 * @return the testCaseList
	 */
	public List<String> getTestCaseList() {
		return testCaseList;
	}
	/**
	 * @param testCaseList the testCaseList to set
	 */
	public void setTestCaseList(List<String> testCaseList) {
		this.testCaseList = testCaseList;
	}
}
