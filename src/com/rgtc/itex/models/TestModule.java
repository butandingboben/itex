/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.models;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * <div class="jp">
 * 
 * </div>
 * <p>
 * <div class="en" style="padding:0em 0.5em" >
 * 
 * </div>
 *
 * @author Joven 'Bob' Montero
 * @version 0.01
 *		<div class="jp">
 *			
 *		</div>
 *		<div class="en" style="padding:0em 0.5em">
 *			
 *		</div>
 * @since 0.01
 */
public class TestModule {

	/** The file containing the test specification for the test module. **/
	private File source;
	/** The name of the test module. **/
	private String name;
	/** The list containing the test cases for this module. **/
	private List<TestCase> testCaseList;

	public TestModule(File source) {
		this.setSource(source);
		this.setName(source.getName().replace("xls", ""));
		this.setTestCaseList(new ArrayList<TestCase>());
	}

	/**
	 * @return the source
	 */
	public File getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(File source) {
		this.source = source;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the testCaseList
	 */
	public List<TestCase> getTestCaseList() {
		return testCaseList;
	}

	/**
	 * @param testCaseList the testCaseList to set
	 */
	public void setTestCaseList(List<TestCase> testCaseList) {
		this.testCaseList = testCaseList;
	}

}
