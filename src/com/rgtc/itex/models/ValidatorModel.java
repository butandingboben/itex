/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Global Technologies Corporation All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.models;

import java.util.ArrayList;
import java.util.List;

/**
 * <div class="jp">
 * 
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 * 
 * </div>
 *
 * @author Eldon Leuterio
 * @version 0.01
 * @since 0.01
 */
public class ValidatorModel {

	private List<String> scenarioList;
	private String message;
	/**
	 * @return the scenarioList
	 */
	public List<String> getScenarioList() {
		return scenarioList;
	}
	/**
	 * @param scenarioList the scenarioList to set
	 */
	public void setScenarioList(List<String> scenarioList) {
		this.scenarioList = scenarioList;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	public void addScenario(String scenarioName){
		if(null == scenarioList){
			scenarioList = new ArrayList<String>();
		}
		scenarioList.add(scenarioName);
	}
	
}
