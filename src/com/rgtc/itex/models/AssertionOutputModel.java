/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) RococoGlobal Technologies, Inc - All Rights Reserved 2014
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.models;

/**
 * <div class="jp">
 *
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 * This class represents an error in assertions found in the unit tests.
 * </div>
 *
 * @author Dyan Despojo
 * @author Jessamine Jimeno
 * @version 0.01
 * @since 0.01
 */
public class AssertionOutputModel {

	/**
	 * Flag for error in assertion.
	 */
	private boolean isError = false;

	/**
	 * Contains the assertion error message.
	 */
	private String error;

	/**
	 * @return the isError
	 */
	public boolean isError() {
		return isError;
	}
	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}
	/**
	 * @param isError the isError to set
	 */
	public void setIsError(boolean isError) {
		this.isError = isError;
	}
	/**
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}
}
