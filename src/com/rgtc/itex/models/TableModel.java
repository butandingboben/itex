/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) RococoGlobal Technologies Corporation - All Rights Reserved 2013
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.models;

import java.util.List;
import java.util.Map;

/**
 * The model for a table.
 * 
 * @author Eldon Ivan
 * @since 12/03/2013
 * @version 1
 */
public class TableModel {

	/**
	 * Table name.
	 */
	private String tableName;
	
	/**
	 * Column model list.
	 */
	private List<TableColumnModel> colModelList;
	
	/**
	 * Column count.
	 */
	private int colCount;
	
	/**
	 * Row count.
	 */
	private int rowCount;

	/**
	 * @return the tableName
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * @param tableName the tableName to set
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * @return the colModelList
	 */
	public List<TableColumnModel> getColModelList() {
		return colModelList;
	}

	/**
	 * @param colModelList the colModelList to set
	 */
	public void setColModelList(List<TableColumnModel> colModelList) {
		this.colModelList = colModelList;
	}

	/**
	 * @return the colCount
	 */
	public int getColCount() {
		return colCount;
	}

	/**
	 * @param colCount the colCount to set
	 */
	public void setColCount(int colCount) {
		this.colCount = colCount;
	}

	/**
	 * @return the rowCount
	 */
	public int getRowCount() {
		return rowCount;
	}

	/**
	 * @param rowCount the rowCount to set
	 */
	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}
	
	public void addColumnModel(TableColumnModel tableColumnModel){
		this.colModelList.add(tableColumnModel);
	}
}
