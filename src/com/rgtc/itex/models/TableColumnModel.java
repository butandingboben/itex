/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) RococoGlobal Technologies Corporation - All Rights Reserved 2013
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.models;

/**
 * The model for a table column.
 * 
 * @author Eldon Ivan
 * @since 12/03/2013
 * @version 1
 */
public class TableColumnModel {

	/**
	 * Column name.
	 */
	private String name;
	
	/**
	 * Column type.
	 */
	private String type;

	/**
	 * The data.
	 */
	private String data;
	
	/**
	 * The precision of the column
	 */
	private int length;

	/**
	 * Flag to determine if data is modified.</br>
	 * if modified, no quotes. 
	 */
	private boolean isModified;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}

	/**
	 * @return the length
	 */
	public int getLength() {
		return length;
	}

	/**
	 * @param length the length to set
	 */
	public void setLength(int length) {
		this.length = length;
	}

	/**
	 * @return the isModified
	 */
	public boolean isModified() {
		return isModified;
	}

	/**
	 * @param isModified the isModified to set
	 */
	public void setModified(boolean isModified) {
		this.isModified = isModified;
	}
}
