/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) RococoGlobal Technologies Corporation - All Rights Reserved 2013
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.models;

import java.util.List;

/**
 * The model for a constraints.
 * 
 * @author Amper, Jotham J.
 * @since 12/09/2013
 * @version 1
 * @deprecated
 */
public class ConstraintModel {
	
	/**
	 * Table name.
	 */
	private String tableName;
	
	/**
	 * Table constraints list.
	 */
	private List<String> tableConstraintsList;
	
	/**
	 * @return the tableName
	 */
	public String getTableName() {
		return tableName;
	}
	
	/**
	 * @param tableName the tableName to set
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	/**
	 * @return the tableConstraintsList
	 */
	
	public List<String> getTableConstraintsList() {
		return tableConstraintsList;
	}
	
	/**
	 * @param tableConstraintsList the tableConstraintsList to set
	 */
	public void setTableConstraintsList(List<String> tableConstraintsList) {
		this.tableConstraintsList = tableConstraintsList;
	}	
}
