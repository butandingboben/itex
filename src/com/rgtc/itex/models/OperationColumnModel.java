/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) RococoGlobal Technologies Corporation - All Rights Reserved 2013
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.models;

/**
 * The model for an operation column.
 * 
 * @author Amper, Jotham J.
 * @since 12/09/2013
 * @version 1
 * @deprecated
 */
public class OperationColumnModel {

	/**
	 * Column name.
	 */
	private String name;

	/**
	 * Column id.
	 */
	private String id;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
}
