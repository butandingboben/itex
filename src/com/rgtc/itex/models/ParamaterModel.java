/**
 * 
 */
package com.rgtc.itex.models;

/**
 * @author Joven Montero
 *
 */
public class ParamaterModel {
	private int startCol = 0;
	private int startRow = 0;
	private int depth = 1;
	private int lastCol = 0;
	private boolean isList = false;
	
	public int getStartCol() {
		return startCol;
	}
	public void setStartCol(int startCol) {
		this.startCol = startCol;
	}
	public int getStartRow() {
		return startRow;
	}
	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}
	public int getDepth() {
		return depth;
	}
	public void setDepth(int depth) {
		this.depth = depth;
	}
	public int getLastCol() {
		return lastCol;
	}
	public void setLastCol(int lastCol) {
		this.lastCol = lastCol;
	}
	public boolean isList() {
		return isList;
	}
	public void setList(boolean isList) {
		this.isList = isList;
	}
	
}
