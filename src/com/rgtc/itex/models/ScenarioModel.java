/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Global Technologies Corporation All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.models;

import java.util.List;

/**
 * <div class="jp">
 * 
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 * 
 * </div>
 *
 * @author Eldon Leuterio
 * @version 0.01
 * @since 0.01
 */
public class ScenarioModel {
	
	private String scenarioName;
	
	private List<TableModel> beforeDataTables;
	
	private List<TableModel> afterDataTables;

	/**
	 * @return the scenarioName
	 */
	public String getScenarioName() {
		return scenarioName;
	}

	/**
	 * @param scenarioName the scenarioName to set
	 */
	public void setScenarioName(String scenarioName) {
		this.scenarioName = scenarioName;
	}

	/**
	 * @return the beforeDataTables
	 */
	public List<TableModel> getBeforeDataTables() {
		return beforeDataTables;
	}

	/**
	 * @param beforeDataTables the beforeDataTables to set
	 */
	public void setBeforeDataTables(List<TableModel> beforeDataTables) {
		this.beforeDataTables = beforeDataTables;
	}

	/**
	 * @return the afterDataTables
	 */
	public List<TableModel> getAfterDataTables() {
		return afterDataTables;
	}

	/**
	 * @param afterDataTables the afterDataTables to set
	 */
	public void setAfterDataTables(List<TableModel> afterDataTables) {
		this.afterDataTables = afterDataTables;
	}
	
	public void addBeforeDataTable(TableModel tableModel){
		beforeDataTables.add(tableModel);
	}
	
	public void addAfterDataTable(TableModel tableModel){
		afterDataTables.add(tableModel);
	}

}
