/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.models;

import org.json.JSONObject;

/**
 * <div class="jp">
 * 
 * </div>
 * <p>
 * <div class="en" style="padding:0em 0.5em" >
 * 
 * </div>
 *
 * @author Joven 'Bob' Montero
 * @version 0.01
 *		<div class="jp">
 *			
 *		</div>
 *		<div class="en" style="padding:0em 0.5em">
 *			
 *		</div>
 * @since 0.01
 */
public class TestCase {

	/** The name of the test case. **/
	private String name;
	/** The expected HTTP status code. **/
	private int expectedStatusCd;
	/** The assertion flag for the test. **/
	private boolean assertFlag = false;
	/** The NA flag for the test. **/
	private boolean invalidFlag = false;
	/** JSONObject instance that will hold the json data for input **/
	private JSONObject inputJSON = new JSONObject();
	/** JSONObject instance that will hold the json data for the expected response including the status code **/
	private JSONObject expectedJSON = new JSONObject();
	/** JSONObject instance that will hold the json data for the request header values **/
	private JSONObject requestHeaderJSON = new JSONObject();
	/** JSONObject instance that will hold the json data for URL parameter values **/
	private JSONObject urlParameterJSON = new JSONObject();
	/** JSONObject instance that will hold the json data for the execution log **/
	private JSONObject executionLogJSON = new JSONObject();
	/** JSONObject instance that will hold the json data for the operation log **/
	private JSONObject operationLogJSON = new JSONObject();
	/** JSONObject instance that will hold the json data for the datastore operation log. **/
	private JSONObject dsOperationLogJSON = new JSONObject();
	/** JSONObject instance that will hold the json data for the task queue. **/
	private JSONObject taskQueueJSON = new JSONObject();

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the expectedStatusCd
	 */
	public int getExpectedStatusCd() {
		return expectedStatusCd;
	}
	/**
	 * @param expectedStatusCd the expectedStatusCd to set
	 */
	public void setExpectedStatusCd(int expectedStatusCd) {
		this.expectedStatusCd = expectedStatusCd;
	}
	/**
	 * @return the assertFlag
	 */
	public boolean isAssertFlag() {
		return assertFlag;
	}
	/**
	 * @param assertFlag the assertFlag to set
	 */
	public void setAssertFlag(boolean assertFlag) {
		this.assertFlag = assertFlag;
	}
	/**
	 * @return the invalidFlag
	 */
	public boolean isInvalidFlag() {
		return invalidFlag;
	}
	/**
	 * @param invalidFlag the invalidFlag to set
	 */
	public void setInvalidFlag(boolean invalidFlag) {
		this.invalidFlag = invalidFlag;
	}
	/**
	 * @return the inputJSON
	 */
	public JSONObject getInputJSON() {
		return inputJSON;
	}
	/**
	 * @param inputJSON the inputJSON to set
	 */
	public void setInputJSON(JSONObject inputJSON) {
		this.inputJSON = inputJSON;
	}
	/**
	 * @return the expectedJSON
	 */
	public JSONObject getExpectedJSON() {
		return expectedJSON;
	}
	/**
	 * @param expectedJSON the expectedJSON to set
	 */
	public void setExpectedJSON(JSONObject expectedJSON) {
		this.expectedJSON = expectedJSON;
	}
	/**
	 * @return the requestHeaderJSON
	 */
	public JSONObject getRequestHeaderJSON() {
		return requestHeaderJSON;
	}
	/**
	 * @param requestHeaderJSON the requestHeaderJSON to set
	 */
	public void setRequestHeaderJSON(JSONObject requestHeaderJSON) {
		this.requestHeaderJSON = requestHeaderJSON;
	}
	/**
	 * @return the urlParameterJSON
	 */
	public JSONObject getUrlParameterJSON() {
		return urlParameterJSON;
	}
	/**
	 * @param urlParameterJSON the urlParameterJSON to set
	 */
	public void setUrlParameterJSON(JSONObject urlParameterJSON) {
		this.urlParameterJSON = urlParameterJSON;
	}
	/**
	 * @return the executionLogJSON
	 */
	public JSONObject getExecutionLogJSON() {
		return executionLogJSON;
	}
	/**
	 * @param executionLogJSON the executionLogJSON to set
	 */
	public void setExecutionLogJSON(JSONObject executionLogJSON) {
		this.executionLogJSON = executionLogJSON;
	}
	/**
	 * @return the operationLogJSON
	 */
	public JSONObject getOperationLogJSON() {
		return operationLogJSON;
	}
	/**
	 * @param operationLogJSON the operationLogJSON to set
	 */
	public void setOperationLogJSON(JSONObject operationLogJSON) {
		this.operationLogJSON = operationLogJSON;
	}
	/**
	 * @return the dsOperationLogJSON
	 */
	public JSONObject getDsOperationLogJSON() {
		return dsOperationLogJSON;
	}
	/**
	 * @param dsOperationLogJSON the dsOperationLogJSON to set
	 */
	public void setDsOperationLogJSON(JSONObject dsOperationLogJSON) {
		this.dsOperationLogJSON = dsOperationLogJSON;
	}
	/**
	 * @return the taskQueueJSON
	 */
	public JSONObject getTaskQueueJSON() {
		return taskQueueJSON;
	}
	/**
	 * @param taskQueueJSON the taskQueueJSON to set
	 */
	public void setTaskQueueJSON(JSONObject taskQueueJSON) {
		this.taskQueueJSON = taskQueueJSON;
	}

}
