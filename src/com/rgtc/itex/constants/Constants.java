/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.constants;

/**
 * <div class="jp">
 *
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 *
 * </div>
 *
 * @author Eldon Leuterio
 * @version 0.01
 * @since 0.01
 */
public interface Constants {

	public final String PATH = "-P";
	public final String FILE = "-f";
	public final String SUFFIX = "-sx";
	public final String PREFIX = "-px";
	public final String USER_NAME = "-u";
	public final String PASSWORD = "-p";
	public final String SCHEMA = "-s";
	public final String URL = "-url";
	public final String TEST_CASE = "-tc";
	public final String IGNORE_TEST_CASE = "-itc";
	public final String MANUAL_TEST_CASE = "-mtc";
	public final String NA_TEST_CASE = "-na";
	public final String OUTPUT_PATH = "-o";

	/* --------------------------------------------------------------------------------
	 * [TESTING ENVIRONMENT]
	 * -------------------------------------------------------------------------------- */

	public static final String REQUEST_SCHEME = "http";
	public static final String REQUEST_HOST = "127.0.0.1";
	public static final int REQUEST_PORT = 8888;
	/** IP Address of the server. **/
	public static final String CONFIG_URL = "jdbc:mysql://localhost:3306";
	/** Name of the schema/database. **/
	public static final String CONFIG_SCHEMA = "mypage";
	/** User name to be used for accessing the database. **/
	public static final String CONFIG_USERNAME = "root";
	/** Password to be used for accessing the database. **/
	public static final String CONFIG_PASSWORD = "root";
	/** Base url for test the rest api. **/
	public static final String BASE_URL = "http://localhost:8888";
	/** The sheet name containing the database data needed for the test. **/
	public static final String DB_DATA_SHEET = "DB_Data";
	/** Main directory containing all IT Specification files. **/
	public static final String MAIN_DIR =
			"C:\\SBC\\Iteration 1\\02_SDLC\\04_Testing\\01_Test Specifications and Results\\01_IT Specifications\\";
	/** Directory for storing sql dump files. **/
	public static final String DUMP_DIR = "Dumps\\";
	/** Valid file format. **/
	public static final String EXCEL_FILE_FORMAT = "xls";
	/** Pre-fix for cron-related test. **/
	public static final String CRON_TEST = "API_02_04_01_01";
	/** Output file name. **/
	public static final String OUTPUT_FILE_NAME = "IT Execution Result";
	/** Output file format. **/
	public static final String OUTPUT_FILE_FORMAT = "XLS";
	public static final String URL_PARAMETER = "URL Parameter";
	public static final String OPERATION_CONTENT = "operationContent";
	public static final String CONTENT = "content";

	public static final String CRON_LEVEL = "API_02_04_01_01";

	/* --------------------------------------------------------------------------------
	 * [UI Constants]
	 * -------------------------------------------------------------------------------- */

	public static final String SAVE_BUTTON = "Save";
	public static final String BROWSE_BUTTON = "Browse";
	public static final String RUN_BUTTON = "Run";
	public static final String TEST_SOURCE_TABLE = "TEST_SOURCE";
	public static final String TEST_CASE_TABLE = "TEST_CASE";

}
