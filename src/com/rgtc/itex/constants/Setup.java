/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.constants;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * <div class="jp">
 * 
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 * 
 * </div>
 *
 * @author Eldon Leuterio
 * @version 0.01
 * @since 0.01
 */
public class Setup {

	private static Setup instance = null;
	private static Properties prop = new Properties();
	
	public static Setup getInstance(){
		if(null == instance){
			instance = new Setup();
			 InputStream is = null;
		        try {
		        	is = Setup.class.getClassLoader().getResourceAsStream("setup.properties");
		            prop.load(is);
		            is.close();
		        } catch (IOException e) {
		        	System.out.println("getInstance"+" IOException: "+e.getMessage());
		        	e.printStackTrace();
		        }
		}
		return instance;
	}
	
	public static String getClassName(String key) {
		String s = prop.getProperty(key);
		String sa[] = s.split(",");
		return sa[0];
	}
	
	public static String getPath(String key) {
		String s = prop.getProperty(key);
		String sa[] = s.split(",");
		return sa[1];
	}
	public static String getRequestType(String key) {
		String s = prop.getProperty(key);
		String sa[] = s.split(",");
		return sa[2];
	}
	
	public static String getAPINumber(String s){
			StringBuffer sb = new StringBuffer();
			sb.append("API");
			for(int i="API".length(); i<s.length(); i++){
				if(isNumericOr_(String.valueOf(s.charAt(i)))){
					sb.append(s.charAt(i));
				}else if(String.valueOf(s.charAt(i)).equalsIgnoreCase(" ")){
					break;
				}
			}
		String tmp = sb.toString();
		if(tmp.endsWith("_")){
			tmp = tmp.substring(0, tmp.length()-1);
		}
		return tmp;
	}
	
	public static boolean isNumericOr_(String s){
		boolean b;
		
		try {
			Integer.parseInt(s);
			b = true;
		} catch (NumberFormatException e) {
			b = false;
		}
		
		if(!b && "_".equalsIgnoreCase(s)){
			b = true;
		}
		return b;
	}
}
