/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.main;

import java.io.File;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.conn.HttpHostConnectException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.appengine.api.datastore.Entity;
import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import com.rgtc.itex.constants.Constants;
import com.rgtc.itex.constants.Setup;
import com.rgtc.itex.exceptions.ParameterException;
import com.rgtc.itex.models.AssertionOutputModel;
import com.rgtc.itex.models.ResponseModel;
import com.rgtc.itex.service.AesService;
import com.rgtc.itex.service.ApiService;
import com.rgtc.itex.service.DatastoreService;
import com.rgtc.itex.service.FileService;
import com.rgtc.itex.service.TaskQueueService;
import com.rgtc.itex.util.JSONExcelUtil;


/**
 * <div class="jp">
 *
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 *
 * </div>
 *
 * @author Eldon Leuterio
 * @version 0.01
 * @since 0.01
 */
public class ITEx {

	private static String filePath;
	private static String outputPath;
	private static String filePrefix;
	private static String[] fileSuffix;
	private static String fileName;
	private static String userName;
	private static String password;
	private static String schema;
	private static String url;
	private static List<String> filePrefixList;
	private static List<String> testCaseList;
	private static List<String> ignoreTestCaseList = new ArrayList<String>();
	private static List<String> invalidTestCaseList = new ArrayList<String>();
	private static int testCount = 0;
	private static int successCount = 0;
	private static int failCount = 0;
	private static int invalidCount = 0;
	private static List<String> failedTestList = new ArrayList<String>();
	private static List<String> failedDbTestList = new ArrayList<String>();
	private static List<String> failedResponseTestList = new ArrayList<String>();
	private static List<String> failedStatusTestList = new ArrayList<String>();
	private static List<String> failedOpLogTestList = new ArrayList<String>();
	private static List<String> failedExecLogTestList = new ArrayList<String>();
	private static List<String> ignoredTestList = new ArrayList<String>();
	private static List<String> naTestList = new ArrayList<String>();
	private static List<String> exceptionTestList = new ArrayList<String>();
	private static String startRange;
	private static String endRange;
	private static boolean rangeFlag = false;
	/**
	 * List of file names retrieved from the user-specified directory.
	 */
	private static List<String> filenameList;

	@SuppressWarnings("static-access")
	public static void main(String[] args) throws Exception {
		List<String> arguments = Arrays.asList(args);
		setParam(arguments, Constants.PATH);
		setParam(arguments, Constants.PREFIX);
		setParam(arguments, Constants.SUFFIX);
		setParam(arguments, Constants.FILE);
		setParam(arguments, Constants.SCHEMA);
		setParam(arguments, Constants.URL);
		setParam(arguments, Constants.USER_NAME);
		setParam(arguments, Constants.PASSWORD);
		setParam(arguments, Constants.TEST_CASE);
		setParam(arguments, Constants.IGNORE_TEST_CASE);
		setParam(arguments, Constants.NA_TEST_CASE);
		setParam(arguments, Constants.OUTPUT_PATH);

		FileService fileService = new FileService();
		filenameList = fileService.getListOfFiles(new File(filePath), filePrefixList ,fileSuffix, fileName);

		File dumpDir = new File(filePath+"/"+ Constants.DUMP_DIR);
		// Deleting all sql dump file inside the Dumps folder.
    	if (dumpDir.exists()) {
			for (File dumpFile : dumpDir.listFiles()) {
				dumpFile.delete();
			}
		} else {
			dumpDir.mkdir();
		}

		for(File testSource: fileService.getFiles()){
			try {
				StringBuffer output = new StringBuffer();
				String outputDir = testSource.getAbsolutePath().replace(testSource.getName(), "") + Constants.DUMP_DIR;
				FileService fservice = new FileService();
				output.append("-----------------------------------------------------------------------------------------------------------------");
				output.append("\n");
				output.append("Test Module:\t" + testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, ""));
				output.append("\n");
				output.append("-----------------------------------------------------------------------------------------------------------------");
				output.append("\n");
				output.append("\n");
				fservice.writeToFile(
						outputDir
							+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
							+ " IT Execution Result.txt",
						output.toString());
				System.out.println(output);

				// Getting the start time of the test.
		    	Long startRunTime = getTime();

		    	// Reset counters and lists
		    	testCount = 0;
		    	successCount = 0;
		    	failCount = 0;
		    	invalidCount = 0;
		    	failedTestList = new ArrayList<String>();
		    	failedDbTestList = new ArrayList<String>();
		    	failedResponseTestList = new ArrayList<String>();
		    	failedStatusTestList = new ArrayList<String>();
		    	failedOpLogTestList = new ArrayList<String>();
		    	ignoredTestList = new ArrayList<String>();
		    	naTestList = new ArrayList<String>();
		    	exceptionTestList = new ArrayList<String>();

		    	// Executing the test.
		    	if (!testSource.getName().contains(Constants.CRON_LEVEL)) {
					executeTest(testSource);
				} else {
					executeCronTest(testSource);
				}
				Long endRunTime = getTime();

				// Calculating the total run time.
				float totalRunTime =  ((float) (endRunTime - startRunTime)) / 1000;
				int totalHours = 0;
				int totalMinutes = 0;
				int totalSeconds = 0;
//				DecimalFormat formatTime = new DecimalFormat("#0.000");

				totalHours = (int) ((totalRunTime / 60) / 60);
				totalMinutes = (int) ((totalRunTime / 60) - (totalHours * 60));
				totalSeconds = (int) totalRunTime - (totalMinutes * 60) - (totalHours * 3600);

				output = new StringBuffer();
				output.append("\n");
				output.append("Test execution finished.");
				output.append("\n");
				output.append("Total Time (sec): ");
				if (0 < totalHours) {
					output.append(totalHours + "hr ");
				}
				if (0 < totalMinutes) {
					output.append(totalMinutes + "min ");
				}
				if (0 < totalSeconds) {
					output.append(totalSeconds + "s");
				}
				output.append("\n");
				output.append("Test Summary:");
				output.append("\n");
				output.append("\tNo. of Tests:\t" + testCount);
				output.append("\n");
				output.append("\t\tSuccess:\t" + successCount);
				output.append("\n");
				output.append("\t\tFailure:\t" + failCount);
				output.append("\n");
				if (0 < ignoredTestList.size()) {
					output.append("\t\tIgnored:\t" + ignoredTestList.size());
					output.append("\n");
				}
				if (0 < invalidCount) {
					output.append("\t\tNA:\t\t\t" + invalidCount);
					output.append("\n");
				}
				if (0 < exceptionTestList.size()) {
					output.append("\t\tError:\t" + exceptionTestList.size());
					output.append("\n");
				}
				output.append("\n");
				if (0 < failCount) {
					output.append("\tFailed Tests:\t");
					output.append("\n");
					output.append(failedTestSummary());
				}
				if (0 < ignoredTestList.size()) {
					output.append("\tIgnored Tests:\t");
					output.append("\n");
					for (String testCase : ignoredTestList) {
						output.append("\t\t" + testCase);
						output.append("\n");
					}
				}
				if (0 < invalidCount) {
					output.append("\tNA Tests:\t");
					output.append("\n");
					for (String testCase : naTestList) {
						output.append("\t\t" + testCase);
						output.append("\n");
					}
				}
				if (0 < exceptionTestList.size()) {
					output.append("\tException Occurred:\t");
					output.append("\n");
					for (String testCase : exceptionTestList) {
						output.append("\t\t" + testCase);
						output.append("\n");
					}
				}
				fservice.writeToFile(
						outputDir
							+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
							+ " IT Execution Result.txt",
						output.toString());
				System.out.println(output);
				System.out.println("Output location: " + dumpDir.getAbsolutePath());
				System.out.println();

			} catch (Exception e) {
				e.printStackTrace();
				System.err.println(testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "") + ": "
						+ "Something went wrong. Unable to run test.");
			}
		}

//		DatastoreService ds = new DatastoreService("localhost", 8888, "XXXX", "XXXX");
//		ds.put();
//		Entity e = ds.getEntry("MypageBackendTaskLog", null);
//		JSONObject actualBackendTaskLog = new JSONObject(e.getProperties());


//		ds.getEntry("MypageOperationLog", null);
//		registrationCodeGenerationBucket
//		CloudStorageService csv = new CloudStorageService("localhost", 8888, "XXXX", "XXXX");
//		System.out.println("default: "+AppIdentityServiceFactory.getAppIdentityService().getDefaultGcsBucketName());
//		GcsFilename fileName = new GcsFilename("app_default_bucket",
//				"key");
//		csv.writeToFile();
//		csv.setupGSC();
//		csv.createBucket(fileName, "test");
//		executeTest(fileService.getFiles());

	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @return output
	 * @since 0.01
	 */
	private static String failedTestSummary() {
		StringBuffer output = new StringBuffer();

		output.append("\t\t----------------------------------------------------------------------------------------------" + "\n");
		output.append("\t\t| Failed Test Case          |    DB    | Response |  Status  | Operation Log | Execution Log |" + "\n");
		output.append("\t\t----------------------------------------------------------------------------------------------" + "\n");
		for (String testCase : failedTestList) {
			if (25 == testCase.length()) {
				output.append("\t\t| " + testCase + " |");
			} else {
				output.append("\t\t| " + testCase + "    |");
			}
			if (failedDbTestList.contains(testCase)) {
				output.append("   " + "Fail" + "   |");
			} else {
				output.append("   " + "    " + "   |");
			}
			if (failedResponseTestList.contains(testCase)) {
				output.append("   " + "Fail" + "   |");
			} else {
				output.append("   " + "    " + "   |");
			}
			if (failedStatusTestList.contains(testCase)) {
				output.append("   " + "Fail" + "   |");
			} else {
				output.append("   " + "    " + "   |");
			}
			if (failedOpLogTestList.contains(testCase)) {
				output.append("     " + "Fail" + "      |");
			} else {
				output.append("     " + "    " + "      |");
			}
			if (failedExecLogTestList.contains(testCase)) {
				output.append("     " + "Fail" + "      |");
			} else {
				output.append("     " + "    " + "      |");
			}
			output.append("\n");
		}
		output.append("\t\t----------------------------------------------------------------------------------------------" + "\n");

		return output.toString();
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * Sets the parameters (optional/required for application use.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param list the list of arguments (key + value)
	 * @param param the parameter to set.
	 * @throws ParameterException
	 * @since 0.01
	 */
	public static void setParam(List<String> list, String param) throws ParameterException{
		try {
			if(list.contains(param)){
				int i = list.indexOf(param);
				String val = list.get(i+1);
				if (isValid(val)) {
					switch (param) {
					case Constants.PATH:
						filePath = val;
						break;
					case Constants.PREFIX:
						filePrefixList = Arrays.asList(val.split(","));
						break;
					case Constants.SUFFIX:
						fileSuffix = val.replaceFirst("^,", "").split(",");
						break;
					case Constants.FILE:
						fileName = val;
						break;
					case Constants.USER_NAME:
						userName = val;
						break;
					case Constants.PASSWORD:
						password = val;
						break;
					case Constants.SCHEMA:
						schema = val;
						break;
					case Constants.URL:
						url = val;
						break;
					case Constants.TEST_CASE:
						if (val.contains("~")) {
							startRange = val.split("~")[0].trim().replace("\n", "");
							endRange = val.split("~")[1].trim().replace("\n", "");
							rangeFlag = true;
						} else {
							testCaseList = Arrays.asList(val.split(","));
						}
						break;
					case Constants.IGNORE_TEST_CASE:
						ignoreTestCaseList = Arrays.asList(val.split(","));
						break;
					case Constants.NA_TEST_CASE:
						invalidTestCaseList = Arrays.asList(val.split(","));
						break;
					case Constants.OUTPUT_PATH:
						outputPath = val;
						break;
					}
				}else{
					throw new Exception();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new ParameterException("argument: ["+param+"] defined but no corresponding value found.");
		}


	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * Checks if the appended value is a real value and not an argument.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param s the string to evaluate
	 * @return boolean
	 * @since 0.01
	 */
	public static boolean isValid(String s){
		boolean isValid;
		switch (s){
		case Constants.PATH:
		case Constants.PREFIX:
		case Constants.SUFFIX:
		case Constants.FILE:
		case Constants.USER_NAME:
		case Constants.PASSWORD:
		case Constants.SCHEMA:
		case Constants.URL:
		case Constants.TEST_CASE:
		case Constants.IGNORE_TEST_CASE:
		case Constants.NA_TEST_CASE:
		case Constants.OUTPUT_PATH:
			isValid = false;
			break;
		default:
			isValid = true;
		}
		return isValid;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param files
	 * @throws Exception
	 * @since 0.01
	 */
	@SuppressWarnings("static-access")
	public static void executeTest(File testSource) throws Exception {
		/** DataLoaderUtil instance. **/
		DataLoaderUtil dataloader = null;
		/** JSONExcelUtil instance **/
		JSONExcelUtil jsonExcelUtil = new JSONExcelUtil();
		/** JSONObject instance that will hold the json data for input **/
		JSONObject inputJSON = new JSONObject();
		/** JSONObject instance that will hold the json data for the expected response including the status code **/
		JSONObject expectedJSON = new JSONObject();
		/** JSONObject instance that will hold the json data for the request header values **/
		JSONObject requestHeaderJSON = new JSONObject();
		/** JSONObject instance that will hold the json data for URL parameter values **/
		JSONObject urlParameterJSON = new JSONObject();
		/** JSONObject instance that will hold the json data for the operation log **/
		JSONObject operationLogJSON = new JSONObject();

		/** The expected response status code **/
		int expectedStatusCd = 500;
		String testCaseName = "";
		String startTimestamp = "";
		String endTimestamp = "";
		String outputDir = null;

		if(outputPath == null){
			outputDir = testSource.getAbsolutePath().replace(testSource.getName(), "") + Constants.DUMP_DIR;
		}else{
			outputDir = outputPath;
		}


		// Initializes the DataLoaderUtil instance.
		dataloader = new DataLoaderUtil(
				schema,
				url,
				userName,
				password);

		// Loads the test source file.
		dataloader.loadFile(testSource.getAbsolutePath());

		// Loads the sheet containing the data to inserted into the database.
		dataloader.loadSheet(Constants.DB_DATA_SHEET);

		// Loads the test source file for getting the JSON inputs.
		jsonExcelUtil.loadTestSource(testSource);

		List<String> testCases = new ArrayList<String>();
		if (null != testCaseList){
			// if a test case is provided, execute it.
//			testCases.add(testCaseList);
			testCases = testCaseList;
		} else {
			// else, execute all test cases.
			testCases = dataloader.getScenarioNames();
		}

		// loop each test case.
		for(String name : testCases) {
			FileService fservice = new FileService();
			// Skipping of non-existent test case number in the current file.
			if (!dataloader.getScenarioNames().contains(name)) {
				continue;
			}

			// Checking if the current test is included in the ignore list.
			if (ignoreTestCaseList.contains(name)) {
				if (rangeFlag) {
					if (startRange.equals(name)) {
						rangeFlag = false;
					}
				} else {
					if (null != endRange
						&& endRange.equals(name)) {
						break;
					}
				}
				ignoredTestList.add(name);
				continue;
			}

			// Checking if the current test is included in the test case range.
			if (rangeFlag) {
				if (startRange.equals(name)) {
					rangeFlag = false;
				} else {
					continue;
				}
			}

			// Generating the initial timestamp.
			startTimestamp = generateTimestamp();
			try {
				testCount++;
				testCaseName = name;
				inputJSON = null;
				expectedJSON = null;
				requestHeaderJSON = null;
				urlParameterJSON = null;
				operationLogJSON = null;
				expectedStatusCd = 500;

				// Removing all operation logs and execution logs;
				emptyLogs("MypageOperationLog");
				emptyLogs("MypageBackendTaskLog");

				// Validation the format of the output from the excel file.
				String message = dataloader.validateScenario(testCaseName);
				if (0 > message.length()) {
					System.err.println("[" + testCaseName + "]\n"
							+ "Dataloader error: "
							+ message + "\n");
				} else {
					// Retrieve the data for this test case from the file.
					inputJSON = jsonExcelUtil.getDataJSONList().get(testCaseName);
					expectedJSON = jsonExcelUtil.getExpectedJSONList().get(testCaseName);
					requestHeaderJSON = jsonExcelUtil.getRequestHeaderJSONList().get(testCaseName);
					urlParameterJSON = jsonExcelUtil.getUrlParameterJSONList().get(testCaseName);
					operationLogJSON = jsonExcelUtil.getOperationLogJSONList().get(testCaseName);

					if (null != expectedJSON) {
						// Get expected response code from the expectedJSON object then
						// remove it from the json object.
						expectedStatusCd = expectedJSON.getInt("status");
						expectedJSON.remove("status");
					} else {
						endTimestamp = generateTimestamp();
						failedTestList.add(name);
						failedDbTestList.add(name);
						failedResponseTestList.add(name);
						failedStatusTestList.add(name);
						failedOpLogTestList.add(name);
						failCount++;

						// Generating the output format.
						StringBuffer output = new StringBuffer();
						output.append("Test Case No.:\t" + testCaseName + "\n");
						output.append("Test Result:\t" + "Test case not found\n");
						output.append("Start Time:\t\t" + startTimestamp + "\n");
						output.append("End Time:\t\t" + endTimestamp + "\n");
						output.append("Input:" + "\n");
						output.append("\tURL Parameter: " + "None\n");
						output.append("\tRequest Header: " + "\n");
						output.append("\t" + "{}" + "\n");
						output.append("\tJSON Data: " + "\n");
						output.append("\t" + "{}" + "\n");
						output.append("\n");
						output.append("Output: " + "\n");
						output.append("\tRequest URL:\t\t\t\t" + "None\n");
						output.append("\tRequest Type:\t\t\t\t" + "None\n");
						output.append("\tHTTP Status Result:\t\t\t" + "NG\n");
						output.append("\t\tExpected HTTP Status:\t" + "None\n");
						output.append("\t\tActual HTTP Status:\t\t" + "None\n");
						output.append("\tJSON Result:\t\t\t\t" + "NG\n");
						output.append("\t\tExpected JSON Output:" + "\n");
						output.append("\t\t" + "{}" + "\n");
						output.append("\t\tActual JSON Output:" + "\n");
						output.append("\t\t" + "{}\n");
						output.append("\tDB Result:\t" + "NG\n");
						output.append("\n");

						System.out.println(output);

						// Saving the output into the results file.
						fservice.writeToFile(
								outputDir
								+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
								+ " IT Execution Result.txt", output.toString());
						if (!rangeFlag) {
							if (null != endRange) {
								if (endRange.equals(name)) {
									break;
								}
							}
						}
						continue;
					}

					if (invalidTestCaseList.contains(testCaseName)) {
						endTimestamp = generateTimestamp();
						naTestList.add(name);
						invalidCount++;

						// Generating the output format.
						StringBuffer output = new StringBuffer();
						output.append("Test Case No.:\t" + testCaseName + "\n");
						output.append("Test Result:\t" + "NA\n");
						output.append("Start Time:\t\t" + startTimestamp + "\n");
						output.append("End Time:\t\t" + endTimestamp + "\n");
						output.append("Input:" + "\n");
						if (null != urlParameterJSON) {
							output.append("\tURL Parameter: ");
							output.append(urlParameterJSON.getString(Constants.URL_PARAMETER) + "\n");
						} else {
							output.append("\tURL Parameter: " + "None\n");
						}
						output.append("\tRequest Header: " + "\n");
						output.append("\t" + requestHeaderJSON.toString(5).replace("\n", "\n\t") + "\n");
						output.append("\tJSON Data: " + "\n");
						if (null != inputJSON) {
							output.append("\t" + inputJSON.toString(5).replace("\n", "\n\t") + "\n");
						} else {
							output.append("\t" + "{}" + "\n");
						}
						output.append("\n");
						output.append("Output: " + "\n");
						output.append("\tRequest URL:\t\t\t\t" + "Invalid\n");
						output.append("\tRequest Type:\t\t\t\t" + "None\n");
						output.append("\tHTTP Status Result:\t\t\t" + "NG\n");
						output.append("\t\tExpected HTTP Status:\t" + expectedStatusCd + "\n");
						output.append("\t\tActual HTTP Status:\t\t" + "None\n");
						output.append("\tJSON Result:\t\t\t\t" + "NG\n");
						output.append("\t\tExpected JSON Output:" + "\n");
						output.append("\t\t" + expectedJSON.toString(5).replace("\n", "\n\t\t") + "\n");
						output.append("\t\tActual JSON Output:" + "\n");
						output.append("\t\t" + "{}\n");
						output.append("\tDB Result:\t" + "NG\n");
						output.append("\n");

						System.out.println(output);

						// Saving the output into the results file.
						fservice.writeToFile(
								outputDir
								+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
								+ " IT Execution Result.txt", output.toString());
						if (!rangeFlag) {
							if (null != endRange) {
								if (endRange.equals(name)) {
									break;
								}
							}
						}
						continue;
					}

					// Function call for loading the needed data into the database.
					dataloader.loadScenario(testCaseName);

					final ApiService apiService = new ApiService();
					ResponseModel outputResponse = apiService.executeRequest(
							Setup.getInstance().getAPINumber(testSource.getName()),
							requestHeaderJSON,
							inputJSON,
							urlParameterJSON,
							testSource);

					AssertionOutputModel responseValidation = new AssertionOutputModel();
					AssertionOutputModel dbValidation = new AssertionOutputModel();
					boolean assertFlag = true;
					boolean assertCodeFlag = true;

					if(null != expectedJSON
							&& 0 < outputResponse.getMessage().length()
							&& 0 != outputResponse.getStatusCode()){
						// Validating the actual json response with the expected json response.
						try {
							responseValidation =
									jsonExcelUtil.validateJsonResponse(
											new JSONObject(outputResponse.getMessage()), expectedJSON);
						} catch (Exception e) {
							outputResponse.setMessage(new JSONObject().toString());
							responseValidation =
									jsonExcelUtil.validateJsonResponse(
											new JSONObject(outputResponse.getMessage()), expectedJSON);
						}

						// Validating the response code with the expected value.
						if(expectedStatusCd != outputResponse.getStatusCode()){
							assertFlag = false;
							assertCodeFlag = false;
							failedStatusTestList.add(name);
						}

						// Checking if the response validation returned fail.
						if(responseValidation.isError()){
							assertFlag = false;
							failedResponseTestList.add(name);
						}
					} else {
						assertFlag = false;
						outputResponse.setMessage("{}");
						failedResponseTestList.add(name);

						// Validating the response code with the expected value.
						if(expectedStatusCd != outputResponse.getStatusCode()){
							assertCodeFlag = false;
							failedStatusTestList.add(name);
						}
					}

					try {
						// Validating the database after the api request has been executed.
						dbValidation = dataloader.checkScenario("", testCaseName);
					} catch (CommunicationsException dbException) {
						// DataLoaderUtil instance is re-initialize after MySQL service connection stoppage.
						dataloader = new DataLoaderUtil(
								schema,
								url,
								userName,
								password);

						// Loads the test source file.
						dataloader.loadFile(testSource.getAbsolutePath());

						// Loads the sheet containing the data to inserted into the database.
						dataloader.loadSheet(Constants.DB_DATA_SHEET);

						// Validation the format of the output from the excel file.
						dataloader.validateScenario(testCaseName);

						// Validating the database after the api request has been executed.
						dbValidation = dataloader.checkScenario("", testCaseName);
					}

					// Checking if the database validation returned fail.
					if(dbValidation.isError()){
						assertFlag = false;
						failedDbTestList.add(name);
					}

					// Executing the taskqueue process.
					sleep(10000);
					apiService.queueProcess(null);

					String operationLogComparison = null;
					String operationContent = "";
					if(null != operationLogJSON
							&& 404 != expectedStatusCd
							&& 405 != expectedStatusCd){
						// Temporary fix for operationContent.
						try {
							operationContent = operationLogJSON.get("*" + Constants.OPERATION_CONTENT).toString();
							if (0 < operationContent.length()) {
								operationLogJSON.put(
										Constants.OPERATION_CONTENT,
										new JSONObject(operationContent));
							} else {
								operationLogJSON.put(
										Constants.OPERATION_CONTENT,
										"");
							}
							operationLogJSON.remove("*operationContent");
						} catch (JSONException e) {}
						operationLogComparison = isSame("MypageOperationLog", null, operationLogJSON, operationContent);
						if (null == operationLogComparison) {
							int count = 1;
							int maxRetry = 4;
							do {
								sleep(2000);
								apiService.queueProcess(null);
								operationLogComparison = isSame(
										"MypageOperationLog", null,
										operationLogJSON, operationContent);
								if (maxRetry > count) {
									count++;
								} else {
									break;
								}
							} while (null == operationLogComparison);
						}
						if(null != operationLogComparison
								&& !operationLogComparison.contains("Result:\tOK")){
							assertFlag = false;
							failedOpLogTestList.add(name);
						} else if (null == operationLogComparison){
							operationLogComparison = "\t\tNo Operation Log found.\n";
							assertFlag = false;
							failedOpLogTestList.add(name);
						}
					}

					endTimestamp = generateTimestamp();

					// Generating the output format.
					StringBuffer output = new StringBuffer();
					output.append("Test Case No.:\t" + testCaseName + "\n");
					output.append("Test Result:\t");
					if (assertFlag) {
						output.append("OK\n");
						successCount++;
					} else {
						output.append("NG\n");
						failedTestList.add(name);
						failCount++;
					}
					output.append("Start Time:\t\t" + startTimestamp + "\n");
					output.append("End Time:\t\t" + endTimestamp + "\n");
					output.append("Input:" + "\n");
					output.append("\tURL Parameter: ");
					if (null != urlParameterJSON) {
						output.append(urlParameterJSON.getString(Constants.URL_PARAMETER) + "\n");
					} else {
						output.append("No data" + "\n");
					}
					output.append("\tRequest Header: " + "\n");
					if (null != requestHeaderJSON) {
						output.append("\t" + requestHeaderJSON.toString(5).replace("\n", "\n\t") + "\n");
					} else {
						output.append("\t" + "No data." + "\n");
					}
					output.append("\tJSON Data: " + "\n");
					if (null != inputJSON) {
						output.append("\t" + inputJSON.toString(5).replace("\n", "\n\t") + "\n");
					} else {
						output.append("\t" + "{}" + "\n");
					}
					output.append("\n");
					output.append("Output: " + "\n");
					output.append("\tRequest URL:\t\t\t\t" + outputResponse.getRequestURL() + "\n");
					output.append("\tRequest Type:\t\t\t\t" + outputResponse.getRequestType() + "\n");
					output.append("\tHTTP Status Result:\t\t\t");
					if (assertCodeFlag) {
						output.append("OK\n");
					} else {
						output.append("NG\n");
					}
					output.append("\t\tExpected HTTP Status:\t" + expectedStatusCd + "\n");
					output.append("\t\tActual HTTP Status:\t\t" + outputResponse.getStatusCode() + "\n");
					output.append("\tJSON Result:\t\t\t\t");
					if (!responseValidation.isError()) {
						output.append("OK\n");
					} else {
						output.append("NG\n");
					}
					output.append("\t\tExpected JSON Output:" + "\n");
					output.append("\t\t" + expectedJSON.toString(5).replace("\n", "\n\t\t") + "\n");
					output.append("\t\tActual JSON Output:" + "\n");
					output.append("\t\t" + new JSONObject(outputResponse.getMessage()).toString(5).replace("\n", "\n\t\t") + "\n");
					if (responseValidation.isError()) {
						output.append("\t\tAssertion Report:" + "\n");
						output.append("\t\t\t" + responseValidation.getError().replace("\n", "\n\t\t\t") + "\n");

					}
					if(null != operationLogComparison){
						output.append(operationLogComparison);
					}
					output.append("\tDB Result:\t");
					if (!dbValidation.isError()) {
						output.append("OK\n");
					} else {
						output.append("NG\n");
					}
					if (null != dbValidation.getError()) {
						output.append("\t\t" + dbValidation.getError().replace("\n", "\n\t\t"));
					}

					output.append("\n");

					System.out.println(output);

					// Saving the output into the results file.
					fservice.writeToFile(
							outputDir
							+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
							+ " IT Execution Result.txt", output.toString());

					// Empty the tables used during the testing.
					dataloader.loadCurrentDatabase("", testCaseName,
							url, userName, password);

					if (!rangeFlag) {
						if (null != endRange) {
							if (endRange.equals(name)) {
								break;
							}
						}
					}
				}
				System.out.println();
			} catch(HttpHostConnectException e) {
				System.err.println("[" + testCaseName + "]: Server not responding.");
				// Empty the tables used during the testing.
				dataloader.loadCurrentDatabase("", testCaseName,
						url, userName, password);
			} catch (Exception e) {
				exceptionTestList.add(testCaseName);

//				e.printStackTrace();
				endTimestamp = generateTimestamp();

				// Generating the output format.
				StringBuffer output = new StringBuffer();
				output.append("Test Case No.:\t" + testCaseName + "\n");
				output.append("Test Result:\t" + "NG (Due to exception)\n");
				output.append("Start Time:\t\t" + startTimestamp + "\n");
				output.append("End Time:\t\t" + endTimestamp + "\n");
				output.append("Cause:" + "\n\t");
				output.append(ExceptionUtils.getStackTrace(e).replace("\n", "\n\t"));
				output.append("\n");

				System.out.println(output);

				// Saving the output into the results file.
				fservice.writeToFile(
						outputDir
						+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
						+ " IT Execution Result.txt", output.toString());

				// Empty the tables used during the testing.
				dataloader.loadCurrentDatabase("", testCaseName,
						url, userName, password);

				if (!rangeFlag) {
					if (null != endRange) {
						if (endRange.equals(name)) {
							break;
						}
					}
				}
			}
		}
	}

	/**
	 * Execution for cron tests.
	 * @maker Nick Faelnar
	 * @version 0.01
	 * @param testSource test file of API to be tested.
	 * @throws Exception generic exception.
	 * @since 0.01
	 */
	public static void executeCronTest(File testSource) throws Exception {
		/** DataLoaderUtil instance. **/
		DataLoaderUtil dataloader = null;
		/** JSONExcelUtil instance **/
		JSONExcelUtil jsonExcelUtil = new JSONExcelUtil();
		/** JSONObject instance that will hold the json data for input **/
		JSONObject inputJSON = new JSONObject();
		/** JSONObject instance that will hold the json data for the expected response including the status code **/
		JSONObject expectedJSON = new JSONObject();
		/** JSONObject instance that will hold the json data for the request header values **/
		JSONObject requestHeaderJSON = new JSONObject();
		/** JSONObject instance that will hold the json data for URL parameter values **/
		JSONObject urlParameterJSON = new JSONObject();
		/** JSONObject instance that will hold the json data for the execution log **/
		JSONObject executionLogJSON = new JSONObject();
		/** JSONObject instance that will hold the json data for the operation log **/
		JSONObject operationLogJSON = new JSONObject();
		/** JSONObject instance that will hold the json data for the datastore operation log. **/
		JSONObject dsOperationLogJSON = new JSONObject();
		/** JSONObject instance that will hold the json data for the task queue. **/
		JSONObject taskQueueJSON = new JSONObject();

		/** The expected response status code **/
		int expectedStatusCd = 500;
		String testCaseName = "";
		String startTimestamp = "";
		String endTimestamp = "";
		String outputDir = testSource.getAbsolutePath().replace(testSource.getName(), "") + Constants.DUMP_DIR;
		boolean isCronJob = false;

		if (testSource.getName().contains("API_02_04_01_01_01")) {
			isCronJob = true;
		}

		if(outputPath == null){
			outputDir = testSource.getAbsolutePath().replace(testSource.getName(), "") + Constants.DUMP_DIR;
		}else{
			outputDir = outputPath;
		}

		// Initializes the DataLoaderUtil instance.
		dataloader = new DataLoaderUtil(
				schema,
				url,
				userName,
				password);

		// Loads the test source file.
		dataloader.loadFile(testSource.getAbsolutePath());

		// Loads the sheet containing the data to inserted into the database.
		dataloader.loadSheet(Constants.DB_DATA_SHEET);

		// Loads the test source file for getting the JSON inputs.
		jsonExcelUtil.loadTestSource(testSource);

		List<String> testCases = new ArrayList<String>();
		if (null != testCaseList){
			// if a test case is provided, execute it.
//			testCases.add(testCaseList);
			testCases = testCaseList;
		} else {
			// else, execute all test cases.
			testCases = dataloader.getScenarioNames();
		}

		// loop each test case.
		for(String name : testCases) {
			FileService fservice = new FileService();
			// Skipping of non-existent test case number in the current file.
			if (!dataloader.getScenarioNames().contains(name)) {
				continue;
			}

			// Checking if the current test is included in the ignore list.
			if (ignoreTestCaseList.contains(name)) {
				if (rangeFlag) {
					if (startRange.equals(name)) {
						rangeFlag = false;
					}
				} else {
					if (null != endRange
						&& endRange.equals(name)) {
						break;
					}
				}
				ignoredTestList.add(name);
				continue;
			}

			// Checking if the current test is included in the test case range.
			if (rangeFlag) {
				if (startRange.equals(name)) {
					rangeFlag = false;
				} else {
					continue;
				}
			}

			// Generating the initial timestamp.
			startTimestamp = generateTimestamp();
			try {
				testCount++;
				testCaseName = name;
				inputJSON = null;
				expectedJSON = null;
				requestHeaderJSON = null;
				urlParameterJSON = null;
				executionLogJSON = null;
				operationLogJSON = null;
				taskQueueJSON = null;
				dsOperationLogJSON = null;
				expectedStatusCd = 500;

				// Removing all operation logs and execution logs;
				emptyLogs("MypageOperationLog");
				emptyLogs("MypageBackendTaskLog");

				// Validation the format of the output from the excel file.
				String message = dataloader.validateScenario(testCaseName);
				if (0 > message.length()) {
					System.err.println("[" + testCaseName + "]\n"
							+ "Dataloader error: "
							+ message + "\n");
				} else {
					// Retrieve the data for this test case from the file.
					inputJSON = jsonExcelUtil.getDataJSONList().get(testCaseName);
					expectedJSON = jsonExcelUtil.getExpectedJSONList().get(testCaseName);
					requestHeaderJSON = jsonExcelUtil.getRequestHeaderJSONList().get(testCaseName);
					executionLogJSON = jsonExcelUtil.getExecutionLogJSONList().get(testCaseName);
					operationLogJSON = jsonExcelUtil.getOperationLogJSONList().get(testCaseName);

					dsOperationLogJSON = jsonExcelUtil.getDsOpLogJSONList().get(testCaseName);
					taskQueueJSON = jsonExcelUtil.getTaskQueueJSONList().get(testCaseName);

					if (null != dsOperationLogJSON) {
						DatastoreService ds = new DatastoreService("localhost", 8888, "XXXX", "XXXX");
						ds.insertOperationLogs(dsOperationLogJSON);
					}

					if (null != taskQueueJSON) {
						TaskQueueService tq = new TaskQueueService("localhost", 8888, "XXXX", "XXXX");
						tq.pushOperationLogs(taskQueueJSON);
					}

					if (null != expectedJSON) {
						// Get expected response code from the expectedJSON object then
						// remove it from the json object.
						expectedStatusCd = expectedJSON.getInt("status");
						expectedJSON.remove("status");
					} else {
						endTimestamp = generateTimestamp();
						failedTestList.add(name);
						failedDbTestList.add(name);
						failedResponseTestList.add(name);
						failedStatusTestList.add(name);
						failedOpLogTestList.add(name);
						failCount++;

						// Generating the output format.
						StringBuffer output = new StringBuffer();
						output.append("Test Case No.:\t" + testCaseName + "\n");
						output.append("Test Result:\t" + "Test case not found\n");
						output.append("Start Time:\t\t" + startTimestamp + "\n");
						output.append("End Time:\t\t" + endTimestamp + "\n");
						output.append("Input:" + "\n");
						output.append("\tURL Parameter: " + "None\n");
						output.append("\tRequest Header: " + "\n");
						output.append("\t" + "{}" + "\n");
						output.append("\tJSON Data: " + "\n");
						output.append("\t" + "{}" + "\n");
						output.append("\n");
						output.append("Output: " + "\n");
						output.append("\tRequest URL:\t\t\t\t" + "None\n");
						output.append("\tRequest Type:\t\t\t\t" + "None\n");
						output.append("\tHTTP Status Result:\t\t\t" + "NG\n");
						output.append("\t\tExpected HTTP Status:\t" + "None\n");
						output.append("\t\tActual HTTP Status:\t\t" + "None\n");
						output.append("\tJSON Result:\t\t\t\t" + "NG\n");
						output.append("\t\tExpected JSON Output:" + "\n");
						output.append("\t\t" + "{}" + "\n");
						output.append("\t\tActual JSON Output:" + "\n");
						output.append("\t\t" + "{}\n");
						output.append("\tDB Result:\t" + "NG\n");
						output.append("\n");

						System.out.println(output);

						// Saving the output into the results file.
						fservice.writeToFile(
								outputDir
								+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
								+ " IT Execution Result.txt", output.toString());
						if (!rangeFlag) {
							if (null != endRange) {
								if (endRange.equals(name)) {
									break;
								}
							}
						}
						continue;
					}

					// For NA test cases.
					if (invalidTestCaseList.contains(testCaseName)) {
						endTimestamp = generateTimestamp();
						naTestList.add(name);
						invalidCount++;

						// Generating the output format.
						StringBuffer output = new StringBuffer();
						output.append("Test Case No.:\t" + testCaseName + "\n");
						output.append("Test Result:\t" + "NA\n");
						output.append("Start Time:\t\t" + startTimestamp + "\n");
						output.append("End Time:\t\t" + endTimestamp + "\n");
						output.append("Input:" + "\n");
						output.append("\tJSON Data: " + "\n");
						if (null != inputJSON) {
							output.append("\t" + inputJSON.toString(5).replace("\n", "\n\t") + "\n");
						} else {
							output.append("\t" + "{}" + "\n");
						}
						output.append("\n");
						output.append("Output: " + "\n");
						output.append("\tJSON Result:\t" + "None\n");
						output.append("\tDB Result:\t" + "None\n");
						output.append("\n");

						System.out.println(output);

						// Saving the output into the results file.
						fservice.writeToFile(
								outputDir
								+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
								+ " IT Execution Result.txt", output.toString());
						if (!rangeFlag) {
							if (null != endRange) {
								if (endRange.equals(name)) {
									break;
								}
							}
						}
						continue;
					}

					// Function call for loading the needed data into the database.
					dataloader.loadScenario(testCaseName);

					ResponseModel outputResponse = null;
					if (isCronJob) {
						final ApiService apiService = new ApiService();
						outputResponse = apiService.executeRequest(
								Setup.getInstance().getAPINumber(testSource.getName()),
								requestHeaderJSON,
								inputJSON,
								urlParameterJSON,
								testSource);
					}

					TaskQueueService taskQueue = new TaskQueueService("localhost", 8888, "XXXX", "XXXX");
					if (!isCronJob) {
						// Insert task to task queue.
						taskQueue.pushTask(testSource.getName(), inputJSON);
					}

					AssertionOutputModel responseValidation = new AssertionOutputModel();
					AssertionOutputModel dbValidation = new AssertionOutputModel();
					boolean assertFlag = true;
					boolean assertCodeFlag = true;

					if (null != executionLogJSON
							&& executionLogJSON.has("entryList")
							&& 1 < executionLogJSON.getJSONArray("entryList").length()) {
							sleep(2 * 60000);
					} else {
						sleep(2 * 60000);
					}

					taskQueue.purgeTaskQueue("mypage-operation-log-entry");

					if (isCronJob) {
						if(null != expectedJSON
								&& 0 != outputResponse.getStatusCode()){
							// Validating the actual json response with the expected json response.
							try {
								responseValidation =
										jsonExcelUtil.validateJsonResponse(
												new JSONObject(outputResponse.getMessage()), expectedJSON);
							} catch (Exception e) {
								outputResponse.setMessage(new JSONObject().toString());
								responseValidation =
										jsonExcelUtil.validateJsonResponse(
												new JSONObject(outputResponse.getMessage()), expectedJSON);
							}

							// Validating the response code with the expected value.
							if(expectedStatusCd != outputResponse.getStatusCode()){
								assertFlag = false;
								assertCodeFlag = false;
								failedStatusTestList.add(name);
							}

							// Checking if the response validation returned fail.
							if(responseValidation.isError()){
								assertFlag = false;
								failedResponseTestList.add(name);
							}
						} else {
							assertFlag = false;
							outputResponse.setMessage("{}");
							failedResponseTestList.add(name);

							// Validating the response code with the expected value.
							if(expectedStatusCd != outputResponse.getStatusCode()){
								assertCodeFlag = false;
								failedStatusTestList.add(name);
							}
						}
					}

					try {
						// Validating the database after the api request has been executed.
						dbValidation = dataloader.checkScenario("", testCaseName);
					} catch (CommunicationsException dbException) {
						// DataLoaderUtil instance is re-initialize after MySQL service connection stoppage.
						dataloader = new DataLoaderUtil(
								schema,
								url,
								userName,
								password);

						// Loads the test source file.
						dataloader.loadFile(testSource.getAbsolutePath());

						// Loads the sheet containing the data to inserted into the database.
						dataloader.loadSheet(Constants.DB_DATA_SHEET);

						// Validation the format of the output from the excel file.
						dataloader.validateScenario(testCaseName);

						// Validating the database after the api request has been executed.
						dbValidation = dataloader.checkScenario("", testCaseName);
					}

					// Checking if the database validation returned fail.
					if(dbValidation.isError()){
						assertFlag = false;
						failedDbTestList.add(name);
					}

					//check datastoreEntries
					String executionLogComparison = null;
					if (assertFlag && null != executionLogJSON) {
						executionLogComparison = isSame("MypageBackendTaskLog", null, executionLogJSON, "");
						if (null == executionLogComparison) {
							int count = 1;
							int maxRetry = 4;
							do {
								sleep(2 * 1000);
								executionLogComparison = isSame(
										"MypageBackendTaskLog", null,
										executionLogJSON, "");
								if (maxRetry > count) {
									count++;
								} else {
									break;
								}
							} while (null == executionLogComparison);
						}
						if (null == executionLogComparison || !executionLogComparison.contains("Result:\tOK")) {
							failedExecLogTestList.add(name);
							if (null == executionLogComparison) {
								executionLogComparison = "\tMypageBackendTaskLog Result:\tNG (No execution log found)\n";
							}
							assertFlag = false;
						}
					}

					endTimestamp = generateTimestamp();

					// Generating the output format.
					StringBuffer output = new StringBuffer();
					output.append("Test Case No.:\t" + testCaseName + "\n");
					output.append("Test Result:\t");
					if (assertFlag) {
						output.append("OK\n");
						successCount++;
					} else {
						output.append("NG\n");
						failedTestList.add(name);
						failCount++;
					}
					output.append("Start Time:\t\t" + startTimestamp + "\n");
					output.append("End Time:\t\t" + endTimestamp + "\n");
					output.append("Input:" + "\n");
					output.append("\tJSON Data: " + "\n");
					if (null != inputJSON) {
						output.append("\t" + inputJSON.toString(5).replace("\n", "\n\t") + "\n");
					} else {
						output.append("\t" + "{}" + "\n");
					}

					output.append("\n");
					output.append("Output: " + "\n");

					if (isCronJob) {
						output.append("\tRequest URL:\t\t\t\t" + outputResponse.getRequestURL() + "\n");
						output.append("\tRequest Type:\t\t\t\t" + outputResponse.getRequestType() + "\n");
						output.append("\tHTTP Status Result:\t\t\t");
						if (assertCodeFlag) {
							output.append("OK\n");
						} else {
							output.append("NG\n");
						}
						output.append("\t\tExpected HTTP Status:\t" + expectedStatusCd + "\n");
						output.append("\t\tActual HTTP Status:\t\t" + outputResponse.getStatusCode() + "\n");
						output.append("\tJSON Result:\t\t\t\t");
						if (!responseValidation.isError()) {
							output.append("OK\n");
						} else {
							output.append("NG\n");
						}
						output.append("\t\tExpected JSON Output:" + "\n");
						output.append("\t\t" + expectedJSON.toString(5).replace("\n", "\n\t\t") + "\n");
						output.append("\t\tActual JSON Output:" + "\n");
						output.append("\t\t" + new JSONObject(outputResponse.getMessage()).toString(5).replace("\n", "\n\t\t") + "\n");
						if (responseValidation.isError()) {
							output.append("\t\tAssertion Report:" + "\n");
							output.append("\t\t\t" + responseValidation.getError().replace("\n", "\n\t\t\t") + "\n");
						}
					}

					if(null != executionLogComparison){
						output.append(executionLogComparison);
					}

					output.append("\tDB Result:\t");
					if (!dbValidation.isError()) {
						output.append("OK\n");
					} else {
						output.append("NG\n");
					}
					if (null != dbValidation.getError()) {
						output.append("\t\t" + dbValidation.getError().replace("\n", "\n\t\t"));
					}

					output.append("\n");

					System.out.println(output);

					// Saving the output into the results file.
					fservice.writeToFile(
							outputDir
							+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
							+ " IT Execution Result.txt", output.toString());

					// Empty the tables used during the testing.
					dataloader.loadCurrentDatabase("", testCaseName,
							url, userName, password);

					if (!rangeFlag) {
						if (null != endRange) {
							if (endRange.equals(name)) {
								break;
							}
						}
					}
				}
				System.out.println();
			} catch(HttpHostConnectException e) {
				System.err.println("[" + testCaseName + "]: Server not responding.");
				// Empty the tables used during the testing.
				dataloader.loadCurrentDatabase("", testCaseName,
						url, userName, password);
			} catch (Exception e) {
				exceptionTestList.add(testCaseName);

//				e.printStackTrace();
				endTimestamp = generateTimestamp();

				// Generating the output format.
				StringBuffer output = new StringBuffer();
				output.append("Test Case No.:\t" + testCaseName + "\n");
				output.append("Test Result:\t" + "NG (Due to exception)\n");
				output.append("Start Time:\t\t" + startTimestamp + "\n");
				output.append("End Time:\t\t" + endTimestamp + "\n");
				output.append("Cause:" + "\n\t");
				output.append(ExceptionUtils.getStackTrace(e).replace("\n", "\n\t"));
				output.append("\n");

				System.out.println(output);

				// Saving the output into the results file.
				fservice.writeToFile(
						outputDir
						+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
						+ " IT Execution Result.txt", output.toString());

				// Empty the tables used during the testing.
				dataloader.loadCurrentDatabase("", testCaseName,
						url, userName, password);

				if (!rangeFlag) {
					if (null != endRange) {
						if (endRange.equals(name)) {
							break;
						}
					}
				}
			}
		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param string
	 * @since 0.01
	 */
	public static void emptyLogs(String kind) {
		DatastoreService ds = new DatastoreService("localhost", 8888, "XXXX", "XXXX");
		ds.removeEntry(kind);
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * Generate timestamp.
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @return
	 * @since 0.01
	 */
	@SuppressWarnings("deprecation")
	public static String generateTimestamp() {
		Timestamp stamp = new Timestamp(System.currentTimeMillis());
    	Date date = new Date(stamp.getTime());
    	DateFormat df = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");

    	return df.format(date);
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * Generate timestamp.
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @return
	 * @since 0.01
	 */
	public static long getTime() {
		Calendar calendar = Calendar.getInstance();
		Date now = calendar.getTime();
		Timestamp timestamp = new Timestamp(now.getTime());

		return timestamp.getTime();
	}

	/**
	 * <div class="jp">
	 * Validated the expected and actual json
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param kind
	 * @param name
	 * @param expected
	 * @param operationContent
	 * @return
	 * @since 0.01
	 */
	public static String isSame(final String kind, final String name, JSONObject expected, String operationContent){
		JSONExcelUtil jsonExcelUtil = new JSONExcelUtil();
		DatastoreService ds = new DatastoreService("localhost", 8888, "XXXX", "XXXX");
		List<Entity> entities = ds.getAllEntities(kind, name);
		if (0 == entities.size()) {
			return null;
		}
		AssertionOutputModel aom = null;
		boolean isValid = false;
		JSONObject actualExecutionLogJSON = null;
		StringBuffer mismatch = new StringBuffer();
		boolean listFlag = false;
		JSONArray entryList = null;
		try {
			entryList = expected.getJSONArray("entryList");
			listFlag = true;
		} catch (Exception e) {
			listFlag = false;
		}
		if (!listFlag) {
			for (Entity e : entities) {
				HashMap<String, Object> props =  new HashMap<String, Object>(e.getProperties());
				Map<String, Object> cleanMap =  new HashMap<String, Object>();
				Iterator it = props.entrySet().iterator();
				while(it.hasNext()){
					Map.Entry<String, Object> pair = (Map.Entry)it.next();
					if(String.valueOf(pair.getValue()).equalsIgnoreCase("null")){
						//props.remove(pair.getKey());
						cleanMap.put(pair.getKey(), "");
					}else{
						cleanMap.put(pair.getKey(), pair.getValue());
					}
					it.remove();
				}
				actualExecutionLogJSON = new JSONObject(cleanMap);

				//Support for TEXT Object of operationContent.
				if (actualExecutionLogJSON.has("operationContent")
					/*&& actualExecutionLogJSON.getJSONObject("operationContent").has("value")*/) {

					JSONObject j = actualExecutionLogJSON.optJSONObject("operationContent");
					if(null != j && j.has("value")){
							String value = actualExecutionLogJSON.getJSONObject("operationContent").getString("value");
							actualExecutionLogJSON.remove("operationContent");
							actualExecutionLogJSON.put("operationContent", value);
					}


				}

				if (0 < operationContent.length()) {
					String actualOperationContent = AesService.decrypt(actualExecutionLogJSON
							.get(Constants.OPERATION_CONTENT).toString());
					actualExecutionLogJSON.put(Constants.OPERATION_CONTENT,
							getOperationContentJSON(actualOperationContent));
				}
				aom = jsonExcelUtil.validateJsonResponse(
						actualExecutionLogJSON, expected);
				mismatch.append("\t\t"
						+ actualExecutionLogJSON.toString(5).replace("\n",
								"\n\t\t") + "\n");
				if (!aom.isError()) {
					isValid = true;
					break;
				} else {
					mismatch.append("\t\t" + "Assertion Report:" + "\n");
					mismatch.append("\t\t\t" + aom.getError().replace("\n",
							"\n\t\t\t") + "\n");
				}
			}
			if (0 < operationContent.length()) {
				expected.put(Constants.OPERATION_CONTENT, new JSONObject(operationContent));
			}
		} else {

			for (Entity e : entities) {
				actualExecutionLogJSON = new JSONObject(e.getProperties());

				try {
					String content = actualExecutionLogJSON.get(Constants.CONTENT).toString();
					actualExecutionLogJSON.put(Constants.CONTENT,
							new JSONObject(content));
				} catch(JSONException err) {}

				mismatch.append("\t\t"
					+ actualExecutionLogJSON.toString(5).replace(
							"\n", "\n\t\t") + "\n");
			}

			for (int i = 0; i < entryList.length(); i++) {
				try {
					String content = entryList.getJSONObject(i).getString(Constants.CONTENT);
					entryList.getJSONObject(i).put(Constants.CONTENT,
							new JSONObject(content));
				} catch(JSONException err) {
					err.printStackTrace();
				}

				for (Entity e : entities) {

					actualExecutionLogJSON = new JSONObject(e.getProperties());
					try {
						actualExecutionLogJSON.put(Constants.CONTENT,
								new JSONObject(actualExecutionLogJSON.get(Constants.CONTENT).toString()));
					} catch(JSONException err) {}

					aom = jsonExcelUtil.validateJsonResponse(
							actualExecutionLogJSON, entryList.getJSONObject(i));
					if (actualExecutionLogJSON.getString("taskName")
							.contains(entryList.getJSONObject(i).getString("taskName"))) {
						System.err.println("=================================================================");
						Iterator keyList = entryList.getJSONObject(i).keys();
							while (keyList.hasNext()) {
								String key = (String) keyList.next();
								System.out.println(key);
								if (entryList.getJSONObject(i).has(key)) {
									System.out.println(entryList.getJSONObject(i).get(key));
								} else {
									System.out.println("None");
								}

								if (actualExecutionLogJSON.has(key)) {
									System.out.println(actualExecutionLogJSON.get(key));
								} else {
									System.out.println("None");
								}

							}
						System.err.println("=================================================================");
					}
					if (!aom.isError()) {
						isValid = true;
						break;
					} else {
						System.err.println(aom.getError());
						isValid = false;
					}
				}

				if (!isValid) {
					break;
				}

			}
		}
		StringBuffer sb = new StringBuffer();
		if(isValid){
			sb.append("\t"+kind+" Result:\tOK\n");
		}else{
			sb.append("\t"+kind+" Result:\tNG\n");
		}
		sb.append("\t\tExpected "+kind+":\n");
		sb.append("\t\t"+expected.toString(5).replace("\n", "\n\t\t") + "\n");
		sb.append("\t\tActual "+kind+"(s):\n");
		sb.append(mismatch.toString());

		//clean up
		if (0 < entities.size()) {
			ds.removeEntry(kind);
		}

		return sb.toString();
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @source http://www.dreamincode.net/forums/topic/172977-how-to-make-a-program-wait-for-a-timer-to-stop-before-continuing/
	 * @version 0.01
	 * @param actualOperationContent
	 * @return
	 * @since 0.01
	 */
	private static JSONObject getOperationContentJSON(
			String actualOperationContent) {
		JSONObject output = new JSONObject();
		String[] data = actualOperationContent.split(",");
		for (int i = 0; i < data.length; i++) {
			try {
				output.put(data[i].split("=")[0], data[i].split("=")[1].replace("\\\\", "\\"));
			} catch (ArrayIndexOutOfBoundsException e) {
				output.put(data[i].split("=")[0], "");
			}
		}

		return output;
	}

	public static void sleep(long milliseconds) {
		Date d;
		long start, now;
		d = new Date();
		start = d.getTime();
		do {
			d = new Date();
			now = d.getTime();
		} while ((now - start) < milliseconds);
	}

}

