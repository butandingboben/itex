/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.main;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

import com.rgtc.itex.util.UtilConstants;

/**
 * <div class="jp">
 *
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 *
 * </div>
 *
 * @author Joven 'Bob' Montero
 * @version 0.01
 * @since 0.01
 */
public class TrialClass {
	
	public DataLoaderUtil getLoaderInstance(String db, String url, String un, String pw) throws SQLException, IOException, Exception{
		return new DataLoaderUtil(db, url, un, pw);
	}
	
	private final static String url = "jdbc:mysql://192.168.1.176:3306";
//	private final static String url = "jdbc:mysql://127.0.0.1:3307";
	private final static String un = "keiba";
	private final static String pw = "keiba";
//	private final static String un = "root";
//	private final static String pw = "root";
	private final static String schema = "keibadb-test1";
//	private final static String schema = "keibadb-nick";
	
//	 priority 1 gendai2StgRaceTbl
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 1\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_ゲンダイ２段出走表.xls";
	
	// priority 1 gendaiTrifectaFocus
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 1\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_ゲンダイ３連単フォーカス.xls";
	
	// priority 1 gendaiStaffYosou
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\Priority 1 - QA\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_QA_ゲンダイスタッフ予想.xls";
	
	// priority 1 gendaiRacePerf
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\Priority 1 - QA\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_QA_ゲンダイレース別成績.xls";
	
	// priority 1 gendai1StgRaceTblHor - Yoko
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 1\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_ゲンダイ一段出走表横.xls";
	
	// priority 1 gendai1StgRaceTblVer
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 1\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_ゲンダイ一段出走表縦.xls";
	
	// priority 1 forecast
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 1\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_ゲンダイ予想者フォーカス.xls";
	
	// priority 1 gendaiHorseReamOdds 27
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 1\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_ゲンダイ馬連オッズ_2.xls";
	
	
//    //PRIORITY 3 of TENKAI
//	private final String testCaseName = "04-01-21-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\Priority 3\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_展開予想.xls";
//		
//		
//  //PRIORITY 3 of regHorse
//	private final String testCaseName = "04-01-18-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\Priority 3\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_登録馬想定表.xls";
		
//	private final String testCaseName = "04-01-03-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 3\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_中央1段出走表.xls";
		
	// local 1 stg
//	private final String testCaseName = "04-01-09-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\Priority 2 - QA\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_QA_地方競馬１段成績_移送項目定義.xls";
	
	//local 2 stg
//	private final String testCaseName = "04-01-08-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 2\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_地方競馬２段成績.xls";
	
	//local conjsales
//	private final String testCaseName = "04-01-13-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\Priority 2 - QA\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_QA_地方競馬成績併売_移送項目定義.xls";
	
	//local curb
//	private final String testCaseName = "04-01-12-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 2\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_地方競馬成績取出場外.xls";
	
	//local big
//	private final String testCaseName = "04-01-10-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\Priority 2 - QA\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_QA_地方競馬成績取出大_移送項目定義.xls";
	
	//local small
//	private final String testCaseName = "04-01-11-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\Priority 2 - QA\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_QA_地方競馬成績取出小_移送項目定義.xls";
	
	//central 2
//	private final String testCaseName = "04-01-07-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 2\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_中央競馬２段成績.xls";
	
	//central big
//	private final String testCaseName = "04-01-05-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 2\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_中央競馬大型成績.xls";
	
	//central small
//	private final String testCaseName = "04-01-06-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\Priority 2 - QA\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_QA_中央競馬成績小型_移送項目定義.xls";

	//priority 2 biblio
//	private final String testCaseName = "01-01-01-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 2\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_書誌情報.xls";
	
	//odds
//	private final String testCaseName = "04-01-25-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 2\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_オッズ.xls";
	
//	index
//	private final String testCaseName = "01-01-01-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\Priority 2 - QA\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_書誌情報_移送項目定義.xls";
	
//	private final String testCaseName = "04-01-21-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\Priority 3\\01_Test Specs\\";
//	private final static String itSpecsFileName = "IT Specification_書誌情報 Part 2.xls";
	
	//PRIORITY 4 ADVANCED SPECIAL RACE
//	private final String testCaseName = "04-01-16-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 4\\01_IT Specs\\";
//	private final static String itSpecsFileName = "IT Specification_先取り特別レース.xls";
	
	//priority 4 win5result
//	private final String testCaseName = "04-01-24-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 4\\01_IT Specs\\";
//	private final static String itSpecsFileName = "IT Specification_WIN5結果.xls";
	
	//priority 4 MEDIA COMPI
//	private final String testCaseName = "04-01-60-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 4\\01_IT Specs\\";
//	private final static String itSpecsFileName = "IT Specification_メディアコンピ.xls";
	
	//priority 4 training tbl
//	private final String testCaseName = "04-01-14-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 4\\01_IT Specs\\";
//	private final static String itSpecsFileName = "IT Specification_調教追い切り表.xls";

	//priority 4 ninki
//	private final String testCaseName = "04-01-23-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 4\\01_IT Specs\\";
//	private final static String itSpecsFileName = "IT Specification_人気度チェック.xls";

	//priority 4 conpi
//	private final String testCaseName = "04-01-04-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 4\\01_IT Specs\\";
//	private final static String itSpecsFileName = "IT Specification_コンピ指数.xls";

	//priority 4 best10
//	private final String testCaseName = "04-01-22-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 4\\01_IT Specs\\";
//	private final static String itSpecsFileName = "IT Specification_坂路ベスト10.xls";

	//priority 4 choukyou
//	private final String testCaseName = "04-01-14-001";
//	private final static String itSpecsFileLocation = "C:\\Nikkan Files\\Nikkan Full JP\\doc\\RGTC\\02_IT Specifications\\JP Review\\Priority 4\\01_IT Specs\\";
//	private final static String itSpecsFileName = "IT Specification_調教追い切り表.xls";

	private final static String itSpecsFileLocation = "C:\\tempfolder\\";
	private final static String itSpecsFileName = "IT Specification_ゲンダイ馬連オッズ.xls";
	
	private final static File dbTables = new File("C:/eldon-workspace/ITEx Refactored/resources/dbTables.txt");
	
	private final static File itSpecsFile = new File(itSpecsFileLocation+itSpecsFileName);

	public static final File MASTER_FILE = new File("C:\\Users\\PC011\\Desktop\\Nikkan\\Nikkan PH\\01_Requirements\\Database\\"
			+ "Code Master" + ".xls"); 
	
	public static void main(String[] args) throws Exception {
		System.out.println("start");

		System.out.println("thread running #: "+schema);
		try {
			DataLoaderUtil dataloader = new DataLoaderUtil(schema, url, un, pw);
			dataloader.disableForeignKeys();
			String testcase = "04-01-27-077";

			dataloader.loadFile(itSpecsFile.getAbsolutePath());
			System.out.println(itSpecsFile.getAbsolutePath() + " loadScenario done");
			dataloader.loadSheet("DB_Data");
			System.out.println(itSpecsFile.getAbsolutePath() + " loadSheet done");
			String s = dataloader.validateScenario(testcase);
			System.out.println(s);
			if (0 != s.length()) {
				PrintWriter writer = new PrintWriter("C:\\eldon-workspace\\ITEx Refactored\\resources\\output.txt", "UTF-8");
				writer.println(s);
				writer.close();
				//throw new Exception(s);
			}

			System.out.println(itSpecsFile.getAbsolutePath() + " validateScenario done");
			dataloader.loadCurrentDatabase(testcase, dbTables);
			dataloader.loadScenario(testcase, dbTables);
			System.out.println(itSpecsFile.getAbsolutePath() + " loadScenario done");

			dataloader.loadFile(MASTER_FILE.getAbsolutePath());
			System.out.println("MASTER loadFile done");
			dataloader.loadSheet(UtilConstants.MASTER_DB_DATA_SHEET);
			System.out.println("MASTER loadSheet done");
			dataloader.validateScenario(UtilConstants.MASTER_TEST_CASE);
			System.out.println("MASTER validateScenario done");
			dataloader.loadCurrentDatabase(UtilConstants.MASTER_TEST_CASE, dbTables);
			dataloader.loadScenario(UtilConstants.MASTER_TEST_CASE, dbTables);
			System.out.println("MASTER loadScenario done");

			dataloader.enableForeignKeys();
		} catch (Exception e) {
			System.out.println("error #: "+schema);
			e.printStackTrace();
		}
		
	}
	

}
