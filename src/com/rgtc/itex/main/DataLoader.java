/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) RococoGlobal Technologies Corporation - All Rights Reserved 2013
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.main;

import static com.rgtc.itex.util.UtilConstants.*;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.commons.lang3.StringUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.tools.remoteapi.RemoteApiInstaller;
import com.google.appengine.tools.remoteapi.RemoteApiOptions;
import com.rgtc.itex.exceptions.DataLoaderException;
import com.rgtc.itex.models.ValidatorModel;
import com.rgtc.itex.service.DataLoadProcess;
import com.rgtc.itex.service.DatabaseValidator;
import com.rgtc.itex.service.SQLWriter;
import com.rgtc.itex.dao.DatabaseWriter;
import com.rgtc.itex.util.UtilConstants;


/**
 * This class runs the program.
 * 
 * @author Eldon Ivan
 * @since 12/03/2013
 * @version 1
 */
public class DataLoader implements ActionListener, DocumentListener, MouseListener{
	
	private JFrame mainFrame;
	private JPanel mainPanel;
	
	private JPanel dbPanel;
	private JLabel dbUrlLabel;
	private JLabel dbDatabaseNameLabel;
	private JLabel dbUserLabel;
	private JLabel dbPassLabel;
	private JTextField dbUrlTextField;
	private JTextField dbDatabaseNameTextField;
	private JTextField dbUserTextField;
	private JTextField dbPassTextField;
	
	
	
	//menu
	private JMenu fileMenu;
	private JMenuBar menuBar;
	private JMenuItem menuItem;
	
	//Details declaration
	private JPanel detailsPanel;
	private GridBagLayout detailsGridBagLayout;
	private GridBagConstraints detailsGridBagConstraints;
	private JButton browseButton;
	private JTextField fileSourcetextField;
	private JButton reloadButton;
	private JLabel sheetLabel;
	private JComboBox<String> sheetComboBox;
	private JButton validateButton;
	private JLabel scenarioLabel;
	private JComboBox<String> scenarioComboBox;
	private JButton scenarioLoadButton;
	private JButton scenarioCheckButton;
	
	
	//generate pop-up fields
	private JLabel fileNameLabel;
	private JTextField fileDestinationTextField;
	
	//generate
	private JPanel generatePanel;
	private JButton generateButton;
	private JLabel generateFilePath;
	private JLabel insertFileNameLabel;
	private JLabel deleteFileNameLabel;
	private JTextField generateFilePathTextField;
	private JTextField insertFileNameTextField;
	private JTextField deleteFileNameTextField;
	
	//pop up file browser
	private JFileChooser fileChooser;

	//create table list objects
	private JList<String> insertList;
	private JPanel insertPanel;
	private JScrollPane insertScrollPane;
	private JButton insertUpButton;
	private JButton insertDownButton;
	
	//delete table list objects
	private JList<String> deleteList;
	private JPanel deletePanel;
	private JScrollPane deleteScrollPane;
	private JButton deleteUpButton;
	private JButton deleteDownButton;
	
	private JButton loadAllAndCheckButton;
	
	private DataLoadProcess loadProcess = new DataLoadProcess();
	private DatabaseValidator validator = new DatabaseValidator();
	public DataLoader(){
		init();
	}
	
	public void setupDetailsPanel(){
		//details panel setup
		browseButton = new JButton(BUTTON_BROWSE);
		browseButton.addActionListener(this);
		browseButton.setMnemonic(KeyEvent.VK_B);
		
		fileSourcetextField = new JTextField();
		fileSourcetextField.setColumns(15);
		fileSourcetextField.getDocument().addDocumentListener(this);
		
		reloadButton = new JButton(BUTTON_RELOAD);
		reloadButton.addActionListener(this);
		browseButton.setMnemonic(KeyEvent.VK_R);
		
		sheetLabel = new JLabel(LABEL_SHEET);
		sheetComboBox = new JComboBox<String>();
		sheetComboBox.addActionListener(this);
		
		validateButton = new JButton(BUTTON_VALIDATE);
		validateButton.addActionListener(this);
		validateButton.setMnemonic(KeyEvent.VK_V);
		
		scenarioLabel = new JLabel(LABEL_SCENARIO);
		scenarioComboBox = new JComboBox<String>();
		scenarioComboBox.addActionListener(this);
		
		scenarioLoadButton = new JButton(BUTTON_LOAD_SCENARIO);
		scenarioLoadButton.addActionListener(this);
		scenarioLoadButton.setMnemonic(KeyEvent.VK_L);
		
		scenarioCheckButton = new JButton(BUTTON_CHECK_SCENARIO);
		scenarioCheckButton.addActionListener(this);
		scenarioLoadButton.setMnemonic(KeyEvent.VK_C);
		
		loadAllAndCheckButton = new JButton("EZ-Check");
		loadAllAndCheckButton.addActionListener(this);
		
		detailsGridBagConstraints = new GridBagConstraints();
		
		//details panel construction
		detailsPanel = new JPanel();
		detailsPanel.setBorder(new TitledBorder(new EtchedBorder(), LABEL_DETAILS));
		detailsPanel.setLayout(new GridBagLayout());
		
		detailsGridBagConstraints.gridy=0;
		detailsGridBagConstraints.gridx=0;
		detailsPanel.add(browseButton, detailsGridBagConstraints);
		
		detailsGridBagConstraints.gridy=0;
		detailsGridBagConstraints.gridx=1;
		detailsPanel.add(fileSourcetextField, detailsGridBagConstraints);
		
		detailsGridBagConstraints.gridy=0;
		detailsGridBagConstraints.gridx=2;
		detailsPanel.add(reloadButton, detailsGridBagConstraints);
		
		detailsGridBagConstraints.gridy=1;
		detailsGridBagConstraints.gridx=0;
		detailsPanel.add(sheetLabel, detailsGridBagConstraints);

		detailsGridBagConstraints.gridy=1;
		detailsGridBagConstraints.gridx=1;
		detailsPanel.add(sheetComboBox, detailsGridBagConstraints);
		
		detailsGridBagConstraints.gridy=1;
		detailsGridBagConstraints.gridx=2;
		detailsPanel.add(validateButton, detailsGridBagConstraints);
		
		detailsGridBagConstraints.gridy=2;
		detailsGridBagConstraints.gridx=0;
		detailsPanel.add(scenarioLabel, detailsGridBagConstraints);
		
		detailsGridBagConstraints.gridy=2;
		detailsGridBagConstraints.gridx=1;
		detailsPanel.add(scenarioComboBox, detailsGridBagConstraints);
		
		detailsGridBagConstraints.gridy=2;
		detailsGridBagConstraints.gridx=2;
		detailsPanel.add(scenarioLoadButton, detailsGridBagConstraints);
//		detailsPanel.add(loadAllAndCheckButton, detailsGridBagConstraints);
		
		detailsGridBagConstraints.gridy=2;
		detailsGridBagConstraints.gridx=3;
//		scenarioCheckButton.setVisible(false);
		detailsPanel.add(scenarioCheckButton, detailsGridBagConstraints);
			
	}
	
	public void initMenu(){
		menuBar = new JMenuBar();
		fileMenu = new JMenu(MENU_FILE);
		fileMenu.setMnemonic(KeyEvent.VK_F);
		fileMenu.getAccessibleContext().setAccessibleDescription(MENU_FILE);
		menuBar.add(fileMenu);
		menuItem = new JMenuItem(MENU_FILE_DB_SETUP);
		menuItem.setMnemonic(KeyEvent.VK_D);
		menuItem.getAccessibleContext().setAccessibleDescription(MENU_FILE_DB_SETUP);
		menuItem.addActionListener(this);
		fileMenu.add(menuItem);
		fileNameLabel = new JLabel(LABEL_FILE);
	}
	
	public void setupPopupDbOptions(){
		dbPanel = new JPanel();
		dbDatabaseNameLabel = new JLabel(LABEL_DATABASE_NAME);
		dbUrlLabel = new JLabel(LABEL_URL);
		dbUserLabel = new JLabel(LABEL_USERNAME);
		dbPassLabel = new JLabel(LABEL_PASSWORD);
		dbUrlTextField = new JTextField(20);
		dbDatabaseNameTextField = new JTextField(20);
		dbUserTextField = new JTextField(20);
		dbPassTextField = new JTextField(20);
		
		dbPanel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.gridy=0;
		gbc.gridx=0;
		dbPanel.add(dbDatabaseNameLabel,gbc);
		
		gbc.gridy=0;
		gbc.gridx=1;
		dbPanel.add(dbDatabaseNameTextField,gbc);
		
		gbc.gridy=1;
		gbc.gridx=0;
		dbPanel.add(dbUrlLabel,gbc);
		
		gbc.gridy=1;
		gbc.gridx=1;
		dbPanel.add(dbUrlTextField,gbc);
		
		gbc.gridy=2;
		gbc.gridx=0;
		dbPanel.add(dbUserLabel,gbc);
		
		gbc.gridy=2;
		gbc.gridx=1;
		dbPanel.add(dbUserTextField,gbc);
		
		gbc.gridy=3;
		gbc.gridx=0;
		dbPanel.add(dbPassLabel,gbc);
		
		gbc.gridy=3;
		gbc.gridx=1;
		dbPanel.add(dbPassTextField,gbc);
	}
	
	public void init(){
		mainFrame = new JFrame("DataLoader v2");
		mainPanel = new JPanel();
		initMenu();
		setupDetailsPanel();
		setupPopupDbOptions();
		
		//create List initialization
		insertList = new JList<String>();
		insertList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		insertList.setLayoutOrientation(JList.VERTICAL);
		insertList.addMouseListener(this);
		
		insertScrollPane = new JScrollPane(insertList);
		insertScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		insertUpButton = new JButton(BUTTON_INSERT_UP);
		insertUpButton.addActionListener(this);
		
		insertDownButton = new JButton(BUTTON_INSERT_DOWN);
		insertDownButton.addActionListener(this);
		
		insertPanel = new JPanel();
		insertPanel.setBorder(new TitledBorder(new EtchedBorder(), LABEL_INSERT));
		insertPanel.add(insertScrollPane);
		insertPanel.add(insertUpButton);
		insertPanel.add(insertDownButton);
		
		//delete text area initialization
		deleteList = new JList<String>();
		deleteList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		deleteList.setLayoutOrientation(JList.VERTICAL);
		deleteList.addMouseListener(this);
		
		deleteScrollPane = new JScrollPane(deleteList);
		deleteScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		deleteUpButton = new JButton(BUTTON_DELETE_UP);
		deleteUpButton.addActionListener(this);
		
		deleteDownButton = new JButton(BUTTON_DELETE_DOWN);
		deleteDownButton.addActionListener(this);
		
		deletePanel = new JPanel();
		deletePanel.setBorder(new TitledBorder(new EtchedBorder(), LABEL_DELETE));
		deletePanel.add(deleteScrollPane);
		deletePanel.add(deleteUpButton);
		deletePanel.add(deleteDownButton);
		
		//Action panel setup
		generateFilePath = new JLabel(LABEL_GENERATE_FILE_PATH);
		generateFilePathTextField = new JTextField(20);
		insertFileNameLabel = new JLabel(LABEL_GENERATE_INSERT_FILE);
		insertFileNameTextField = new JTextField(20);
		deleteFileNameLabel = new JLabel (LABEL_GENERATE_DELETE_FILE);
		deleteFileNameTextField = new JTextField(20);
						
		generateButton = new JButton(BUTTON_GENERATE);
		generateButton.addActionListener(this);
		generateButton.setMnemonic(KeyEvent.VK_G);
		
		generatePanel = new JPanel();
		generatePanel.setBorder(new TitledBorder(new EtchedBorder(), LABEL_GENERATE));
		generatePanel.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.gridy=0;
		gbc.gridx=0;
		generatePanel.add(generateFilePath, gbc);
		
		gbc.gridy=0;
		gbc.gridx=1;		
		generatePanel.add(generateFilePathTextField, gbc);
		
//		gbc.gridy=1;
//		gbc.gridx=0;		
//		generatePanel.add(insertFileNameLabel, gbc);
		
//		gbc.gridy=1;
//		gbc.gridx=1;		
//		generatePanel.add(insertFileNameTextField, gbc);
		
//		gbc.gridy=2;
//		gbc.gridx=0;		
//		generatePanel.add(deleteFileNameLabel, gbc);
		
//		gbc.gridy=2;
//		gbc.gridx=1;		
//		generatePanel.add(deleteFileNameTextField, gbc);
		
//		gbc.gridy=3;
//		gbc.gridx=1;
		gbc.gridy=1;
		gbc.gridx=1;
		generatePanel.add(generateButton, gbc);
		
		//generate pop-up
		fileDestinationTextField = new JTextField();
		fileDestinationTextField.setColumns(15);
		
		//construction of elements
		mainPanel.add(detailsPanel);
		mainPanel.add(insertPanel);
		mainPanel.add(deletePanel);
		mainPanel.add(generatePanel);
		
		mainFrame.setJMenuBar(menuBar);
		mainFrame.add(mainPanel);
		mainFrame.setSize(500, 500);
		mainFrame.setResizable(true);
		mainFrame.setVisible(true);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		fileChooser = new JFileChooser();
		setButtonStatus();
	}
	

	/**
	 * Runs the program.
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		new DataLoader();
		
//        RemoteApiOptions options = new RemoteApiOptions()
//            .server("localhost", 8888).credentials("XXXX", "XXXX");
//        System.out.println(options.getHostname());
//        RemoteApiInstaller installer = new RemoteApiInstaller();
//        installer.install(options);
//        try {
//            DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
//            System.out.println("Key of new entity is " +
//                ds.put(new Entity("Hello Remote API!")));
//        } finally {
//            installer.uninstall();
//        }
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
//		System.out.println("ActionCommand: "+e.getActionCommand());
//		System.out.println("sheet?: "+e.getSource().equals(sheetComboBox));
//		System.out.println("scenario?: "+e.getSource().equals(scenarioComboBox));
		if(MENU_FILE_DB_SETUP.equalsIgnoreCase(e.getActionCommand())){
			//pop up database config
			try {
				showDatabaseConfig();
			} catch (SQLException e1) {
				JOptionPane.showMessageDialog(null, "error showDatabaseConfig. errorcode: "+e1.getErrorCode()+", sqlstate:"+e1.getSQLState()+". Message: "+e1.getMessage());
				e1.printStackTrace();
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(null, "Error: showDatabaseConfig: "+e1.getMessage());
				e1.printStackTrace();
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(null, "Error: showDatabaseConfig: "+e1.getMessage());
				e1.printStackTrace();
			}
			
		}else if(BUTTON_BROWSE.equalsIgnoreCase(e.getActionCommand())){
			//pop up dialog box for browsing a file
			
			if(dbDatabaseNameTextField.getText().isEmpty()){
				String databaseName = "mypage";
				String url = "jdbc:mysql://192.168.1.245";//localhost:3306";
				String userName = "sbc-test";
				String password = "sbc-test";
//				System.out.println("databaseName: "+databaseName+" url: "+url+" user: "+userName+" pass: "+password);
				try {
					loadProcess.setDatabasePreferences(url, databaseName, userName, password);
					validator.setTransactionManager(loadProcess.getTransactionInstance());
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			
			
			if(JFileChooser.APPROVE_OPTION == fileChooser.showOpenDialog(mainPanel)){
				File file = fileChooser.getSelectedFile();
				fileSourcetextField.setText(file.getAbsolutePath());
//				System.out.println(file.getName());
				loadProcess.loadWorkbook(file);
				loadSheetNames();
				setButtonStatus();
			}
		}else if(BUTTON_RELOAD.equals(e.getActionCommand())){
			clean();
			loadProcess.reloadWorkbook(new File(fileSourcetextField.getText()));
			loadSheetNames();
			setButtonStatus();
		}else if(BUTTON_VALIDATE.equals(e.getActionCommand())){
			validator.setScenarioModelMap(loadProcess.getScenarioModelMap());
			ValidatorModel model = validator.validateScenarioModelMap();
			if(null != model.getMessage()){
				JOptionPane.showMessageDialog(null, model.getMessage());
				for(String name: model.getScenarioList()){
					removeScenario(name);
				}
			}
		}else if(scenarioLoadButton.equals(e.getSource())){			
			List<String> statements = SQLWriter.getInsertStatementsList(loadProcess.getScenario(scenarioComboBox.getSelectedItem().toString()));
			List<String> deleteStatements = SQLWriter.getCleanupInsertStatementsList(loadProcess.getScenario(scenarioComboBox.getSelectedItem().toString()));
//			Iterator<String> itr = statements.iterator();
//			while(itr.hasNext()){
//				System.out.println("statements: "+itr.next());
//			}
			try {
				loadProcess.getTransactionInstance().beginTransactional();
				DatabaseWriter.insertToDB(loadProcess.getTransactionInstance(), statements);
				loadProcess.getTransactionInstance().commitTransactional();
//				scenarioLoadButton.setVisible(false);
//				scenarioCheckButton.setVisible(true);
				JOptionPane.showMessageDialog(null, "Data Loaded!");
			} catch (SQLException e1) {
				try {
					loadProcess.getTransactionInstance().rollbackTransaction();
					JOptionPane.showMessageDialog(null, "error inserting. errorcode: "+e1.getErrorCode()+", sqlstate:"+e1.getSQLState()+". Message: "+e1.getMessage());
					e1.printStackTrace();
				} catch (SQLException e2) {
					JOptionPane.showMessageDialog(null, "error rolling back. Please restart.");
					e2.printStackTrace();
				}
			}
			
			//cleanup
			try{
				loadProcess.getTransactionInstance().beginTransactional();
				DatabaseWriter.deleteFromDB(loadProcess.getTransactionInstance(), deleteStatements);
				loadProcess.getTransactionInstance().commitTransactional();
			} catch (SQLException e1) {
				try {
					loadProcess.getTransactionInstance().rollbackTransaction();
					JOptionPane.showMessageDialog(null, "error inserting. errorcode: "+e1.getErrorCode()+", sqlstate:"+e1.getSQLState()+". Message: "+e1.getMessage());
					e1.printStackTrace();
				} catch (SQLException e2) {
					JOptionPane.showMessageDialog(null, "error rolling back. Please restart.");
					e2.printStackTrace();
				}
			}
			
			//for easy loading and test.
		}else if (loadAllAndCheckButton.equals(e.getSource())){
			validator.setScenarioModelMap(loadProcess.getScenarioModelMap());
			ValidatorModel model = validator.validateScenarioModelMap();
			if(null != model.getMessage()){
				JOptionPane.showMessageDialog(null, model.getMessage());
//				for(String name: model.getScenarioList()){
//					removeScenario(name);
//				}
			}
			for(int i=0;i<scenarioComboBox.getItemCount();i++){
				List<String> statements = SQLWriter.getInsertStatementsList(loadProcess.getScenario(scenarioComboBox.getItemAt(i)));
				List<String> deleteStatements = SQLWriter.getCleanupInsertStatementsList(loadProcess.getScenario(scenarioComboBox.getItemAt(i)));
				
				try {
					loadProcess.getTransactionInstance().beginTransactional();
					System.out.println("loading: "+scenarioComboBox.getItemAt(i));
					DatabaseWriter.insertToDB(loadProcess.getTransactionInstance(), statements);
					loadProcess.getTransactionInstance().commitTransactional();
				} catch (SQLException e1) {
					try {
						loadProcess.getTransactionInstance().rollbackTransaction();
						JOptionPane.showMessageDialog(null, "error inserting for "+scenarioComboBox.getItemAt(i) +". \n errorcode: "+e1.getErrorCode()+", sqlstate:"+e1.getSQLState()+". Message: "+e1.getMessage());
						e1.printStackTrace();
					} catch (SQLException e2) {
						JOptionPane.showMessageDialog(null, "error rolling back. Please restart.");
						e2.printStackTrace();
					}
				}
				
				//cleanup
				try{
					loadProcess.getTransactionInstance().beginTransactional();
					System.out.println("cleaning: "+scenarioComboBox.getItemAt(i));
					DatabaseWriter.deleteFromDB(loadProcess.getTransactionInstance(), deleteStatements);
					loadProcess.getTransactionInstance().commitTransactional();
				} catch (SQLException e1) {
					try {
						loadProcess.getTransactionInstance().rollbackTransaction();
						JOptionPane.showMessageDialog(null, "error inserting. errorcode: "+e1.getErrorCode()+", sqlstate:"+e1.getSQLState()+". Message: "+e1.getMessage());
						e1.printStackTrace();
					} catch (SQLException e2) {
						JOptionPane.showMessageDialog(null, "error rolling back. Please restart.");
						e2.printStackTrace();
					}
				}
			}
			JOptionPane.showMessageDialog(null, "Process Done!");
		}else if(scenarioCheckButton.equals(e.getSource())){
			String message = null;
			try {
				message = validator.checkScenario(loadProcess.getScenario(scenarioComboBox.getSelectedItem().toString()));
				if(!message.isEmpty()){
//					System.out.println("check message: "+message);
					JOptionPane.showMessageDialog(null, message);
				}else{
					JOptionPane.showMessageDialog(null, "Data Valid!");
				}
//				scenarioLoadButton.setVisible(true);
//				scenarioCheckButton.setVisible(false);
			} catch (SQLException e1) {
				JOptionPane.showMessageDialog(null, "error retrieveing. errorcode: "+e1.getErrorCode()+", sqlstate:"+e1.getSQLState()+". Message: "+e1.getMessage());
				e1.printStackTrace();
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(null, "error retrieveing. Message: "+e1.getMessage());
				e1.printStackTrace();
			}

		}else if(generateButton.equals(e.getSource())){
			String fileName = scenarioComboBox.getSelectedItem().toString().concat(".sql");
			String directory = generateFilePathTextField.getText();
			if(directory.endsWith(SUFFIX_SQL)||directory.endsWith(SUFFIX_TXT)){
				
			}else{
				try {
					SQLWriter.writeScenarioStatements(loadProcess.getScenario(scenarioComboBox.getSelectedItem().toString()), directory, fileName);
				} catch (IOException e1) {
					JOptionPane.showMessageDialog(null, "Error: unable to write file :( "+e1.getMessage());
				}
			}
//			try {
//				DataLoaderUtil dataLoader = new DataLoaderUtil(
//						"mypage",
//						"jdbc:mysql://127.0.0.1",
//						"root",
//						"root");
//				File file = fileChooser.getSelectedFile();
//				dataLoader.loadFile(file.getPath().toString());
//				dataLoader.loadSheet("DB_Data");
//				dataLoader.validateScenario("API_02_04_02_03_01-001");
//				dataLoader.saveCurrentDatabase(
//						"C:\\Users\\butandingboben\\Desktop\\excel\\",
//						this.getClass().getSimpleName() + "_SQL_Dump.sql");
//				dataLoader.loadScenario("API_02_04_02_03_01-001");
//				dataLoader.loadCurrentDatabase(
//						"C:\\Users\\butandingboben\\Desktop\\excel\\",
//						this.getClass().getSimpleName() + "_SQL_Dump.sql");
//			} catch (Exception err) {
//				err.printStackTrace();
//			}
		}else if(sheetComboBox.equals(e.getSource()) && null != sheetComboBox.getSelectedItem()){
			scenarioComboBox.removeAllItems();
//			clearStringList(insertList);
//			clearStringList(deleteList);
//			System.out.println("sheet selected item: "+sheetComboBox.getSelectedItem());
			String sheetName = sheetComboBox.getSelectedItem().toString();
			loadProcess.setSheet(sheetName);
			loadProcess.populateScenariosMap();
			try {
				loadScenarioNames();
			} catch (DataLoaderException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}else if(scenarioComboBox.equals(e.getSource()) && null != scenarioComboBox.getSelectedItem()){
			String scenarioTitle = scenarioComboBox.getSelectedItem().toString();
//			System.out.println("Loading table names");
			loadTableNames(scenarioTitle);
			setButtonStatus();

		}
		
	}
	
	public void removeScenario(String scenarioName){
		for(int i=0;i<scenarioComboBox.getItemCount();i++){
			if(scenarioName.equalsIgnoreCase(scenarioComboBox.getItemAt(i))){
				scenarioComboBox.removeItemAt(i);
			}
		}
	}
	
	public void clearStringList(JList<String> jlist){
		DefaultListModel list = (DefaultListModel) jlist.getModel();
		list.removeAllElements();
	}
	
	public void loadSheetNames(){
		for(String item : loadProcess.getSheetNames()){
			sheetComboBox.addItem(item);
		}
	}
	
	public void loadScenarioNames() throws DataLoaderException{
		for(String item : loadProcess.getScenarioNames()){
			scenarioComboBox.addItem(item);
		}
	}
	
	public void clean(){
		sheetComboBox.removeAllItems();
		scenarioComboBox.removeAllItems();
		insertList.removeAll();
		deleteList.removeAll();
		setButtonStatus();
	}
	
	public void showDatabaseConfig() throws SQLException, IOException, Exception{
		if(JOptionPane.OK_OPTION == JOptionPane.showConfirmDialog(null,dbPanel, MENU_FILE_DB_SETUP,JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE)){
			String databaseName = dbDatabaseNameTextField.getText();
			String url = dbUrlTextField.getText();
			String userName = dbUserTextField.getText();
			String password = dbPassTextField.getText();
//			System.out.println("databaseName: "+databaseName+" url: "+url+" user: "+userName+" pass: "+password);
			loadProcess.setDatabasePreferences(url, databaseName, userName, password);
			validator.setTransactionManager(loadProcess.getTransactionInstance());
		}
	}
	
	public void loadTableNames(String scenarioTitle){
		Map<String,List<String>> tablesMap = loadProcess.getTableNames(scenarioTitle);
		
		//populate JList
		insertList.setModel(getTables(tablesMap.get(UtilConstants.BEFORE)));
		deleteList.setModel(getTables(tablesMap.get(UtilConstants.AFTER)));
	}
	
	public DefaultListModel<String> getTables(List<String> list){
		DefaultListModel<String> l = new DefaultListModel<String>();
		Iterator<String> it = list.iterator();
		while(it.hasNext()){
			l.addElement(it.next());
		}
		return l;
	}
	
	public void setButtonStatus(){
		
		reloadButton.setEnabled(false);
		validateButton.setEnabled(false);
		scenarioLoadButton.setEnabled(false);
		insertUpButton.setEnabled(false);
		insertDownButton.setEnabled(false);
		deleteUpButton.setEnabled(false);
		deleteDownButton.setEnabled(false);
		generateButton.setEnabled(false);
		scenarioCheckButton.setEnabled(true);
		
		//reload button
		if(StringUtils.isNotEmpty(fileSourcetextField.getText())){
			reloadButton.setEnabled(true);
		}
		
		//validate button
		if(null != sheetComboBox.getSelectedItem() && null != loadProcess.getTransactionInstance()){
			validateButton.setEnabled(true);
		}
		
		//load/check button
		if(null != scenarioComboBox.getSelectedItem()&& null != loadProcess.getTransactionInstance()){
			scenarioLoadButton.setEnabled(true);
			scenarioCheckButton.setEnabled(false);
			scenarioCheckButton.setEnabled(true); //turned off;

		}
		
//		if(-1 != insertList.getSelectedIndex()){
//			insertUpButton.setEnabled(true);
//			insertDownButton.setEnabled(true);
//		}
//
//		if(-1 != deleteList.getSelectedIndex()){
//			deleteUpButton.setEnabled(true);
//			deleteDownButton.setEnabled(true);
//		}
		
		if(null != generateFilePathTextField){
			generateButton.setEnabled(true);
		}
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		// TODO Auto-generated method stub
		setButtonStatus();
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		// TODO Auto-generated method stub
		setButtonStatus();
	}

	@Override
	public void changedUpdate(DocumentEvent e) {
		// TODO Auto-generated method stub
		setButtonStatus();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		setButtonStatus();
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
