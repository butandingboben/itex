/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Global Technologies Corporation All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONObject;

import com.rgtc.itex.dao.DatabaseWriter;
import com.rgtc.itex.exceptions.DataLoaderException;
import com.rgtc.itex.models.AssertionOutputModel;
import com.rgtc.itex.models.ScenarioModel;
import com.rgtc.itex.models.ValidatorModel;
import com.rgtc.itex.service.DataLoadProcess;
import com.rgtc.itex.service.DatabaseValidator;
import com.rgtc.itex.service.SQLWriter;

/**
 * <div class="jp">
 *
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 * This util class calls the methods of the data loader.
 * </div>
 *
 * @author Emman Resmeros
 * @version 0.01
 * @since 0.01
 */
public class DataLoaderUtil {

	/**
	 * Covers most processes for loading and generating SQL Statements for DB processes.
	 */
	private DataLoadProcess loaderProcess = new DataLoadProcess();

	/**
	 * The Validator for DB entries.
	 */
	private DatabaseValidator validator = new DatabaseValidator();

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Emman Resmeros
	 * @version 0.01
	 * @param db The database.
	 * @param url The url.
	 * @param un The user name.
	 * @param pw The password.
	 * @throws Exception 
	 * @throws IOException 
	 * @throws SQLException 
	 * @since 0.01
	 */
	public DataLoaderUtil(String db, String url, String un, String pw) throws SQLException, IOException, Exception {
		loaderProcess.setDatabasePreferences(url, db, un, pw);
		validator.setTransactionManager(loaderProcess.getTransactionInstance());
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Emman Resmeros
	 * @version 0.01
	 * @param path The file path.
	 * @since 0.01
	 */
	public void loadFile(String path) {
		loaderProcess.loadWorkbook(new File(path));
	}
	
	public Workbook getLoadedWorkbook() {
		return loaderProcess.getLoadedWorkbook();
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Emman Resmeros
	 * @version 0.01
	 * @param sheetName The name of the sheet to be loaded.
	 * @throws Exception 
	 * @since 0.01
	 */
	public void loadSheet(String sheetName) throws Exception {
		if (loaderProcess.getSheetNames().contains(sheetName)) {
			loaderProcess.setSheet(sheetName);
			loaderProcess.populateScenariosMap();
		} else {
			throw new Exception("Sheet not found");
		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Emman Resmeros
	 * @version 0.01
	 * @param scenarioName The name of the sheet to be validated.
	 * @return String
	 * @since 0.01
	 */
	public String validateScenario(String scenarioName) {
		validator.setScenarioModelMap(loaderProcess.getScenarioModelMap());
		ValidatorModel model = validator.validateScenarioModelMap();
		if ("correct".equals(model.getMessage())) {
			model.setMessage("");
		}

		return model.getMessage();
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Emman Resmeros
	 * @version 0.01
	 * @param scenarioName The name of the scenario to be loaded.
	 * @since 0.01
	 */
	public void loadScenario(String scenarioName) throws Exception {
		List<String> statements = SQLWriter.getInsertStatementsList(loaderProcess.getScenario(scenarioName));

		try {
			loaderProcess.getTransactionInstance().beginTransactional();

			DatabaseWriter.insertToDB(loaderProcess.getTransactionInstance(), statements);

			loaderProcess.getTransactionInstance().commitTransactional();
		} catch (SQLException e1) {
			try {
				e1.printStackTrace();
				loaderProcess.getTransactionInstance().rollbackTransaction();
				throw new Exception("Error on loading scenario\n" + e1.getMessage());
			} catch (SQLException e2) {
				e2.printStackTrace();
			}

		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Emman Resmeros
	 * @version 0.01
	 * @param scenarioName The name of the scenario to be loaded.
	 * @since 0.01
	 */
	public void loadScenario(String scenarioName, File dbTables) throws Exception {
		if (!dbTables.exists()) {
			throw new FileNotFoundException();
		}

		// Getting the list of database tables ranking
		BufferedReader reader = new BufferedReader(new FileReader(dbTables));
		Map<String, Integer> rankings = new HashMap<String, Integer>();
		int rank = 1;
		String table = null;

		while(null != (table = reader.readLine())) {
			rankings.put(table.trim(), rank);
			rank++;
		}

		List<String> statements = SQLWriter.getInsertStatementsList(loaderProcess.getScenario(scenarioName), rankings);
		System.out.println("Insert statements:");
		for (int i = 0; i < statements.size(); i++) {
			System.out.println("\t" + statements.get(i));
		}

		try {
			loaderProcess.getTransactionInstance().beginTransactional();

			DatabaseWriter.insertToDB(loaderProcess.getTransactionInstance(), statements);

			loaderProcess.getTransactionInstance().commitTransactional();
		} catch (SQLException e1) {
			try {
				e1.printStackTrace();
				loaderProcess.getTransactionInstance().rollbackTransaction();
				throw new Exception("Error on loading scenario\n" + e1.getMessage());
			} catch (SQLException e2) {
				e2.printStackTrace();
			}

		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Emman Resmeros
	 * @version 0.01
	 * @param path The file path.
	 * @param scenarioName The name of the scenario to be checked.
	 * @return AssertionOutputModel
	 * @throws Exception The exception to be thrown.
	 * @since 0.01
	 */
	public AssertionOutputModel checkScenario(String path, String scenarioName) throws Exception {

//		ScenarioModel scenario = Script.getScenario(path, scenarioName);
        ScenarioModel scenario = loaderProcess.getScenario(scenarioName);

		AssertionOutputModel model = new AssertionOutputModel();

		String result = validator.checkScenario(scenario);
		if (0 == result.length()) {
			model.setIsError(false);
		} else {
//			System.err.println(result);
			model.setError(result);
			model.setIsError(true);
		}

		return model;
	}
	
	public AssertionOutputModel saveCurrentDatabase(
			String directory,
			String filename,
			String url,
			String username,
			String password) throws Exception {
		AssertionOutputModel model = new AssertionOutputModel();
		model.setIsError(false);
		model.setError("");
//		String message = validator.createSQLDump(directory, filename + "_MYPAGE_DUMP.sql");
//		if (0 >= message.length()) {
//			model.setError(message);
//			model.setIsError(true);
//		} else {
//			System.out.println(message);
//		}
//		message = validator.createCoreSQLDump(directory, filename + "_CORE_DUMP.sql", url, username, password);
//		if (0 >= message.length()) {
//			model.setError(message);
//			model.setIsError(true);
//		}
		
		return model;
	}
	
	public AssertionOutputModel loadCurrentDatabase(
			String directory,
			String filename,
			String url,
			String username,
			String password) throws Exception {
		AssertionOutputModel model = new AssertionOutputModel();
//		model.setIsError(false);
//		String message = validator.loadSQLDump(directory, filename + "_MYPAGE_DUMP.sql");
//		if (0 >= message.length()) {
//			model.setError(message);
//			model.setIsError(true);
//		}
//		message = validator.loadCoreSQLDump(directory, filename + "_CORE_DUMP.sql", url, username, password);
//		if (0 >= message.length()) {
//			model.setError(message);
//			model.setIsError(true);
//		}
		List<String> deleteStatements = SQLWriter.getCleanupInsertStatementsList(loaderProcess.getScenario(filename));

		try {
			loaderProcess.getTransactionInstance().beginTransactional();

			DatabaseWriter.deleteFromDB(loaderProcess.getTransactionInstance(), deleteStatements);

			loaderProcess.getTransactionInstance().commitTransactional();
		} catch (SQLException e1) {
			try {
				loaderProcess.getTransactionInstance().rollbackTransaction();
				throw new Exception("Error on reverting database\n" + e1.getMessage());
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
		}
		loaderProcess.getTransactionInstance().endTransactional();

		return model;
	}

	public void loadCurrentDatabase(
			String scenarioName,
			File dbTables) throws Exception {

		// Getting the list of database tables ranking
		BufferedReader reader = new BufferedReader(new FileReader(dbTables));
		Map<String, Integer> rankings = new HashMap<String, Integer>();
		int rank = 1;
		String table = null;

		while(null != (table = reader.readLine())) {
			rankings.put(table.trim(), rank);
			rank++;
		}

		List<String> deleteStatements = SQLWriter.getCleanupInsertStatementsList(loaderProcess.getScenario(scenarioName), rankings);
		System.out.println("Clean-up statements:");
		for (int i = 0; i < deleteStatements.size(); i++) {
			System.out.println("\t" + deleteStatements.get(i));
		}

		try {
			loaderProcess.getTransactionInstance().beginTransactional();

			DatabaseWriter.deleteFromDB(loaderProcess.getTransactionInstance(), deleteStatements);

			loaderProcess.getTransactionInstance().commitTransactional();
		} catch (SQLException e1) {
			try {
				loaderProcess.getTransactionInstance().rollbackTransaction();
				throw new Exception("Error on reverting database\n" + e1.getMessage());
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
		}
		loaderProcess.getTransactionInstance().endTransactional();
	}
	
	public AssertionOutputModel loadCurrentDatabaseCron(
			String directory,
			String filename,
			String url,
			String username,
			String password) throws Exception {
		AssertionOutputModel model = new AssertionOutputModel();
//		model.setIsError(false);
//		String message = validator.loadSQLDump(directory, filename + "_MYPAGE_DUMP.sql");
//		if (0 >= message.length()) {
//			model.setError(message);
//			model.setIsError(true);
//		}
//		message = validator.loadCoreSQLDump(directory, filename + "_CORE_DUMP.sql", url, username, password);
//		if (0 >= message.length()) {
//			model.setError(message);
//			model.setIsError(true);
//		}
		return model;
	}

	public List<String> getScenarioNames() throws DataLoaderException{
		return loaderProcess.getScenarioNames();
	}
	
	public void loadJSONInputParams(String sheetName){
		loaderProcess.loadDynamicJsonParams(sheetName);
	}
	
	public TreeMap<String,JSONObject> getJsonDynamicParams(){
		return loaderProcess.getDynamicParams();
	}
	
	/**
	 * Function for generating the insert statements and returning it in a list
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param scenarioName
	 * @param dbTables
	 * @return List<String>
	 * @throws Exception
	 * @since 0.01
	 */
	public List<String> getInsertStatements(String scenarioName, File dbTables) throws Exception {
		if (!dbTables.exists()) {
			throw new FileNotFoundException();
		}

		// Getting the list of database tables ranking
		BufferedReader reader = new BufferedReader(new FileReader(dbTables));
		Map<String, Integer> rankings = new HashMap<String, Integer>();
		int rank = 1;
		String table = null;

		while(null != (table = reader.readLine())) {
			rankings.put(table.trim(), rank);
			rank++;
		}

		List<String> statements = SQLWriter.getInsertStatementsList(loaderProcess.getScenario(scenarioName), rankings);

		return statements;
	}
	
	/**
	 * Function for generating the insert statements and returning it in a list
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param scenarioName
	 * @param dbTables
	 * @return List<String>
	 * @throws Exception
	 * @since 0.01
	 */
	public List<String> getCleanupStatements(String scenarioName, File dbTables) throws Exception {
		if (!dbTables.exists()) {
			throw new FileNotFoundException();
		}

		// Getting the list of database tables ranking
		BufferedReader reader = new BufferedReader(new FileReader(dbTables));
		Map<String, Integer> rankings = new HashMap<String, Integer>();
		int rank = 1;
		String table = null;

		while(null != (table = reader.readLine())) {
			rankings.put(table.trim(), rank);
			rank++;
		}

		List<String> statements = SQLWriter.getCleanupInsertStatementsList(loaderProcess.getScenario(scenarioName), rankings);

		return statements;
	}
	
	
	public void disableForeignKeys() {
		try {
			String disableGlobalForeignKeySql = "SET @@global.foreign_key_checks = 0;";
			System.out.println("[DataLoader] Disabling global foreign keys using '" + disableGlobalForeignKeySql + "'.");
			String disableSessionForeignKeySql = "SET foreign_key_checks = 0;";
			System.out.println("[DataLoader] Disabling session foreign keys using '" + disableSessionForeignKeySql + "'.");
			List<String> statements = new ArrayList<String>();
			statements.add(disableGlobalForeignKeySql);
			statements.add(disableSessionForeignKeySql);
			DatabaseWriter.configureDatabase(loaderProcess.getTransactionInstance(), statements);
			System.out.println("[DataLoader] Foreign keys disabled.");
		} catch (SQLException e) {
			System.out.println("[DataLoader] Failed to disable foreign keys.");
			e.printStackTrace();
		}
	}
	
	public void enableForeignKeys() {
		try {
			String enableGlobalForeignKeySql = "SET @@global.foreign_key_checks = 1;";
			System.out.println("[DataLoader] Enabling foreign keys using '" + enableGlobalForeignKeySql + "'.");
			String enableSessionForeignKeySql = "SET foreign_key_checks = 1;";
			System.out.println("[DataLoader] Enabling session foreign keys using '" + enableSessionForeignKeySql + "'.");
			List<String> statements = new ArrayList<String>();
			statements.add(enableGlobalForeignKeySql);
			statements.add(enableSessionForeignKeySql);
			DatabaseWriter.configureDatabase(loaderProcess.getTransactionInstance(), statements);
			System.out.println("[DataLoader] Foreign keys enabled.");
		} catch (SQLException e) {
			System.out.println("[DataLoader] Failed to enable foreign keys.");
			e.printStackTrace();
		}
	}
	
}
