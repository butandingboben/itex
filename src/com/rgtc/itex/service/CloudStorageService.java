/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

import com.google.api.services.storage.Storage;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.api.files.AppEngineFile;
import com.google.appengine.api.files.FileService;
import com.google.appengine.api.files.FileServiceFactory;
import com.google.appengine.api.files.FileWriteChannel;
import com.google.appengine.api.files.GSFileOptions.GSFileOptionsBuilder;
import com.google.appengine.api.utils.SystemProperty;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RawGcsService;
import com.google.appengine.tools.cloudstorage.RawGcsService.RawGcsCreationToken;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.google.appengine.tools.cloudstorage.dev.LocalRawGcsServiceFactory;
import com.google.appengine.tools.development.testing.LocalBlobstoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.appengine.tools.remoteapi.RemoteApiInstaller;
import com.google.appengine.tools.remoteapi.RemoteApiOptions;


/**
 * <div class="jp">
 * 
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 * 
 * </div>
 *
 * @author Eldon Leuterio
 * @version 0.01
 * @since 0.01
 */
public class CloudStorageService {

	private RemoteApiOptions options;
	
	public static Charset charset = Charset.forName("UTF-8");
	public static CharsetEncoder encoder = charset.newEncoder();
	public static CharsetDecoder decoder = charset.newDecoder();
	
//	private final GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
//		      .initialRetryDelayMillis(10)
//		      .retryMaxAttempts(10)
//		      .totalRetryPeriodMillis(15000)
//		      .build());
	
	
	
	public CloudStorageService(String url, int port, String user, String pass){
		options = new RemoteApiOptions().server(url, port).credentials(user, pass);
	}
	
	public boolean writeToFile(){
		RemoteApiInstaller installer = new RemoteApiInstaller();
		try {
			installer.install(options);
//			RawGcsService raw = LocalRawGcsServiceFactory.createLocalRawGcsService();
//			
//			raw.putObject(fileName, GcsFileOptions.getDefaultInstance(), encoder.encode(CharBuffer.wrap(content)), 15000L);
			
			
			
			System.out.println(SystemProperty.environment.value());
//			SystemProperty.environment.set(SystemProperty.Environment.Value.Production);
			System.out.println(SystemProperty.environment.value());
			
//			 LocalServiceTestHelper helper = new LocalServiceTestHelper(
//						new LocalBlobstoreServiceTestConfig(), new LocalDatastoreServiceTestConfig());
//			 helper.setUp();
			
			GcsService gcsService = 
					GcsServiceFactory.createGcsService(RetryParams.getDefaultInstance());
			
			
			
			
			GcsFilename fileName = new GcsFilename("app_default_bucket",
					"key");
			String content = "test";
			
			
			
			GcsOutputChannel outputChannel = gcsService.createOrReplace(fileName,
					GcsFileOptions.getDefaultInstance());
			outputChannel.write(ByteBuffer.wrap(content.getBytes()));
			outputChannel.close();
			
//			helper.tearDown();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			installer.uninstall();
		}
		return false;
	}
	
	public void setupGSC(){
		RemoteApiInstaller installer = new RemoteApiInstaller();
		try {
			installer.install(options);
			System.out.println("default: "+AppIdentityServiceFactory.getAppIdentityService().getDefaultGcsBucketName());
		  FileService fileService = FileServiceFactory.getFileService();
		  GSFileOptionsBuilder optionsBuilder = new GSFileOptionsBuilder()
          .setBucket("bucket")
          .setKey("key")
          .setAcl("public-read")
          .setMimeType("text/html");
		 AppEngineFile writableFile = fileService.createNewGSFile(optionsBuilder.build());
		// Open a channel for writing
	        boolean lockForWrite = true; // Do you want to exclusively lock this object?
	        FileWriteChannel writeChannel = fileService.openWriteChannel(writableFile, lockForWrite);
	        // For this example, we write to the object using the PrintWriter
	        PrintWriter out = new PrintWriter(Channels.newWriter(writeChannel, "UTF8"));
	        out.println("activation");

	        // Close the object without finalizing.
	        out.close();

	        // Finalize the object
	        writeChannel.closeFinally();
	        System.out.println("Completed writing to google storage");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			installer.uninstall();
		}
	}
	
	public void createBucket(GcsFilename fileName, Object content){
		RemoteApiInstaller installer = new RemoteApiInstaller();
		RawGcsService local = LocalRawGcsServiceFactory.createLocalRawGcsService();
		try {
			installer.install(options);
			RawGcsCreationToken tkn = local.beginObjectCreation(fileName, GcsFileOptions.getDefaultInstance(), 15000);
		
			local.putObject(fileName, GcsFileOptions.getDefaultInstance(), (ByteBuffer) content, 15000);
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			installer.uninstall();
		}
		
	}
}
