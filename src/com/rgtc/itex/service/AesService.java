/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.service;

import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import com.rgtc.itex.util.Encryption;

/**
 *
 * <div class="jp">
 * AES Encryptionユーティリティを処理するクラス
 * </div>
 * <p>
 * <div class="en" style="padding:0em 0.5em" >
 * Class to handle <code>AES Encryption</code> utility.
 * </div>
 *
 * @author Richard Go
 * @version 0.01
 * @since 0.01
 */
public class AesService {

    /** Salt */
    private static String SALT = "06BA542225226738DA6E27829034DBF27F3D006CA201FEB2251A1350A579C3E4";

    /** IV */
    private static String IV = "CAE885B09FB9056E6AFFDBD0580FE482";

    /** Pass phrase */
    private static String PASSPHRASE = "sbcPHKey0655";

    /**
     * UTF-8.
     */
    private static final String UTF8 = "UTF-8";

    /**
     * Cipher transformation method.
     */
    private static final String CIPHER_TRANSFORMATION_METHOD = "AES/CBC/PKCS5Padding";

    /**
     * Secret-key Algorithm.
     */
    private static final String SECRET_KEY_ALGORITHM = "PBKDF2WithHmacSHA1";

    /**
     * AES algorithm.
     */
    private static final String AES_ALGORITHM = "AES";

    /**
     * Key size.
     */
    private static final int keySize = 128;

    /**
     * Iteration count.
     */
    private static final int iterationCount = 7;

    /**
     * Cipher.
     */
    private static Cipher cipher;

    /**
     * Constructor.
     * @maker Richard Go
     * @version 0.01
     * @param keySize key size
     * @param iterationCount iteration count
     * @since 0.01
     */
    public AesService(int keySize, int iterationCount) {
        try {
            cipher = Cipher.getInstance(CIPHER_TRANSFORMATION_METHOD);
        } catch (NoSuchAlgorithmException ae) {
            throw fail(ae);
        } catch (NoSuchPaddingException pe) {
            throw fail(pe);
        }
    }

    /**
     * Method to encrypt plain text.
     * @param salt salt string
     * @param iv initialization vector
     * @param passphrase passphrase string
     * @param plaintext text to encrypt
     * @return String encrypted text
     */
    public String encrypt(String salt, String iv, String passphrase, String plaintext) {
        SecretKey key = generateKey(salt, passphrase);
        byte[] encrypted = doFinal(Cipher.ENCRYPT_MODE, key, iv, plaintext.getBytes(Charset.forName(UTF8)));
        return Encryption.base64(encrypted);
    }

    /**
     * Method to decrypt cipher text.
     * @param salt salt string
     * @param iv initialization vector
     * @param passphrase passphrase string
     * @param ciphertext enrypted text
     * @return String decrypted text
     */
    public String decrypt(String salt, String iv, String passphrase, String ciphertext) {
        SecretKey key = generateKey(salt, passphrase);
        byte[] decrypted = doFinal(Cipher.DECRYPT_MODE, key, iv, Encryption.base64(ciphertext));
        return new String(decrypted, Charset.forName(UTF8));
    }

    /**
     * Method to encrypt plain text.
     * @param plaintext text to encrypt
     * @return String encrypted text
     */
    public static String encrypt(String plaintext) {
        initializeCipher();
        SecretKey key = generateKey(SALT, PASSPHRASE);
        byte[] encrypted = doFinal(Cipher.ENCRYPT_MODE, key, IV, plaintext.getBytes(Charset.forName(UTF8)));
        return Encryption.base64(encrypted);
    }

    /**
     * Method to decrypt cipher text.
     * @param ciphertext enrypted text
     * @return String decrypted text
     */
    public static String decrypt(String ciphertext) {
        initializeCipher();
        SecretKey key = generateKey(SALT, PASSPHRASE);
        byte[] decrypted = doFinal(Cipher.DECRYPT_MODE, key, IV, Encryption.base64(ciphertext));
        return new String(decrypted, Charset.forName(UTF8));
    }

    /**
     * Final encryption.
     * @maker Richard Go
     * @version 0.01
     * @param encryptMode encrypt mode
     * @param key secret key
     * @param iv initialization vector
     * @param bytes input buffer
     * @return the new buffer with the result
     * @since 0.01
     */
    private static byte[] doFinal(int encryptMode, SecretKey key, String iv, byte[] bytes) {
        try {
            cipher.init(encryptMode, key, new IvParameterSpec(Encryption.hex(iv)));
            return cipher.doFinal(bytes);
        } catch (InvalidKeyException invalidKey) {
            throw fail(invalidKey);
        } catch (InvalidAlgorithmParameterException algoParam) {
            throw fail(algoParam);
        } catch (IllegalBlockSizeException blockSize) {
            throw fail(blockSize);
        } catch (BadPaddingException badPadding) {
            throw fail(badPadding);
        }
    }

    /**
     * Generate secret key.
     * @maker Richard Go
     * @version 0.01
     * @param salt salt string
     * @param passphrase passphrase string
     * @return generated secret key
     * @since 0.01
     */
    private static SecretKey generateKey(String salt, String passphrase) {
        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance(SECRET_KEY_ALGORITHM);
            KeySpec spec = new PBEKeySpec(passphrase.toCharArray(), Encryption.hex(salt), iterationCount, keySize);
            return new SecretKeySpec(factory.generateSecret(spec).getEncoded(), AES_ALGORITHM);
        } catch (NoSuchAlgorithmException noAlgo) {
            throw fail(noAlgo);
        } catch (InvalidKeySpecException keySpec) {
            throw fail(keySpec);
        }
    }

    /**
     * Throws Illegal State Exception with the provided exception.
     * @maker Richard Go
     * @version 0.01
     * @param e exception
     * @return Illegal State Exception
     * @since 0.01
     */
    private static IllegalStateException fail(Exception e) {
        return new IllegalStateException(e);
    }

    /**
     * Initialize cipher to its transformation method.
     * @maker Richard Go
     * @version 0.01
     * @since 0.01
     */
    private static void initializeCipher() {
        try {
            cipher = Cipher.getInstance(CIPHER_TRANSFORMATION_METHOD);
        } catch (NoSuchAlgorithmException ae) {
            throw fail(ae);
        } catch (NoSuchPaddingException pe) {
            throw fail(pe);
        }
    }
}
