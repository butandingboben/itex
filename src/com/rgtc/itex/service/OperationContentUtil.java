/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.service;

import java.util.ArrayList;
import java.util.List;

import com.rgtc.itex.util.Encryption;

/**
 * <div class="jp">
 *
 * </div>
 * <p>
 * <div class="en" style="padding:0em 0.5em" >
 * A Utility that converts Operation Content Data to a comma delimited String.
 * <p>
 * Usage:</br>
 * <code>OperationContentUtil.getNewInstance().add("hello","world").toContentData();</code>
 * <p>
 * The above code yields a string <code>hello=world</code>
 *
 * </div>
 *
 * @author Eldon Leuterio
 * @version 0.01
 * @since 0.01
 */
public class OperationContentUtil {

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em">
	 * 	Constant value for equals (=).
	 * </div>
	 */
	private static final String EQUALS = "=";

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em">
	 * 	Constant value for next line or line break (\n).
	 * </div>
	 */
	private static final String NEXT_LINE = "\n";

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em">
	 * 	Constant value for double quote (").
	 * </div>
	 */
	private static final String DOUBLE_QUOTE = "\"";

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em">
	 * 	Constant value for paired double quote ("").
	 * </div>
	 */
	private static final String PAIRED_DOUBLE_QUOTE = "\"\"";

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em">
	 * 	Constant value for quote (').
	 * </div>
	 */
	private static final String QUOTE = "\'";

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em">
	 * 	Constant value for comma (,).
	 * </div>
	 */
	private static final String COMMA = ",";

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em">
	 * 	the instance of this class.
	 * </div>
	 */
	private static OperationContentUtil instance;

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em">
	 * 	the list of content data.
	 * </div>
	 */
	private List<ContentData> listData = new ArrayList<ContentData>();

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em">
	 * 	private default constructor to discourage instantiation.
	 * </div>
	 */
	private OperationContentUtil(){}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * A non singleton implementation. Always generates a new instance.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @return
	 *		<div class="jp">
	 *
	 *		</div>
	 *		<p>
	 *		<div class="en" style="padding:0em 0.5em">
	 *			the new instance.
	 *		</div>
	 * @since 0.01
	 */
	public static OperationContentUtil getNewInstance(){
		instance = new OperationContentUtil();
		return instance;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * A singleton implementation.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @return
	 *		<div class="jp">
	 *
	 *		</div>
	 *		<p>
	 *		<div class="en" style="padding:0em 0.5em">
	 *			instance
	 *		</div>
	 * @since 0.01
	 */
//	public static OperationContentUtil getInstance(){
//		if(null == instance){
//			instance = new OperationContentUtil();
//		}
//		return instance;
//	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * Adds an operation content entry.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param item
	 * 		<div class="jp">
	 *
	 *		</div>
	 *		<p>
	 *		<div class="en" style="padding:0em 0.5em">
	 *			item name.
	 *		</div>
	 * @param value
	 * 		<div class="jp">
	 *
	 *		</div>
	 *		<p>
	 *		<div class="en" style="padding:0em 0.5em">
	 *			value.
	 *		</div>
	 * @return
	 *		<div class="jp">
	 *
	 *		</div>
	 *		<p>
	 *		<div class="en" style="padding:0em 0.5em">
	 *			the instance.
	 *		</div>
	 * @since 0.01
	 */
	public OperationContentUtil add(String item, String value){
		listData.add(new ContentData(item, value));
		return this;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * Converts the accumulated content data to a comma separated string.
	 * <p>
	 * This comma separated string encloses the value in double quotes if
	 * either quote('), double quote("), next line (\n) or comma(,) is detected.</br>
	 * Furthermore, in the event that a double quote is detected, replaces each
	 * double quote with a pair of double quotes.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @return
	 *		<div class="jp">
	 *
	 *		</div>
	 *		<p>
	 *		<div class="en" style="padding:0em 0.5em">
	 *			the converted String.
	 *		</div>
	 * @since 0.01
	 */
	public String toContentData(){
		StringBuffer sb = new StringBuffer();
		for(int i=0; i<listData.size();i++){
			ContentData cd = listData.get(i);
			sb.append(cd.item);
			sb.append(EQUALS);
			if(null != cd.value){
				//checks if value should be enclosed in quotes
				if (cd.value.contains(COMMA)
						|| cd.value.contains(DOUBLE_QUOTE)
						|| cd.value.contains(NEXT_LINE)) {
					sb.append(DOUBLE_QUOTE);
					//special handling for double quote, replace them with a pair.
					if(cd.value.contains(DOUBLE_QUOTE)){
						sb.append(cd.value.replace(DOUBLE_QUOTE, PAIRED_DOUBLE_QUOTE));
					}else{
						sb.append(cd.value);
					}
					sb.append(DOUBLE_QUOTE);
				}else{
					sb.append(cd.value);
				}
			}

			//adds comma if not end of list
			if(i<listData.size()-1){
				sb.append(COMMA);
			}
		}
		return sb.toString();
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * Encrypts the content data.
	 * <p>
	 * Encryption is done via base 64 hash.</br>
	 * In the event that an error occurs while encrypting, the default un-encrypted data will be returned.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param secretKey
	 * 		<div class="jp">
	 *
	 *		</div>
	 *		<p>
	 *		<div class="en" style="padding:0em 0.5em">
	 *			the secretKey.
	 *		</div>
	 * @return
	 *		<div class="jp">
	 *
	 *		</div>
	 *		<p>
	 *		<div class="en" style="padding:0em 0.5em">
	 *			the encrypted data.
	 *		</div>
	 * @since 0.01
	 * @see Encryption#encrpytedHash(String, String)
	 */
	public String toEncryptedContentData(String secretKey){
		String encryptedMessage = null;
		String message = toContentData();
		System.out.println(message);

		//encryption
		try {
			encryptedMessage = Encryption.encrpytedHash(secretKey, message);
		} catch (Exception e) {
		}

		return encryptedMessage;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * Holds content Data.
	 * </div>
	 *
	 * @author Eldon Leuterio
	 * @version 0.01
	 * @since 0.01
	 */
	private class ContentData{

		/**
		 * <div class="jp">
		 *
		 * </div>
		 * <p>
		 * <div class="en" style="padding:0em 0.5em" >
		 * Default constructor
		 * </div>
		 * @maker Eldon Leuterio
		 * @version 0.01
		 * @param item
		 * 		<div class="jp">
		 *
		 *		</div>
		 *		<p>
		 *		<div class="en" style="padding:0em 0.5em">
		 *			item name.
		 *		</div>
		 * @param value
		 *		<div class="jp">
		 *
		 *		</div>
		 *		<p>
		 *		<div class="en" style="padding:0em 0.5em">
		 *			item value.
		 *		</div>
		 * @since 0.01
		 */
		public ContentData(String item, String value){
			this.item = item;
			this.value = value;
		}


		/**
		 * <div class="jp">
		 *
		 * </div>
		 * <div class="en" style="padding:0em 0.5em">
		 *	Item name.
		 * </div>
		 */
		private String item;

		/**
		 * <div class="jp">
		 *
		 * </div>
		 * <div class="en" style="padding:0em 0.5em">
		 *	Item value.
		 * </div>
		 */
		private String value;
	}
}
