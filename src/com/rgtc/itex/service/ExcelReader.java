/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) RococoGlobal Technologies Corporation - All Rights Reserved 2013
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.service;

import static com.rgtc.itex.util.UtilConstants.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.rgtc.itex.models.OperationColumnModel;
import com.rgtc.itex.models.OperationModel;
import com.rgtc.itex.models.TableColumnModel;
import com.rgtc.itex.models.TableModel;
import com.rgtc.itex.util.Converter;

/**
 * This class handles [READING] functions for Excel files.
 * 
 * @author Eldon Ivan
 * @since 12/03/2013
 * @version 1
 */
public class ExcelReader {
	private List<TableModel> tmList;
	private OperationModel selectOm;
	private OperationModel updateOm;
	private OperationModel deleteOm;

	/**
	 * This method reads the Excel file.
	 * 
	 * @param file
	 * @return List<DataScriptModel>
	 * @throws InvalidFormatException
	 * @throws IOException
	 */
	public void readExcel(File file) throws InvalidFormatException, IOException {
		Workbook wb = WorkbookFactory.create(file);
		List<TableModel> tmList = new ArrayList<TableModel>();
		
		for (int i = 0; i < wb.getNumberOfSheets(); i++) {
			Sheet sheet = wb.getSheetAt(i);
			
			if (!(SELECT.equals(sheet.getSheetName())
					|| UPDATE.equals(sheet.getSheetName())
					|| DELETE.equals(sheet.getSheetName()))) {
				tmList.add(getColumnPropertiesForTable(sheet));
			}
		}
		
		this.tmList = tmList;
		this.selectOm = getColumnPropertiesForOperation(wb.getSheet(SELECT));
		this.updateOm = getColumnPropertiesForOperation(wb.getSheet(UPDATE));
		this.deleteOm = getColumnPropertiesForOperation(wb.getSheet(DELETE));
	}
	
	/**
	 * This method defines the column properties.
	 * 1st row = column name.
	 * 2nd row = column type.
	 * 3nd row = column constraints.
	 * 
	 * @param sheet
	 * @return DataScriptModel
	 */
	public TableModel getColumnPropertiesForTable(Sheet sheet) {
		TableModel tm = new TableModel();
		List<TableColumnModel> tcmList = new ArrayList<TableColumnModel>();

		Row nameRow = sheet.getRow(0);
		Row typeRow = sheet.getRow(1);
		Row constraintRow = sheet.getRow(2);
		Row constraintTableRow = sheet.getRow(3);
		Row constraintKeyRow = sheet.getRow(4);
		
		tm.setTableName(sheet.getSheetName());		
		tm.setColCount(nameRow.getPhysicalNumberOfCells());
		tm.setRowCount(sheet.getLastRowNum() - HEADER_COUNT_TABLE);
		
		for (int row = HEADER_COUNT_TABLE; row <= sheet.getLastRowNum(); row++) {
			if (null != nameRow && null != typeRow) {
				for (int i = 0; i < nameRow.getPhysicalNumberOfCells(); i++) {
					TableColumnModel tcm = new TableColumnModel();
					Cell nameCell = nameRow.getCell(i);
					Cell typeCell = typeRow.getCell(i);
					Cell constraintCell = constraintRow.getCell(i, Row.CREATE_NULL_AS_BLANK);
					Cell constraintTableCell = constraintTableRow.getCell(i, Row.CREATE_NULL_AS_BLANK);
					Cell constraintKeyCell = constraintKeyRow.getCell(i, Row.CREATE_NULL_AS_BLANK);					
					Cell dataCell = sheet.getRow(row).getCell(i, Row.CREATE_NULL_AS_BLANK);

					if (null != nameCell && null != typeCell) {
						tcm.setName(nameCell.getStringCellValue());
						tcm.setType(typeCell.getStringCellValue());
//						tcm.setConstraint(constraintCell.getStringCellValue());
//						tcm.setConstraintTable(constraintTableCell.getStringCellValue());
//						tcm.setConstraintKey(constraintKeyCell.getStringCellValue());

						switch (dataCell.getCellType()) {
						case Cell.CELL_TYPE_STRING:
							if (SYSDATE.equalsIgnoreCase(dataCell.getStringCellValue())) {
								tcm.setData(dataCell.getStringCellValue());
							} else {
								tcm.setData(Converter.toInsertString(dataCell.getStringCellValue()));
							}
							break;					
						case Cell.CELL_TYPE_NUMERIC:
							if (VARCHAR.equalsIgnoreCase(tcm.getType()) || CHAR.equalsIgnoreCase(tcm.getType())) {
								Double d = dataCell.getNumericCellValue();
								tcm.setData(Converter.toInsertString(String.valueOf(d.intValue())));
							} else if (DateUtil.isCellDateFormatted(dataCell)) {
								if (DATE.equalsIgnoreCase(tcm.getType())) {
									tcm.setData(Converter.dateToString(dataCell.getDateCellValue()));
								} else if (TIMESTAMP.equalsIgnoreCase(tcm.getType())) {
									tcm.setData(Converter.dateToTimestampString(dataCell.getDateCellValue()));
								} else {
									tcm.setData(NULL);
								}								
							} else {
								if (INTEGER.equalsIgnoreCase(tcm.getType())) {
									Double d = dataCell.getNumericCellValue();
									tcm.setData(String.valueOf(d.intValue()));
								} else {
									tcm.setData(String.valueOf(dataCell.getNumericCellValue()));
								}								
							}
							break;
						case Cell.CELL_TYPE_BOOLEAN:
							tcm.setData(String.valueOf(dataCell.getBooleanCellValue()));
							break;
						case Cell.CELL_TYPE_BLANK:
							if (VARCHAR.equalsIgnoreCase(tcm.getType())) {
								tcm.setData(DEFAULT_VARCHAR);
							} else if (CHAR.equalsIgnoreCase(tcm.getType())) {
								tcm.setData(DEFAULT_CHAR);
							} else if (INTEGER.equalsIgnoreCase(tcm.getType())) {
								tcm.setData(DEFAULT_INTEGER);
							} else if (DOUBLE.equalsIgnoreCase(tcm.getType())) {
								tcm.setData(DEFAULT_DOUBLE);
							} else if (FLOAT.equalsIgnoreCase(tcm.getType())) {
								tcm.setData(DEFAULT_FLOAT);
							} else if (DATE.equalsIgnoreCase(tcm.getType())) {
								tcm.setData(DEFAULT_DATE);
							} else if (TIMESTAMP.equalsIgnoreCase(tcm.getType())) {
								tcm.setData(DEFAULT_TIMESTAMP);
							} else {
								tcm.setData(NULL);
							}
							break;
						}
						tcmList.add(tcm);
					}
				}
				tm.setColModelList(tcmList);
			}		
		}
		return tm;
	}
	
	public OperationModel getColumnPropertiesForOperation(Sheet sheet) {		
		OperationModel om = new OperationModel();
		List<OperationColumnModel> ocmList = new ArrayList<OperationColumnModel>();
		
		Row tableRow = sheet.getRow(0);
		
		om.setTableName(sheet.getSheetName());		
		om.setColCount(tableRow.getPhysicalNumberOfCells());
		om.setRowCount(sheet.getLastRowNum() - HEADER_COUNT_OPERATION);
		
		for (int row = HEADER_COUNT_OPERATION; row <= sheet.getLastRowNum(); row++) {
			if (null != tableRow) {
				for (int i = 0; i < tableRow.getPhysicalNumberOfCells(); i++) {
					OperationColumnModel ocm = new OperationColumnModel();
					Cell tableCell = tableRow.getCell(i);
					Cell dataCell = sheet.getRow(row).getCell(i, Row.CREATE_NULL_AS_BLANK);
					
					if (null != tableCell) {
						ocm.setName(tableCell.getStringCellValue());
						
						Double d = dataCell.getNumericCellValue();
						ocm.setId(String.valueOf(d.intValue()));
						
						ocmList.add(ocm);
					}
				}
			}
			om.setColModelList(ocmList);
		}
		return om;
	}
}
