/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.service.nikkan;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.common.base.Throwables;
import com.rgtc.itex.models.AssertionOutputModel;
import com.rgtc.itex.service.CSVReader;
import com.rgtc.itex.util.UtilConstants;
import com.rgtc.itex.util.ExcelService;

/**
 * Class for handling the format of the output CSV
 * @author Joven 'Bob' Montero
 * @version 0.01
 * @since 0.01
 * 
 * Date			Author			Tag						Remarks
 * 2015/05/29	Nick Faelnar	[2015/05/29 - Nick]		Added errors in analysis. 
 */
public class CSVDataService {

	private final int START_COL = 5;
	private final int LINE_COL = 6;
	private final int COUNT_COL = 61;
	private final int TITLE_COL = 26;
	private final int FORMAT_COL = 35;

	/**
	 * The file containing the test specifications
	 */
	private File testFile;
	
	private Workbook testWorkbook;
	
	/**
	 * The test case name to be tested
	 */
	private String testCaseName;
	/**
	 * The ExcelService instance
	 */
	private ExcelService excelService;
	/**
	 * The list containing the specified format of each column in every table found
	 */
	private List<JSONObject> formatList;
	/**
	 * The list containing the specified format of each column in every table found
	 */
	private List<List<String>> expectedList;
	/**
	 * The list containing the specified format of each column in every table found
	 */
	private List<JSONObject> failureList;
	
	public CSVDataService(File testFile, String testCaseName) {
		this.testFile = testFile;
		this.testCaseName = testCaseName;
		excelService = new ExcelService();
		excelService.loadFile(testFile);
		formatList = new ArrayList<JSONObject>();
		expectedList = new ArrayList<List<String>>();
		setFailureList(new ArrayList<JSONObject>());
		populateFormatList();
		populateExpectedList();
	}
	
	public CSVDataService(Workbook testWorkbook, String testCaseName) {
		this.testWorkbook = testWorkbook;
		this.testCaseName = testCaseName;
		excelService = new ExcelService();
		excelService.loadWorkbook(testWorkbook);
		formatList = new ArrayList<JSONObject>();
		expectedList = new ArrayList<List<String>>();
		setFailureList(new ArrayList<JSONObject>());
		populateFormatList();
		populateExpectedList();
	}
	
	/**
	 * Function for getting the expected CSV data
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @since 0.01
	 */
	private void populateExpectedList() {
		// Get the sheet containing the file name format
		Sheet csvDataSheet = excelService.getSheet(UtilConstants.CSV_DATA_CHECK_SHEET);

		// First loop: loop for the entire sheet starting in cell G6 
		for (int i = 1; i <= csvDataSheet.getLastRowNum(); i++) {
			Row row = csvDataSheet.getRow(i);

			try {
				if (testCaseName.equals(row.getCell(2).getStringCellValue())) {
					List<String> list = new ArrayList<String>();;
					for (int j = i + 1; j <= csvDataSheet.getLastRowNum(); j++) {
						String cellValue;
						try {
//							System.out.println("ROW: "+j);
							cellValue = csvDataSheet.getRow(j).getCell(1).getStringCellValue().toString();
						} catch (NullPointerException e) {
							cellValue = "";
						}
						
						if (UtilConstants.TEST_FILE_TAG.toLowerCase().equals(cellValue.toLowerCase())) {
							if (0 < list.size()) {
								expectedList.add(list);
								list = new ArrayList<String>();
							}
						} else if (UtilConstants.TEST_CASE_END.toLowerCase().equals(cellValue.toLowerCase())) {
							expectedList.add(list);
							i = csvDataSheet.getLastRowNum() + 1;
							break;
						} else {
							list.add(cellValue);
						}
					}
				} else {
					continue;
				}
			} catch (NullPointerException e) {
				
				continue;
			}
		}
	}

	/**
	 * Function for getting all formats from test file
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @since 0.01
	 */
	private void populateFormatList() {
		// Get the sheet containing the file name format
		Sheet formatSheet = excelService.getSheet(UtilConstants.FORMAT_CHECK_SHEET);

		// First loop: loop for the entire sheet starting in cell G6 
		for (int i = START_COL; i <= formatSheet.getLastRowNum(); i++) {
			Row row = formatSheet.getRow(i);
			Map<String,Object> record = new HashMap<String, Object>();

			try {
				// Column G value
				record.put("line", row.getCell(LINE_COL).getStringCellValue());
				if (Cell.CELL_TYPE_STRING == row.getCell(COUNT_COL).getCellType()) {
					// Column BJ value
					record.put("count", row.getCell(COUNT_COL).getStringCellValue());
				} else if (Cell.CELL_TYPE_NUMERIC == row.getCell(COUNT_COL).getCellType()) {
					// Column BJ value
					record.put("count", (int) row.getCell(COUNT_COL).getNumericCellValue());
				}
				
				List<String> titles = new ArrayList<String>();
				List<String> formats = new ArrayList<String>();
				int colCnt = 0;
				// Second loop: loop for getting all the titles and formats of the current line
				for (int j = i + 1; j <= formatSheet.getLastRowNum(); j++) {
					//System.out.println("[DEBUG] row " + j);
					//System.out.println("[DEBUG] column " + i);
					Row nextRow = formatSheet.getRow(j);
					if (0 == nextRow.getCell(TITLE_COL).getStringCellValue().trim().length()) {
						i = j - 1;
						break;
					}
					
					// Column AA value
					String title = nextRow.getCell(TITLE_COL).getStringCellValue().trim();
					// Column AJ value
					String format = nextRow.getCell(FORMAT_COL).getStringCellValue().trim();
					titles.add(title);
					formats.add(format);
					colCnt++;
				}
				record.put("titles", titles);
				record.put("formats", formats);
				record.put("colCnt", colCnt);
				record.put("actualCnt", 0);
				getFormatList().add(new JSONObject(record));
			} catch (NullPointerException e) {
				//System.out.println("[DEBUG] " + e.getMessage());
				//System.out.println("[DEBUG] column " + i);
				continue;
			} catch (IllegalStateException e) {
				//System.out.println("[DEBUG] " + e.getMessage());
				//System.out.println("[DEBUG] column " + i);
				continue;
			}
		}
	}

	/**
	 * Function for identifying if the current line contains titles or data.
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param string
	 * @return boolean
	 * @since 0.01
	 */
	private JSONObject identifyLine(String line) {
		String[] actualLine = line.split(",", -1);
		List<JSONObject> match = new ArrayList<JSONObject>();
		
		for (JSONObject jsonObject : formatList) {
			//System.out.println("[DEBUG] formatList item: " + jsonObject.toString(5));
//			if ("E".equals(jsonObject.get("count").toString())) {
//				break;
//			}
			String title = jsonObject.getJSONArray("titles").get(0).toString();
			// Handle line with delimiters
			if (title.contains("(")) {
				title = title.split("\\(")[0];
			}
			if (actualLine[0].equals(title)) {
				match.add(jsonObject);
			}
		}
		if (1 == match.size()) {
			return match.get(0);
		} else {
			for (JSONObject record : match) {
				if (actualLine.length == record.getInt("colCnt")) {
					return record;
				}
			}
		}

		return null;
	}

	/**
	 * @return the formatList
	 */
	public List<JSONObject> getFormatList() {
		return formatList;
	}

	/**
	 * @param formatList the formatList to set
	 */
	public void setFormatList(List<JSONObject> formatList) {
		this.formatList = formatList;
	}

	/**
	 * @return the expectedList
	 */
	public List<List<String>> getExpectedList() {
		return expectedList;
	}

	/**
	 * @param expectedList the expectedList to set
	 */
	public void setExpectedList(List<List<String>> expectedList) {
		this.expectedList = expectedList;
	}

	/**
	 * Function for validating the data of the CSV files in a list
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param outputCSVFiles
	 * @return AssertionOutputModel
	 * @since 0.01
	 */
	public AssertionOutputModel validateData(File[] outputCSVFiles) {
		AssertionOutputModel output = new AssertionOutputModel();
		StringBuffer errorMessage = new StringBuffer();
		errorMessage.append("CSV Data Check:");
		
		if (0 == expectedList.size()) {
			errorMessage.append("\n\n" + "Final Result:" + "\t" + "NA");
			errorMessage.append("\n" + "Cause:" + "\n\t" + "No expected data found for " + testCaseName);
			output.setIsError(false);
			output.setError(errorMessage.toString());
			return output;
		} else if (1 == expectedList.size()) {
			if (expectedList.get(0).get(0).equals(UtilConstants.NO_OUTPUT_CSV)) {
				if (0 < outputCSVFiles.length) {
					// ADD [2015/05/29 - Nick] START 
					JSONObject failure = new JSONObject();
					failure.put("testCaseName", testCaseName);
					failure.put("line", "");
					failure.put("col", "");
					failure.put("colNo", -1);
					failure.put("rowNo", -1);
					failure.put("cause", "The number of expected files(0) and actual files(" + outputCSVFiles.length + ") are not equal.");
					failure.put("actualData", "");
					failure.put("expectedData", "");
					getFailureList().add(failure);
					// ADD [2015/05/29 - Nick] END
					
					errorMessage.append("\n\n" + "Final Result:" + "\t" + "NG");
					errorMessage.append("\n" + "Cause:" + "\n\t" + "The number of expected files(0) and actual files(" + outputCSVFiles.length + ") are not equal.");
					output.setIsError(true);
					output.setError(errorMessage.toString());
					return output;
				}
				errorMessage.append("\n\n" + "Final Result:" + "\t" + "OK");
				errorMessage.append("\n" + "Remark:" + "\n\t" + "No expected CSV output.");
				output.setIsError(false);
				output.setError(errorMessage.toString());
				
				return output;
			} else if (outputCSVFiles.length != expectedList.size()) {
				// ADD [2015/05/29 - Nick] START 
				JSONObject failure = new JSONObject();
				failure.put("testCaseName", testCaseName);
				failure.put("line", "");
				failure.put("col", "");
				failure.put("colNo", -1);
				failure.put("rowNo", -1);
				failure.put("cause", "The number of expected files(1) and actual files(" + outputCSVFiles.length + ") are not equal.");
				failure.put("actualData", "");
				failure.put("expectedData", "");
				getFailureList().add(failure);
				// ADD [2015/05/29 - Nick] END
				
				errorMessage.append("\n\n" + "Final Result:" + "\t" + "NG");
				errorMessage.append("\n" + "Cause:" + "\n\t" + "The number of expected files(" + expectedList.size() + ") and actual files(" + outputCSVFiles.length + ") are not equal.");
				output.setIsError(true);
				output.setError(errorMessage.toString());
				return output;
			}
		}
		if (outputCSVFiles.length != expectedList.size()) {
			// ADD [2015/05/29 - Nick] START 
			JSONObject failure = new JSONObject();
			failure.put("testCaseName", testCaseName);
			failure.put("line", "");
			failure.put("col", "");
			failure.put("colNo", -1);
			failure.put("rowNo", -1);
			failure.put("cause", "The number of expected files(" + expectedList.size() + ") and actual files(" + outputCSVFiles.length + ") are not equal.");
			failure.put("actualData", "");
			failure.put("expectedData", "");
			getFailureList().add(failure);
			// ADD [2015/05/29 - Nick] END
			
			errorMessage.append("\n\n" + "Final Result:" + "\t" + "NG");
			errorMessage.append("\n" + "Cause:" + "\n\t" + "The number of expected files(" + expectedList.size() + ") and actual files(" + outputCSVFiles.length + ") are not equal.");
			output.setIsError(true);
			output.setError(errorMessage.toString());
			return output;
		}
	
		boolean validationFlag = true;
		for (int i = 0; i < outputCSVFiles.length; i++) {
			File csvFile = outputCSVFiles[i];
			if (!csvFile.exists()) {
				validationFlag = false;
				String cause = "File not found.";
				errorMessage.append("\n\n\t" + "File No.:" + "\t" + (i + 1));
				errorMessage.append("\n\t" + "File Name:" + "\t\t" +  csvFile.getName());
				errorMessage.append("\n\t" + "Result: " + "\t" + "NG");
				errorMessage.append("\n\t\t" + "Cause: " + "\t\t" + cause);
				continue;
			}
			List<String> expectedDataList = expectedList.get(i);
			String cause = "";
			try {
				List<String> readLines = CSVReader.readCSV(csvFile);
				boolean perFileFlag = true;
				JSONObject prevRecord = new JSONObject();
				int backtrackCnt = 0;
				int backtrackExpectedCnt = 0;
				// Validate the CSV data with expected data
//				System.out.println("[DEBUG HERE] Start DEBUG");
//				System.out.println("[DEBUG HERE] ReadLine Size: " + readLines.size());
				for (int j = 0; j < readLines.size() && j < expectedDataList.size(); j++) {
//					errorMessage.append("\n----------------------\n" + readLines.get(j - backtrackCnt) + "\n----------------------\n");
//					if ("蝨ｰ譁ｹ遶ｶ鬥ｬ蜈ｨ謌千ｸｾ陦ｨ".equals(readLines.get(j - backtrackCnt)) ||
//							"縲宣幕蛯ｬ蝨ｰ諠�蝣ｱ縲�".equals(readLines.get(j - backtrackCnt)) ||
//							"縲舌Ξ繝ｼ繧ｹ諠�蝣ｱ縲�".equals(readLines.get(j - backtrackCnt))) {
//						continue;
//					}
//					System.out.println("[DEBUG HERE] 1 J: " + j);
//					System.out.println("[DEBUG HERE] 1 backtrackCnt: " + backtrackCnt);
//					System.out.println("[DEBUG HERE] 1 backtrackExpectedCnt: " + backtrackExpectedCnt);
					JSONObject failure;
					String[] actualLine = readLines.get(j - backtrackCnt).split(",", -1);
					String[] expectedLine = expectedDataList.get(j - backtrackExpectedCnt).split(",", -1);

					JSONObject actualRecord = identifyLine(readLines.get(j - backtrackCnt));
					JSONObject expectedRecord = identifyLine(expectedDataList.get(j - backtrackExpectedCnt));
					
//					System.out.println("[DEBUG HERE] actualRecord: " + actualRecord);
//					System.out.println("[DEBUG HERE] expectedRecord: " + expectedRecord);
					
					if (null != actualRecord) {
						prevRecord = actualRecord;
					}
					if (actualRecord != null && expectedRecord == null) {
//						System.out.println("[DEBUG HERE] actualRecord != null && expectedRecord == null");
						actualLine = new String[0];
						backtrackCnt = backtrackCnt + 1;
					}
//					System.out.println("[DEBUG HERE] 2 backtrackCnt: " + backtrackCnt);
					if (actualRecord == null && expectedRecord != null) {
//						System.out.println("actualRecord == null && expectedRecord != null");
						expectedLine = new String[0];
						backtrackExpectedCnt = backtrackExpectedCnt + 1;
					}
//					System.out.println("[DEBUG HERE] 2 backtrackExpectedCnt: " + backtrackExpectedCnt);
					if (!prevRecord.has("line")) {
						prevRecord.put("line", "\"" + (j + 1) + "\"");
					}

					if (0 == actualLine.length) {
						// ADD [2015/05/29 - Nick] START 
						JSONObject failure1 = new JSONObject();
						failure1.put("testCaseName", testCaseName);
						failure1.put("line", "");
						failure1.put("col", "");
						failure1.put("colNo", -1);
						failure1.put("rowNo", (j + 1));
						failure1.put("cause", "No actual data found for this line.");
						failure1.put("actualData", "");
						failure1.put("expectedData", "");
						getFailureList().add(failure1);
						// ADD [2015/05/29 - Nick] END
						
						validationFlag = false;
						perFileFlag = false;
						cause = "No actual data found for this line.";
						errorMessage.append("\n\n\t" + "File No.:" + "\t" + (i + 1));
						errorMessage.append("\n\t" + "File Name:" + "\t\t" +  csvFile.getName());
						errorMessage.append("\n\t" + "Result: " + "\t" + "NG");
						errorMessage.append("\n\t\t" + "Row:" + "\t\t" + (j + 1));
						errorMessage.append("\n\t\t" + "Cause: " + "\t\t" + cause);
						continue;
//						break;
					} else if (0 == expectedLine.length) {
						// ADD [2015/05/29 - Nick] START 
						JSONObject failure1 = new JSONObject();
						failure1.put("testCaseName", testCaseName);
						failure1.put("line", "");
						failure1.put("col", "");
						failure1.put("colNo", -1);
						failure1.put("rowNo", (j + 1));
						failure1.put("cause", "Expected data count exceeded.");
						failure1.put("actualData", "");
						failure1.put("expectedData", "");
						getFailureList().add(failure1);
						// ADD [2015/05/29 - Nick] END
						
						validationFlag = false;
						perFileFlag = false;
						cause = "Expected data count exceeded.";
						errorMessage.append("\n\n\t" + "File No.:" + "\t" + (i + 1));
						errorMessage.append("\n\t" + "File Name:" + "\t\t" +  csvFile.getName());
						errorMessage.append("\n\t" + "Result: " + "\t" + "NG");
						errorMessage.append("\n\t\t" + "Row:" + "\t\t" + (j + 1));
						errorMessage.append("\n\t\t" + "Cause: " + "\t\t" + cause);
						continue;
					}

					if (expectedLine.length != actualLine.length) {
						// ADD [2015/05/29 - Nick] START 
						JSONObject failure1 = new JSONObject();
						failure1.put("testCaseName", testCaseName);
						failure1.put("line", "");
						failure1.put("col", "");
						failure1.put("colNo", -1);
						failure1.put("rowNo", (j + 1));
						failure1.put("cause", "The actual column count did not match the expected column count.");
						failure1.put("actualData", "");
						failure1.put("expectedData", "");
						getFailureList().add(failure1);
						// ADD [2015/05/29 - Nick] END
						
						validationFlag = false;
						perFileFlag = false;
						cause = "The actual column count did not match the expected column count.";
						errorMessage.append("\n\n\t" + "File No.:" + "\t" + (i + 1));
						errorMessage.append("\n\t" + "File Name:" + "\t\t" +  csvFile.getName());
						errorMessage.append("\n\t" + "Result: " + "\t" + "NG");
						errorMessage.append("\n\t\t" + "Row:" + "\t\t" + (j + 1));
						errorMessage.append("\n\t\t" + "Cause: " + cause);
						errorMessage.append("\n\t\t\t" + "Actual Count:" + "\t" + actualLine.length);
						errorMessage.append("\n\t\t\t" + "Expected Count:" + "\t" + expectedLine.length);
//						continue;
//						break;
					}

					int maxColCnt = 0;
					if (actualLine.length < expectedLine.length) {
						maxColCnt = expectedLine.length;
					} else {
						maxColCnt = actualLine.length;
					}
					for (int k = 0; k < maxColCnt; k++) {
						boolean lineCheck = true;
						String actualData;
						try {
							actualData = actualLine[k];
						} catch (IndexOutOfBoundsException e) {
							actualData = "<None>";
						}
						String expectedData;
						try {
							expectedData = expectedLine[k];
						} catch (IndexOutOfBoundsException e) {
							expectedData = "<None>";
						}
						// TODO Remove this bypass
//						if (1 == j && 11 == k) {
//							continue;
//						}
						if (expectedData.contains(UtilConstants.DYNAMIC_TAG)
								&& 0 >= actualData.length()) {
							lineCheck = false;
							perFileFlag = false;
							cause = "Actual data must not be empty for the specified column.";
						} else if (expectedData.contains(UtilConstants.DYNAMIC_TAG)
								&& 0 < actualData.length()) {
//							validationFlag = true;
							continue;
						} else if (!expectedData.equals(actualData)) {
							lineCheck = false;
							perFileFlag = false;
							cause = "The actual data did not match the expected data.";
						}
						if (!lineCheck) {
							errorMessage.append("\n\n\t" + "File No.:" + "\t" + (i + 1));
							errorMessage.append("\n\t" + "File Name:" + "\t" +  csvFile.getName());
							errorMessage.append("\n\t" + "Result: " + "\t" + "NG");
							errorMessage.append("\n\t\t" + "Row:" + "\t\t" + (j + 1));
							errorMessage.append("\n\t\t" + "Column:" + "\t\t" + (k + 1));
							errorMessage.append("\n\t\t" + "Cause: " + cause);
							errorMessage.append("\n\t\t\t" + "Actual:" + "\t\t" + actualData);
							errorMessage.append("\n\t\t\t" + "Expected:" + "\t" + expectedData);

							// Failure compilation
							// {
							//		line: "",
							//		col: "",
							//		colNo: "",
							//		rowNo: "",
							//		cause: "",
							//		actualData: "",
							//		expectedData: ""
							// }
							failure = new JSONObject();
							failure.put("testCaseName", testCaseName);
							if (null != actualRecord) {
								failure.put("line", actualRecord.get("line".toString()));
								// MOD [2015/05/29 - Nick] START
								// failure.put("col", actualRecord.getJSONArray("titles").get(k));
								try {
									failure.put("col", actualRecord.getJSONArray("titles").get(k));
								} catch (Exception e) {
									failure.put("col", "**NO HEADER**");
								}
								// MOD [2015/05/29 - Nick] END
							} else {
								failure.put("line", prevRecord.get("line".toString()));
								// MOD [2015/05/29 - Nick] START
								// failure.put("col", prevRecord.getJSONArray("titles").get(k));
								try {
									failure.put("col", prevRecord.getJSONArray("titles").get(k));
								} catch (Exception e) {
									failure.put("col", "**NO HEADER**");
								}
								// MOD [2015/05/29 - Nick] END
							}
							failure.put("colNo", (k + 1));
							failure.put("rowNo", (j + 1));
							failure.put("cause", cause);
							failure.put("actualData", actualData);
							failure.put("expectedData", expectedData);
							getFailureList().add(failure);
//							break;
						}
						if (!lineCheck && validationFlag) {
							validationFlag = false;;
						}
					}
				}
				
				if (readLines.size() != expectedDataList.size()) {
					JSONObject failure1 = new JSONObject();
					failure1.put("testCaseName", testCaseName);
					failure1.put("line", "");
					failure1.put("col", "");
					failure1.put("colNo", -1);
					failure1.put("rowNo", (readLines.size() + 1));
					failure1.put("cause", "The actual no. of lines does not match the expected no. of lines.");
					failure1.put("actualData", Integer.toString(readLines.size()));
					failure1.put("expectedData", Integer.toString(expectedDataList.size()));
					getFailureList().add(failure1);
					
					validationFlag = false;
					perFileFlag = false;
					cause = "The actual no. of lines does not match the expected no. of lines.";
					errorMessage.append("\n\n\t" + "File No.:" + "\t" + (i + 1));
					errorMessage.append("\n\t" + "File Name:" + "\t\t" +  csvFile.getName());
					errorMessage.append("\n\t" + "Result: " + "\t" + "NG");
					errorMessage.append("\n\t\t" + "Row:" + "\t\t" + (readLines.size() + 1));
					errorMessage.append("\n\t\t" + "Cause: " + cause);
					errorMessage.append("\n\t\t\t" + "Actual Count:" + "\t" + Integer.toString(readLines.size()));
					errorMessage.append("\n\t\t\t" + "Expected Count:" + "\t" + Integer.toString(expectedDataList.size()));
				}
				
				if (perFileFlag) {
					errorMessage.append("\n\n\t" + "File No.:" + "\t" + (i + 1));
					errorMessage.append("\n\t" + "File Name:" + "\t" +  csvFile.getName());
					errorMessage.append("\n\t" + "Result: " + "\t" + "OK");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		errorMessage.append("\n\n" + "Final Result:" + "\t");
		if (!validationFlag) {
			errorMessage.append("NG");
		} else {
			errorMessage.append("OK");
		}
		output.setIsError(!validationFlag);
		output.setError(errorMessage.toString());

		return output;
	}

	/**
	 * Function for validating the format of the data in the CSV files
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param outputCSVFiles
	 * @return AssertionOutputModel
	 * @since 0.01
	 */
	public AssertionOutputModel validateFormat(File[] outputCSVFiles) {
		AssertionOutputModel output = new AssertionOutputModel();
		StringBuffer errorMessage = new StringBuffer();
		StringBuffer failedValidations = new StringBuffer();
		errorMessage.append("CSV Format Check:" + "\n");

//		Workbook analysisWorkbook = new XSSFWorkbook();
//		CreationHelper creationHelper = analysisWorkbook.getCreationHelper();
//        Sheet sheet = analysisWorkbook.createSheet("Analysis");
//        Row actualDataRow = sheet.createRow(0);
//        Row expectedDataRow = sheet.createRow(1);

		if (0 == expectedList.size()) {
			errorMessage.append("\n\n" + "Final Result:" + "\t" + "NA");
			errorMessage.append("\n" + "Cause:" + "\t\t\t" + "No expected data found for " + testCaseName);
			output.setIsError(false);
			output.setError(errorMessage.toString());
			return output;
		}
		
		
		boolean validationFlag = true;
		for (int i = 0; i < outputCSVFiles.length; i++) {
			File csvFile = outputCSVFiles[i];
			if (!csvFile.exists()) {
				validationFlag = false;
				String cause = "File not found.";
				errorMessage.append("\n\n\t" + "File No.:" + "\t" + (i + 1));
				errorMessage.append("\n\t" + "File Name:" + "\t\t" +  csvFile.getName());
				errorMessage.append("\n\t" + "Result: " + "\t" + "NG");
				errorMessage.append("\n\t\t" + "Cause: " + "\t\t" + cause);
				continue;
			}
			try {
				errorMessage.append("\t" + "File No.:" + "\t" + (i + 1) + "\n");
				errorMessage.append("\t" + "File Name:" + "\t" + csvFile.getName() + "\n");

				List<String> readLines = CSVReader.readCSV(csvFile);
				JSONObject prevRecord = new JSONObject();
				prevRecord.put("actualCnt", 0);
				StringBuffer partialOutput = new StringBuffer();
				for (int j = 0; j < readLines.size(); j++) {
					JSONObject record = identifyLine(readLines.get(j));
//					failedValidations.append("\n" + "======================================\n" + 
//							readLines.get(j) + "-" + record + "\n======================================\n");
					if (null != record) {
//						if ("蝨ｰ譁ｹ遶ｶ鬥ｬ蜈ｨ謌千ｸｾ陦ｨ".equals(readLines.get(j)) ||
//								"縲宣幕蛯ｬ蝨ｰ諠�蝣ｱ縲�".equals(readLines.get(j)) ||
//								"縲舌Ξ繝ｼ繧ｹ諠�蝣ｱ縲�".equals(readLines.get(j))) {
//							continue;
//						}
						record.put("actualCnt", 1);
						prevRecord = record;
						j = j + 1;

						// Get the expected line count
						// When the expected line count is 'N',
						// 		the interger's max value is returned
						int recordCnt = 1;
						try {
							recordCnt = record.getInt("count");
						} catch (Exception e) {
							if ("E".equals(record.getString("count"))) {
								break;
							}
							recordCnt = Integer.MAX_VALUE;
						}

						// Checks the total number of line for the current record
						// If there are no lines found, the validation fails for
						//		the current line then proceeds to the next record
						// Else if, checks if the total line count exceeds the expected count
//						failedValidations.append(recordCnt + "-" + readLines.get(j) + "\n" + identifyLine(readLines.get(j)) + "\n");
						if (null != identifyLine(readLines.get(j))) {
							j = j - 1;
							validationFlag = false;
							failedValidations.append("\t" + "File No.:" + "\t\t" + (i + 1) + "\n");
							failedValidations.append("\t" + "File Name.:" + "\t" + csvFile.getName() + "\n");
							failedValidations.append("\t\t" + "Record:" + "\t" + record.getString("line") + "\n");
							failedValidations.append("\t\t\t" + "No line found." + "\n");
							continue;
						} else if (lineCount(j, readLines) > recordCnt) {
							int actualLineCnt = lineCount(j, readLines);
							failedValidations.append("\t" + "File No.:" + "\t\t" + (i + 1) + "\n");
							failedValidations.append("\t" + "File Name.:" + "\t" + csvFile.getName() + "\n");
							failedValidations.append("\t\t" + "Record:" + "\t" + record.getString("line") + "\n");
							failedValidations.append("\t\t\t" + "Expected line count exceeded." + "\n");
							failedValidations.append("\t\t\t" + "Actual:" + "\t\t" + actualLineCnt + "\n");
							failedValidations.append("\t\t\t" + "Expected:" + "\t" + record.getInt("count") + "\n\n");
							j = j + actualLineCnt - 1;
							continue;
						}
						partialOutput.append("\t\t" + "Record:" + "\t" + record.getString("line") + "\n");
					} else {
						prevRecord.put("actualCnt", prevRecord.getInt("actualCnt") + 1);
						record = prevRecord;
					}
//					System.out.println(record.toString(5));

					String[] dataList = readLines.get(j).split(",", -1);
					partialOutput.append("\t\t" + "Row:" + "\t" + (j + 1) + "\n");
					partialOutput.append("\t\t\t" + "Item" + "\t" + "Result" + "\n");
					int formatCnt = 0;
					for (int k = 0; k < dataList.length; k++) {
						// TODO Remove this bypass
//						if (1 == j && 11 == k) {
//							continue;
//						}
						// Check specified format, else do nothing
						try {
							record.getJSONArray("formats").getString(k).length();
						} catch (JSONException e) {
							e.printStackTrace();
							validationFlag = false;
							failedValidations.append("\t" + "File No.:" + "\t" + (i + 1) + "\n");
							failedValidations.append("\t" + "File Name:" + "\t" + csvFile.getName() + "\n");
							failedValidations.append("\t\t" + "Row:" + "\t\t" + (j + 1) + "\n");
							failedValidations.append("\t\t" + "Item:" + "\t\t" + (k + 1) + "\n");
							failedValidations.append("\t\t"
									+ "Exception:" + "\n\t\t\t"
									+ Throwables.getStackTraceAsString(e).replace("\n", "\n\t\t\t") + "\n");
							break;
						}
						if (0 < record.getJSONArray("formats").getString(k).length()) {
							formatCnt++;
							boolean check = check(record.getJSONArray("formats").getString(k), dataList[k]);
							partialOutput.append("\t\t\t" + (k + 1) + "\t\t");
							if (check) {
								partialOutput.append("OK");
							} else {
								validationFlag = false;
								partialOutput.append("NG");
								failedValidations.append("\t" + "File No.:" + "\t" + (i + 1) + "\n");
								failedValidations.append("\t" + "File Name:" + "\t" + csvFile.getName() + "\n");
								failedValidations.append("\t\t" + "Row:" + "\t\t" + (j + 1) + "\n");
								failedValidations.append("\t\t" + "Item:" + "\t\t" + (k + 1) + "\n");
								failedValidations.append("\t\t" + "Value:" + "\t\t" + dataList[k] + "\n");
								failedValidations.append("\t\t" + "Format:" + "\t\t" + record.getJSONArray("formats").getString(k) + "\n\n");
							}
							partialOutput.append("\t\t" + "Value:" + "\t" + dataList[k]);
							partialOutput.append("\n\t\t\t\t\t\t\t" + "Format:" + "\t" + record.getJSONArray("formats").getString(k) + "\n");
						}
					}
					if (0 == formatCnt) {
						partialOutput.append("\t\t\t" + "No formats found" + "\t\t\n");
					}
					partialOutput.append("\n");

					// Check row count against format.count
//					try {
//						int expectedCnt = prevRecord.getInt("count");
//						if (expectedCnt < prevRecord.getInt("actualCnt")) {
//							validationFlag = false;
//							failedValidations.append("\t" + "File No.:" + "\t\t" + (i + 1) + "\n");
//							failedValidations.append("\t" + "File Name.:" + "\t" + csvFile.getName() + "\n");
//							failedValidations.append("\t\t" + "Expected line count exceeded." + "\n");
//							failedValidations.append("\t\t\t" + "Actual:" + "\t\t" + prevRecord.getInt("actualCnt") + "\n");
//							failedValidations.append("\t\t\t" + "Expected:" + "\t" + expectedCnt + "\n\n");
//						}
//					} catch (NumberFormatException e) {
//						// When the expected line count for the current record is greater than 1
//					} catch (JSONException e) {
//						// When the expected line count for the current record is greater than 1
//					} catch (Exception e) {
//						// When the expected line count for the current record is greater than 1
//						e.printStackTrace();
//					}
				}
				errorMessage.append(partialOutput.toString() + "\n");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		errorMessage.append("Final Result: ");
		if (!validationFlag) {
			output.setIsError(true);
			errorMessage.append("NG" + "\n");
		} else {
			output.setIsError(false);
			errorMessage.append("OK" + "\n");
		}
		if (0 < failedValidations.toString().length()) {
			errorMessage.append("Failed validations: " + "\n");
			errorMessage.append(failedValidations.toString());
		}
		
		output.setError(errorMessage.toString());
		
		return output;
	}

	/**
	 * Function for getting the total line count of the current record
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param currentIndex
	 * @param readLines
	 * @return int
	 * @since 0.01
	 */
	private int lineCount(int currentIndex, List<String> readLines) {
		int output = 0;

		for (int i = currentIndex; i < readLines.size(); i++) {
			if (null == identifyLine(readLines.get(i))) {
				output = output + 1;
			} else {
				break;
			}
		}

		return output;
	}

	/**
	 * Function for checking the specified with format using the provided data
	 * <br>Supported formats:
	 * <br>		DATE_FORMAT(datePattern)
	 * <br>		DATE_FORMAT(datePattern, boolean)
	 * <br>		LPAD(length, paddingChar)
	 * <br>		FIXED(fixedValue)
	 * <br>		RANGE(min, max)
	 * <br>		RANGE(min, max, boolean)
	 * <br>		MATCH(regex)
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param format
	 * @param data
	 * @return boolean
	 * @since 0.01
	 */
	private boolean check(String format, String data) {
		
		if (UtilConstants.DYNAMIC_TAG.equals(data)
				&& 0 < data.length()) {
			return true;
		} else if (UtilConstants.DYNAMIC_TAG.equals(data)
				&& 0 >= data.length()) {
			return false;
		}

		if (StringUtils.containsIgnoreCase(format, UtilConstants.DATE_FORMAT)) {
			// Usage: DATE_FORMAT(pattern) or 
			// Usage: DATE_FORMAT(pattern, boolean)
			// Parse the expected datePattern parameter from the format string
			String[] params = format.replace(UtilConstants.DATE_FORMAT + "(", "").replace(")", "").split(",");
			String datePattern = params[0];
			boolean emptyFlag = false;
			SimpleDateFormat formatter = new SimpleDateFormat(datePattern);

			// Checks if the emptyFlag was set
			if (1 < params.length) {
				emptyFlag = Boolean.parseBoolean(params[1].trim());
			}

			// Parse the data using the formatter
			// Since the formatter is not lenient, an exception will be thrown
			// when invalid strings are parsed into a date.
			try {
				if (0 == data.length() && emptyFlag) {
					return true;
				} else if (0 < data.length()) {
					formatter.setLenient(false);
					formatter.parse(data);
					return true;
				}
			} catch (Exception e) {}
		} else if (StringUtils.containsIgnoreCase(format, UtilConstants.LPAD)) {
			// Usage: LPAD(length, paddingChar)
			// Parse the value of the length and paddingCharacter parameters from the format string
			int length;
			try {
				length = Integer.parseInt(format.replace(UtilConstants.LPAD + "(", "").replace(")", "").split(",")[0]);
			} catch (NumberFormatException e) {
				return false;
			}
			String paddingChar = format.replace(UtilConstants.LPAD + "(", "").replace(")", "").split(",")[1];

			if (length != data.length()) {
				return false;
			}

			// Get the padding pattern using a loop
			// Sample: 
			// 	length = 4
			//	paddingChar = 0
			// 	paddingPattern = 0000
			StringBuffer paddingPattern = new StringBuffer();
			for (int i = 0; i < length; i++) {
				paddingPattern.append(paddingChar);
			}

			int dataInt = Integer.parseInt(data);
			DecimalFormat lpad = new DecimalFormat(paddingPattern.toString()); 
			// Check if the local padding output is equal with the data
			if (lpad.format(dataInt).equals(data)) {
				return true;
			}
		} else if (StringUtils.containsIgnoreCase(format, UtilConstants.FIXED)) {
			// Usage: FIXED(fixedValue)
			// Parse the fixedValue parameter from the format string
			String fixedValue = format.replace(UtilConstants.FIXED + "(", "").replace(")", "");
			
			// Check if the data is equal to the specified fixedValue 
			if (fixedValue.equals(data)) {
				return true;
			}
		} else if (StringUtils.containsIgnoreCase(format, UtilConstants.RANGE)) {
			// Usage: RNAGE(min, max)
			// Parse the min and max parameters from the format strings
			int min;
			int max;
			try {
				String params[] = format.toLowerCase().replace(UtilConstants.RANGE.toLowerCase() + "(", "")
						.replace(")", "").split(",");
				boolean emptyFlag = false;
				min = Integer.parseInt(params[0]);
				max = Integer.parseInt(params[1]);

				// Check if the emptyFlag was set
				if (2 < params.length) {
					emptyFlag = Boolean.parseBoolean(params[2].trim());
				}
				
				// Check if the data is empty
				if (0 == data.length() && !emptyFlag) {
					return false;
				} else if (0 == data.length() && emptyFlag) {
					return true;
				}

				// Check if max is greater than min
				// Else, return false
				if (max < min) {
					return false;
				}

				int dataInt = Integer.parseInt(data);

				// Check if the data inclusively within the min and max
				if (min <= dataInt && max >= dataInt) {
					return true;
				}
			} catch (NumberFormatException e) {
				return false;
			}
		} else if (StringUtils.containsIgnoreCase(format, UtilConstants.MATCH)) {
			// Usage: MATCH("regex")
			// Parse the regex parameter from the format string that is enclosed in double quotations
			String regex = format.replace(UtilConstants.MATCH + "(\"", "").replace("\")", "");
			
			// Check if the data matches with the regex
			if (data.matches(regex)) {
				return true;
			}
		}
		
		return false;
	}
	
	public void close(){
		if(null != excelService){
			excelService.closeWorkbook();
		}
		excelService = null;
		formatList = null;
		expectedList = null;
	}

	/**
	 * @return the failureList
	 */
	public List<JSONObject> getFailureList() {
		return failureList;
	}

	/**
	 * @param failureList the failureList to set
	 */
	public void setFailureList(List<JSONObject> failureList) {
		this.failureList = failureList;
	}

}
