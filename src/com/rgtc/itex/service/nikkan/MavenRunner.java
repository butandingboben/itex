/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.service.nikkan;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class for running the maven command.
 * @author Joven 'Bob' Montero
 * @version 0.01
 * @since 0.01
 */
public class MavenRunner {

	public static void runMaven(String root, String testFile) {
		try {
    		String command = "cd " + root + " "
    				+ "&& "
    				+ "mvn -Dtest=" + testFile + " test -X";

    		System.out.println("Running command:\n" + command);
			ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", command);
			builder.redirectErrorStream(true);
			Process p = builder.start();
			BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			while (true) {
			    line = r.readLine();
			    if (line == null) { break; }
			    System.out.println(line);
			}
			System.out.println("Generating CSV finished");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
