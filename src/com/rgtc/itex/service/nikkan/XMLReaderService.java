/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.service.nikkan;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import com.rgtc.itex.models.AssertionOutputModel;
import com.rgtc.itex.service.CSVReader;
import com.rgtc.itex.util.ExcelService;
import com.rgtc.itex.util.UtilConstants;

/**
 * The class that will run the test for comparison of the actual and expected \n
 * XML file result.
 *
 * @author RGTC-PC0013
 * @version 0.01
 * 		Initial codes.
 */
public class XMLReaderService {

	private static final String TABLE_INFO_START = "<表情報>";
	private static final String TABLE_INFO_START_V2 = "<表情報>[Argument]";
	private static final String TABLE_INFO_END = "</表情報>";
	private static final String MATERIAL_START = "<素材>";
	private static final String MATERIAL_END = "</素材>";
	private static final String HEADER = "<?xml version=\"1.0\" encoding=\"UTF-16\" standalone=\"no\"?>";

	/**
	 * The file containing the test specifications
	 */
	private File testFile;

	/**
	 * The test case name to be tested
	 */
	private String testCaseName;

	/**
	 * The ExcelService instance
	 */
	private ExcelService excelService;

	/**
	 * The list of expected data for the current test case
	 */
	private List<List<String>> expectedList;

	/**
	 * The list containing the failed test cases.
	 */
	private List<JSONObject> failureList;

	/**
	 * Initialize the service.
	 * @version 0.01
	 * @param testFile
	 * 		The test file to be run.
	 * @param testCaseName
	 *		The name of the test case.
	 * @since 0.01
	 */
	public XMLReaderService(File testFile, String testCaseName) {
		// Initialize all class members
		this.excelService = new ExcelService();
		this.testFile = testFile;
		this.testCaseName = testCaseName;
		expectedList = new ArrayList<List<String>>();
		failureList = new ArrayList<JSONObject>();
		excelService.loadFile(this.testFile);
		// Populate all "Expected Data" from excel file.
		populateExpectedList();
	}

	/**
	 * Function for getting the expected CSV data
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @since 0.01
	 */
	private void populateExpectedList() {
		// Get the sheet containing the file name format
		Sheet xmlDataSheet = excelService.getSheet(UtilConstants.XMLDATA_CHECK_SHEET);
		// First loop: loop for the entire sheet starting in cell G6 
		for (int i = 1; i <= xmlDataSheet.getLastRowNum(); i++) {
			Row row = xmlDataSheet.getRow(i);
			try {
				if (testCaseName.equals(row.getCell(2).getStringCellValue())) {
					List<String> list = new ArrayList<String>();;
					for (int j = i + 1; j <= xmlDataSheet.getLastRowNum(); j++) {
						String cellValue;
						try {
							cellValue = xmlDataSheet.getRow(j).getCell(1).getStringCellValue().toString();
						} catch (NullPointerException e) {
							cellValue = "";
						}
						if (UtilConstants.TEST_FILE_TAG.toLowerCase().equals(cellValue.toLowerCase())) {
							if (0 < list.size()) {
								expectedList.add(list);
								list = new ArrayList<String>();
							}
						} else if (UtilConstants.TEST_CASE_END.toLowerCase().equals(cellValue.toLowerCase())) {
							expectedList.add(list);
							i = xmlDataSheet.getLastRowNum() + 1;
							break;
						} else {
							list.add(cellValue);
						}
					}
				} else {
					continue;
				}
			} catch (NullPointerException e) {
				continue;
			}
		}
	}

	/**
	 * Function for validating the data of the CSV files in a list
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param outputXMLFiles
	 * 		The actual XML files to be tested against.
	 * @return AssertionOutputModel
	 * @since 0.01
	 */
	public AssertionOutputModel validateData(File[] actualXMLFiles) {
		AssertionOutputModel output = new AssertionOutputModel();
		StringBuffer errorMessage = new StringBuffer();
		errorMessage.append("XML Data Check:");

		if (0 == expectedList.size()) {
			errorMessage.append("\n\n" + "Final Result:" + "\t" + "NA");
			errorMessage.append("\n" + "Cause:" + "\n\t" + "No expected data found for " + testCaseName);
			output.setIsError(false);
			output.setError(errorMessage.toString());
			return output;
		} else if (1 == expectedList.size()) {
			if (expectedList.get(0).get(0).equals(UtilConstants.NO_OUTPUT_CSV)) {
				if (0 < actualXMLFiles.length) {
					errorMessage.append("\n\n" + "Final Result:" + "\t" + "NG");
					errorMessage.append("\n" + "Cause:" + "\n\t" + "The number of expected files(0) and actual files("
							+ actualXMLFiles.length + ") are not equal.");
					JSONObject failure = new JSONObject();
					failure.put("testCaseName", testCaseName);
					failure.put("expectedData",  "0");
					failure.put("actualData", Integer.toString(actualXMLFiles.length));
					failure.put("cause", "The number of files is not equal.");
					failure.put("line", "-");
					failure.put("col", "-");
					failure.put("colNo", 1);
					failure.put("rowNo", 0);
					failureList.add(failure);
					output.setIsError(true);
					output.setError(errorMessage.toString());
					return output;
				}
				errorMessage.append("\n\n" + "Final Result:" + "\t" + "OK");
				errorMessage.append("\n" + "Remark:" + "\n\t" + "No expected CSV output.");
				
				output.setIsError(false);
				output.setError(errorMessage.toString());
				return output;
			} else if (actualXMLFiles.length != expectedList.size()) {
				errorMessage.append("\n\n" + "Final Result:" + "\t" + "NG");
				errorMessage.append("\n" + "Cause:" + "\n\t" + "The number of expected files(" + expectedList.size()
						+ ") and actual files(" + actualXMLFiles.length + ") are not equal.");
				JSONObject failure = new JSONObject();
				failure.put("testCaseName", testCaseName);
				failure.put("expectedData",  Integer.toString(expectedList.size()));
				failure.put("actualData", Integer.toString(actualXMLFiles.length));
				failure.put("cause", "The number of files is not equal.");
				failure.put("line", "-");
				failure.put("col", "-");
				failure.put("colNo", 1);
				failure.put("rowNo", 0);
				failureList.add(failure);
				output.setIsError(true);
				output.setError(errorMessage.toString());
				return output;
			}
		}

		if (actualXMLFiles.length != expectedList.size()) {
			errorMessage.append("\n\n" + "Final Result:" + "\t" + "NG");
			errorMessage.append("\n" + "Cause:" + "\n\t" + "The number of expected files(" + expectedList.size()
					+ ") and actual files(" + actualXMLFiles.length + ") are not equal.");
			JSONObject failure = new JSONObject();
			failure.put("testCaseName", testCaseName);
			failure.put("expectedData",  Integer.toString(expectedList.size()));
			failure.put("actualData", Integer.toString(actualXMLFiles.length));
			failure.put("cause", "The number of files is not equal.");
			failure.put("line", "-");
			failure.put("col", "-");
			failure.put("colNo", 1);
			failure.put("rowNo", 0);
			failureList.add(failure);
			output.setIsError(true);
			output.setError(errorMessage.toString());
			return output;
		}

		boolean validationFlag = true; 

		// Loop all actual XML files
		for (int i = 0; i < actualXMLFiles.length; i++) {
			File outputFile = actualXMLFiles[i];
			if (!outputFile.exists()) {
				validationFlag = false;
				String cause = "File not found.";
				errorMessage.append("\n\n\t" + "File No.:" + "\t" + (i + 1));
				errorMessage.append("\n\t" + "File Name:" + "\t\t" +  outputFile.getName());
				errorMessage.append("\n\t" + "Result: " + "\t" + "NG");
				errorMessage.append("\n\t\t" + "Cause: " + "\t\t" + cause);
				JSONObject failure = new JSONObject();
				failure.put("testCaseName", testCaseName);
				failure.put("expectedData",  outputFile.getName());
				failure.put("actualData", "-");
				failure.put("cause", cause);
				failure.put("line", "-");
				failure.put("col", "-");
				failure.put("colNo", 1);
				failure.put("rowNo", 0);
				failureList.add(failure);
				continue;
			}

			List<String> expectedStrList = expectedList.get(i);
			String cause = "";

			try {
				List<String> readLines = CSVReader.readCSV(outputFile);
				boolean perFileFlag = true;

				int readLinesLength = readLines.size();

				if (readLinesLength != expectedStrList.size()) {
					JSONObject failure = new JSONObject();
					cause = "Total line count is not equal.";
					errorMessage.append("\n\n\t" + "File No.:" + "\t" + (i + 1));
					errorMessage.append("\n\t" + "File Name:" + "\t" +  outputFile.getName());
					errorMessage.append("\n\t" + "Result: " + "\t" + "NG");
					errorMessage.append("\n\t\t" + "Cause: " + cause);
					errorMessage.append("\n\t\t\t" + "Actual count:" + "\t\t" + readLinesLength);
					errorMessage.append("\n\t\t\t" + "Expected count:" + "\t\t" + expectedStrList.size());
					
					failure.put("testCaseName", testCaseName);
					failure.put("expectedData", Integer.toString(expectedStrList.size()));
					failure.put("actualData", Integer.toString(readLinesLength));
					failure.put("cause", cause);
					failure.put("line", "-");
					failure.put("col", "-");
					failure.put("colNo", 1);
					failure.put("rowNo", 0);
					failureList.add(failure);
					
					validationFlag = false;
					perFileFlag = false;
				}
				
				for (int j = 0; j < readLinesLength; j ++) {
					String actRecord = readLines.get(j);
					String expRecord = expectedStrList.get(j);
					JSONObject failure = new JSONObject();
					
					String line;
					if (actRecord.contains(">")) {
						line = actRecord.substring(0, actRecord.indexOf(">") + 1);
					} else if (actRecord.contains("=")) {
						line = actRecord.substring(0, actRecord.indexOf("="));
					} else {
						line = actRecord;
					}
					
					if (!actRecord.equals(expRecord)) {
						cause = "Does not match";
						errorMessage.append("\n\n\t" + "File No.:" + "\t" + (i + 1));
						errorMessage.append("\n\t" + "File Name:" + "\t" +  outputFile.getName());
						errorMessage.append("\n\t" + "Result: " + "\t" + "NG");
						errorMessage.append("\n\t\t" + "Row:" + "\t\t" + (j + 1));
						errorMessage.append("\n\t\t" + "Cause: " + cause);
						errorMessage.append("\n\t\t\t" + "Actual:" + "\t\t" + actRecord);
						errorMessage.append("\n\t\t\t" + "Expected:" + "\t" + expRecord);
						
						failure.put("testCaseName", testCaseName);
						failure.put("tag", line);
						failure.put("expectedData", expRecord);
						failure.put("actualData", actRecord);
						failure.put("cause", cause);
						failure.put("line", line);
						failure.put("col", "-");
						failure.put("colNo", 1);
						failure.put("rowNo", 0);
						failureList.add(failure);
						validationFlag = false;
						perFileFlag = false;
					}
				}
				

//				for (int j = 0; j < readLinesLength; j ++) {
//					JSONObject failure = new JSONObject();
//					String actRecord = readLines.get(j);
//					String expRecord = expectedStrList.get(j);
//
//					if (TABLE_INFO_START.equals(actRecord)
//							|| TABLE_INFO_END.equals(actRecord)
//							|| TABLE_INFO_START_V2.equals(actRecord)
//							|| MATERIAL_START.equals(actRecord)
//							|| MATERIAL_END.equals(actRecord)
//							|| HEADER.equals(actRecord)) {
//						if (!actRecord.equals(expRecord)) {
//							errorMessage.append("\n\n\t" + "File No.:" + "\t" + (i + 1));
//							errorMessage.append("\n\t" + "File Name:" + "\t" +  outputFile.getName());
//							errorMessage.append("\n\t" + "Result: " + "\t" + "NG");
//							errorMessage.append("\n\t\t" + "Row:" + "\t\t" + (j + 1));
//							errorMessage.append("\n\t\t" + "Cause: " + cause);
//							errorMessage.append("\n\t\t\t" + "Actual:" + "\t\t" + actRecord);
//							errorMessage.append("\n\t\t\t" + "Expected:" + "\t" + expRecord);
//							validationFlag = false;
//							perFileFlag = false;
//						}
//					} else {
//						String parsedActData = "";
//						String parsedExpData = "";
//						try {
//							parsedActData = parseLine(actRecord);
//						} catch (JSONException e) {
//							cause = e.getMessage();
////							System.out.println(cause);
//							if (cause.indexOf("at ") != -1) {
//								cause = cause.substring(0, cause.indexOf("at "));
//							}
//							errorMessage.append("\n\t" + "Test File No.:" + "\t" + (i + 1));
//							errorMessage.append("\n\t" + "File Name:" + "\t" +  outputFile.getName());
//							errorMessage.append("\n\t" + "Result: " + "\t" + "NG");
//							errorMessage.append("\n\t\t" + "Cause: " + cause);
//							validationFlag = false;
//							perFileFlag = false;
//							continue;
//						}
//
//						try {
//							parsedExpData = parseLine(expRecord);
//						} catch (JSONException e) {
////							cause = e.getMessage();
//							if (cause.indexOf("at ") != -1) {
//								cause = cause.substring(0, cause.indexOf("at "));
//							}
//							errorMessage.append("\n\t" + "Test File Name:" + "\t" +  testFile.getName());
//							errorMessage.append("\n\t" + "Test Case Name:" + "\t" +  testCaseName);
//							errorMessage.append("\n\t" + "Result: " + "\t" + "NG");
//							errorMessage.append("\n\t\t" + "Cause: " + cause);
//							validationFlag = false;
//							perFileFlag = false;
//							continue;
//						}
//
//						String[] splitParsedActData = parsedActData.split(":");
//						String[] splitParsedExpData = parsedExpData.split(":");
//
//						// TODO: Add codes to check for dynamic values, etc.
//						if (1 < splitParsedActData.length && 1 < splitParsedExpData.length) {
//							if (!splitParsedActData[1].equals(splitParsedExpData[1])) {
//								cause = "The actual data did not match the expected data.";
//								errorMessage.append("\n\n\t" + "File No.:" + "\t" + (i + 1));
//								errorMessage.append("\n\t" + "File Name:" + "\t" +  outputFile.getName());
//								errorMessage.append("\n\t" + "Result: " + "\t" + "NG");
//								errorMessage.append("\n\t\t" + "Row:" + "\t\t" + (j + 1));
//								errorMessage.append("\n\t\t" + "Cause: " + cause);
//								errorMessage.append("\n\t\t\t" + "Actual:" + "\t\t" + splitParsedActData[1]);
//								errorMessage.append("\n\t\t\t" + "Expected:" + "\t" + splitParsedExpData[1]);
//								validationFlag = false;
//								perFileFlag = false;
//
//								// Storing for failure compilations.
//								failure.put("testCaseName", testCaseName);
//								failure.put("tag", splitParsedActData[0]);
//								failure.put("expectedData", splitParsedExpData[1].replace("[Empty]", ""));
//								failure.put("actualData", splitParsedActData[1].replace("[Empty]", ""));
//								failure.put("cause", cause);
//								failure.put("line", splitParsedActData[0]);
//								failure.put("col", "-");
//								failure.put("colNo", 1);
//								failure.put("rowNo", 0);
//								failureList.add(failure);
//							}
//						} else {
//							if (!splitParsedActData[0].equals(splitParsedExpData[0])) {
//								cause = "The actual data did not match the expected data.";
//								errorMessage.append("\n\n\t" + "File No.:" + "\t" + (i + 1));
//								errorMessage.append("\n\t" + "File Name:" + "\t" +  outputFile.getName());
//								errorMessage.append("\n\t" + "Result: " + "\t" + "NG");
//								errorMessage.append("\n\t\t" + "Row:" + "\t\t" + (j + 1));
//								errorMessage.append("\n\t\t" + "Cause: " + cause);
//								errorMessage.append("\n\t\t\t" + "Actual:" + "\t\t" + splitParsedActData[0]);
//								errorMessage.append("\n\t\t\t" + "Expected:" + "\t" + splitParsedExpData[0]);
//								validationFlag = false;
//								perFileFlag = false;
//
//								// Storing for failure compilations.
//								failure.put("testCaseName", testCaseName);
//								failure.put("tag", splitParsedActData[0]);
//								failure.put("expectedData", splitParsedExpData[0]);
//								failure.put("actualData", splitParsedActData[0]);
//								failure.put("cause", cause);
//								failure.put("line", splitParsedActData[0]);
//								failure.put("col", "-");
//								failure.put("colNo", 1);
//								failure.put("rowNo", 0);
//								failureList.add(failure);
//							}
//						}
//					}
//				}

				if (perFileFlag) {
					errorMessage.append("\n\n\t" + "File No.:" + "\t" + (i + 1));
					errorMessage.append("\n\t" + "File Name:" + "\t" +  outputFile.getName());
					errorMessage.append("\n\t" + "Result: " + "\t" + "OK");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		errorMessage.append("\n\n" + "Final Result:" + "\t");
		if (!validationFlag) {
			errorMessage.append("NG");
		} else {
			errorMessage.append("OK");
		}
		output.setIsError(!validationFlag);
		output.setError(errorMessage.toString());
		return output;
	}

	public String parseLine(String line) throws JSONException{
		JSONObject xmlJSON = new JSONObject();
		StringBuffer sb = new StringBuffer();
		if (line.startsWith("[")) {
			sb.append(line);
			sb.append(":");
			sb.append("[Empty]");
			return sb.toString();
		} else if (line.startsWith("<")) {
			xmlJSON = XML.toJSONObject(line);

			@SuppressWarnings("unchecked")
			Iterator<String> keys = xmlJSON.keys();
			while(keys.hasNext()) {
				String key = keys.next();
				Object val = null;
				try {
					val = xmlJSON.getString(key);
				} catch (Exception e) {
					val = (Integer)xmlJSON.getInt(key);
				}
				sb.append(key);
				sb.append(":");
				if(null != val) {
					sb.append(val.toString());
				} else {
					sb.append("[Empty]");
				}
			}
		} else if (line.startsWith("(")) {
			sb.append(line);
			sb.append(":");
			sb.append("[Empty]");
			return sb.toString();
		} else {
			String[] split = line.split("=");
			String value = "[Empty]";
			if (split.length > 1) {
				value = split[1];
			}
			sb.append(split[0]);
			sb.append(":");
			sb.append(value);
		}
		return sb.toString();
	}

	public void close(){
		if(null != excelService){
			excelService.closeWorkbook();
		}
		excelService = null;
		expectedList = null;
	}

	// SETTERS AND GETTERS

	public File getTestFile() {
		return testFile;
	}

	public void setTestFile(File testFile) {
		this.testFile = testFile;
	}

	public String getTestCaseName() {
		return testCaseName;
	}

	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}

	public ExcelService getExcelService() {
		return excelService;
	}

	public void setExcelService(ExcelService excelService) {
		this.excelService = excelService;
	}

	public List<List<String>> getExpectedList() {
		return expectedList;
	}

	public void setExpectedList(List<List<String>> expectedList) {
		this.expectedList = expectedList;
	}

	public List<JSONObject> getFailureList() {
		return failureList;
	}

	public void setFailureList(List<JSONObject> failureList) {
		this.failureList = failureList;
	}
}
