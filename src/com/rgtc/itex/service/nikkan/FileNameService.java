/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.service.nikkan;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import com.rgtc.itex.models.AssertionOutputModel;
import com.rgtc.itex.util.UtilConstants;
import com.rgtc.itex.util.ExcelService;

/**
 * Class for handling the file name of the output CSV
 * @author Joven 'Bob' Montero
 * @version 0.01
 * @since 0.01
 */
public class FileNameService {

	/**
	 * The file containing the test specifications
	 */
	private File testFile;
	/**
	 * The test case name to be tested
	 */
	private String testCaseName;
	/**
	 * The ExcelService instance
	 */
	private ExcelService excelService;
	
	public FileNameService(File testFile, String testCaseName) {
		this.testFile = testFile;
		this.testCaseName = testCaseName;
		excelService = new ExcelService();
		excelService.loadFile(testFile);
	}
	
	/**
	 * Function for getting the specified format of an output file
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param fileNo The index of the CSV file to check. This should be ZERO when getting only ONE file name format. 
	 * @return List<String>
	 * @since 0.01
	 */
	private List<String> getFormat(int fileNo) {
		List<String> format = new ArrayList<String>();

		// Get the sheet containing the file name format
		Sheet filenameSheet = excelService.getSheet(UtilConstants.FILENAME_CHECK_SHEET);
		int fileCnt = 0;

		// First loop: loop for the entire sheet starting in index 1 not 0
		for (int i = 1; i <= filenameSheet.getLastRowNum(); i++) {
			Row row = filenameSheet.getRow(i);

			try {
				testCaseName.equals(row.getCell(2).getStringCellValue());
			} catch (NullPointerException e) {
				continue;
			}

			// Checks if the test case name is equal to the value of the cell in index 2
			if (testCaseName.equals(row.getCell(2).getStringCellValue())) {
				// Second loop: loop for getting the nth file name format specified by fileNo
				for (int j = i + 1; j <= filenameSheet.getLastRowNum(); j++) {
					String initialCellVal = filenameSheet.getRow(j).getCell(1).getStringCellValue().trim();

					// Checks if the value of the first cell in the row contains the test file tag (Test File)
					// Else if the fileCnt increased, proceeds to the next row until it encounters test file tag again
					// Else, proceeds to getting a single file name format
					if (UtilConstants.TEST_FILE_TAG.toLowerCase().equals(initialCellVal.toLowerCase())) {
						if (fileNo != fileCnt) {
							fileCnt++;
							continue;
						} else {
							// Changes the index to the next row
							j++;
						}
					} else if (0 < fileCnt) {
						continue;
					}

					// Third loop: loop for getting the file name format until it encounters 
					// the end of the test case or the another test file tag
					for (int k = j; k <= filenameSheet.getLastRowNum(); k++) {
						try {
							Cell cell = filenameSheet.getRow(k).getCell(1);
							String formatValue = "";
							if (!UtilConstants.TEST_FILE_TAG.equals(cell.getStringCellValue().trim())
									&& !UtilConstants.TEST_CASE_END.equals(cell.getStringCellValue().trim())) {
								formatValue = filenameSheet.getRow(k).getCell(2).getStringCellValue().trim();
								format.add(formatValue);
							} else {
								break;
							}
						} catch (NullPointerException e) {
							break;
						}
					}
					break;
				}
				break;
			}
		}

		return format;
	}
	
	/**
	 * Function for getting the total number of formats
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @return int
	 * @since 0.01
	 */
	private int getTotalFormatCount() {
		int totalCount = 0;

		// Get the sheet containing the file name format
		Sheet filenameSheet = excelService.getSheet(UtilConstants.FILENAME_CHECK_SHEET);
		for (int i = 1; i <= filenameSheet.getLastRowNum(); i++) {
			Row row = filenameSheet.getRow(i);
			
			try {
				testCaseName.equals(row.getCell(2).getStringCellValue());
			} catch (NullPointerException e) {
				continue;
			}
			
			// Checks if the test case name is equal to the value of the cell in index 2
			if (testCaseName.equals(row.getCell(2).getStringCellValue())) {
				// Second loop: loop for getting the file name formats
				for (int j = i + 1; j <= filenameSheet.getLastRowNum(); j++) {
					String initialCellVal = filenameSheet.getRow(j).getCell(1).getStringCellValue().trim();

					// Checks if the value of the first cell in the row contains the test file tag (Test File)
					// Else if the fileCnt increased, proceeds to the next row until it encounters test file tag again
					// Else, proceeds to getting a single file name format
					if (UtilConstants.TEST_FILE_TAG.toLowerCase().equals(initialCellVal.toLowerCase())) {
						totalCount++;
					} else if(UtilConstants.TEST_CASE_END.equals(initialCellVal)) {
						return totalCount;
					} else {
						continue;
					}
				}
				break;
			}
		}

		return totalCount;
	}

	/**
	 * Function for validating the file name format of the file
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param outputCSVFiles
	 * @return AssertionOutputModel
	 * @since 0.01
	 */
	public AssertionOutputModel validate(File[] outputCSVFiles) {
		AssertionOutputModel output = new AssertionOutputModel();
		StringBuffer errorMessage = new StringBuffer();
		List<String> failureList = new ArrayList<String>();
		errorMessage.append("File Name Format Check: ");
		
		int formatCnt = getTotalFormatCount();
		if (0 == formatCnt) {
			errorMessage.append("\n\n" + "Final Result:" + "\t" + "NA");
			errorMessage.append("\n" + "Cause:" + "\t\t\t" + "No formats found");
			output.setIsError(false);
			output.setError(errorMessage.toString());
			return output;
		} else if (formatCnt != outputCSVFiles.length) {
			errorMessage.append("\n\n" + "Final Result:" + "\t" + "NG");
			errorMessage.append("\n" + "Cause:" + "\t\t\t" + "The number of formats(" + formatCnt + ") and files(" + outputCSVFiles.length + ") are not equal.");
			output.setIsError(true);
			output.setError(errorMessage.toString());
			return output;
		}
		
		boolean error = false;
		for (int i = 0; i < outputCSVFiles.length; i++) {
			String fileName = outputCSVFiles[i].getName();
			String finalFormat = "";
			for (String partial: getFormat(i)) {
				if (partial.contains(UtilConstants.TIME_FORMAT)) {
					finalFormat = finalFormat.concat(UtilConstants.TIME_HHMMSS);
				} else {
					finalFormat = finalFormat.concat(partial);
				}
			}
	
			errorMessage.append("\n\n\t" + "File No.:" + "\t" + (i + 1));
			errorMessage.append("\n\t" + "File Name:" + "\t" + fileName);
			errorMessage.append("\n\t" + "Format:" + "\t\t" + finalFormat);
			errorMessage.append("\n\t" + "Result:" + "\t\t");
			if (!fileName.matches(finalFormat)) {
				error = true;
				errorMessage.append("NG");
				failureList.add((i + 1) + "\t\t\t" + fileName);
			} else {
				errorMessage.append("OK");
			}
	
		}
		errorMessage.append("\n\n" + "Final Result:" + "\t");
		if (error) {
			errorMessage.append("NG");
			errorMessage.append("\n" + "Failed:");
			errorMessage.append("\n\t" + "File No." + "\t" + "File Name");
			for (String fileName: failureList) {
				errorMessage.append("\n\t" + fileName);
			}
		} else {
			errorMessage.append("OK");
		}
		output.setIsError(error);
		output.setError(errorMessage.toString());
		
		return output;
	}
	
	public void close(){
		if(null != excelService){
			excelService.closeWorkbook();
		}
		excelService = null;
		testFile = null;
	}
	
}
