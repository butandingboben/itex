/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Util for reading CSV files
 * @author Joven 'Bob' Montero
 * @version 0.01
 * @since 0.01
 */
public class CSVReader {

	/** 
	 * Different encodings will have different BOMs. This is for UTF-8.
	 */
	private static final int[] BYTE_ORDER_MARK = {239, 187, 191};

	/**
	 * Function for reading and parsing a CSV file.
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param csvFile
	 * @throws Exception
	 * @since 0.01
	 */
	public static List<String> readCSV(File csvFile) throws Exception {
		List<String> outputList = new ArrayList<String>();
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile), "UTF-8"));

		String line = "";
		while (null != (line = reader.readLine())) {
			// Proceeds to the next line when the current line is empty
			if (line.equals("")) {
//				continue;
			}

			outputList.add(line);
		}
		reader.close();

		return outputList;
	}

	/**
	 * <div class="jp">
	 * Function for comparing the expected and actual CSV results.
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param expected
	 * @param actual
	 * @return
	 * @since 0.01
	 */
	private static boolean compareResults(List<Object> expected, List<Object> actual) {
		System.out.println("Started compareResults");
//		boolean equalFlag = true;

		for (int i = 0; i < expected.size(); i++) {
			if (!expected.get(i).toString().equals(actual.get(i).toString())) {
				System.out.println("Failed compareResults");
				return false;
			}
		}

		System.out.println("Finished compareResults");
		return true;
	}
	
	/**
	 * Function for checking if the encoding of the csv file is UTF-8
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param csvFile
	 * @return
	 * @throws FileNotFoundException 
	 * @since 0.01
	 */
	public static boolean isUTF8EncodedWithoutBOM(File csvFile) throws FileNotFoundException {
		boolean startsWithBom = false;
		int[] firstFewBytes = new int[BYTE_ORDER_MARK.length];
	    InputStream input = null;
	    try {
	    	input = new FileInputStream(csvFile);
	    	for(int index = 0; index < BYTE_ORDER_MARK.length; ++index){
	    		firstFewBytes[index] = input.read(); //read a single byte
	    	}
	    	startsWithBom = Arrays.equals(firstFewBytes, BYTE_ORDER_MARK);
	    } catch (Exception e) {
			e.printStackTrace();
		} finally {
	  		try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	    if (!startsWithBom) {
			return true;
		}
		return false;
	}
}
