/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.tools.remoteapi.RemoteApiInstaller;
import com.google.appengine.tools.remoteapi.RemoteApiOptions;

/**
 * <div class="jp">
 *
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 *
 * </div>
 *
 * @author Eldon Leuterio
 * @version 0.01
 * @since 0.01
 */
public class DatastoreService {

	private RemoteApiOptions options;

	public DatastoreService(String url, int port, String user, String pass) {
		options = new RemoteApiOptions().server(url, port).credentials(user,
				pass);
	}

	public List<Entity> getAllEntities(String kind, String taskName) {
		RemoteApiInstaller installer = new RemoteApiInstaller();
		List<Entity> entries = new ArrayList<Entity>();
		try {

			installer.install(options);
			com.google.appengine.api.datastore.DatastoreService ds = DatastoreServiceFactory
					.getDatastoreService();

			Query query = null;
			if(null != kind){
				query = new Query(kind);
			}else{
				query = new Query();
			}


			PreparedQuery pQuery = ds.prepare(query);
			List<Entity> list = pQuery.asList(FetchOptions.Builder.withDefaults());
			for (Entity e : list) {
//				Map<String, Object> map = e.getProperties();
//				System.out.println("kind: " + e.getKind());
//				System.out.println("taskName: " + e.getProperty("taskName"));
//				System.out.println("namespace: " + e.getNamespace());
//				System.out.println("content: " + e.getProperty("content"));
//				if (taskName == (String) e.getProperty("taskName")) {
//					entries = new ArrayList<Entity>();
//					entries.add(e);
//					break;
//				}else{
					entries.add(e);
//				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			installer.uninstall();
		}
		return entries;
	}

	public void removeEntry(String kind) {
		RemoteApiInstaller installer = new RemoteApiInstaller();
		try {

			installer.install(options);
			com.google.appengine.api.datastore.DatastoreService ds = DatastoreServiceFactory
					.getDatastoreService();
			Query query = new Query(kind);
			PreparedQuery pQuery = ds.prepare(query);
			List<Entity> list = pQuery.asList(FetchOptions.Builder
					.withDefaults());
			for (Entity e : list) {
				ds.delete(e.getKey());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			installer.uninstall();
		}
	}

	public void put() {
		RemoteApiInstaller installer = new RemoteApiInstaller();
		Entity entity = null;
		try {

			installer.install(options);
			com.google.appengine.api.datastore.DatastoreService ds = DatastoreServiceFactory
					.getDatastoreService();
			ds.put(new Entity("hello world!"));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			installer.uninstall();
		}
	}

	/**
	 * Inserts operation logs to the datastore.
	 */
	public void insertOperationLogs(JSONObject operationLogs) {
		RemoteApiInstaller installer = new RemoteApiInstaller();

		try {
			installer.install(options);
			com.google.appengine.api.datastore.DatastoreService ds = DatastoreServiceFactory
					.getDatastoreService();
			for (int i = 0; i < operationLogs.getJSONArray("entryList").length(); i++) {
				com.google.appengine.api.datastore.Key logKey
					= KeyFactory.createKey("MypageOperationLog", "operationLog");
				Entity operationLog = new Entity("MypageOperationLog", logKey);

				JSONObject currentLog = operationLogs.getJSONArray("entryList").getJSONObject(i);
				operationLog.setProperty("operationAt", currentLog.getString("operationAt"));
				operationLog.setProperty("operatorAccountId", currentLog.getString("operatorAccountId"));
				operationLog.setProperty("operatorType", currentLog.getString("operatorType"));
				operationLog.setProperty("operationTargetMemberId", currentLog.getString("operationTargetMemberId"));
				operationLog.setProperty("operationType", currentLog.getString("operationType"));
				operationLog.setProperty("operationContent", new Text(AesService.encrypt(currentLog.optJSONObject("*operationContent").toString())));
				operationLog.setProperty("cleanupAt", currentLog.getString("cleanupAt"));

				ds.put(operationLog);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			installer.uninstall();
		}
	}

//	public static void main(String[] args) {
//		JSONObject json = new JSONObject();
//		JSONArray array = new JSONArray();
//
//		JSONObject log = new JSONObject();
//		log.put("operationAt", "operationAt");
//		log.put("operatorAccountId", "operatorAccountId");
//		log.put("operatorType", "operatorType");
//		log.put("operationTargetMemberId", "operationTargetMemberId");
//		log.put("operationType", "operationType");
//		log.put("operationContent", "operationContent");
//		log.put("cleanupAt", "cleanupAt");
//		log.put("timestamp", "timestamp");
//		array.put(log);
//
//		log = new JSONObject();
//		log.put("operationAt", "operationAt2");
//		log.put("operatorAccountId", "operatorAccountId2");
//		log.put("operatorType", "operatorType2");
//		log.put("operationTargetMemberId", "operationTargetMemberId2");
//		log.put("operationType", "operationType2");
//		log.put("operationContent", "operationContent2");
//		log.put("cleanupAt", "cleanupAt2");
//		log.put("timestamp", "timestamp2");
//		array.put(log);
//
//		json.put("entryList", array);
//
//		DatastoreService ds = new DatastoreService("localhost", 8888, "XXXX", "XXXX");
//		ds.insertOperationLog(json);
//	}
}
