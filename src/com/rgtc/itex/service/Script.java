/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.service;

import java.io.File;
import java.io.IOException;
import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.text.TabExpander;

import com.rgtc.itex.models.ScenarioModel;
import com.rgtc.itex.models.TableColumnModel;
import com.rgtc.itex.models.TableModel;
import com.rgtc.itex.util.UtilConstants;
import com.rgtc.itex.util.PropertiesUtil;

/**
 * <div class="jp">
 *
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 * Class for generationg sql statements and saving it to properties file.
 * </div>
 *
 * @author Recuerdo Bregente
 * @version 0.01
 * @since 0.01
 */
public class Script {

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Generates the file used to save sql statements.
	 * </div>
	 * @maker Recuerdo Bregente
	 * @version 0.01
	 * @param filePath
	 * @param before
	 * @param after
	 * @return String Error message.
	 * @since 0.01
	 */
	public static String generateScriptFile(String filePath, Map<String, List<String>> before, Map<String, List<String>> after){

		String message = null;
		if(before.isEmpty() && after.isEmpty()){
			return "Database data not found";
		}else if(before.isEmpty()){
			return "Prerequisite Database data not found!";
		}else if(after.isEmpty()){
			return "Expected Database data not found!";
		}
		String newFilePath = filePath.concat("\\"+UtilConstants.SCRIPT_FILENAME);
		File file = new File(newFilePath);
	
		try{
			if(!file.exists()){
				file.createNewFile();
			}
			generateStatements(newFilePath, before, UtilConstants.INSERT_TYPE);
			generateStatements(newFilePath, after, UtilConstants.SELECT_TYPE);
			
		}catch(IOException e){
			message = e.toString();
		}
		
		return message;
	}
	
	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Generates statements in the Script file.
	 * </div>
	 * @maker Recuerdo Bregente
	 * @version 0.01
	 * @param filePath
	 * @param map
	 * @param type
	 * @throws IOException
	 * @since 0.01
	 */
	private static void generateStatements(String filePath, Map<String, List<String>> map, String type) throws IOException{
		for(Map.Entry<String, List<String>> entry: map.entrySet()){
			try{
				PropertiesUtil.removeStatementsFromScenario(filePath, entry.getKey(), type);
			}catch(IOException e){
				e.printStackTrace();
			}
			int index = 1;
			for(String statement: entry.getValue()){
				PropertiesUtil.set(filePath, generateKey(entry.getKey(), type, index++), statement);
			}
		}
	}
	
	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Generates key based on the scenarion name
	 * </div>
	 * @maker Recuerdo Bregente
	 * @version 0.01
	 * @param scenario
	 * @param type
	 * @param index
	 * @return
	 * @since 0.01
	 */
	private static String generateKey(String scenario, String type, int index){
		String key = scenario.replace(' ', '_');
		StringBuffer sb = new StringBuffer(key);
		sb.append("_");
		sb.append(type);
		sb.append("_");
		sb.append(index);
		return sb.toString();
	}
	
	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Returns the an object type of all the data for a scenario.
	 * </div>
	 * @maker Recuerdo Bregente
	 * @version 0.01
	 * @param path The file path.
	 * @param scenarioName
	 * @return ScenarioModel Contains all the data of a scenario.
	 * @throws IOException 
	 * @since 0.01
	 */
	public static ScenarioModel getScenario(String path, String scenarioName) throws IOException{
		ScenarioModel model = new ScenarioModel();
		model.setScenarioName(scenarioName);
		model.setAfterDataTables(getAllSelectTables(UtilConstants.SCRIPT_FILE_PATH, scenarioName, UtilConstants.SELECT_TYPE));
		return model;
	}
	
	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Method for populating scenario data.
	 * </div>
	 * @maker Recuerdo Bregente
	 * @version 0.01
	 * @param filePath
	 * @param scenarioName
	 * @param operation
	 * @return List<TableModel> List of table data of a scenario.
	 * @throws IOException
	 * @since 0.01
	 */
	private static List<TableModel> getAllSelectTables(String filePath, String scenarioName, String operation) throws IOException{
		//Calling properties utility to retrieve all select statement of a scenario
		List<String> statements = PropertiesUtil.getStatementsFromScenario(filePath, scenarioName, operation);		
		List<TableModel> tableModelList = new ArrayList<TableModel>();
		int tableListIndex = -1;
		//Loop through all select statement of a scenario
		for (String statement : statements) {
			//Get the Model ojbect of a statement
			TableModel tempTableModel = parseStatementToModel(statement);
			tableListIndex = 0;
			//Check if table model already exist in the list
			while(tableListIndex < tableModelList.size() && 
					!tableModelList.get(tableListIndex).getTableName()
					.equals(tempTableModel.getTableName())){
				tableListIndex++;
			}
			//Table model does not yet exist in the list 
			if(tableModelList.size() == tableListIndex){
				tempTableModel.setRowCount(1);
				tableModelList.add(tempTableModel);
			}
			else{
				//Add all the column models to the existing table model
				for (TableColumnModel columnModel : tempTableModel.getColModelList()) {
					tableModelList.get(tableListIndex).getColModelList().add(columnModel);
				}
				//Increment row count
				tableModelList.get(tableListIndex).setRowCount(tableModelList.get(tableListIndex).getRowCount()+1);
			}
			
		}
		return tableModelList;
	}
	
	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Converts and returns an object type of all metadata of a select query.
	 * </div>
	 * @maker Recuerdo Bregente
	 * @version 0.01
	 * @param statement
	 * @return TableModel Converted select query statement to model.
	 * @since 0.01
	 */
	private static TableModel parseStatementToModel(String statement){
		TableModel tableModel = new TableModel();
		String newStatement = statement.replace(",", " ");
		String[] statementString = newStatement.split("\\s+");
		tableModel.setTableName(getTableName(statementString));
		tableModel.setColCount(getColumnCount(statementString));
		tableModel.setColModelList(getTableColumnList(statementString));
		return tableModel;
	}
	
	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Search and returns for the table name used in the select query.
	 * </div>
	 * @maker Recuerdo Bregente
	 * @version 0.01
	 * @param statements
	 * @return String Table name of used in the select query.
	 * @since 0.01
	 */
	private static String getTableName(String[] statements){
		String tableName = "";
		
		for (int i = 0; i < statements.length; i++) {
			if("FROM".equalsIgnoreCase(statements[i])){
				tableName = statements[++i];
				break;
			}
		}
		
		return tableName;
	}
	
	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Counts the number of column names used in the select query.
	 * </div>
	 * @maker Recuerdo Bregente
	 * @version 0.01
	 * @param statements
	 * @return int The total number if columns in the select query.
	 * @since 0.01
	 */
	private static int getColumnCount(String[] statements){
		int columnCount = 0;
		for (int i = 1; i < statements.length && !"FROM".equalsIgnoreCase(statements[i]); i++) {
			columnCount++;
		}
		
		return columnCount;
	}
	
	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Search and parses the query statement to produce a list of column objects.
	 * </div>
	 * @maker Recuerdo Bregente
	 * @version 0.01
	 * @param statements
	 * @return List<TableColumnModel> List of all the columns as object in the select query.
	 * @since 0.01
	 */
	private static List<TableColumnModel> getTableColumnList(String[] statements){
		List<TableColumnModel> tableColumnList = new ArrayList<TableColumnModel>();
		TableColumnModel tableColumnModel = null;
		//Loop through all the words in the statement from after SELECT until it reaches to FROM
		//Save the values as name of the column model
		for (int colIdIterator = 1; colIdIterator < statements.length && !"FROM".equalsIgnoreCase(statements[colIdIterator]); colIdIterator++) {
			tableColumnModel = new TableColumnModel();
			tableColumnModel.setName(statements[colIdIterator]);
			//Loop through all the statements starting from FROM to the end of the statement.
			for (int valueIterator = getColumnCount(statements)+2; valueIterator < statements.length; valueIterator++) {
				//Checks if the column model name is equal to the current condition column name
				if(statements[valueIterator].equals(tableColumnModel.getName())){
					//Check if the value for the column is NOT NULL
					if("NOT".equalsIgnoreCase(statements[valueIterator+2]) && "NULL".equalsIgnoreCase(statements[valueIterator+3])){
						tableColumnModel.setData("*D");
					}
					//Check it the values for the column is NULL
					else if("NULL".equalsIgnoreCase(statements[valueIterator+2])){
						tableColumnModel.setData("-");
					}else{
						int dataIndex = valueIterator+2;
						StringBuffer sb = new StringBuffer(statements[dataIndex]);
						//Check if value has multiple strings
						while(++dataIndex < statements.length && 
								!statements[dataIndex].equalsIgnoreCase("AND")){
							sb.append(" ");
							sb.append(statements[dataIndex]);
						}
						String value = sb.toString().replace("'", "");
						//Check if value is empty
						if(value.isEmpty()){
							value = "*EMP";
						}
						tableColumnModel.setData(value);
					}
				}
			}
			tableColumnList.add(tableColumnModel);
		}
		
		return tableColumnList;
	}
}
