package com.rgtc.itex.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.rgtc.itex.exceptions.FileReadException;

public class FileService {

	private static List<File> files;

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * Retrieves the list of file in a directory and also sub directories.<br>
	 * Files can be filtered by providing a prefix and suffix.
	 * <p>
	 * Also populates <code>files<codes> for easier loading.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param path the directory path
	 * @param prefix the file prefix for filtering.
	 * @param extensions the file suffix (can be multiple)
	 * @param absolute the absolute file for single loading.
	 * @return list of filenames.
	 * @since 0.01
	 */
	public List<String> getListOfFiles(File path, List<String> prefixList, String[] extensions, String absolute){

		List<String> fileNames = new ArrayList<String>();

		for (String prefix : prefixList) {
			//check if path or file
			if(path.isDirectory()){
				Collection<File> rawFiles = FileUtils.listFiles(path, extensions, true);
				for(File file : rawFiles){
					if(null == getFiles()){
						setFiles(new ArrayList<File>());
					}

					if(null != prefix){
						if(file.isFile() && file.getName().startsWith(prefix)){
							fileNames.add(file.getName());
							getFiles().add(file);
						}
					}else{
						if(file.isFile()){
							fileNames.add(file.getName());
							getFiles().add(file);
						}
					}
				}
			}else if(path.isFile()){
				if(null == getFiles()){
					setFiles(new ArrayList<File>());
				}
				if(null != prefix){
					if(path.isFile() && path.getName().startsWith(prefix)){
						fileNames.add(path.getName());
						getFiles().add(path);
					}
				}else{
					if(path.isFile()){
						fileNames.add(path.getName());
						getFiles().add(path);
					}
				}
			}
		}
		return fileNames;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * Retrieves the file form files.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param name
	 * @return
	 * @throws FileReadException
	 * @since 0.01
	 */
	public File getFile(String name) throws FileReadException{
		for(File file : getFiles()){
			if( file.isFile() && name.equalsIgnoreCase(file.getName())){
				return file;
			}
		}
		throw new FileReadException("File not found: "+name);
	}

	/**
	 * @return the files
	 */
	public static List<File> getFiles() {
		return files;
	}

	/**
	 * @param files the files to set
	 */
	public static void setFiles(List<File> files) {
		FileService.files = files;
	}

	public static void writeToFile(String pathname, String content) throws IOException{
		File file = new File(pathname);

		// if file doesn't exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}
		FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
		BufferedWriter bw = new BufferedWriter(fw);
		bw.append(content);
		bw.flush();
		bw.close();
	}

	public static void writeToFile(String pathname, String content, boolean append) throws IOException {
		File file = new File(pathname);

		// if file doesn't exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}
		FileWriter fw = new FileWriter(file.getAbsoluteFile(), append);
		BufferedWriter bw = new BufferedWriter(fw);
		bw.append(content);
		bw.flush();
		bw.close();
	}
}
