/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.service;

import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;

import com.rgtc.itex.main.ITEx;

/**
 * <div class="jp">
 * 
 * </div>
 * <p>
 * <div class="en" style="padding:0em 0.5em" >
 * 
 * </div>
 *
 * @author Joven 'Bob' Montero
 * @version 0.01
 *		<div class="jp">
 *			
 *		</div>
 *		<div class="en" style="padding:0em 0.5em">
 *			
 *		</div>
 * @since 0.01
 */
public class CustomWebDriverListener extends AbstractWebDriverEventListener {
	/* (non-Javadoc)
	 * @see org.openqa.selenium.support.events.AbstractWebDriverEventListener#beforeClickOn(org.openqa.selenium.WebElement, org.openqa.selenium.WebDriver)
	 */
	@Override
	public void beforeClickOn(WebElement element, WebDriver driver) {
//		System.out.println("Before clicking element.");
//		System.out.println("Element: " + element);
//		System.out.println("Element Id: " + element.getAttribute("id"));
//		System.out.println("Element Class: " + element.getAttribute("class"));
//		System.out.println("Element Name: " + element.getAttribute("name"));

		// Sample Js execution and alert handling
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		js.executeScript("alert('Cick me!');");
//		try
//	    {
//	        Alert alert = driver.switchTo().alert();
//	        alert.accept();
//	    }
//	    catch ( Exception n)
//	    {
//	        return; 
//	    }
	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.support.events.AbstractWebDriverEventListener#onException(java.lang.Throwable, org.openqa.selenium.WebDriver)
	 */
	@Override
	public void onException(Throwable throwable, WebDriver driver) {
		super.onException(throwable, driver);
	}
}
