/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Global Technologies Corporation All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.rgtc.itex.models.ScenarioModel;
import com.rgtc.itex.models.TableColumnModel;
import com.rgtc.itex.models.TableModel;
import com.rgtc.itex.models.ValidatorModel;
import com.rgtc.itex.dao.DatabaseReader;
import com.rgtc.itex.dao.DatabaseWriter;
import com.rgtc.itex.util.UtilConstants;
import com.rgtc.itex.util.TransactionManager;

/**
 * <div class="jp">
 *
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 *
 * </div>
 *
 * @author Eldon Leuterio
 * @version 0.01
 * @since 0.01
 */
public class DatabaseValidator {

	/**
	 * The transaction manager to be used to access the DB.
	 */
	private TransactionManager transactionManager;

	/**
	 * The scenario map to be later filled with data from the Excel file.
	 */
	private Map<String, ScenarioModel> scenarioModelMap;

	/**
	 * The specific table models of the scenario model.
	 */
	private TableModel tableModel;

	/**
	 *
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Contains the possible types of error to be encountered in the class.
	 * </div>
	 *
	 * @author Emman Resmeros
	 * @version 0.01
	 * @since 0.01
	 */
	private enum Error {TABLE_ERROR, EMPTY_TABLE_ERROR};

	/**
	 * The initial column index used by ResultSetMetaData.
	 */
	private final static int INITIAL_COLUMN_INDEX = 1;

	/**
	 * The initial row value to replace zero.
	 */
	private final static int INITIAL_ROW_VALUE = 1;

	/**
	 * @return the transactionManager
	 */
	public TransactionManager getTransactionManager() {
		return transactionManager;
	}

	/**
	 * @param transactionManager the transactionManager to set
	 */
	public void setTransactionManager(TransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}

	/**
	 * @return the scenarioModelMap
	 */
	public Map<String, ScenarioModel> getScenarioModelMap() {
		return scenarioModelMap;
	}

	/**
	 * @param scenarioModelMap the scenarioModelMap to set
	 */
	public void setScenarioModelMap(Map<String, ScenarioModel> scenarioModelMap) {
		this.scenarioModelMap = scenarioModelMap;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Validates the ScenarioModelMap.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @return the validatorModel
	 * @throws SQLException
	 * @since 0.01
	 * @see DatabaseValidator#validateScenarioList(Map)
	 */
	public ValidatorModel validateScenarioModelMap(){
		ScenarioModel scenarioModel;
		ValidatorModel model = new ValidatorModel();
		String value = null;
		String scenarioName = null;
		Set<Entry<String, ScenarioModel>> set = scenarioModelMap.entrySet();
		for(Map.Entry<String, ScenarioModel> entry : set){
			boolean scenarioError = false;
			scenarioName = entry.getKey();
			scenarioModel = entry.getValue();
			//check before tables
			
			List<TableModel> bList = scenarioModel.getBeforeDataTables();
			for(int index=0; index<bList.size();index++){
				this.tableModel=null;
				this.tableModel = bList.get(index);
				String beforeMessage = validateTableModel();
				if(null != beforeMessage){
					value += scenarioModel.getScenarioName() + "\n" + beforeMessage;
					scenarioError = true;
				}
				//replace
				scenarioModel.getBeforeDataTables().set(index, tableModel);
			}
			//check After tables
			List<TableModel> aList = scenarioModel.getAfterDataTables();
			for(int index=0; index<aList.size();index++){
				this.tableModel=null;
				this.tableModel = aList.get(index);
				String afterMessage = validateTableModel();
				if(null != afterMessage){
					value += scenarioModel.getScenarioName() + "\n" + afterMessage;
					scenarioError = true;
				}
				//replace
				scenarioModel.getAfterDataTables().set(index, tableModel);
			}
			//replace
			scenarioModelMap.put(scenarioName, scenarioModel);
			scenarioModel = null;
			if(scenarioError){
				model.addScenario(scenarioName);
			}
		}
		if(scenarioModelMap.isEmpty()){
			value ="No scenarios found.";
		}else if(null == value){
			value ="correct";
		}
		model.setMessage(value);
		return model;
	}

	/**
	 *
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Validates the tablemodel taken from the Excel file if they fit
	 * the DB descriptions.
	 * </div>
	 * @maker Emman Resmeros
	 * @version 0.01
	 * @return the error message.
	 * @since 0.01
	 */
	private String validateTableModel(){
		String value = null;

		try {
			transactionManager.beginTransactional();
			Map<String, TableColumnModel> dbMap= DatabaseReader.getDatabaseTableMap(transactionManager,
					tableModel.getTableName());
			transactionManager.commitTransactional();
			int excelColSize = tableModel.getColModelList().size();
			int excelColCount = tableModel.getColCount();
			int dbColCount = dbMap.keySet().size();
			if(!dbMap.isEmpty() && (excelColCount <= dbColCount)){
				for(int index=0; index<excelColSize; index++){
					
					//validation should be skipped on dynamic characters
					if(!tableModel.getColModelList().get(index).getData().startsWith(UtilConstants.EMPTY_TAG) 
							&& !tableModel.getColModelList().get(index).getData().startsWith(UtilConstants.DYNAMIC_TAG)
							&& !tableModel.getColModelList().get(index).getData().startsWith(UtilConstants.NULL_TAG)){
						
						String columnName = tableModel.getColModelList().get(index).getName();
						//check if column name matches
						if(dbMap.containsKey(columnName.toUpperCase())){
							//then set the type
							TableColumnModel tcm = dbMap.get(columnName.toUpperCase());
//							String columnType = dbMap.get(columnName.toUpperCase());
//							System.out.println(columnName+" :: "+tcm.getType());
							if(tcm.getType().contains(UtilConstants.VARBINARY)){
								tcm.setType(UtilConstants.STRING);
//								columnType = Constants.STRING;
							}
							
							if(null == tableModel.getColModelList().get(index).getType()){
								tableModel.getColModelList().get(index).setType(tcm.getType());
							}
							
							
							//skip encrypt flag
							//skip date flag if modified.
							// skip length validation if data is modified -eldon 20140730
							if(tcm.getLength()<tableModel.getColModelList().get(index).getData().length() 
//									&& Constants.ENCRYPT_FLAG != tableModel.getColModelList().get(index).getType()){
									&& !tableModel.getColModelList().get(index).isModified()){
								value += "Unable to insert " + tableModel.getColModelList().get(index).getData() +
										" into " + tableModel.getTableName() + "." +columnName+". Length of Data is: "+
										tableModel.getColModelList().get(index).getData().length()+
										" while column length is: "+tcm.getLength()+".\n";
							}
							
						}else{
							value += "Column " + columnName +
									" is not found in table " +
									tableModel.getTableName() + ".\n";
						}
					}
				}
			}else if(excelColCount != dbColCount){
				
				value = "Table: "+tableModel.getTableName()+" column counts did not match. [DB]="+dbColCount+" [EXCEL]="+excelColCount+"\n";
			}else{
				value = "Table "+tableModel.getTableName() +
						" is not found or is empty.\n Excel Column count: " +
						excelColCount + " Database Column count: "+dbColCount +"\n";
			}
		} catch (SQLException e) {
			value = "Table "+tableModel.getTableName()+" is not found or is empty.\n";
		}
		return value;
	}

	/**
	 *
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Checks if after values is the same with the current database values.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param model the model in question.
	 * @return
	 * @throws SQLException
	 * @throws IOException 
	 * @since 0.01
	 */
	public String checkScenario(ScenarioModel model) throws SQLException, IOException{
		String value = "";
        List<String> statements = SQLWriter.getSelectStatementList(model);
//		List<String> statements = PropertiesUtil.getStatementsFromScenario(Constants.SCRIPT_FILE_PATH, model.getScenarioName(), Constants.SELECT_TYPE);
		List<List<TableColumnModel>> tableColumnData = new ArrayList<List<TableColumnModel>>();
		List<TableModel> tables = model.getAfterDataTables();

		//Get all column model for each table.
		int initIndex = 0;
		for(TableModel tableModel : tables) {
			int increment = tableModel.getColCount();

			//tableModel.getColModelList() is a linear list of all columns and row of each table.
			while (initIndex < tableModel.getColModelList().size()) {
				tableColumnData.add(tableModel.getColModelList().subList(initIndex, initIndex + increment));
				initIndex += increment;
			}
			if (0 >= tableModel.getColModelList().size()) {
				tableColumnData.add(new ArrayList<TableColumnModel>());
			}
			initIndex = 0;
		}

		int statementsCount = statements.size();
		String prevTableName = UtilConstants.EMPTY_STRING;
		int rowIndex = 0;
		//Loop through all the created SQL statements and retrieved column models.
		for(int index=0; index < statementsCount; index++) {
//			System.out.println(statements.get(index));
			ResultSet rs = DatabaseReader.retrieveFromDB(transactionManager, statements.get(index));
			ResultSetMetaData rsmd = rs.getMetaData();
			
			String tableName = rsmd.getTableName(INITIAL_COLUMN_INDEX);
			if (!prevTableName.equals(tableName)) {
				prevTableName = tableName;
				rowIndex = 0;
			}
			
			//eldon 8/20/2014
			if(statements.get(index).contains("row_count") && rs.next()){
				int rowCount = rs.getInt("row_count");
				if(0 != rowCount){
					//means the table is not empty; throw error
					value += generateMessage(Error.TABLE_ERROR, tableName, null, rowIndex);
					// Print failed check statement
					System.err.print("Check statement: " + statements.get(index));
					value += getDBSnapshot(statements.get(index));
				}
			}
			
			if (!rs.next() && statements.get(index).contains("=")) {
				value += "\n" + "Checking failed: " + generateMessage(Error.TABLE_ERROR, tableName, null, rowIndex);
				// Print failed check statement
				value += "Check statement: " + statements.get(index) + "\n";
				value += getDBSnapshot(statements.get(index));
			} else {
				if (tableColumnData.get(index).size() <= 0) {
					value += "\n" + "Checking failed: " + generateMessage(Error.EMPTY_TABLE_ERROR, tableName, null, rowIndex);
					// Include failed check statement
					value += "Check statement: " + statements.get(index) + "\n";
					value += getDBSnapshot(statements.get(index));
				}
			}

			rowIndex++;
			rs.close();
		}
		return value;
	}
	
	public String getDBSnapshot(String statement) throws SQLException{
		String[] columns = SQLWriter.simplifySelect(statement).split(UtilConstants.COMMA);
		ResultSet rs = DatabaseReader.retrieveFromDB(transactionManager, SQLWriter.simplifySelect(statement));
		ResultSetMetaData rsmd = rs.getMetaData();
		
//		StringBuffer colNames = new StringBuffer();
//		StringBuffer entries = new StringBuffer();
		List<List<String>> entryList = new ArrayList<List<String>>();
		List<String> headerList = new ArrayList<String>();
		boolean isInitRun = true;
		while(rs.next()){
			List<String> entry = new ArrayList<String>();
			if(!isInitRun){
				for(int j = 1;j< columns.length+1; j++){
					Object obj = rs.getObject(j);
					String objStr = String.valueOf(obj);
					String type = rsmd.getColumnTypeName(j);
					if (UtilConstants.TINYINT.equals(type)) {
						if ("true".equals(objStr)) {
							objStr = "1";
						} else if ("false".equals(objStr)) {
							objStr = "0";
						}
					}
					entry.add(objStr);
				}
			}else{
				for(int j = 1;j< columns.length+1; j++){
					Object obj = rs.getObject(j);
					String objStr = String.valueOf(obj);
					String colName = rsmd.getColumnName(j);
					String type = rsmd.getColumnTypeName(j);
					if (UtilConstants.TINYINT.equals(type)) {
						if ("true".equals(objStr)) {
							objStr = "1";
						} else if ("false".equals(objStr)) {
							objStr = "0";
						}
					}
					entry.add(objStr);
					headerList.add(colName);
					isInitRun = false;
				}
			}
			entryList.add(entry);
		}
		rs.close();
		int width[] = new int[headerList.size()];
		for (int i = 0; i < headerList.size(); i++) {
			width[i] = getMaxWidth(entryList, i);
		}

		return databasePrintOutput(headerList, entryList, width);
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param headerList
	 * @param entryList
	 * @param width
	 * @return 
	 * @since 0.01
	 */
	private String databasePrintOutput(
			List<String> headerList,
			List<List<String>> entryList,
			int[] width) {
		StringBuffer headerString = new StringBuffer();
		int totalWidth = 0;
		for (int i = 0; i < headerList.size(); i++) {
			if (i == 0) {
				headerString.append("|");
			}
			headerString.append(" ");
			int remainingSpace = 0;
			if (headerList.get(i).length() > width[i]) {
				width[i] = headerList.get(i).length();
			} else {
				remainingSpace = width[i] - headerList.get(i).length();
			}
			headerString.append(headerList.get(i));
			for (int j = 0; j < remainingSpace; j++) {
				headerString.append(" ");
			}
			headerString.append(" |");
		}
		totalWidth = headerString.length();
		StringBuffer divider = new StringBuffer();
		for (int i = 0; i < totalWidth; i++) {
			divider.append("-");
		}

		StringBuffer finalOutput = new StringBuffer();
		finalOutput
			.append(divider).append("\n")
			.append(headerString).append("\n")
			.append(divider).append("\n");

		for (List<String> entry : entryList) {
			for (int i = 0; i < entry.size(); i++) {
				if (i == 0) {
					finalOutput.append("|");
				}
				finalOutput.append(" ");
				int remainingSpace = 0;
				if (entry.get(i).length() < width[i]) {
					remainingSpace = width[i] - entry.get(i).length();
				}
				finalOutput.append(entry.get(i));
				for (int j = 0; j < remainingSpace; j++) {
					finalOutput.append(" ");
				}
				finalOutput.append(" |");
			}
			finalOutput.append("\n");
		}
		finalOutput.append(divider).append("\n");

		return finalOutput.toString();
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Get the max length for the specified column then return it as max width.
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param entryList
	 * @return
	 * @since 0.01
	 */
	private int getMaxWidth(List<List<String>> entryList, int column) {
		int maxWidth = 0;
		for (List<String> entry : entryList) {
			String entryValue = entry.get(column);
			if (maxWidth < entryValue.length()) {
				maxWidth = entryValue.length();
			}
		}
		return maxWidth;
	}

	/**
	 *
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Generates a message based on the parameters submitted.
	 * </div>
	 * @maker Emman Resmeros
	 * @version 0.01
	 * @param e The error type.
	 * @param tableName The name of the table in question.
	 * @param colName The name of the column in question.
	 * @param actualResult The result taken from the database.
	 * @param expectedResult The result taken from the Excel file.
	 * @return
	 * @since 0.01
	 */
	private String generateMessage(Error e, String tableName, String columnName, int rowIndex) {
		String result = "";

		switch(e) {
		case TABLE_ERROR:
			result = String.format("Row %d of Table %s was not found in the database.\n",
					rowIndex + INITIAL_ROW_VALUE, tableName.toUpperCase());
			break;
		case EMPTY_TABLE_ERROR:
			result = String.format("Database table %s is not empty\n", tableName.toUpperCase());
		}
		return result;
	}
	
	public String createSQLDump(String filePath, String fileName) throws Exception {
		String assertion = "";
		List<String> dbTableNames = new ArrayList<String>();
		List<String> selectAllStatements = new ArrayList<String>();
		List<String> insertStatements = new ArrayList<String>();
		File dumpDir = new File(filePath);
		if (!dumpDir.exists()) {
			dumpDir.mkdir();
		}
		DatabaseMetaData dbmd;
		try {
			dbmd = transactionManager.getConnection().getMetaData();
			ResultSet rs = dbmd.getTables(null, null, "%", null);
			
			while(rs.next()) {
				dbTableNames.add(rs.getString(3));
			}
			
			selectAllStatements = SQLWriter.getSelectAllStatementsList(dbTableNames);
			
			for (String sqlStatement : selectAllStatements) {
				ResultSet result = DatabaseReader.retrieveFromDB(transactionManager, sqlStatement);
				ResultSetMetaData rsmd = result.getMetaData();
				List<String> columnNames = new ArrayList<String>();
				List<String> data = new ArrayList<String>();
				List<String> types = new ArrayList<String>();
				String tableName = "";
				while (result.next()) {
					tableName = rsmd.getTableName(INITIAL_COLUMN_INDEX);
					for (int i = INITIAL_COLUMN_INDEX; i <= rsmd.getColumnCount(); i++) {
						columnNames.add(rsmd.getColumnName(i));
						String resultData = result.getString(rsmd.getColumnName(i));
						if (null == resultData) {
							resultData = UtilConstants.NULL_TAG;
						}
						data.add(resultData);
//						System.out.println("Column Type: " + rsmd.getColumnType(i));
						if (Types.VARCHAR == rsmd.getColumnType(i)) {
//							System.out.println("Type: " + Constants.STRING);
							types.add(UtilConstants.STRING);
						} else if (Types.INTEGER == rsmd.getColumnType(i)) {
//							System.out.println("Type: " + Constants.INTEGER);
							types.add(UtilConstants.INTEGER);
						} else if (Types.TINYINT == rsmd.getColumnType(i)) {
//							System.out.println("Type: " + Constants.TINYINT);
							types.add(UtilConstants.TINYINT);
						} else if (Types.BIT == rsmd.getColumnType(i)) {
//							System.out.println("Type: " + Constants.BIT);
							types.add(UtilConstants.BIT);
						}
					}
//					System.out.println("Table name: " + tableName);
//					System.out.println("Column names: " + columnNames);
//					System.out.println("Data: " + data);
//					System.out.println("Types: " + types);
					insertStatements.add(SQLWriter.getInsertStatementsList(tableName, columnNames, data, types));
					columnNames.clear();
					data.clear();
					types.clear();
					SQLWriter.createDumpFile(insertStatements, filePath, fileName);
				}
			}
		} catch (SQLException e) {
			assertion = "Error while trying to connect to the database.";
		} catch (IOException e) {
			assertion = "Error while trying to dump to file.";
		}
		
		return assertion;
	}

	public String createCoreSQLDump(
			String filePath,
			String fileName,
			String url,
			String username,
			String password) throws Exception {
		String assertion = "";
		List<String> dbTableNames = new ArrayList<String>();
		List<String> selectAllStatements = new ArrayList<String>();
		List<String> insertStatements = new ArrayList<String>();
		File dumpDir = new File(filePath);
		if (!dumpDir.exists()) {
			dumpDir.mkdir();
		}
		DatabaseMetaData dbmd;
		TransactionManager coreTransactionManager =
				new TransactionManager(url, "core", username, password);
		try {
			dbmd = coreTransactionManager.getConnection().getMetaData();
			ResultSet rs = dbmd.getTables(null, null, "%", null);

			while(rs.next()) {
				dbTableNames.add(rs.getString(3));
			}

			selectAllStatements = SQLWriter.getSelectAllStatementsList(dbTableNames);

			for (String sqlStatement : selectAllStatements) {
				ResultSet result = DatabaseReader.retrieveFromDB(coreTransactionManager, sqlStatement);
				ResultSetMetaData rsmd = result.getMetaData();
				List<String> columnNames = new ArrayList<String>();
				List<String> data = new ArrayList<String>();
				List<String> types = new ArrayList<String>();
				String tableName = "";
				while (result.next()) {
					tableName = "core." + rsmd.getTableName(INITIAL_COLUMN_INDEX);
					for (int i = INITIAL_COLUMN_INDEX; i <= rsmd.getColumnCount(); i++) {
						columnNames.add(rsmd.getColumnName(i));
						String resultData = result.getString(rsmd.getColumnName(i));
						if (null == resultData) {
							resultData = UtilConstants.NULL_TAG;
						}
						data.add(resultData);
						if (Types.VARCHAR == rsmd.getColumnType(i)) {
							types.add(UtilConstants.STRING);
						} else if (Types.INTEGER == rsmd.getColumnType(i)) {
							types.add(UtilConstants.INTEGER);
						} else if (Types.TINYINT == rsmd.getColumnType(i)) {
							types.add(UtilConstants.TINYINT);
						} else if (Types.BIT == rsmd.getColumnType(i)) {
							types.add(UtilConstants.BIT);
						}
					}
					insertStatements.add(SQLWriter.getInsertStatementsList(tableName, columnNames, data, types));
					columnNames.clear();
					data.clear();
					types.clear();
					SQLWriter.createDumpFile(insertStatements, filePath, fileName);
				}
			}
		} catch (Exception e) {
			assertion = "Error while trying to connect to the database.";
		}
		return assertion;
	}
	
	public String loadSQLDump(String filePath, String fileName) {
		String assertion = "";
		
		List<String> statements = new ArrayList<String>();
		List<String> dbTableNames = new ArrayList<String>();
		List<String> truncateStatements = new ArrayList<String>();
		File file = new File (filePath + fileName);
		
		BufferedReader bf = null;
		DatabaseMetaData dbmd;
		try {
			dbmd = transactionManager.getConnection().getMetaData();
			ResultSet rs = dbmd.getTables(null, null, "%", null);
			
			while (rs.next()) {
				dbTableNames.add(rs.getString(3));
			}
			
			truncateStatements = SQLWriter.getTruncateStatementsList(dbTableNames);
			transactionManager.beginTransactional();
			DatabaseWriter.insertToDB(transactionManager, truncateStatements);
			transactionManager.commitTransactional();
			
			bf = new BufferedReader(new FileReader(file));
			
			String line = null;
			line = bf.readLine();
			
			while(line != null) {
				statements.add(line);
				line = bf.readLine();
			}
			
			bf.close();

			transactionManager.beginTransactional();
			DatabaseWriter.insertToDB(transactionManager, statements);
			transactionManager.commitTransactional();

			transactionManager.endTransactional();
		} catch (FileNotFoundException e) {
			assertion = "Error while trying to open file: File not found.";
		} catch (IOException e) {
			assertion = "Error while reading file contents.";
		} catch (SQLException e) {
			try {
				transactionManager.rollbackTransaction();
				assertion = "Error while trying to connect to the database.";
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return assertion;
	}


	
	public String loadCoreSQLDump(
			String filePath,
			String fileName,
			String url,
			String username,
			String password) throws Exception {
		String assertion = "";
		
		List<String> statements = new ArrayList<String>();
		List<String> dbTableNames = new ArrayList<String>();
		List<String> truncateStatements = new ArrayList<String>();
		File file = new File (filePath + fileName);
		
		BufferedReader bf = null;
		DatabaseMetaData dbmd;
		TransactionManager coreTransactionManager =
				new TransactionManager(url, "core", username, password);
		try {
			dbmd = coreTransactionManager.getConnection().getMetaData();
			ResultSet rs = dbmd.getTables(null, null, "%", null);
			
			while (rs.next()) {
				dbTableNames.add(rs.getString(3));
			}
			
			truncateStatements = SQLWriter.getTruncateStatementsList(dbTableNames);
			coreTransactionManager.beginTransactional();
			DatabaseWriter.insertToDB(coreTransactionManager, truncateStatements);
			coreTransactionManager.commitTransactional();
			
			bf = new BufferedReader(new FileReader(file));
			
			String line = null;
			line = bf.readLine();
			
			while(line != null) {
				statements.add(line);
				line = bf.readLine();
			}
			
			bf.close();

			coreTransactionManager.beginTransactional();
			DatabaseWriter.insertToDB(coreTransactionManager, statements);
			coreTransactionManager.commitTransactional();

			coreTransactionManager.endTransactional();
		} catch (FileNotFoundException e) {
			assertion = "Error while trying to open file: File not found.";
		} catch (IOException e) {
			assertion = "Error while reading file contents.";
		} catch (SQLException e) {
			try {
				coreTransactionManager.rollbackTransaction();
				assertion = "Error while trying to connect to the database.";
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return assertion;
	}
}
