/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.rgtc.itex.models.ResponseModel;
import com.rgtc.itex.util.UtilConstants;

/**
 *
 * @author Joven 'Bob' Montero
 * @version 0.01
 * @since 0.01
 */
public class HTTPService {
	/**
	 *
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 */
	private static CloseableHttpClient client = HttpClients.createDefault();

	/**
	 * The function for executing HTTP related requests.
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param requestURL 		- The HTTP request URL.
	 * @param requestType 		- The HTTP request type (GET, POST, PUT, DELETE).
	 * @param requestHeader 	- The headers for the HTTP request.
	 * @param requestBody 		- The payload for the HTTP request.
	 * 							  {'type': 'form', 'data': []}
	 * 							  {'type': 'json', 'data': {}}
	 * @param requestURLParams 	- The parameters in the URL to be mapped.
	 * @return ResponseModel
	 * @throws Exception
	 * @since 0.01
	 */
	public static ResponseModel executeRequest(
			String requestURL,
			int requestType,
			JSONObject requestHeader,
			JSONObject requestBody,
			JSONObject requestURLParams) throws Exception {
		//System.out.println("[HTTPService]: executeRequest started.");
		//System.out.println("[HTTPService]: URL: " + requestURL);

		ResponseModel output = new ResponseModel();

		// TODO Map the request parameters into the url
		URI url = new URI(requestURL);
		output.setRequestURL(url);

		// Construct the appropriate HTTP method type
		HttpUriRequest request = null;
		switch (requestType) {
		case UtilConstants.HTTP_GET_METHOD:
			output.setRequestType("GET");
			request = doGet(url, requestHeader, requestBody);
			break;
		case UtilConstants.HTTP_POST_METHOD:
			output.setRequestType("POST");
			request = doPost(url, requestHeader, requestBody);
			break;
		case UtilConstants.HTTP_PUT_METHOD:
			output.setRequestType("PUT");
			request = doPut(url, requestHeader, requestBody);
			break;
		case UtilConstants.HTTP_DELETE_METHOD:
			output.setRequestType("DELETE");
			request = doDelete(url, requestHeader, requestBody);
			break;

		default:
			break;
		}

		// Execute the HTTP request
		CloseableHttpResponse response = client.execute(request);
		output.setMessage(EntityUtils.toString(response.getEntity()));
		output.setStatusCode(response.getStatusLine().getStatusCode());
		response.close();

		//System.out.println("[HTTPService]: executeRequest finished.");
		return output;
	}

	/**
	 *
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param requestURL
	 * @param requestHeader
	 * @param requestBody
	 * @return
	 * @since 0.01
	 */
	private static HttpGet doGet(URI requestURL, JSONObject requestHeader, JSONObject requestBody) {

		HttpGet output = new HttpGet(requestURL);
		if (null != requestHeader) {
			Set<String> jsonKeys = requestHeader.keySet();
			for (String key : jsonKeys) {
				output.setHeader(key, requestHeader.getString(key));
			}
		} else {
			throw new AssertionError("[]:\n"
					+ "Incomplete data: "
					+ "No request headers found.");
		}

		return output;
	}

	/**
	 *
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param requestURL
	 * @param requestHeader
	 * @param requestBody
	 * @return HttpPost
	 * @throws Exception
	 * @since 0.01
	 */
	public static HttpPost doPost(URI requestURL, JSONObject requestHeader, JSONObject requestBody) throws Exception {
		//System.out.println("[HTTPService]: doPost started.");

		HttpPost output = new HttpPost(requestURL);
		if (null != requestHeader) {
			Set<String> jsonKeys = requestHeader.keySet();
			for (String key : jsonKeys) {
				output.setHeader(key, requestHeader.getString(key));
			}
		} else {
			throw new AssertionError("[]:\n"
					+ "Incomplete data: "
					+ "No request headers found.");
		}

		if (null  == requestBody) {
			//System.out.println("[HTTPService]: doPost finished.");
			return output;
		}
		if (requestBody.getString("type").equals("form")) {
			List<NameValuePair> formData = new ArrayList<NameValuePair>();
			//System.out.println("[HTTPService]: doPost preparing form payload.");
			JSONArray params = requestBody.getJSONArray("data");
			for (int i = 0; i < params.length(); i++) {
				JSONObject param = params.getJSONObject(i);
				Set<String> jsonKeys = param.keySet();
				for (String key : jsonKeys) {
					formData.add(new BasicNameValuePair(key, param.getString(key)));
				}
			}
			output.setEntity(new UrlEncodedFormEntity(formData, "UTF-8"));
		} else if (requestBody.getString("type").equals("json")) {
			//System.out.println("[HTTPService]: doPost preparing json payload.");
			StringEntity payloadEntity = new StringEntity(requestBody.get("data").toString());
			output.setEntity(payloadEntity);
		}

		//System.out.println("[HTTPService]: doPost finished.");
		return output;
	}

	/**
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param requestURL
	 * @param requestHeader
	 * @param requestBody
	 * @return
	 * @since 0.01
	 */
	private static HttpPut doPut(URI requestURL, JSONObject requestHeader,
			JSONObject requestBody) {

		HttpPut output = new HttpPut(requestURL);
		if (null != requestHeader) {
			Set<String> jsonKeys = requestHeader.keySet();
			for (String key : jsonKeys) {
				output.setHeader(key, requestHeader.getString(key));
			}
		} else {
			throw new AssertionError("[]:\n"
					+ "Incomplete data: "
					+ "No request headers found.");
		}

		return output;
	}

	/**
	 *
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param requestURL
	 * @param requestHeader
	 * @param requestBody
	 * @return
	 * @since 0.01
	 */
	private static HttpDelete doDelete(URI requestURL, JSONObject requestHeader,
			JSONObject requestBody) {

		HttpDelete output = new HttpDelete(requestURL);
		if (null != requestHeader) {
			Set<String> jsonKeys = requestHeader.keySet();
			for (String key : jsonKeys) {
				output.setHeader(key, requestHeader.getString(key));
			}
		} else {
			throw new AssertionError("[]:\n"
					+ "Incomplete data: "
					+ "No request headers found.");
		}

		return output;
	}
}
