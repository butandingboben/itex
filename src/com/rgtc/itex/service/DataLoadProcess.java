/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Global Technologies Corporation All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.json.JSONObject;

import com.rgtc.itex.exceptions.DataLoaderException;
import com.rgtc.itex.models.ScenarioModel;
import com.rgtc.itex.models.TableColumnModel;
import com.rgtc.itex.models.TableModel;
import com.rgtc.itex.util.UtilConstants;
import com.rgtc.itex.util.Converter;
import com.rgtc.itex.util.TransactionManager;

/**
 * <div class="jp">
 *
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 *
 * </div>
 *
 * @author Eldon Leuterio
 * @version 0.01
 * @since 0.01
 */
public class DataLoadProcess {

	/**
	 *
	 *
	 */
	private static TransactionManager transactionInstance;
	/**
	 *
	 *
	 */
	private Workbook workbook;
	/**
	 *
	 *
	 */
	private Sheet sheet;
	/**
	 * A map of all scenarios. </br>
	 * key = scenario title.
	 * value = ScenarioModel.
	 */
	private Map<String, ScenarioModel> scenarioModelMap;
	/**
	 *
	 *
	 */
	private ScenarioModel scenarioModel;
	/**
	 *
	 *
	 */
	private TableModel tableModel;
	/**
	 *
	 */
	private List<String> columNames;
	
	private TreeMap<String,JSONObject> dynamicJsonParams;

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Loads the Workbook.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param file The file to be loaded.
	 * @since 0.01
	 */
	public String loadWorkbook(File file) {
		String mes = null;
		try {
			FileInputStream fis = new FileInputStream(file);
			workbook = WorkbookFactory.create(fis);
			fis.close();
			
		} catch (InvalidFormatException e) {
			mes = e.toString();
		} catch(IOException e){
			mes = e.toString();
		}
		return mes;
	}
	
	public Workbook getLoadedWorkbook() {
		return this.workbook;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Reloads the workbook to refelct changes.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param file The file to reload.
	 * @since 0.01
	 */
	public String reloadWorkbook(File file) {
		workbook = null;
		workbook = null;
		sheet = null;
		scenarioModelMap = null;
		scenarioModel = null;
		tableModel  = null;
		columNames = null;
		return loadWorkbook(file);
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Retrieves the sheet names from the workbook.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param workBook
	 * @return sheetNames
	 * @throws InvalidFormatException
	 * @throws IOException
	 * @since 0.01
	 */
	public List<String> getSheetNames() {
		List<String> sheetNames = new ArrayList<String>();
		if(null != workbook){
			for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
				sheetNames.add(workbook.getSheetAt(i).getSheetName());
			}
		}
		return sheetNames;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Sets the sheet based on sheet name.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param sheetName The sheet.
	 * @since 0.01
	 * @see Sheet
	 */
	public void setSheet(String sheetName) {
		if(null != workbook){
			for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
				if(sheetName.equals(workbook.getSheetAt(i).getSheetName())){
					sheet = workbook.getSheetAt(i);
				}
			}
		}
	}
	
	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Identify if "Data" sheet exists on the list of sheets on the excel file.
	 * </div>
	 * @maker Vincent Racaza
	 * @version 0.01
	 * @param sheetName The sheet.
	 * @return boolean
	 * @since 0.01
	 * @see Sheet
	 */
	public boolean isDataSheetExist(String sheetName) {
		boolean isExist = false;
		if(null != workbook){
			for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
				if(sheetName.equals(workbook.getSheetAt(i).getSheetName())){
					sheet = workbook.getSheetAt(i);
					isExist = true;
				}
			}
		}
		return isExist;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Returns a list of Scenario names from the sheet.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param sheet
	 * @return List<String>
	 * @throws DataLoaderException 
	 * @since 0.01
	 * @see Sheet
	 */
	public List<String> getScenarioNames() throws DataLoaderException {
		//scenarios should be populated prior to this method call.
		List<String> scenarioList = new ArrayList<String>();
		if(null == scenarioModelMap){
			throw new DataLoaderException("Please initialize scenarioModelMap before retrieving scenario names");
		}else{
			for(String title: scenarioModelMap.keySet()){
				scenarioList.add(title);
			}
		}
		Collections.sort(scenarioList);
		return scenarioList;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Retrieves the list of table names from the scenario.</br>
	 * These names will be used to view insert and delete list.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param scenarioTitle The title of the scenario.
	 * @return Map<String, List<String>>
	 * @since 0.01
	 */
	public Map<String, List<String>> getTableNames(String scenarioTitle) {
		ScenarioModel scenarioModel = scenarioModelMap.get(scenarioTitle);
		List<String> beforeTableList = new ArrayList<String>();
		List<String> afterTableList = new ArrayList<String>();
		Iterator<TableModel> i1 = scenarioModel.getBeforeDataTables().iterator();
		Iterator<TableModel> i2 = scenarioModel.getAfterDataTables().iterator();
		while(i1.hasNext()){
			beforeTableList.add(i1.next().getTableName());
		}
		while(i2.hasNext()){
			afterTableList.add(i2.next().getTableName());
		}
		Map<String, List<String>> tables = new HashMap<String,List<String>>();
		tables.put(UtilConstants.BEFORE, beforeTableList);
		tables.put(UtilConstants.AFTER, afterTableList);

		return tables;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Retrieves a String representation of a cell specified by the cellIndex.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param row The row.
	 * @param cellIndex The cell index.
	 * @return String
	 * @since 0.01
	 */
	public String getCellStringFromRow(Row row, int cellIndex) {
		String value = null;
		Cell cell = row.getCell(cellIndex, Row.RETURN_BLANK_AS_NULL);
		if(null != cell){
			switch(cell.getCellType()){

			case Cell.CELL_TYPE_STRING:
				value = cell.getStringCellValue();
				break;

			case Cell.CELL_TYPE_NUMERIC:
				Double d = cell.getNumericCellValue();
				value = String.valueOf(d.intValue());
				break;

			default:
			}
		}
		return value;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Loads the sheet for processing and populates scenarios map.</br>
	 * Scenarios map contains the loaded sheet.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @since 0.01
	 */
	public void populateScenariosMap() {
		scenarioModelMap = new HashMap<String, ScenarioModel>();
		int minRowIx = sheet.getFirstRowNum();
		int maxRowIx = sheet.getLastRowNum();

		String scenarioName = null;
		boolean startReadBeforeFlag = false;
		boolean startReadAfterFlag = false;
		String tableName = null;

		for(int rownum = minRowIx; rownum<=maxRowIx; rownum++) {
			Row row = sheet.getRow(rownum);
			//check if populated cells in a row is 1. this is probably a header
			//row.getPhysicalNumberOfCells()
			if(null != row && null != getCellStringFromRow(row, UtilConstants.COLUMN_SCENARIO_INDEX)){
				String value = getCellStringFromRow(row, UtilConstants.COLUMN_SCENARIO_INDEX);
				if(value.contains(UtilConstants.SCENARIO_NAME)){
					scenarioName = value.replace(UtilConstants.SCENARIO_NAME, "");
//					System.out.println("Reading: "+scenarioName);
					scenarioModel = new ScenarioModel();
					scenarioModel.setScenarioName(scenarioName);
					scenarioModel.setBeforeDataTables(new ArrayList<TableModel>());
					scenarioModel.setAfterDataTables(new ArrayList<TableModel>());
				}else if(value.equalsIgnoreCase(UtilConstants.DATABASE_READ_BEFORE_START)){
					startReadBeforeFlag = true;
					startReadAfterFlag = false;
				}else if(value.contains(UtilConstants.DATABASE_TABLE_NAME)){
					tableName = value.replace(UtilConstants.DATABASE_TABLE_NAME, "");
//					System.out.println("Reading: "+tableName);
					tableModel = new TableModel();
					tableModel.setTableName(tableName);
					tableModel.setColModelList(new ArrayList<TableColumnModel>());
				}else if(value.equalsIgnoreCase(UtilConstants.DATABASE_READ_BEFORE_END)){
					startReadBeforeFlag = false;
					startReadAfterFlag = false;
				}else if(value.equalsIgnoreCase(UtilConstants.DATABASE_READ_AFTER_START)){
					startReadAfterFlag = true;
					startReadBeforeFlag = false;
				}else if(value.equalsIgnoreCase(UtilConstants.DATABASE_READ_AFTER_END)){
					startReadBeforeFlag = false;
					startReadBeforeFlag = false;
				}else if(value.equalsIgnoreCase(UtilConstants.SCENARIO_END)){
					scenarioModelMap.put(scenarioName, scenarioModel);
					scenarioName = null;
				}else if(UtilConstants.C_TAG.equalsIgnoreCase(
						getCellStringFromRow(row, UtilConstants.COLUMN_INDEX_0))){
					setTableColumns(row);
					tableModel.setColCount(columNames.size());
				}else if(UtilConstants.E_TAG.equalsIgnoreCase(
						getCellStringFromRow(row, UtilConstants.COLUMN_INDEX_0))){
					setTableEntries(row);
				} else if(UtilConstants.ET_TAG.equalsIgnoreCase(
						getCellStringFromRow(row, UtilConstants.COLUMN_INDEX_0))) {
					setTableColumns(row);
					tableModel.setColCount(columNames.size());
					if(startReadBeforeFlag){
						scenarioModel.addBeforeDataTable(tableModel);
					}else if(startReadAfterFlag){
						scenarioModel.addAfterDataTable(tableModel);
					}
					tableName = null;
					tableModel = null;
				}else if(UtilConstants.EE_TAG.equalsIgnoreCase(
						getCellStringFromRow(row, UtilConstants.COLUMN_INDEX_0))){
					setTableEntries(row);
					if(startReadBeforeFlag){
						scenarioModel.addBeforeDataTable(tableModel);
					}else if(startReadAfterFlag){
						scenarioModel.addAfterDataTable(tableModel);
					}
					tableName = null;
					tableModel = null;
				}
			}
		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker
	 * @version 0.01
	 * @param row The row.
	 * @since 0.01
	 */
	public void setTableColumns(Row row) {
		columNames = new ArrayList<String>();
		Iterator<Cell> cellIterator= row.cellIterator();
		while(cellIterator.hasNext()){
			Cell cell = cellIterator.next();
			String value = cell.getStringCellValue();
			if(UtilConstants.EMPTY_STRING != value 
					&& !UtilConstants.C_TAG.equalsIgnoreCase(value)
					&& !UtilConstants.ET_TAG.equalsIgnoreCase(value)){
				columNames.add(cell.getStringCellValue());
			}
		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Retrieves table entries from a row.</br>
	 * TableNames should be populated prior to this method call.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param row The row.
	 * @since 0.01
	 */
	public void setTableEntries(Row row) {
//		System.out.println("row: "+row.getRowNum());
		Iterator<Cell> cellIterator= row.cellIterator();
		int idx=0;
		while(cellIterator.hasNext()){
			Cell cell = cellIterator.next();
			String cellValue;
			if(Cell.CELL_TYPE_NUMERIC == cell.getCellType()){
//				System.out.println("Double to String: "+ String.valueOf((int) cell.getNumericCellValue()));
//				cellValue = String.valueOf((int) cell.getNumericCellValue());
				// Date: 2015-05-20
				// By: Joven Montero
				// Fix for numeric values with floating values were casted as int
				cellValue = String.valueOf(cell.getNumericCellValue());
			}else{
				cellValue = "";
				try {
					cellValue = cell.getStringCellValue();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if(UtilConstants.EMPTY_STRING != cellValue
					&& !UtilConstants.E_TAG.equalsIgnoreCase(cellValue)
					&& !UtilConstants.EE_TAG.equalsIgnoreCase(cellValue)){
				TableColumnModel model = new TableColumnModel();
				
				// eldon 2014/07/16
				// Change log: added automatic encryption for columns
				// that has encrypt flag (denoted by *).
				// The data under this column will be encrypted using
				// hash base 64. As of now, the key is hard coded in
				// the constants file.
				String columnName = columNames.get(idx);
				String data = Converter.cleanString(cellValue);
//				if (data.equals("3006")) {
//					System.out.println(columnName);
//					System.out.println(data);
//				}
				if(columnName.contains(UtilConstants.ENCRYPT_FLAG)){
					String cleanColumnName = columnName.replace(UtilConstants.ENCRYPT_FLAG, "");
					//defaulting the value
					String encryptedData = data;
					try {

//						encryptedData = Encryption.encrpytedHash(Constants.ENCRYPTION_KEY, data);
						encryptedData = getEncryptionScript(encryptedData, cell);

					} catch (Exception e) {
						System.out.println("ERROR: ENCRYPTION FAILED, DEFAULTING VALUE.");
						e.printStackTrace();
					}
					model.setName(cleanColumnName);
					model.setData(encryptedData);
					model.setModified(true);
					model.setType(UtilConstants.ENCRYPT_FLAG);

				}else if (columnName.contains(UtilConstants.DATE_FLAG)){
					// Added functionality to automatically compute the date to be inserted
					String cleanColumnName = columnName.replace(UtilConstants.DATE_FLAG, "");
					model.setName(cleanColumnName);
					
					String cleanData = SQLWriter.getFormulatedDate(data);
					model.setData(cleanData);
					
					if(String.valueOf(data) != String.valueOf(cleanData)){
						//set to true if and only if the data was modified.
						model.setModified(true);
						model.setType(UtilConstants.DATE_FLAG);
					}else{
						//retain original value
						model.setModified(false);
						model.setType(data);
					}
					
					
					
				}else{
					model.setName(columnName);
					model.setData(data);
					model.setModified(false);
				}
									
//				try {
				tableModel.addColumnModel(model);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
				idx++;
			}
		}
	}
	
	/**
	 * Ugly implementation.
	 */
	public String getEncryptionScript(String data, Cell type){
		// For handling tags
		if (UtilConstants.EMPTY_TAG.equals(data)) {
			data = "";
		}
		// Handling NULL tag.
//		else if (Constants.NULL_TAG.equals(data)) {
//			return Constants.NULL;
//		}
		
		String prefix = "HEX(AES_ENCRYPT(";
		String suffix = "))";
		Converter.toInsertString(data);
		Converter.toInsertString(UtilConstants.ENCRYPTION_KEY);
		
		String result = prefix;
		if(Cell.CELL_TYPE_NUMERIC == type.getCellType()){
			result = result + data;
		}else{
			result = result + Converter.toInsertString(data);
		}
		result = result + ","+Converter.toInsertString(UtilConstants.ENCRYPTION_KEY) + suffix;
		
//		System.out.println("result: "+result);
		return result;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker
	 * @version 0.01
	 * @param row The row.
	 * @since 0.01
	 */
	public void setTableModelName(Row row) {
		String tableName = getCellStringFromRow(row, UtilConstants.COLUMN_SCENARIO_INDEX);
		tableModel.setTableName(tableName);
		tableModel.setColModelList(new ArrayList<TableColumnModel>());
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Initialize TransactionManager Instance.
	 * </div>
	 * @maker Eldon Leuterio
	 * @param url The url.
	 * @param databaseName The database name.
	 * @param userName The user name.
	 * @param password The password.
	 * @version 0.01
	 * @throws Exception 
	 * @throws IOException 
	 * @throws SQLException 
	 * @since 0.01
	 */
	public void setDatabasePreferences(String url, String databaseName, String userName, String password) throws SQLException, IOException, Exception {
		transactionInstance = null;
		if(null == transactionInstance){
				transactionInstance = new TransactionManager(url, databaseName, userName, password);
		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker
	 * @version 0.01
	 * @param scenarioName The scenario name.
	 * @return ScenarioModel
	 * @since 0.01
	 */
	public ScenarioModel getScenario(String scenarioName) {
		return scenarioModelMap.get(scenarioName);
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker
	 * @version 0.01
	 * @throws SQLException The exception to be thrown.
	 * @since 0.01
	 */
	public void endProcess() throws SQLException {
		transactionInstance.endNonTransactional();
	}

	/**
	 * @return the transactionInstance
	 */
	public static TransactionManager getTransactionInstance() {
		return transactionInstance;
	}

	/**
	 * @return the scenarioModelMap
	 */
	public Map<String, ScenarioModel> getScenarioModelMap() {
		return scenarioModelMap;
	}
	
	/**
	 * Loads the parameters of jsonBased input.
	 * @param sheetName
	 */
	public void loadDynamicJsonParams(String sheetName){
		Sheet paramInputSheet = workbook.getSheet(sheetName);
		int minRowIx = paramInputSheet.getFirstRowNum();
		int maxRowIx = paramInputSheet.getLastRowNum();
		
		String testCaseNo = null;
		JSONObject jObj = null;
		
		for(int rownum = minRowIx; rownum<=maxRowIx; rownum++) {
			Row row = paramInputSheet.getRow(rownum);
			if(null !=row && null != getCellStringFromRow(row, UtilConstants.COLUMN_B)){
				String value = getCellStringFromRow(row, UtilConstants.COLUMN_B);
				if(UtilConstants.PARAM_INPUT_TESTCASENO.equalsIgnoreCase(value)){
					//start creation of entry
					testCaseNo = getCellStringFromRow(row, UtilConstants.COLUMN_C);
					jObj = new JSONObject();
					// Added NA attribute for NA test cases
					if (null != getCellStringFromRow(row, UtilConstants.COLUMN_D)) {
						jObj.put(getCellStringFromRow(row, UtilConstants.COLUMN_D), "YES");
					}
				}else if(UtilConstants.PARAM_INPUT_TESTCASEEND.equalsIgnoreCase(value)){
					//end creation. finalize and reset
					if(null == dynamicJsonParams){
						//initialize
						dynamicJsonParams = new TreeMap<String, JSONObject>();
					}
					dynamicJsonParams.put(testCaseNo, jObj);
					testCaseNo = null;
					jObj = null;
				}else{
					// populate JSON
					String jKey = getCellStringFromRow(row, UtilConstants.COLUMN_D);
					String jVal = getCellStringFromRow(row, UtilConstants.COLUMN_C);
					if(null != jKey && null !=jVal){
						jObj.put(jKey, jVal);
					}
				}
			}
		}		
	}
	
	public TreeMap<String, JSONObject> getDynamicParams(){
		return dynamicJsonParams;
	}
}
