/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.service;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.json.JSONObject;

import com.rgtc.itex.util.UtilConstants;
import com.rgtc.itex.util.JSONExcelUtil;
import com.rgtc.itex.util.PropertiesUtil;

/**
 * <div class="jp">
 *
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 * Class for generating JSON statements and saving it to properties file.
 * </div>
 *
 * @author Vincent Racaza
 * @version 0.01
 * @since 0.01
 */
public class JSONScript {
	
	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * This method is used to generate JSON Script.
	 * </div>
	 * @maker Vincent Racaza
	 * @version 0.01
	 * @param filePath The path of the property file.
	 * @param jsonExcelUtil The utility to read and get the json statements from the excel file.
	 * @return String The message
	 * @since 0.01
	 */
	public String generateJSONScript(String filePath, JSONExcelUtil jsonExcelUtil) {
		String newFilePath = filePath.concat("\\"+UtilConstants.JSON_FILENAME);
		File file = new File(newFilePath);
		
		String message = null;
			
		try{
			if(!file.exists()){
				file.createNewFile();
			}
			generateStatements(newFilePath, jsonExcelUtil.getDataJSONList(), "_DATA");
			generateStatements(newFilePath, jsonExcelUtil.getExpectedJSONList(), "_EXPECTED");
			
		}catch(IOException e){
			message = e.toString();
		}
		
		return message;
	}
	
	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * This method is used to find a key in the property file and delete or add the key-value json object.
	 * </div>
	 * @maker Vincent Racaza
	 * @version 0.01
	 * @param filePath The path of the property file.
	 * @param jsonObject Contains the key and value.
	 * @param keyAppend The string to append on the key.
	 * @since 0.01
	 */
	private void generateStatements(String filePath, Map<String, JSONObject> jsonObject, String keyAppend) throws IOException{
		for (Map.Entry<String, JSONObject> entry : jsonObject.entrySet()) {
			String key = entry.getKey().concat(keyAppend);
		    JSONObject value = entry.getValue(); 
			try{
				PropertiesUtil.removeStatementsFromScenario(filePath, key, null);
			}catch(IOException e){
				e.printStackTrace();
			}
//			System.out.println("key: " +key);
			PropertiesUtil.set(filePath, key.replace(' ', '_'), value.toString());
		}
		
	}
	
}
