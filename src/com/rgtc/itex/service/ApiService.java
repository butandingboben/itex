/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.service;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Set;

import org.apache.http.annotation.NotThreadSafe;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.rgtc.itex.constants.Constants;
import com.rgtc.itex.constants.Setup;
import com.rgtc.itex.models.ResponseModel;

/**
 * <div class="jp">
 *
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 *
 * </div>
 *
 * @author Eldon Leuterio
 * @version 0.01
 * @since 0.01
 */
public class ApiService {

//	private HttpClient client = HttpClientBuilder.create().setMaxConnPerRoute(5).setMaxConnTotal(5).build();
//	private ExecutorService exec = Executors.newFixedThreadPool(5);
//	private FutureRequestExecutionService future = new FutureRequestExecutionService(client, exec);
//
//	private ResponseHandler<Boolean> handler = new ResponseHandler<Boolean>() {
//		@Override
//		public Boolean handleResponse(HttpResponse response)
//				throws ClientProtocolException, IOException {
//			return response.getStatusLine().getStatusCode() == 200;
//		}
//	};

	CloseableHttpClient client = HttpClients.createDefault();

	/**
	 * <div class="jp">
	 * Initialize task queue for datastore.
	 * url: /job/operation_logs/append
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param url
	 * @throws Exception
	 * @since 0.01
	 */
	public void queueProcess(String url) throws Exception {
		HttpGet request = null;
		if(null == url){
			request = new HttpGet("http://localhost:8888/job/operation_logs/append");
		}else{
			request = new HttpGet(url);
		}

		int timeout = 10;
		RequestConfig config = RequestConfig.custom()
				.setConnectTimeout(timeout * 1000)
				.setConnectionRequestTimeout(timeout * 1000)
				.setSocketTimeout(timeout * 1000).build();
		CloseableHttpClient queueClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
		CloseableHttpResponse response = queueClient.execute(request);
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param testModule
	 * @param requestHeaderJSON
	 * @param inputJSON
	 * @param urlParameterJSON
	 * @return ReponseModel
	 * @throws IOException
	 * @throws URISyntaxException
	 * @since 0.01
	 */
	public ResponseModel executeRequest(
			String testModule,
			JSONObject requestHeaderJSON,
			JSONObject inputJSON,
			JSONObject urlParameterJSON,
			File file) throws IOException, URISyntaxException {
		ResponseModel output = new ResponseModel();

		String testURL = "";
		if(null == urlParameterJSON){
			testURL = Setup.getPath(file.getName());
		}else{
			testURL = urlParameterJSON.getString(Constants.URL_PARAMETER);
		}

		URI requestURL = new URI(Constants.REQUEST_SCHEME,
				null,
				Constants.REQUEST_HOST,
				Constants.REQUEST_PORT,
				testURL,
				null,
				null);

		if (testURL.contains("?")
				&& 1 < testURL.split("\\?").length) {
			requestURL = new URI(Constants.REQUEST_SCHEME,
					null,
					Constants.REQUEST_HOST,
					Constants.REQUEST_PORT,
					testURL.split("\\?")[0],
					testURL.split("\\?")[1],
					null);
		} else if (testURL.contains("?")
				&& 1 == testURL.split("\\?").length) {
			requestURL = new URI(Constants.REQUEST_SCHEME,
					null,
					Constants.REQUEST_HOST,
					Constants.REQUEST_PORT,
					testURL.split("\\?")[0],
					"",
					null);
		}

		output.setRequestURL(requestURL);

		HttpClient httpClient = new DefaultHttpClient();

		HttpUriRequest req = null;
		String requestType = Setup.getInstance().getRequestType(file.getName());
		output.setRequestType(requestType);

		if (null == inputJSON) {
			inputJSON = new JSONObject();
		}

		switch (requestType) {
		case "POST":
			req = doPost(requestHeaderJSON, urlParameterJSON,
					requestURL.toString(),
					new StringEntity(String.valueOf(inputJSON), "UTF-8"));
			break;
		case "PUT":
			req = doPut(requestHeaderJSON, urlParameterJSON,
					requestURL.toString(),
					new StringEntity(String.valueOf(inputJSON), "UTF-8"));
			break;
		case "GET":
			req = doGet(requestHeaderJSON, urlParameterJSON,
					requestURL.toString(),
					new StringEntity(String.valueOf(inputJSON), "UTF-8"));
			break;
		case "DELETE":
			if ("{}".equals(String.valueOf(inputJSON))) {
				req = doDelete(requestHeaderJSON, urlParameterJSON,
						requestURL.toString(),
						new StringEntity(String.valueOf(inputJSON), "UTF-8"));
			} else {
				req = doDeleteWithBody(requestHeaderJSON, urlParameterJSON,
						requestURL.toString(),
						new StringEntity(String.valueOf(inputJSON), "UTF-8"));
			}
			break;
		}

		CloseableHttpResponse response = client.execute(req);

		String bodyAsString = EntityUtils.toString(response.getEntity());
		output.setMessage(bodyAsString);
		output.setStatusCode(response.getStatusLine().getStatusCode());
		response.close();

		return output;
	}

	public HttpPost doPost(JSONObject requestHeaderJSON,
			JSONObject urlParameterJSON,
			String requestURL,
			StringEntity entity) throws UnsupportedEncodingException, JSONException {
		HttpPost post = new HttpPost(requestURL);
		if (null != requestHeaderJSON) {
			Set<String> jsonKeys = requestHeaderJSON.keySet();
			for (String key : jsonKeys) {
//				post.setHeader(key, requestHeaderJSON.getString(key));
				post.setHeader(key, URLEncoder.encode(requestHeaderJSON.getString(key), "UTF-8"));
			}
		} else {
			throw new AssertionError("[]:\n"
					+ "Incomplete data: "
					+ "No request headers found.");
		}
		post.setEntity(entity);
		return post;
	}

	public HttpPut doPut(JSONObject requestHeaderJSON,
			JSONObject urlParameterJSON,
			String requestURL,
			StringEntity entity) throws UnsupportedEncodingException, JSONException {
		HttpPut put = new HttpPut(requestURL);
		if (null != requestHeaderJSON) {
			Set<String> jsonKeys = requestHeaderJSON.keySet();
			for (String key : jsonKeys) {
//				put.setHeader(key, requestHeaderJSON.getString(key));
				put.setHeader(key, URLEncoder.encode(requestHeaderJSON.getString(key), "UTF-8"));
			}
		} else {
			throw new AssertionError("[]:\n"
					+ "Incomplete data: "
					+ "No request headers found.");
		}
		put.setEntity(entity);
		return put;
	}

	public HttpGet doGet(JSONObject requestHeaderJSON,
			JSONObject urlParameterJSON,
			String requestURL,
			StringEntity entity) throws UnsupportedEncodingException, JSONException {
		HttpGet get = new HttpGet(requestURL);
		if (!requestURL.contains("/job/")) {
			if (null != requestHeaderJSON) {
				Set<String> jsonKeys = requestHeaderJSON.keySet();
				for (String key : jsonKeys) {
					get.setHeader(key, URLEncoder.encode(requestHeaderJSON.getString(key), "UTF-8"));
				}
			} else {
				throw new AssertionError("[]:\n"
						+ "Incomplete data: "
						+ "No request headers found.");
			}
		}
//		get.setEntity(entity);
		return get;
	}

	public HttpDelete doDelete(JSONObject requestHeaderJSON,
			JSONObject urlParameterJSON,
			String requestURL,
			StringEntity entity) throws UnsupportedEncodingException, JSONException {
		HttpDelete del = new HttpDelete(requestURL);
		if (null != requestHeaderJSON) {
			Set<String> jsonKeys = requestHeaderJSON.keySet();
			for (String key : jsonKeys) {
//				del.setHeader(key, requestHeaderJSON.getString(key));
				del.setHeader(key, URLEncoder.encode(requestHeaderJSON.getString(key), "UTF-8"));
			}
		} else {
			throw new AssertionError("[]:\n"
					+ "Incomplete data: "
					+ "No request headers found.");
		}
//		del.setEntity(entity);
		return del;
	}

	public HttpDeleteWithBody doDeleteWithBody(JSONObject requestHeaderJSON,
			JSONObject urlParameterJSON,
			String requestURL,
			StringEntity entity) throws UnsupportedEncodingException, JSONException {
		HttpDeleteWithBody del = new HttpDeleteWithBody(requestURL);
		if (null != requestHeaderJSON) {
			Set<String> jsonKeys = requestHeaderJSON.keySet();
			for (String key : jsonKeys) {
//				del.setHeader(key, requestHeaderJSON.getString(key));
				del.setHeader(key, URLEncoder.encode(requestHeaderJSON.getString(key), "UTF-8"));
			}
		} else {
			throw new AssertionError("[]:\n"
					+ "Incomplete data: "
					+ "No request headers found.");
		}
		del.setEntity(entity);
		return del;
	}

	@NotThreadSafe
	class HttpDeleteWithBody extends HttpEntityEnclosingRequestBase {
	    public static final String METHOD_NAME = "DELETE";
	    @Override
		public String getMethod() { return METHOD_NAME; }

	    public HttpDeleteWithBody(final String uri) {
	        super();
	        setURI(URI.create(uri));
	    }
	    public HttpDeleteWithBody(final URI uri) {
	        super();
	        setURI(uri);
	    }
	    public HttpDeleteWithBody() { super(); }
	}
}
