/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) RococoGlobal Technologies Corporation - All Rights Reserved 2013
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.service;

import static com.rgtc.itex.util.UtilConstants.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Pattern;

import com.rgtc.itex.models.ScenarioModel;
import com.rgtc.itex.models.TableColumnModel;
import com.rgtc.itex.models.TableModel;
import com.rgtc.itex.util.Converter;
import com.rgtc.itex.util.UtilConstants;

/**
 * This class handles [WRITING] functions for SQL files.
 * 
 * @author Eldon Ivan
 * @since 12/03/2013
 * @version 1
 */
public class SQLWriter {
	
	private static enum Operation {SELECT, INSERT, DELETE, SS};
	

	public static List<String> getInsertStatementsList(ScenarioModel scenarioModel){
		return getScenarioStatements(sortModel(scenarioModel.getBeforeDataTables(), Operation.INSERT), Operation.INSERT);
	}

	public static List<String> getInsertStatementsList(ScenarioModel scenarioModel, Map<String, Integer> rankings){
		return getScenarioStatements(sortModel(scenarioModel.getBeforeDataTables(), Operation.INSERT, rankings), Operation.INSERT);
	}
	
	// cleans up the insert statements
	public static List<String> getCleanupInsertStatementsList(ScenarioModel scenarioModel){
		List<String> deleteStatements = new ArrayList<String>();
		List<TableModel> beforeTables = scenarioModel.getBeforeDataTables();
		List<TableModel> afterTables = scenarioModel.getAfterDataTables();
		List<TableModel> testTables = new ArrayList<TableModel>();

		for (TableModel tableModel : beforeTables) {
			boolean existFlag = false;
			for (TableModel compare : testTables) {
				if (compare.getTableName().equals(tableModel.getTableName())) {
					existFlag = true;
				}
			}
			if (!existFlag) {
				testTables.add(tableModel);
			}
		}
		for (TableModel tableModel : afterTables) {
			boolean existFlag = false;
			for (TableModel compare : testTables) {
				if (compare.getTableName().equals(tableModel.getTableName())) {
					existFlag = true;
				}
			}
			if (!existFlag) {
				testTables.add(tableModel);
			}
		}
		List<String> outputStatements = getScenarioStatements(sortModel(testTables, Operation.DELETE), Operation.DELETE);

		for (String statement : outputStatements) {
			if (!deleteStatements.contains(statement)) {
				deleteStatements.add(statement);
			}
		}

		return deleteStatements;
	}
	
	/**
	 * <div class="jp">
	 * Function for generating the list of statements for cleaning up the
	 * database tables used during the testing.
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param scenarioModel
	 * @param rankings 
	 * @return List<String>
	 * @since 0.01
	 */
	public static List<String> getCleanupInsertStatementsList(ScenarioModel scenarioModel, Map<String, Integer> rankings){
		List<String> deleteStatements = new ArrayList<String>();
		List<TableModel> beforeTables = scenarioModel.getBeforeDataTables();
		List<TableModel> afterTables = scenarioModel.getAfterDataTables();
		List<TableModel> testTables = new ArrayList<TableModel>();

		for (TableModel tableModel : beforeTables) {
			boolean existFlag = false;
			for (TableModel compare : testTables) {
				if (compare.getTableName().equals(tableModel.getTableName())) {
					existFlag = true;
				}
			}
			if (!existFlag) {
				testTables.add(tableModel);
			}
		}
		for (TableModel tableModel : afterTables) {
			boolean existFlag = false;
			for (TableModel compare : testTables) {
				if (compare.getTableName().equals(tableModel.getTableName())) {
					existFlag = true;
				}
			}
			if (!existFlag) {
				testTables.add(tableModel);
			}
		}
		List<String> outputStatements = getScenarioStatements(sortModel(testTables, Operation.DELETE, rankings), Operation.DELETE);

		for (String statement : outputStatements) {
			if (!deleteStatements.contains(statement)) {
				deleteStatements.add(statement);
			}
		}

		return deleteStatements;
	}
	
	/**
	 * Sorts the list of queries to follow constraints.
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param tableModel
	 * @param kind
	 * @return
	 * @since 0.01
	 */
	public static List<TableModel> sortModel(List<TableModel> tableModel, Operation kind){
		
		Map<String, Integer> rank = new HashMap<String, Integer>();
		rank.put("core.m006_prefecture", 1);//
		rank.put("mypage.m002_mypage_account", 2);//
		rank.put("mypage.m001_mypage_member", 3);//
		rank.put("core.m004_external_system", 4);
		rank.put("mypage.t003_mypage_authn_token", 5);
		rank.put("mypage.m003_mypage_numbering_seed", 6);
		rank.put("mypage.t005_mypage_tmp_member_activation", 7);//
		rank.put("mypage.t004_mypage_tmp_member", 8);//
		rank.put("mypage.t008_mypage_member_email", 9);//
		rank.put("mypage.t009_mypage_member_phone_number", 10);
		rank.put("mypage.t010_mypage_tmp_member_phone_number", 11);
		rank.put("mypage.m011_mypage_user", 12);
		rank.put("mypage.t011_mypage_tmp_member_email", 13);//
		rank.put("mypage.t006_mypage_member_email_activation", 14);
		rank.put("mypage.t012_mypage_member_inactive_email", 15);
		rank.put("mypage.t013_mypage_account_tmp_password", 16);
		rank.put("mypage.m013_mypage_numbering", 17);
		rank.put("mypage.m013_mypage_numbering", 18);
		rank.put("core.m014_external_system_authn", 19);
		rank.put("core.m005_system_settings", 20);
		
		//make an ugly map based on key = rank; value = list of table models (ugly!)
		Map<Integer, List<TableModel>> uglyMap = new HashMap<Integer, List<TableModel>>();

		for(int i=0; i<tableModel.size();i++){
			TableModel m = tableModel.get(i);
			
			int r = rank.get(m.getTableName());
			if(uglyMap.containsKey(r)){
				uglyMap.get(r).add(m);
			}else{
				List<TableModel> l = new ArrayList<TableModel>();
				l.add(m);
				uglyMap.put(r, l);
			}
		}
		
		List<TableModel> sortedModel = new ArrayList<TableModel>();
		SortedSet<Integer> keys = new TreeSet<Integer>(uglyMap.keySet());
		for(Integer	key : keys){
			sortedModel.addAll(uglyMap.get(key));
		}
		
		if(kind == Operation.DELETE){
			List<TableModel> reverse = new ArrayList<TableModel>(sortedModel.size());
			//reverse
			for(int i = sortedModel.size() - 1 ; i >= 0 ; i--){
				reverse.add(sortedModel.get(i));
			}
//			System.out.println(reverse);
			return reverse;
		}
//		System.out.println(sortedModel);
		return sortedModel;
	}
	
	/**
	 * Sorts the list of queries to follow constraints.
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param tableModel
	 * @param kind
	 * @return
	 * @since 0.01
	 */
	public static List<TableModel> sortModel(List<TableModel> tableModel, Operation kind, Map<String, Integer> rankings){
		
		Map<String, Integer> rank = rankings;
		
		//make an ugly map based on key = rank; value = list of table models (ugly!)
		Map<Integer, List<TableModel>> uglyMap = new HashMap<Integer, List<TableModel>>();

		for(int i=0; i<tableModel.size();i++){
			TableModel m = tableModel.get(i);
			System.out.println("[DEBUG HERE] " + m.getTableName());
			int r = rank.get(m.getTableName());
			
			if(uglyMap.containsKey(r)){
				uglyMap.get(r).add(m);
			}else{
				List<TableModel> l = new ArrayList<TableModel>();
				l.add(m);
				uglyMap.put(r, l);
			}
		}
		
		List<TableModel> sortedModel = new ArrayList<TableModel>();
		SortedSet<Integer> keys = new TreeSet<Integer>(uglyMap.keySet());
		for(Integer	key : keys){
			sortedModel.addAll(uglyMap.get(key));
		}
		
		if(kind == Operation.DELETE){
			List<TableModel> reverse = new ArrayList<TableModel>(sortedModel.size());
			//reverse
			for(int i = sortedModel.size() - 1 ; i >= 0 ; i--){
				reverse.add(sortedModel.get(i));
			}
//			System.out.println(reverse);
			return reverse;
		}
//		System.out.println(sortedModel);
		return sortedModel;
	}
	
	/**
	 * 
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Emman Resmeros
	 * @version 0.01
	 * @param tableName
	 * @param colName
	 * @param colData
	 * @param colType
	 * @return
	 * @throws SQLException
	 * @since 0.01
	 */
	public static String getInsertStatementsList(String tableName, 
			List<String> colName, 
			List<String> colData, 
			List<String> colType) throws SQLException{
		
		return insertStatementGenerator(tableName, colName, colData, colType);
	}
	
	public static List<String> getSelectStatementList(ScenarioModel scenarioModel){
		return getScenarioStatements(scenarioModel.getAfterDataTables(),Operation.SELECT);
	}
	
	public static List<String> getSimpleSelectStatementList(ScenarioModel scenarioModel){
		return getScenarioStatements(scenarioModel.getAfterDataTables(),Operation.SS);
	}
	
	/**
	 * 
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Emman Resmeros
	 * @version 0.01
	 * @param tableNames
	 * @return
	 * @since 0.01
	 */
	public static List<String> getSelectAllStatementsList (List<String> tableNames) {
		List<String> statements = new ArrayList<String>();
		
		for(String tableName : tableNames) {
			statements.add(selectAllStatementGenerator(tableName));
		}
		
		return statements;
	}
	
	/**
	 *
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Emman Resmeros
	 * @version 0.01
	 * @param tableNames
	 * @return
	 * @since 0.01
	 */
	public static List<String> getTruncateStatementsList (List<String> tableNames) {
		List<String> statements = new ArrayList<String>();
		
		statements.add(SQL_DISABLE_FOREIGN_KEY_CHECK);
		for(String tableName : tableNames) {
			statements.add(truncateStatementGenerator(tableName));
		}
		statements.add(SQL_ENABLE_FOREIGN_KEY_CHECK);
		return statements;
	}
	
	/**
	 * 
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Emman Resmeros
	 * @version 0.01
	 * @param statements
	 * @param path
	 * @param filename
	 * @throws IOException
	 * @since 0.01
	 */
	public static void createDumpFile(List<String> statements, String path, String filename) 
			throws IOException{
		StringBuffer content = new StringBuffer();
		
		for (String statement : statements) {
			content.append(statement);
			content.append(UtilConstants.NEXTLINE);
		}
		writeToFile(path, filename, content.toString());
	}
	
	public static void writeScenarioStatements(ScenarioModel scenarioModel, String directory, String fileName) throws IOException{
		createDirectoryIfNotExists(directory);
		List<String> bInsertList = getScenarioStatements(sortModel(scenarioModel.getBeforeDataTables(),Operation.INSERT),Operation.INSERT);
		List<String> bDeleteList = getScenarioStatements(sortModel(scenarioModel.getBeforeDataTables(),Operation.DELETE),Operation.DELETE);
		
		List<String> aInsertList = getScenarioStatements(sortModel(scenarioModel.getAfterDataTables(),Operation.INSERT),Operation.INSERT);
		List<String> aDeleteList = getScenarioStatements(sortModel(scenarioModel.getAfterDataTables(),Operation.DELETE),Operation.DELETE);
		
		List<String> aSelectList = getScenarioStatements(sortModel(scenarioModel.getAfterDataTables(),Operation.SELECT),Operation.SELECT);
		
		StringBuffer sb = new StringBuffer();
		sb.append("--Scenario: ");
		sb.append(scenarioModel.getScenarioName());
		sb.append("\n");
		sb.append("--Before Script: insert");
		sb.append("\n");
		for(String s:bInsertList){
			sb.append(s);
			sb.append(";");
			sb.append("\n");
		}
		sb.append("--Before Script: delete");
		sb.append("\n");
		for(String s:bDeleteList){
			sb.append("--");
			sb.append(s);
			sb.append(";");
			sb.append("\n");
		}
		
		sb.append("\n");
		sb.append("--After Script: insert");
		sb.append("\n");
		for(String s:aInsertList){
			sb.append(s);
			sb.append(";");
			sb.append("\n");
		}
		sb.append("--After Script: delete");
		sb.append("\n");
		for(String s:aDeleteList){
			sb.append("--");
			sb.append(s);
			sb.append(";");
			sb.append("\n");
		}
		
		sb.append("\n");
		sb.append("--After Script: select");
		sb.append("\n");
		for(String s:aSelectList){
			sb.append(s);
			sb.append(";");
			sb.append("\n");
		}
		
		
		sb.append("--end");
		writeToFile(directory, fileName, sb.toString());
	}
	
	private static List<String> getScenarioStatements(List<TableModel> list, Operation kind){
		List<String> statements = new ArrayList<String>();
		for(TableModel tableModel: list){
			List<String> columnNameList = new ArrayList<String>();
			List<String> columnDataList = new ArrayList<String>();
			List<String> columnTypeList = new ArrayList<String>();
			int insertSize =  tableModel.getColCount();
			int ctr = 0;
			if (tableModel.getColModelList().size() > 0) {
				for(TableColumnModel tableColumnModel : tableModel.getColModelList()){
					
					if (tableColumnModel.getData().contains("\\")) {
						String data = tableColumnModel.getData();
						tableColumnModel.setData(data.replace("\\", "\\\\"));
					}
					columnNameList.add(tableColumnModel.getName());
					columnDataList.add(tableColumnModel.getData());
					columnTypeList.add(tableColumnModel.getType());
					
					ctr++;
					if(ctr == insertSize){
						if(Operation.INSERT.equals(kind)){
							//check of all is not the same
							if(!isAllSame(columnDataList,UtilConstants.NULL_TAG)){
								statements.add(insertStatementGenerator(tableModel.getTableName(), columnNameList, columnDataList, columnTypeList));
							}
						}else if(Operation.SELECT.equals(kind)){
							statements.add(selectStatementGenerator(tableModel.getTableName(), columnNameList, columnDataList, columnTypeList));
						}else if(Operation.SS.equals(kind)){
							statements.add(simpleSelectStatementGenerator(tableModel.getTableName(), columnNameList));
						}else{
							//delete
							statements.add(deleteStatementGenerator(tableModel.getTableName(), columnNameList, columnDataList, columnTypeList));
						}
						insertSize +=tableModel.getColCount();
						columnNameList = new ArrayList<String>();
						columnDataList = new ArrayList<String>();
						columnTypeList = new ArrayList<String>();
					}
				}
			} else {
				statements.add(selectAllStatementGenerator(tableModel.getTableName()));
			}
		}

		return statements;
	}
	
	private static String insertStatementGenerator(String tableName, List<String> columns, List<String> data, List<String> types){
		StringBuffer sb = new StringBuffer();
		sb.append(SQL_INSERT);
		sb.append(tableName);
		sb.append(OPEN_PAR);
		sb.append(Converter.listToCommaDelimitedString(columns));
		sb.append(CLOSE_PAR);
		sb.append(SQL_VALUES);
		sb.append(OPEN_PAR);
		sb.append(Converter.getInsertValues(data, types));
		sb.append(CLOSE_PAR);
		return sb.toString();
	}
	
	private static String selectStatementGenerator(String tableName, List<String> columns, List<String> data, List<String> types){
		StringBuffer sb = new StringBuffer();
		sb.append(SQL_SELECT);

		//eldon 8/20/2014
		// checks if the elements are the same; if true change the query for select (*).
		if(isAllSame(data,UtilConstants.NULL_TAG)){
			// select count(*) from table
			sb.append(" count(*) as row_count ");
			sb.append(SQL_FROM);
			sb.append(tableName);
			sb.append(";");
		}else{
			sb.append(Converter.listToCommaDelimitedString(columns));
			sb.append(SQL_FROM);
			sb.append(tableName);
			sb.append(SQL_WHERE);
			sb.append(Converter.getSelectWhereValues(columns, data, types));
		}
		return sb.toString();
		
	}
	
	private static String simpleSelectStatementGenerator(String tableName, List<String> columns){
		StringBuffer sb = new StringBuffer();
		sb.append(SQL_SELECT);
		sb.append(Converter.listToCommaDelimitedString(columns));
		sb.append(SQL_FROM);
		sb.append(tableName);
		return sb.toString();
		
	}
	
	/**
	 * 
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Emman Resmeros
	 * @version 0.01
	 * @param tableName
	 * @return
	 * @since 0.01
	 */
	private static String selectAllStatementGenerator(String tableName){
		StringBuffer sb = new StringBuffer();
		sb.append(SQL_SELECT);
		sb.append(SELECT_ALL_COLUMNS);
		sb.append(SQL_FROM);
		sb.append(tableName);
		return sb.toString();
		
	}
	
	private static String truncateStatementGenerator(String tableName) {
		StringBuffer sb = new StringBuffer();
		sb.append(SQL_TRUNCATE);
		sb.append(tableName);
		return sb.toString();
	}
	
	private static String deleteStatementGenerator(String tableName, List<String> columns, List<String> data, List<String> types){
		StringBuffer sb = new StringBuffer();
		sb.append(SQL_DELETE);
		sb.append(tableName);
		//eldon clean up all.
		//sb.append(SQL_WHERE);
		//sb.append(Converter.getSelectWhereValues(columns, data, types));
		return sb.toString();
	}

	private static void writeToFile(String path, String filename, String content) throws IOException{
		File file = new File(path + filename);
		
		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(content);
		bw.flush();
		bw.close();
	}
	
	private static void createDirectoryIfNotExists(String directory){
		File f = new File(directory);
		if(!f.exists()){
			f.mkdir();
		}
	}
	
	/**
	 * Returns a formulated date based on the input form
	 * the excel file. The required input should have the 
	 * following format:
	 * <p>
	 * <code>now+x y</code>
	 * <p>
	 * The syntax <code>now</code> signifies that the system will use the current date.</br>
	 * The syntax <code>+</code> signifies the operation to be used.</br>
	 * The syntax <code>n</code> signifies the amount to be computed.</br>
	 * The syntax <code>y</code> signifies the unit to be used.</br>
	 * Examples:
	 * <p>
	 * <code>now+100 second</code></br>
	 * <code>now+10 minute</code></br>
	 * <code>now+2 hour</code></br>
	 * <code>now+1 day</code></br>
	 * <code>now-10 month</code></br>
	 * <code>now-12 year</code>
	 * <p>
	 * The current implementation only supports yyymmddhhMMss format.</br>
	 * Returns the current date if null is passed.
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param data from the excel file.
	 * @return the formulated date
	 * @since 0.01
	 */
	public static String getFormulatedDate(String data){
		// *data should consist of "now+ 1 day" or just "now"
		if(isDate(data)){
			return data;
		}else{
			StringBuffer sb = new StringBuffer();
			sb.append("DATE_FORMAT(DATE_ADD(NOW(), INTERVAL ");
			sb.append(SQLWriter.getOperation(data));
			sb.append(SQLWriter.numBuilder(data));
			sb.append(" ");
			sb.append(SQLWriter.getUnit(data));
			sb.append("),'%Y%m%d%H%i%s')");
			return sb.toString();
		}
		
	}
	
	/**
	 * Builds an int representation from the provided String.
	 * This will omit all non numeric characters and combine all
	 * numeric characters into one int value. Defaults to 0.
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param data
	 * @return
	 * @since 0.01
	 */
	public static int numBuilder(String data){
		StringBuffer sb = new StringBuffer();
		if(null != data){
			for(int i=0;i<data.length();i++){
				String s = String.valueOf(data.charAt(i));
				boolean isParsable = false;
				try {
					Integer.parseInt(s);
					isParsable = true;
				} catch (NumberFormatException e) {
				}
				if(isParsable){
					sb.append(s);
				}
			}
		}
		
		try {
			return Integer.valueOf(sb.toString());
		} catch (NumberFormatException e) {
			return 0;
		}
	}
	
	/**
	 * Retrieves the unit to be used for
	 * date computation. Defaults to "second".
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param data
	 * @return
	 * @since 0.01
	 */
	public static String getUnit(String data){
		String unit = "second";
		if(null == data){
			unit = "second";
		}else if(Pattern.compile(Pattern.quote("second"), Pattern.CASE_INSENSITIVE).matcher(data).find()){
			unit = "second";
		}else if(Pattern.compile(Pattern.quote("minute"), Pattern.CASE_INSENSITIVE).matcher(data).find()){
			unit = "minute";
		}else if(Pattern.compile(Pattern.quote("hour"), Pattern.CASE_INSENSITIVE).matcher(data).find()){
			unit = "hour";
		}else if(Pattern.compile(Pattern.quote("day"), Pattern.CASE_INSENSITIVE).matcher(data).find()){
			unit = "day";
		}else if(Pattern.compile(Pattern.quote("month"), Pattern.CASE_INSENSITIVE).matcher(data).find()){
			unit = "month";
		}else if(Pattern.compile(Pattern.quote("year"), Pattern.CASE_INSENSITIVE).matcher(data).find()){
			unit = "year";
		}
		return unit;
	}
	
	/**
	 * Retrieves the operation to be used for
	 * date computation. Defaults to an empty string.
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param data
	 * @return
	 * @since 0.01
	 */
	public static String getOperation(String data){
		String operation = "";
		if(null == data){
			operation = "";
		}else if(Pattern.compile(Pattern.quote("+"), Pattern.CASE_INSENSITIVE).matcher(data).find()){
			operation = "+";
		}else if(Pattern.compile(Pattern.quote("-"), Pattern.CASE_INSENSITIVE).matcher(data).find()){
			operation = "-";
		}
		return operation;
	}
	
	/**
	 * Validates if the provided data is parsable to date.
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param data
	 * @return
	 * @since 0.01
	 */
	public static boolean isDate(String data){
		try {
			new SimpleDateFormat(UtilConstants.DATE_FORMAT_yyyyMMddHHmmss).parse(String.valueOf(data));
			return true;
		} catch (ParseException e) {
			return false;
		}
	}
	
	public static String cleanColumnName(String s){
		if(s.contains(UtilConstants.SQL_SELECT)){
			s = s.replace(UtilConstants.SQL_SELECT, UtilConstants.EMPTY_STRING);
		}
		
		if(s.contains(UtilConstants.SQL_FROM)){
			s = s.substring(0, s.indexOf(UtilConstants.SQL_FROM));
		}
		s=s.trim();
		return s;
	}
	
	public static String cleanTableName(String s){
		if(s.contains(UtilConstants.SQL_FROM)){
			s = s.substring(s.indexOf(UtilConstants.SQL_FROM), s.length());
			s = s.replace(UtilConstants.SQL_FROM, UtilConstants.EMPTY_STRING);
		}
		s=s.trim();
		return s;
	}
	
	public static String simplifySelect(String s){
		if(s.contains(UtilConstants.SQL_WHERE)){
			s = s.substring(0, s.indexOf(UtilConstants.SQL_WHERE));
		}
		s=s.trim();
		return s;
	}
	
	public static boolean isListOfSameElements(List<? extends Object> l) {
	    Set<Object> set = new HashSet<Object>(l.size());
	    for (Object o : l) {
	        if (set.isEmpty()) {
	            set.add(o);
	        } else {
	            if (set.add(o)) {
	                return false;
	            }
	        }
	    }
	    return true;
	}
	
	public static boolean isAllSame(List<String> l, String value){
		int cnt =0;
		for(int i =0; i<l.size(); i++){
			String tmp = l.get(i);
			if(value.equalsIgnoreCase(tmp)){
				cnt++;
			}
		}
		if(cnt == l.size()){
			return true;
		}else{
			return false;			
		}
	}
}
