package com.rgtc.itex.service;

import java.io.IOException;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import com.google.appengine.tools.remoteapi.RemoteApiInstaller;
import com.google.appengine.tools.remoteapi.RemoteApiOptions;
import com.rgtc.itex.constants.Setup;
import com.rgtc.itex.main.ITEx;

public class TaskQueueService {

	private RemoteApiOptions options;

	public TaskQueueService(String url, int port, String user, String pass) {
		options = new RemoteApiOptions().server(url, port).credentials(user,
				pass);
	}

	/**
	 * Insert task to task queue depending on cron api.
	 */
	public void pushTask(String cronTask, JSONObject jsonData) {
		RemoteApiInstaller installer = new RemoteApiInstaller();

		try {
			installer.install(options);
			String queueName = "";
			String urlProcessor = "";
			Method method = null;

			// Queue name for daily cron tasks.
			if (cronTask.contains("API_02_04_01_01_02")
					|| cronTask.contains("API_02_04_01_01_03")
					|| cronTask.contains("API_02_04_01_01_04")
					|| cronTask.contains("API_02_04_01_01_05")
					|| cronTask.contains("API_02_04_01_01_06")
					|| cronTask.contains("API_02_04_01_01_07_01")) {
				queueName = "mypage-daily-process";
				method = Method.DELETE;
			}
			// Queue name for monthly cron tasks.
			else if (cronTask.contains("API_02_04_01_01_08_01")) {
				queueName = "mypage-monthly-process";
				method = Method.POST;
			}
			// Queue name for yearly cron tasks.
			else if (cronTask.contains("API_02_04_01_01_08_02")) {
				queueName = "mypage-yearly-process";
				method = Method.POST;
			} else if(cronTask.contains("API_02_04_01_01_07_02")) {
				queueName = "mypage-operation-log-append";
				method = Method.POST;
			}

			Queue queue = QueueFactory.getQueue(queueName);
			TaskOptions taskOptions = null;
			// Options if DELETE task with parameters.
			if (Method.DELETE == method && null!= jsonData) {
				taskOptions = TaskOptions.Builder.withUrl(Setup.getInstance().getPath(cronTask)).method(method);
				Set<String> keySet = jsonData.keySet();
				for (String key : keySet) {
					taskOptions.param(key, jsonData.getString(key));
				}
			}
			// Options if POST task with parameters.
			else if (Method.POST == method && null != jsonData){
				taskOptions = TaskOptions.Builder.withUrl(Setup.getInstance().getPath(cronTask)).method(method)
					.payload(jsonData.toString().getBytes("UTF-8"), "application/json");
			}
			// Options if task has no parameters.
			else {
				taskOptions = TaskOptions.Builder.withUrl(Setup.getInstance().getPath(cronTask)).method(method);
			}

			queue.add(taskOptions);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			installer.uninstall();
		}
	}

	public void pushOperationLogs(JSONObject operationLogs) {
		RemoteApiInstaller installer = new RemoteApiInstaller();

		try {
			installer.install(options);

			Queue queue = QueueFactory.getQueue("mypage-operation-log-entry");

			for (int i = 0; i < operationLogs.getJSONArray("entryList").length(); i++) {
				TaskOptions taskOptions = null;
				JSONObject currentLog = operationLogs.getJSONArray("entryList").getJSONObject(i);
				String operationContent = currentLog.getJSONObject("operation_content").toString();
				currentLog.remove("operation_content");
				currentLog.put("operation_content", AesService.encrypt(operationContent));
				taskOptions = TaskOptions.Builder.withMethod(Method.PULL);
				taskOptions = taskOptions.payload(currentLog.toString().getBytes("UTF-8"));
				queue.add(taskOptions);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			installer.uninstall();
		}
	}

	public void purgeTaskQueue(String taskQueue) {
		RemoteApiInstaller installer = new RemoteApiInstaller();

		try {
			installer.install(options);

			Queue queue = QueueFactory.getQueue("mypage-operation-log-entry");
			ITEx.sleep(10 * 1000);
			queue.purge();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			installer.uninstall();
		}
	}
}
