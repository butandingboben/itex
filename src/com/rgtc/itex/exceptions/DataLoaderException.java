/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.exceptions;

/**
 * <div class="jp">
 *
 * </div>
 * <p>
 * <div class="en" style="padding:0em 0.5em" >
 * Custom Exception for data loader.
 * </div>
 *
 * @author Eldon Leuterio
 * @version 0.01
 * @since 0.01
 */
public class DataLoaderException extends Exception {

	public DataLoaderException (String msg){
		super(msg);
	}
}
