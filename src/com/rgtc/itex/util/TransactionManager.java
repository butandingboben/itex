/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.rgtc.itex.util.TransactionManager;

/**
 * <div class="jp">
 * 繝医Λ繝ｳ繧ｶ繧ｯ繧ｷ繝ｧ繝ｳ繧堤ｮ｡�ｽ?�ｽ�ｽ繧具ｿｽ?
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 * Manages the transactions.
 * </div>
 *
 * @author Eldon Leuterio
 * @version 0.01
 * @since 0.01
 */
public class TransactionManager {

	/**
	 * <div class="jp">
	 * 繧ｷ繝ｳ繧ｰ繝ｫ繝医Φ縺ｫ菴ｿ逕ｨ縺吶ｋ<code>TransactionManager</code>縺ｮ繧､繝ｳ繧ｹ繧ｿ繝ｳ繧ｹ�ｽ?/br>
	 * <code>Transactional</code>騾壻ｿ｡縺励°繧ｵ繝晢ｿｽ?繝医＠縺ｾ縺帙ｓ�ｽ?
	 * </div>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * The instance of <code>TransactionManager</code> that is intended for singleton use.</br>
	 * Supports <code>Transactional</code> connections only.
	 * </div>
	 */
	private static TransactionManager instance;

	/**
	 * <div class="jp">
	 * <code>Connection</code>縺ｮ繧､繝ｳ繧ｹ繧ｿ繝ｳ繧ｹ縺ｧ縺呻ｿｽ?<code>Connection</code>縺ｯ繧ｳ繝ｳ繧ｹ繝医Λ繧ｯ繧ｿ繝ｼ縺ｧ蛻晄悄縺輔ｌ縺ｦ�ｽ?�ｽ�ｽ�ｽ?
	 * </div>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * The instance of a <code>Connection</code>. The <code>Connection</code> is initialized in the constructor.
	 * </div>
	 * @see Connection
	 * @see TransactionManager#TransactionManager()
	 * @see TransactionManager#TransactionManager(String, String, String, String)
	 */
	private Connection connection;

	private static String url;
	private static String databaseName;
	private static String userName;
	private static String password;
	/**
	 * <div class="jp">
	 * <code>TransactionManager</code>縺ｮ繝代Λ繝｡繝ｼ繧ｿ繝ｼ蛹悶↓縺励◆繧ｳ繝ｳ繧ｹ繝医Λ繧ｯ繧ｿ繝ｼ�ｽ?
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Parameterized Constructor of <code>TransactionManager</code>.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param url The url.
	 * @param databaseName The database.
	 * @param userName The user name.
	 * @param password The password.
	 * @throws SQLException The type of exception to be throw.
	 * @throws IOException The type of exception to be throw.
	 * @since 0.01
	 */
	public TransactionManager(
			String url,
			String databaseName,
			String userName,
			String password) throws SQLException, IOException, Exception {
		this.url = url;
		this.databaseName = databaseName;
		this.userName = userName;
		this.password = password;

		initConn(url, databaseName, userName, password);
	}

	/**
	 * <div class="jp">
	 * <code>TransactionManager</code>縺ｮ繧ｷ繝ｳ繧ｰ繝ｫ繝医Φ繝ｻ繧､繝ｳ繧ｹ繧ｿ繝ｳ繧ｹ繧定ｿ斐☆�ｽ?/br>
	 * <code>Transactional</code>騾壻ｿ｡縺励°繧ｵ繝晢ｿｽ?繝医＠縺ｾ縺帙ｓ�ｽ?
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Returns the singleton instance of <code>TransactionManager</code>.</br>
	 * Supports <code>Transactional</code> connections only.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param url The url.
	 * @param databaseName The database.
	 * @param userName The user name.
	 * @param password The password.
	 * @return TransactionManager
	 * @throws SQLException, Exception 
	 * @since 0.01
	 */
//	public static TransactionManager getInstance(
//			String url,
//			String databaseName,
//			String userName,
//			String password) throws SQLException, Exception {
//		this.url = url;
//		this.databaseName = databaseName;
//		this.userName = userName;
//		this.password = password;
//		if (null == instance) {
//			instance = new TransactionManager(url, databaseName, userName, password);
//		}
//		return instance;
//	}

	/**
	 * <div class="jp">
	 * 繝代Λ繝｡繝ｼ繧ｿ繝ｼ縺ｫ蝓ｺ縺･縺上ョ繝ｼ繧ｿ繝呻ｿｽ?繧ｹ騾壻ｿ｡繧抵ｿｽ?譛溘☆繧具ｿｽ?
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Initializes the database connection based on the parameters.
	 * </div>
	 * @maker Robert Lao
	 * @version 0.01
	 * @param url The url.
	 * @param databaseName The database.
	 * @param userName The user name.
	 * @param password The password.
	 * @throws SQLException
	 * @throws IOException
	 * @since 0.01
	 */
	private void initConn(
			String url,
			String databaseName,
			String userName,
			String password) throws SQLException, Exception {
		if (null == this.connection) {
			//test
			this.connection = (Connection) DriverManager.getConnection(url
					+ "/" + databaseName, userName, password);

		}
	}

	/**
	 * <div class="jp">
	 * �ｽ??繧ｿ繝呻ｿｽ?繧ｹ縺ｮ繧ｨ繝ｳ繝医Μ繝ｼ繧呈峩譁ｰ縺励◆繧奇ｿｽ? <code>Transactional</code>�ｽ?code>Non-Transactional</code>縺ｮ�ｽ??繧ｿ繝呻ｿｽ?繧ｹ騾壻ｿ｡縺ｮ蠖ｱ髻ｿ繧貞女縺代◆陦後ｒ霑斐＠縺溘ｊ縲√☆繧具ｿｽ?
	 * <p>
	 * <code>parameters</code>縺ｨ�ｽ?�ｽ�ｽ縺ｮ縺ｯ<code>Parameter</code>縺ｮ繝ｪ繧ｹ繝茨ｿｽ?縺翫ｈ縺ｳ<code>NULL</code>縺ｧ縺ゅｋ�ｽ?
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Updates a database entry and returns the row(s) affected of a
	 * <code>Transactional</code> or <code>Non-Transactional</code> database connection.
	 * <p>
	 * The <code>parameters</code> is either a list of <code>Parameter</code>(s) or <code>NULL</code>.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param sqlStatement
	 * @param parameters
	 * @return rowsAffected
	 * @throws SQLException
	 * @since 0.01
	 * @see Parameter
	 */
	public int update(String sqlStatement, List<Object> parameters) throws SQLException {
		if(StringUtils.isEmpty(sqlStatement)){
			throw new SQLException("The sqlStatement cannot be empty.");
		}else{
			//initialize prepared statement.
			PreparedStatement preparedStatement = getConnection().prepareStatement(sqlStatement);
			if (null != parameters) {
				setStatementParameters(preparedStatement, parameters);
			}
			int rowsAffeted = preparedStatement.executeUpdate(); 
			return rowsAffeted;
		}
		
	}

	/**
	 * <div class="jp">
	 * �ｽ??繧ｿ繝呻ｿｽ?繧ｹ縺ｮ繧ｨ繝ｳ繝医Μ繝ｼ繧貞炎髯､縺励◆繧奇ｿｽ?<code>Transactional</code>�ｽ?code>Non-Transactional</code>縺ｮ�ｽ??繧ｿ繝呻ｿｽ?繧ｹ騾壻ｿ｡縺ｮ蠖ｱ髻ｿ繧貞女縺代◆陦後ｒ霑斐＠縺溘ｊ縲√☆繧具ｿｽ?
	 * <p>
	 * <code>parameters</code>縺ｨ�ｽ?�ｽ�ｽ縺ｮ縺ｯ<code>Parameter</code>縺ｮ繝ｪ繧ｹ繝茨ｿｽ?縺翫ｈ縺ｳ<code>NULL</code>縺ｧ縺ゅｋ�ｽ?
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Deletes a database entry and returns the row(s) affected of a
	 * <code>Transactional</code> or <code>Non-Transactional</code> database connection.
	 * <p>
	 * The <code>parameters</code> is either a list of <code>Parameter</code>(s) or <code>NULL</code>.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param sqlStatement
	 * @param parameters
	 * @return rowsAffected
	 * @throws SQLException
	 * @since 0.01
	 * @see Parameter
	 */
	public int delete(String sqlStatement, List<Object> parameters)
			throws SQLException {
		if(StringUtils.isEmpty(sqlStatement)){
			throw new SQLException("The sqlStatement cannot be empty.");
		}else{
			PreparedStatement preparedStatement = getConnection().prepareStatement(sqlStatement);
			if (null != parameters) {
				setStatementParameters(preparedStatement, parameters);
			}
			int rowsAffeted = preparedStatement.executeUpdate(); 
			return rowsAffeted;
		}
		
	}

	/**
	 * <div class="jp">
	 * �ｽ??繧ｿ繝呻ｿｽ?繧ｹ縺ｮ繧ｨ繝ｳ繝医Μ繝ｼ繧剃ｽ懶ｿｽ?縺励◆繧奇ｿｽ?<code>Transactional</code>�ｽ?code>Non-Transactional</code>縺ｮ�ｽ??繧ｿ繝呻ｿｽ?繧ｹ騾壻ｿ｡縺ｮ蠖ｱ髻ｿ繧貞女縺代◆陦後ｒ霑斐＠縺溘ｊ縲√☆繧具ｿｽ?
	 * <p>
	 * <code>parameters</code>縺ｨ�ｽ?�ｽ�ｽ縺ｮ縺ｯ<code>Parameter</code>縺ｮ繝ｪ繧ｹ繝茨ｿｽ?縺翫ｈ縺ｳ<code>NULL</code>縺ｧ縺ゅｋ�ｽ?
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Creates a database entry and returns the row(s) affected of a
	 * <code>Transactional</code> or <code>Non-Transactional</code> database connection.
	 * <p>
	 * The <code>parameters</code> is either a list of <code>Parameter</code>(s) or <code>NULL</code>.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param sqlStatement
	 * @param parameters
	 * @return rowsAffected
	 * @throws SQLException
	 * @since 0.01
	 * @see Parameter
	 */
	public int create(String sqlStatement, List<Object> parameters)
			throws SQLException {
		if(StringUtils.isEmpty(sqlStatement)){
			throw new SQLException("The sqlStatement cannot be empty.");
		}else{
			PreparedStatement preparedStatement = getConnection().prepareStatement(sqlStatement);
			if (null != parameters) {
				setStatementParameters(preparedStatement, parameters);
			}
			int rowsAffeted = preparedStatement.executeUpdate(); 
			return rowsAffeted;
		}
	}

	/**
	 * <div class="jp">
	 * <code>Transactional</code>縲√♀繧茨ｿｽ?<code>Non-Transactional</code>縺ｮ�ｽ??繧ｿ繝呻ｿｽ?繧ｹ繧ｯ繧ｨ繝ｪ縺ｮ<code>ResultSet</code>繧定ｿ斐☆�ｽ?/br>
	 * <code>ResultSet</code>縺ｯ
	 * <p>
	 * <code>parameters</code>縺ｨ�ｽ?�ｽ�ｽ縺ｮ縺ｯ<code>Parameter</code>縺ｮ繝ｪ繧ｹ繝茨ｿｽ?縺翫ｈ縺ｳ<code>NULL</code>縺ｧ縺ゅｋ�ｽ?
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Retrieves a <code>ResultSet</code> of a <code>Transactional</code> 
	 * or <code>Non-Transactional</code> database query.</br>
	 * <code>ResultSet</code>縺ｯ蜿ゑｿｽ?豕輔↓繧医▲縺ｦ隗｣譫舌＆繧後ｋ�ｽ?
	 * <p>
	 * The <code>parameters</code> is either a list of <code>Parameter</code>(s) or <code>NULL</code>..
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param sqlStatement
	 * @param parameters
	 * @return ResultSet
	 * @throws SQLException
	 * @since 0.01
	 * @see Object
	 * @see ResultSet
	 */
	public ResultSet retrieve(String sqlStatement, List<Object> parameters)
			throws SQLException {
		if(StringUtils.isEmpty(sqlStatement)){
			throw new SQLException("The sqlStatement cannot be empty.");
		}else{
			PreparedStatement preparedStatement = getConnection().prepareStatement(sqlStatement);
			if (null != parameters) {
				setStatementParameters(preparedStatement, parameters);
			}
			return preparedStatement.executeQuery(); 
		}
	}

	public ResultSet configureDb(String sqlStatement, List<Object> parameters) throws SQLException {
		if (StringUtils.isEmpty(sqlStatement)) {
			throw new SQLException("The sqlStatement cannot be empty.");
		} else {
			PreparedStatement preparedStatement = getConnection().prepareStatement(sqlStatement);
			if (null != parameters) {
				setStatementParameters(preparedStatement, parameters);
			}
			return preparedStatement.executeQuery(); 
		}
	}
	
	/**
	 * <div class="jp">
	 * <code>Transactional</code>�ｽ??繧ｿ繝呻ｿｽ?繧ｹ騾壻ｿ｡繧帝幕蟋九☆繧具ｿｽ?
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Begins a <code>Transactional</code> database connection.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @throws SQLException
	 * @since 0.01
	 * @see Connection
	 */
	public void beginTransactional() throws SQLException {
		if(null != getConnection()){
			getConnection().setAutoCommit(false);
			getConnection().setTransactionIsolation(Connection.TRANSACTION_READ_UNCOMMITTED);
		}
	}

	/**
	 * <div class="jp">
	 * <code>Non-Transactional</code>�ｽ??繧ｿ繝呻ｿｽ?繧ｹ騾壻ｿ｡繧帝幕蟋九☆繧具ｿｽ?
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Begins a <code>Non-Transactional</code> database connection.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @throws SQLException
	 * @since 0.01
	 * @see Connection
	 */
	public void beginNonTransactional() throws SQLException {
		if(null != getConnection()){
			getConnection().setAutoCommit(true);
		}
	}

	/**
	 * <div class="jp">
	 * <code>Transactional</code>�ｽ??繧ｿ繝呻ｿｽ?繧ｹ騾壻ｿ｡縺ｮ荳ｭ縺ｫ縺ゅｋ<code>Savepoint</code>繧抵ｿｽ?繝ｼ繧ｯ縺吶ｋ�ｽ?
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Marks the <code>Savepoint</code> within the <code>Transactional</code> database connection.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @throws SQLException
	 * @since 0.01
	 * @see Savepoint
	 */
	public void setSavepoint() throws SQLException {
		if(null != getConnection()){
			if(!getConnection().getAutoCommit()){
				getConnection().setSavepoint();
			}else{
				throw new SQLException("Cannot set Savepont to a non-transactional connection.");
			}
		}
	}

	/**
	 * <div class="jp">
	 * <code>Savepoint</code>縺ｫ<code>Transaction</code>縺ｮ迥ｶ諷九ｒ霑斐☆�ｽ?
	 * <code>Savepoint</code>縺瑚ｨｭ螳壹＆繧後※�ｽ?�ｽ�ｽ�ｽ??蜷茨ｿｽ?�ｽ? <code>Transaction</code>縺ｯ蛻晄悄迥ｶ諷九↓謌ｻ縺呻ｿｽ? 
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Returns the state of the <code>Transaction</code> to the <code>Savepoint</code>.
	 * If a <code>Savepoint</code> is not set, the <code>Transaction</code> returns to its initial state. 
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @throws SQLException
	 * @since 0.01
	 */
	public void rollbackTransaction() throws SQLException {
		if(null != getConnection()){
			if(!getConnection().getAutoCommit()){
				getConnection().rollback();
			}else{
				throw new SQLException("Cannot rollback a non-transactional connection.");
			}
		}
	}

	/**
	 * <div class="jp">
	 * <code>Transactional</code>�ｽ??繧ｿ繝呻ｿｽ?繧ｹ騾壻ｿ｡繧堤ｵゑｿｽ?�ｽ�ｽ繧具ｿｽ?
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Ends a <code>Transactional</code> database connection.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @throws SQLException
	 * @since 0.01
	 */
	public void endTransactional() throws SQLException {
		if(null != getConnection()){
			if(!getConnection().getAutoCommit()){
				getConnection().commit();
				getConnection().close();
				setConnection(null);
			}else{
				throw new SQLException("Cannot close a non-transactional connection with endTransactional(). Use endNonTransactional() instead."); 
			}
		}		
	}
	
	public void commitTransactional() throws SQLException{
		getConnection().commit();
	}

	/**
	 * <div class="jp">
	 * <code>Non-Transactional</code>�ｽ??繧ｿ繝呻ｿｽ?繧ｹ騾壻ｿ｡繧堤ｵゑｿｽ?�ｽ�ｽ繧具ｿｽ?
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Ends a <code>Non-Transactional</code> database connection.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @throws SQLException
	 * @since 0.01
	 */
	public void endNonTransactional() throws SQLException {
		if(null != getConnection()){
			if(getConnection().getAutoCommit()){
				getConnection().close();
				setConnection(null);
			}else{
				throw new SQLException("Cannot close a transactional connection with endNonTransactional(). Use endTransactional() instead.");
			}
		}
	}

	/**
	 * <div class="jp">
	 * <code>PreparedStatement</code>縺ｮ縺溘ａ縺ｮ<code>Parameter</code>蛟､繧定ｨｭ螳壹☆繧具ｿｽ?
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Sets the <code>Parameter</code> values for the <code>PreparedStatement</code>.
	 * <p>
	 * Does <b><u>not</u></b> support SQL "IN" clause.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param preparedStatement
	 * @param parameters
	 * @return PreparedStatement
	 * @throws SQLException
	 * @since 0.01
	 * @see PreparedStatement
	 */
	private PreparedStatement setStatementParameters(
			PreparedStatement preparedStatement, List<Object> parameters)
			throws SQLException {
		Iterator<Object> it = parameters.iterator();
		int parameterIndex = 1;
		while (it.hasNext()) {
			Object entry = it.next();
			
			// parse each entry to its equivalent type.
			if (entry instanceof String) {
				preparedStatement.setString(parameterIndex,
						String.valueOf(entry));
			} else if (entry instanceof Integer) {
				preparedStatement.setInt(parameterIndex,
						(Integer) entry);
			} else if (entry instanceof Boolean) {
				preparedStatement.setBoolean(parameterIndex,
						(Boolean) entry);
			} else if (entry instanceof Double) {
				preparedStatement.setDouble(parameterIndex,
						(Double) entry);
			} else if (entry instanceof Timestamp) {
				preparedStatement.setTimestamp(parameterIndex,
						(Timestamp) entry);
			} else if (entry instanceof Date) {
				preparedStatement.setDate(parameterIndex,
						(Date) entry);
			} else if (entry instanceof Long) {
				preparedStatement.setLong(parameterIndex,
						(Long) entry);
			} else if (entry instanceof Float) {
				preparedStatement.setFloat(parameterIndex,
						(Float) entry);
			} else if (entry instanceof List) {
				// TODO: implement "IN" clause logic.
				List<Object> list = (List<Object>) entry;
			} else {
				throw new SQLException("[setStatementParameters] value: "+entry+ " is not supported at index "+parameterIndex);
			}
			parameterIndex++;
		}
		return preparedStatement;
	}

	/**
	 * @maker Emman Meier Resmeros
	 * @return the connection
	 */
	public Connection getConnection() {
		if (null == connection) {
			try {
				initConn(this.url, this.databaseName, this.userName, this.password);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return connection;
	}

	/**
	 * @maker Emman Meier Resmeros
	 * @param connection the connection to set
	 */
	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	

}
