package com.rgtc.itex.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class ItexFileReader {
	
	private static ItexFileReader instance;
	private File directory;
	private String prefix;
	
	public static ItexFileReader getInstance(){
		if(null == instance){
			instance = new ItexFileReader();
		}
		return instance;
	}
	
	public ItexFileReader readFilesStartsWith(String pre){
		prefix = pre;
		return instance;
	}
	
	public ItexFileReader readFilesFrom(String dir){
		directory = new File(dir);
		if(!directory.isDirectory()){
//			throw error
		}
		return instance;
	}
	
	public File[] get(){
		List<File> files = new ArrayList<File>(Arrays.asList(directory.listFiles()));
		Iterator<File> itr = files.iterator();
		while(itr.hasNext()){
			File f = itr.next();
			if(!f.getName().startsWith(prefix)){
				itr.remove();
			}
		}
		return files.toArray(new File[files.size()]);
	}

}
