/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.rgtc.itex.util.PropertiesUtil;


/**
 * 
 * <div class="jp">
 * Property ファイルの動�?
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 * Operations on <code>Property</code> file.
 * </div>
 *
 * @author richard.go
 * @version 0.01
 * @since 0.01
 */
public class PropertiesUtil {

    protected static String PROP_KEY_DELIM             = ".";
    protected static String _FILE_SUFIX                = ".properties";

    /** CLOUD�?QL用のプロ�?��ー�?��キー、対応JDBCドライバ�?�?*/
    protected static String CONFIG_DRIVER              = "driver";

    /** CLOUD�?QL用のプロ�?��ー�?��キー、対応URL�?*/
    protected static String CONFIG_URL                 = "url";

    /** CLOUD�?QL用のプロ�?��ー�?��キー、対応DBユーザ名�? */
    protected static String CONFIG_USERNAME            = "username";

    /** CLOUD�?QL用のプロ�?��ー�?��キー、対応DBパスワード�? */
    protected static String CONFIG_PASSWORD            = "password";

    /** CLOUD�?QL用のプロ�?��ー�?��キー、対応DBスキーマ�? */
    protected static String CONFIG_SCHEMA              = "schema";

    /** カスタ�?���?��ージキー�?*/
    protected static String MESSAGE_PREFIX             = "custom.message";

    /** CLOUD�?QLプロ�?��ー�?��ファイル。�?*/
    protected static String _CLOUD_SQL_PROPERTIES      = "cloud";

    /** メ�?��ージプロ�?��ー�?��ファイル。�?*/
    protected static String _SYS_MSG_PREFIX_PROPERTIES = "application_";

    /**
     * �?��されて�?��の�?��キーを取得する�?
     * Retrieve the content key of the specified file.
     *
     * @param filePath file path
     * @param filePath ファイルパス
     * @param key
     * @return 対応キーの値
     * @return target key value
     */
    public static String get(String filePath, String key) {
        String message = null;
        InputStream is = null;
        try {
        	Properties prop = new Properties();
        	is = PropertiesUtil.class.getClassLoader().getResourceAsStream(filePath);
            prop.load(is);
            message = prop.getProperty(key);
        } catch (IOException e) {
        	System.out.println("get"+" IOException: "+e.getMessage());
        }
        catch (NullPointerException e){
        	System.out.println("get"+" NullPointerException: "+e.getMessage());
        }
        return message;
    }

    /**
     * Get key from generated property file.
     * @maker Dyan Despojo
     * @version 0.01
     * @param filePath path
     * @param key method name
     * @return String
     * @since 0.01
     */
    public static String getFromGenerated(String filePath, String key) {
        String message = null;
        InputStream is = null;
        try {
        	Properties prop = new Properties();
        	is = new FileInputStream(filePath);
            prop.load(is);
            message = prop.getProperty(key);
        } catch (IOException e) {
        	System.out.println("get"+" IOException: "+e.getMessage());
        }
        catch (NullPointerException e){
        	System.out.println("get"+" NullPointerException: "+e.getMessage());
        }
        return message;
    }

    /**
     * Set property key with value.
     * @maker Dyan Despojo
     * @version 0.01
     * @param filePath path to properties file
     * @param key property
     * @param value of key
     * @throws IOException 
     * @since 0.01
     */
    public static void set(String filePath,String key,String value) throws IOException {
    	FileInputStream in = new FileInputStream(filePath);
        Properties props = new Properties();
        props.load(in);
        in.close();
    	FileOutputStream out = new FileOutputStream(filePath);
        props.setProperty(key, value);
        props.store(out, null);
        out.close();
    }

    /**
     * Update value of key in properties file.
     * @maker Dyan Despojo
     * @version 0.01
     * @param filePath path
     * @param key method name
     * @param value
     * @throws IOException exception
     * @since 0.01
     */
    public static void updateValue(String filePath,String key,String value) throws IOException {
    	FileInputStream in = new FileInputStream(filePath);
        Properties props = new Properties();
        props.load(in);
        in.close();

        FileOutputStream out = new FileOutputStream(filePath);
        props.setProperty(key, value);
        props.store(out, null);
        out.close();
    }
    
    /**
     * <div class="jp">
     *
     * </div>
     * <p>
     * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
     * Method for removing all property data of a specific scenario.
     * </div>
     * @maker Recuerdo Bregente
     * @version 0.01
     * @param filePath
     * @param startStr
     * @throws IOException
     * @since 0.01
     */
    public static void removeStatementsFromScenario(String filePath, String scenarionName, String operationType) throws IOException {
    	FileInputStream in = new FileInputStream(filePath);
        Properties props = new Properties();
        props.load(in);
        in.close();
		Enumeration enumProps = props.propertyNames();
		String key = "";
		String keyWithUnderscore = scenarionName.replace(" ", "_");

		while ( enumProps.hasMoreElements() ) {

			key = (String) enumProps.nextElement();
			if (key.contains(keyWithUnderscore) &&
					(null == operationType || (null != operationType && key.contains(operationType)))) {
				props.remove(key);
			}

		}

		FileOutputStream out = new FileOutputStream(filePath);
        props.store(out, null);
        out.close();

	}
    
    public static List<String> getStatementsFromScenario(String filePath, String scenarionName, String operation) throws IOException {
    	
    	List<String> statements = new ArrayList<String>();
    	FileInputStream in = new FileInputStream(filePath);
        Properties props = new Properties();
        props.load(in);
        in.close();
		Enumeration enumProps = props.propertyNames();
		String key = "";
		String keyWithUnderscore = scenarionName.replace(" ", "_");
		while ( enumProps.hasMoreElements() ) {

			key = (String) enumProps.nextElement();
			if (key.contains(keyWithUnderscore) && key.contains(operation)) {
				statements.add((String) props.get(key));
			}

		}
        return statements;
	}
    
    /**
     * 指定されていたの内容キーを取得する。
     * Retrieve the content key of the specified file.
     *
     * @param filePath file path
     * @param filePath ファイルパス
     * @param key
     * @return 対応キーの値
     * @return target key value
     */
    public static String getValue(String filePath, String key) throws IOException, NullPointerException {
        String message = null;
    	FileInputStream in = new FileInputStream(filePath);
    	Properties prop = new Properties();
        prop.load(in);
        message = prop.getProperty(key);
        return message;
    }

    /**
     * <div class="jp">
     * 利用可能な検証エラー·メ�?��ージの現在のリストを取得します�?</br>
     * ファイルに使用するため、シス�?��ロケールをロケールとして使用する�?/br>
     * application_&lt;locale&gt;.properties�?��らメ�?��ージを取得する�?
     * the a prefix custom.message.XXXXX.
     * <p>
     * WHERE: XXXXX メ�?��ージのIDに関�?
     * </div>
     * <p>
     * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
     * Retrieves the current listing of available validation error messages.</br>
     * System locale will be used as locale to use to what file to use.</br>
     * Messages are retrieved from application_&lt;locale&gt;.properties,
     * the a prefix custom.messaage.XXXXX.
     * <p>
     * WHERE: XXXXX pertains to the ID of the message
     * <p>
     * Source file: application_<locale>.properties
     * </div>
     * @maker Richard Go
     * @version 0.01
     * @return Map listing of all custom messages, with key as ID and value as the actual message.
     * @throws IOException thrown when utility fails to read the properties file.
     * @since 0.01
     */
    public static Map<String, String> getCustomMessage() throws IOException {
        return getCustomMessage(new java.util.Locale("ja"));
    }

    
    /**
     * パラメーターロケールとメ�?��ージコードに基づ�?��、ターゲ�?��メ�?��ージ値を取得する�?</br>
     * ファイルに使用するため、シス�?��ロケールをロケールとして使用する�?/br>
     * 
     * Retrieves the target message value based on the parameter locale and message code.</br>
     * System locale will be used as locale to use to what file to use.</br>
     * 
     * Source file: application_<locale>.properties
     *
     * @param locale target locale of messages to retrieve.
     * @param messageCode target message code to retrieve from the properties file.
     * @return Value of the message code.
     * @throws IOException thrown when utility fails to read the properties file.
     */
    public static String getCustomMessage(String messageCode) throws IOException {
        return getCustomMessage(new java.util.Locale("ja"), messageCode);
    }


    /**
     * 利用可能な検証エラー·メ�?��ージの現在のリストを取得します�?</br>
     * application_&lt;locale&gt;.properties�?��らメ�?��ージを取得する�?
     * the a prefix custom.messaage.XXXXX.</br>
     * 
     * Retrieves the current listing of available validation error messages.</br>
     * Messages are retrieved from application_&lt;locale&gt;.properties,
     * the a prefix custom.messaage.XXXXX.</br>
     * 
     * WHERE: XXXXX pertains to the ID of the message</br>
     * Source file: application_<locale>.properties
     *
     * @param locale target locale of messages to retrieve.
     * @return Map listing of all custom messages, with key as ID and value as the actual message.
     * @throws IOException thrown when utility fails to read the properties file.
     */
    public static Map<String, String> getCustomMessage(java.util.Locale locale) throws IOException {
        Properties prop = new Properties();
        prop.load(PropertiesUtil.class.getClassLoader()
                    .getResourceAsStream(_SYS_MSG_PREFIX_PROPERTIES + locale.getLanguage() + _FILE_SUFIX));
        Map<String, String> contents = new HashMap<String, String>();
        for (String key : prop.stringPropertyNames()) {
            if (!key.startsWith(MESSAGE_PREFIX)) { continue; }
            contents.put(key.replace(MESSAGE_PREFIX + PROP_KEY_DELIM, ""), prop.getProperty(key));
        }
        return contents;
    }


    /**
     * パラメーターロケールとメ�?��ージコードに基づ�?��、ターゲ�?��メ�?��ージ値を取得する�?</br>
     * Retrieves the target message value based on the parameter locale and message code.</br>
     * Source file: application_<locale>.properties
     *
     * @param locale target locale of messages to retrieve.
     * @param messageCode target message code to retrieve from the properties file.
     * @return Value of the message code.
     * @throws IOException thrown when utility fails to read the properties file.
     */
    public static String getCustomMessage(java.util.Locale locale, String messageCode) throws IOException {
        Properties prop = new Properties();
        prop.load(PropertiesUtil.class.getClassLoader()
                    .getResourceAsStream(_SYS_MSG_PREFIX_PROPERTIES + locale.getLanguage() + _FILE_SUFIX));
        return prop.getProperty(MESSAGE_PREFIX + PROP_KEY_DELIM + messageCode);
    }
}
