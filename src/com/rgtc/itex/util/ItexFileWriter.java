package com.rgtc.itex.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.json.JSONObject;

public class ItexFileWriter {
	
	public static void propertiesMapWriter(String absolutePath, TreeMap<String,JSONObject> map){
		System.out.println("writer");
		Writer out = null;
		try {
			//check directory
			File file = new File(absolutePath+".properties");
			if(!file.exists()){
				file.getParentFile().mkdirs();
				file.createNewFile();
			}
			FileWriter w = new FileWriter(file.getAbsoluteFile());
			out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),Charset.forName("UTF-8")));
			out.write("######################### ITEX File Writer V1 ########################\n");
			out.write("## Please use notepad++ for editing this file.                      ##\n");
			out.write("## Editing using eclipse messes up the character encoding.          ##\n");
			out.write("## Always set Character Encoding to UTF-8 to save non-english text. ##\n");
			out.write("######################################################################\n");
			Iterator i = map.entrySet().iterator();
			while(i.hasNext()){
				Map.Entry<String, JSONObject> pair = (Entry<String, JSONObject>) i.next();
				String key = pair.getKey();
				JSONObject value = pair.getValue();
				out.write(key);
				out.write("=");
				
				out.write(value.toString());
				out.write("\n");
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			if(null != out){
				try {
					out.flush();
					out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}

}
