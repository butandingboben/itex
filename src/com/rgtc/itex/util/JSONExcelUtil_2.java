/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.json.JSONException;
import org.json.JSONObject;
import org.skyscreamer.jsonassert.JSONCompare;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.JSONCompareResult;

import com.rgtc.itex.models.AssertionOutputModel;
import com.rgtc.itex.models.JSONModel;
import com.rgtc.itex.models.ParamaterModel;
import com.rgtc.itex.util.UtilConstants;

/**
 * <div class="jp">
 *
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 * Util class for generating data and expected json from a formatted excel file
 * </div>
 *
 * @author Joven Montero
 * @author Dyan Despojo
 * @author Jessamine Jimeno
 * @version 0.01
 * @since 0.01
 */
public class JSONExcelUtil_2 {

	/**
	 * The excel file source.
	 */
	private HSSFWorkbook testSource;
	/**
	 * The sheet for the json request.
	 */
	private HSSFSheet dataSheet;
	/**
	 * The sheet for the expected json response.
	 */
	private HSSFSheet expectedSheet;
	/**
	 * The sheet for the expected datastore result.
	 */
	private HSSFSheet executionLogSheet;
	/**
	 * The sheet for the status sheet.
	 */
	private HSSFSheet statusSheet;
	/**
	 * The sheet for the request headers sheet.
	 */
	private HSSFSheet requestHeaderSheet;
	/**
	 * The sheet for the status sheet.
	 */
	private HSSFSheet operationLogSheet;
	/**
	 * The sheet for the status sheet.
	 */
	private HSSFSheet urlParameterSheet;
	/**
	 * The sheet for the taskqueue sheet.
	 */
	private HSSFSheet taskQueueSheet;
	/**
	 * The sheet for the datastore operation log sheet.
	 */
	private HSSFSheet dsOpLogSheet;
	/**
	 * The list containing the json request of each test case.
	 */
	private Map<String, JSONObject> dataJSONList = new HashMap<String, JSONObject>();
	/**
	 * The list containing the expected json response of each test case.
	 */
	private Map<String, JSONObject> expectedJSONList = new HashMap<String, JSONObject>();
	/**
	 * The list containing the expected datastore result of each test case.
	 */
	private Map<String, JSONObject> executionLogJSONList = new HashMap<String, JSONObject>();
	/**
	 * The list containing the expected datastore result of each test case.
	 */
	private Map<String, JSONObject> statusJSONList = new HashMap<String, JSONObject>();
	/**
	 * The list containing the expected datastore result of each test case.
	 */
	private Map<String, JSONObject> requestHeaderJSONList = new HashMap<String, JSONObject>();
	/**
	 * The list containing the expected datastore result of each test case.
	 */
	private Map<String, JSONObject> operationLogJSONList = new HashMap<String, JSONObject>();
	/**
	 * The list containing the expected datastore result of each test case.
	 */
	private Map<String, JSONObject> urlParameterJSONList = new HashMap<String, JSONObject>();
	/**
	 * The list containing the expected datastore result of each test case.
	 */
	private Map<String, JSONObject> taskQueueJSONList = new HashMap<String, JSONObject>();
	/**
	 * The list containing the expected datastore result of each test case.
	 */
	private Map<String, JSONObject> dsOpLogJSONList = new HashMap<String, JSONObject>();
	/**
	 * The list containing the test cases of JSON_Data sheet.
	 */
	private List<String> dataJSONCases = new ArrayList<String>();
	/**
	 * The list containing the test cases of JSON_Expected sheet.
	 */
	private List<String> expectedJSONCases = new ArrayList<String>();
	/**
	 * The list containing the test cases of Datastore_Expected sheet.
	 */
	private List<String> executionLogJSONCases = new ArrayList<String>();
	/**
	 * The list containing the test cases of Datastore_Expected sheet.
	 */
	private List<String> statusJSONCases = new ArrayList<String>();
	/**
	 * The list containing the test cases of Datastore_Expected sheet.
	 */
	private List<String> requestHeaderJSONCases = new ArrayList<String>();
	/**
	 * The list containing the test cases of Datastore_Expected sheet.
	 */
	private List<String> operationLogJSONCases = new ArrayList<String>();
	/**
	 * The list containing the test cases of Datastore_Expected sheet.
	 */
	private List<String> urlParameterJSONCases = new ArrayList<String>();
	/**
	 * The list containing the test cases of Datastore_Expected sheet.
	 */
	private List<String> taskQueueJSONCases = new ArrayList<String>();
	/**
	 * The list containing the test cases of Datastore_Expected sheet.
	 */
	private List<String> dsOpLogJSONCases = new ArrayList<String>();

	/**
	 * <div class="jp">
	 * Getting all the json data from a specified excel file dynamically by
	 * providing the file and the name of a specific sheet.
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param testFile
	 * @return
	 * @throws Exception 
	 * @since 0.01
	 */
	public JSONModel getAllJSON(File testFile, String sheetName) throws Exception {
		JSONModel output = new JSONModel();
		HSSFSheet sheet = null;
		String error = "";

		try {
			testSource = new HSSFWorkbook(new FileInputStream(testFile));
			sheet = testSource.getSheet(sheetName);
			if (null != sheet) {
				getJSONData(sheet, output);
			} else {
				throw new Exception("SHEET_ERR");
			}
		} catch (FileNotFoundException e) {
			throw new FileNotFoundException("Specified file does not exist.");
		} catch (Exception e) {
			error = e.toString();
			if ("SHEET_ERR".equals(e.getMessage())) {
				throw new Exception("Sheet not found.");
			}
		}

		return output;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Function for loading the data from an excel file.
	 * </div>
	 * @maker Joven Montero
	 * @version 0.01
	 * @param file	The excel file.
	 * @since 0.01
	 */
	public String loadTestSource(File file) {
		String mes = null;
		try {
			testSource = new HSSFWorkbook(new FileInputStream(file));
			getSheets();
			if (null != dataSheet) {
				getAllJSON(dataSheet);
			}
			if (null != expectedSheet) {
				getAllJSON(expectedSheet);
			}
			if (null != executionLogSheet) {
				getAllJSON(executionLogSheet);
			}
			if (null != statusSheet) {
				getAllJSON(statusSheet);
			}
			if (null != requestHeaderSheet) {
				getAllJSON(requestHeaderSheet);
			}
			if (null != operationLogSheet) {
				getAllJSON(operationLogSheet);
			}
			if (null != urlParameterSheet) {
				getAllJSON(urlParameterSheet);
			}
			if (null != taskQueueSheet) {
				getAllJSON(taskQueueSheet);
			}
			if (null != dsOpLogSheet) {
				getAllJSON(dsOpLogSheet);
			}
		} catch (FileNotFoundException e) {
			mes = e.toString();
		} catch (IOException e) {
			mes = e.toString();
		} catch (OfficeXmlFileException e) {
			mes = e.toString();
		}
		return mes;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Function for getting the data and expected sheet from the excel file.
	 * </div>
	 * @maker Joven Montero
	 * @version 0.01
	 * @since 0.01
	 */
	public void getSheets() {
		if (null != testSource){
			dataSheet = testSource.getSheet(UtilConstants.DATA_SHEET);
			expectedSheet = testSource.getSheet(UtilConstants.EXPECTED_SHEET);
			executionLogSheet = testSource.getSheet(UtilConstants.EXECUTION_LOG_SHEET);
			statusSheet = testSource.getSheet(UtilConstants.STATUS_SHEET);
			requestHeaderSheet = testSource.getSheet(UtilConstants.REQUEST_HEADER_SHEET);
			operationLogSheet = testSource.getSheet(UtilConstants.OPERATION_LOG_SHEET);
			urlParameterSheet = testSource.getSheet(UtilConstants.URL_PARAMETER_SHEET);
			taskQueueSheet = testSource.getSheet(UtilConstants.TASK_QUEUE_SHEET);
			dsOpLogSheet = testSource.getSheet(UtilConstants.DS_OPERATION_LOG_SHEET);
		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Function for getting all the valid JSON.
	 * </div>
	 * @maker Joven Montero
	 * @version 0.01
	 * @param sheet The user-specified sheet to be assessed by the function.
	 * @since 0.01
	 */
	public void getAllJSON(HSSFSheet sheet) {
		ParamaterModel params = null;
		String testCase = null;
		Map<String, Object> jsonMap = null;
		// loop for getting all the json from excel depending on the specified sheet
		for (int i = 0; i < sheet.getLastRowNum(); i++) {
			try {
				jsonMap = new HashMap<String, Object>();
				testCase = sheet.getRow(i).getCell(0, HSSFRow.CREATE_NULL_AS_BLANK).toString();
				if (UtilConstants.TEST_CASE.equals(testCase.toLowerCase())) {
					params = new ParamaterModel();
					params.setStartRow(i + 1);
					params.setLastCol(sheet.getRow(i).getLastCellNum());
					HSSFCell cell = sheet.getRow(i).getCell(1, HSSFRow.CREATE_NULL_AS_BLANK);
					testCase = sheet.getRow(i).getCell(1, HSSFRow.CREATE_NULL_AS_BLANK).toString();

					// checks if the cell is a formula,
					// then validates the cell value of the formula
					if (HSSFCell.CELL_TYPE_FORMULA == cell.getCellType()) {
						FormulaEvaluator evaluator = testSource.getCreationHelper()
								.createFormulaEvaluator();
						testCase = evaluator.evaluate(cell).formatAsString().replace("\"", "");
					} else {
						testCase = cell.toString();
					}
					switch (sheet.getSheetName()) {
					case UtilConstants.DATA_SHEET:
						dataJSONCases.add(testCase);
						break;
					case UtilConstants.EXPECTED_SHEET:
						expectedJSONCases.add(testCase);
						break;
					case UtilConstants.EXECUTION_LOG_SHEET:
						executionLogJSONCases.add(testCase);
						break;
					case UtilConstants.STATUS_SHEET:
						statusJSONCases.add(testCase);
						break;
					case UtilConstants.REQUEST_HEADER_SHEET:
						requestHeaderJSONCases.add(testCase);
						break;
					case UtilConstants.OPERATION_LOG_SHEET:
						operationLogJSONCases.add(testCase);
						break;
					case UtilConstants.URL_PARAMETER_SHEET:
						urlParameterJSONCases.add(testCase);
						break;
					case UtilConstants.TASK_QUEUE_SHEET:
						taskQueueJSONCases.add(testCase);
						break;
					case UtilConstants.DS_OPERATION_LOG_SHEET:
						dsOpLogJSONCases.add(testCase);
						break;
					}

					jsonMap = generateJSON(params, sheet);
					jsonMap.remove(UtilConstants.DEPTH);
					if (UtilConstants.DATA_SHEET.equals(sheet.getSheetName())) {
						getDataJSONList().put(testCase, new JSONObject(jsonMap));
					} else if (UtilConstants.EXPECTED_SHEET.equals(sheet.getSheetName())) {
						getExpectedJSONList().put(testCase, new JSONObject(jsonMap));
					} else if (UtilConstants.EXECUTION_LOG_SHEET.equals(sheet.getSheetName())) {
						getExecutionLogJSONList().put(testCase, new JSONObject(jsonMap));
					} else if (UtilConstants.STATUS_SHEET.equals(sheet.getSheetName())) {
						getStatusJSONList().put(testCase, new JSONObject(jsonMap));
					} else if (UtilConstants.REQUEST_HEADER_SHEET.equals(sheet.getSheetName())) {
						getRequestHeaderJSONList().put(testCase, new JSONObject(jsonMap));
					} else if (UtilConstants.OPERATION_LOG_SHEET.equals(sheet.getSheetName())) {
						getOperationLogJSONList().put(testCase, new JSONObject(jsonMap));
					} else if (UtilConstants.URL_PARAMETER_SHEET.equals(sheet.getSheetName())) {
						getUrlParameterJSONList().put(testCase, new JSONObject(jsonMap));
					} else if (UtilConstants.TASK_QUEUE_SHEET.equals(sheet.getSheetName())) {
						getTaskQueueJSONList().put(testCase, new JSONObject(jsonMap));
					} else if (UtilConstants.DS_OPERATION_LOG_SHEET.equals(sheet.getSheetName())) {
						getDsOpLogJSONList().put(testCase, new JSONObject(jsonMap));
					}
//					switch (sheet.getSheetName()) {
//					case Constants.DATA_SHEET:
//						dataJSONCases.add(testCase);
//						getDataJSONList().put(testCase, new JSONObject(jsonMap));
//						break;
//					case Constants.EXPECTED_SHEET:
//						expectedJSONCases.add(testCase);
//						getExpectedJSONList().put(testCase, new JSONObject(jsonMap));
//						break;
//					case Constants.EXECUTION_LOG_SHEET:
//						executionLogJSONCases.add(testCase);
//						getExecutionLogJSONList().put(testCase, new JSONObject(jsonMap));
//						break;
//					case Constants.STATUS_SHEET:
//						statusJSONCases.add(testCase);
//						getStatusJSONList().put(testCase, new JSONObject(jsonMap));
//						break;
//					case Constants.REQUEST_HEADER_SHEET:
//						requestHeaderJSONCases.add(testCase);
//						getRequestHeaderJSONList().put(testCase, new JSONObject(jsonMap));
//						break;
//					case Constants.OPERATION_LOG_SHEET:
//						operationLogJSONCases.add(testCase);
//						getOperationLogJSONList().put(testCase, operationContentConvert(new JSONObject(jsonMap)));
//						break;
//					case Constants.URL_PARAMETER_SHEET:
//						urlParameterJSONCases.add(testCase);
//						getUrlParameterJSONList().put(testCase, new JSONObject(jsonMap));
//						break;
//					}
				}
			} catch (Exception e) {
				if (null == jsonMap) {
					System.err.println("[" + sheet.getSheetName() + ": " + testCase + "]: "
							+ "Invalid json format, please check input file");
//					break;
				}
			}
		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param jsonObject
	 * @return
	 * @since 0.01
	 */
	private JSONObject operationContentConvert(JSONObject jsonObject) {
		String operationContent = jsonObject.get("*Operation Content").toString()
				.replace("\":\"", "=")
				.replace("\",\"", ",")
				.replace("\"", "")
				.replace("{", "")
				.replace("}", "");
		jsonObject.remove("*Operation Content");
		jsonObject.put("Operation Content", operationContent);
		return jsonObject;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Function for getting the range of columns of a merged cell.
	 * </div>
	 * @maker Joven Montero
	 * @version 0.01
	 * @param sheet	The user-specified sheet to be assessed by the function.
	 * @param row	The row index of the cell.
	 * @param col	The column index of the cell.
	 * @return CellRangeAddress
	 * @since 0.01
	 */
	private CellRangeAddress returnRange(Sheet sheet, int row, int col) {
		for(int i = 0; i < sheet.getNumMergedRegions(); i++) {
			if (row == sheet.getMergedRegion(i).getFirstRow()
					&& col == sheet.getMergedRegion(i).getFirstColumn()) {
				return sheet.getMergedRegion(i);
			}
		}
		return null;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Function for determining if a cell is merged or not.
	 * </div>
	 * @maker Joven Montero
	 * @version 0.01
	 * @param sheet The user-specified sheet to be assessed by the function.
	 * @param row	The row index of the cell.
	 * @param col	The column index of the cell.
	 * @return boolean
	 * @since 0.01
	 */
	private boolean isMerged(Sheet sheet, int row, int col) {
		for(int i = 0; i < sheet.getNumMergedRegions(); i++) {
			if (row == sheet.getMergedRegion(i).getFirstRow()
					&& col == sheet.getMergedRegion(i).getFirstColumn()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Recursive function for generating the function for a single test case from the excel file.
	 * </div>
	 * @maker Joven Montero
	 * @version 0.01
	 * @param params	The object containing the parameters for the current function call.
	 * @param sheet		The user-specified sheet to be assessed by the function.
	 * @return Map<String, Object>
	 * @since 0.01
	 */
	public Map<String, Object> generateJSON(ParamaterModel params, HSSFSheet sheet) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		int col = params.getStartCol();
		int row = params.getStartRow();
		int depth = params.getDepth();
		int lastCol = params.getLastCol();
		boolean isList = params.isList();
		boolean noKeyFlag = false;
		HSSFRow rowData = null;
		HSSFCell cellData = null;
		List<Object> itemList = new ArrayList<Object>();

		// loop for getting the key and value column by column
		while(col <= lastCol) {
			ParamaterModel newParams;
			String key = null;
			String value = null;
			String firstKey = null;
			String nextKey = null;
			short boldWeight = 0;
			try {
				rowData = sheet.getRow(row);
				cellData = rowData.getCell(col, HSSFRow.CREATE_NULL_AS_BLANK);
				boldWeight = cellData.getCellStyle().getFont(testSource).getBoldweight();
				key = cellData.toString();

				// checks if cellData is a key
				if (UtilConstants.BOLD_FONT == boldWeight) {
					// checks if cellData is null
					if (!isList && UtilConstants.EMPTY_STRING.equals(key)
							|| UtilConstants.DASH.equals(key)) {
						return null;
					}
					// checks if cellData contains the empty string tag,
					// then removes the tag
					if (key.contains(UtilConstants.EMPTY_TAG)) {
						key = key.replace(UtilConstants.EMPTY_TAG, UtilConstants.EMPTY_STRING);
					}
					// checks if cellData is a list,
					// an object with several elements or a single object
					if (key.contains(UtilConstants.LIST_DELIMITER)) {
						// removes the double asterisk list delimiter
						if (key.contains(UtilConstants.LIST_DELIMITER)) {
							key = key.replace(UtilConstants.LIST_DELIMITER,
									UtilConstants.EMPTY_STRING);
						}
						newParams = new ParamaterModel();
						newParams.setStartRow(row + 1);
						newParams.setStartCol(col);
						newParams.setList(true);
						newParams.setLastCol(col + 1);
						// checks if the cellData is merged cell,
						// then gets the range and set the last column
						if (isMerged(sheet, row, col)) {
							CellRangeAddress range = returnRange(sheet, row, col);
							col = range.getLastColumn();
							newParams.setLastCol(col);
						} else if (!isMerged(sheet, row, col)) {
							newParams.setLastCol(col);
						}

						// calls the recursive function generateJSON
						// when a list is encountered
						Map<String, Object> outputMap = generateJSON(newParams, sheet);
						// checks if the output list is empty
						if (UtilConstants.EMPTY_LIST.equals(outputMap.get(UtilConstants.OUTPUT_LIST).toString())
								|| outputMap.get(UtilConstants.OUTPUT_LIST).toString().equals("[" + UtilConstants.EMPTY_OBJECT + "]")) {
							outputMap.put(UtilConstants.OUTPUT_LIST, new ArrayList<Object>());
						}
						jsonMap.put(key, outputMap.get(UtilConstants.OUTPUT_LIST));

						// checks if the previous column's depth is lesser
						// than the current column
						if (depth < Integer.parseInt(outputMap.get(UtilConstants.DEPTH)
								.toString())) {
							depth = Integer.parseInt(outputMap.get(UtilConstants.DEPTH)
									.toString());
						}
					} else if (isMerged(sheet, row, col)) {
						CellRangeAddress range = returnRange(sheet, row, col);
						newParams = new ParamaterModel();
						newParams.setStartRow(row + 1);
						newParams.setStartCol(col);
						newParams.setList(false);
						newParams.setLastCol(range.getLastColumn());

						// calls the recursive function generateJSON
						// when an object with several elements is encounter
						Map<String, Object> outputMap = generateJSON(newParams, sheet);
						outputMap.remove(UtilConstants.DEPTH);
						if (outputMap.toString().equals(UtilConstants.EMPTY_OBJECT)) {
							jsonMap.put(key, null);
						} else {
							jsonMap.put(key, outputMap);
						}
						col = newParams.getLastCol();
					} else {
						short nextRowBoldweight = sheet.getRow(row + 1).getCell(col, HSSFRow.CREATE_NULL_AS_BLANK)
								.getCellStyle().getFont(testSource).getBoldweight();
						if (UtilConstants.BOLD_FONT == nextRowBoldweight) {
							newParams = new ParamaterModel();
							newParams.setStartRow(row + 1);
							newParams.setStartCol(col);
							newParams.setList(false);
							newParams.setLastCol(col);
							Map<String, Object> outputMap = generateJSON(newParams, sheet);
							depth += Integer.parseInt(outputMap.get(UtilConstants.DEPTH).toString()) - 1;
							outputMap.remove(UtilConstants.DEPTH);
							jsonMap.put(key, outputMap);
							col = newParams.getLastCol();
						} else {
							value = sheet.getRow(row + 1).getCell(col, HSSFRow.CREATE_NULL_AS_BLANK)
									.toString();
							// checks if cell is numeric or not
							if(HSSFCell.CELL_TYPE_NUMERIC == sheet.getRow(row + 1)
									.getCell(col, HSSFRow.CREATE_NULL_AS_BLANK)
									.getCellType()) {
								jsonMap.put(key, sheet.getRow(row + 1)
										.getCell(col).getNumericCellValue());
							} else {
								if (value.contains(UtilConstants.EMPTY_TAG)) {
									jsonMap.put(key, UtilConstants.EMPTY_STRING);
								} else if (!UtilConstants.EMPTY_STRING.equals(value)
										&& !UtilConstants.DASH.equals(value)) {
									jsonMap.put(key, value);
								}
							}
						}
					}
					// checks if there are remaining items for a list element of the json
					if (isList && col == lastCol) {
						itemList.add(jsonMap);
						firstKey = sheet.getRow(row).getCell(params.getStartCol(),
								HSSFRow.RETURN_NULL_AND_BLANK).toString();
						nextKey = sheet.getRow(row + depth + 1).getCell(params.getStartCol(),
								HSSFRow.CREATE_NULL_AS_BLANK).toString();
						if (!UtilConstants.EMPTY_STRING.equals(nextKey)
								&& firstKey.equals(nextKey)) {
							col = params.getStartCol();
							row = row + depth + 1;
							depth = params.getDepth();
							jsonMap = new HashMap<String, Object>();
							continue;
						}
						depth = params.getStartCol();
					}
				} else {
					// enters when a list contains values with no assigned keys
					if (isList) {
						noKeyFlag = true;
						nextKey = sheet.getRow(row + 1).getCell(params.getStartCol(),
								HSSFRow.CREATE_NULL_AS_BLANK).toString();
						while(!UtilConstants.EMPTY_STRING.equals(nextKey)) {
							itemList.add(rowData.getCell(col,
									HSSFRow.CREATE_NULL_AS_BLANK).toString());
							nextKey = sheet.getRow(row + 1).getCell(params.getStartCol(),
									HSSFRow.CREATE_NULL_AS_BLANK).toString();
							row++;
						}
					}
				}
			} catch (NullPointerException e) {
			}
			col++;
		}
		// checks if list,
		// then adds "depth" to the map, its value depends on the size of itemList
		if (isList) {
			jsonMap = new HashMap<String, Object>();
			jsonMap.put(UtilConstants.OUTPUT_LIST, itemList);
			if (noKeyFlag) {
				jsonMap.put(UtilConstants.DEPTH, (itemList.size()));
			} else {
				jsonMap.put(UtilConstants.DEPTH, (itemList.size() * 2));
			}
		} else {
			jsonMap.put(UtilConstants.DEPTH, params.getDepth() + depth);
		}

		return jsonMap;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Function for asserting the expected and response JSON.
	 * </div>
	 * @maker Dyan Despojo
	 * @maker Jessamine Jimeno
	 * @version 0.01
	 * @param jsonActual	The actual response json from the controller.
	 * @param jsonExpected	The expected response json from the excel file.
	 * @return AssertionOutputModel
	 * @since 0.01
	 */
	public AssertionOutputModel validateJsonResponse(JSONObject jsonActual, JSONObject jsonExpected){
		//Add JSONAssert jar from lib to java build path
    	JSONCompareResult result = JSONCompare.compareJSON(
    			jsonExpected.toString(),
    			jsonActual.toString(),
    			JSONCompareMode.LENIENT);
    	String message = result.getMessage();
    	AssertionOutputModel outputModel = new AssertionOutputModel();

    	if(result.failed()){
    		outputModel.setIsError(true);
    		outputModel.setError(message);
    	} else{
    		outputModel.setIsError(false);
    		outputModel.setError(null);
    	}

    	return outputModel;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Function for validating an input if it a valid json.
	 * </div>
	 * @maker Dyan Despojo
	 * @maker Jessamine Jimeno
	 * @version 0.01
	 * @param output	The input to be assessed.
	 * @return boolean
	 * @since 0.01
	 */
	public boolean isJSONValid(Map<String, Object> output) {
	    try {
	        new JSONObject(output);
	    } catch(JSONException ex) {
	        return false;
	    }
        return true;
	}

	/**
	 * @return the dataJSONList
	 */
	public Map<String, JSONObject> getDataJSONList() {
		return dataJSONList;
	}

	/**
	 * @param dataJSONList the dataJSONList to set
	 */
	public void setDataJSONList(Map<String, JSONObject> dataJSONList) {
		this.dataJSONList = dataJSONList;
	}

	/**
	 * @return the expectedJSONList
	 */
	public Map<String, JSONObject> getExpectedJSONList() {
		return expectedJSONList;
	}

	/**
	 * @param expectedJSONList the expectedJSONList to set
	 */
	public void setExpectedJSONList(Map<String, JSONObject> expectedJSONList) {
		this.expectedJSONList = expectedJSONList;
	}

	/**
	 * @return the datastoreJSONList
	 */
	public Map<String, JSONObject> getExecutionLogJSONList() {
		return executionLogJSONList;
	}

	/**
	 * @param datastoreJSONList the datastoreJSONList to set
	 */
	public void setExecutionLogJSONList(Map<String, JSONObject> datastoreJSONList) {
		this.executionLogJSONList = datastoreJSONList;
	}

	/**
	 * @return the dataJSONCases
	 */
	public List<String> getDataJSONCases() {
		return dataJSONCases;
	}

	/**
	 * @param dataJSONCases the dataJSONCases to set
	 */
	public void setDataJSONCases(List<String> dataJSONCases) {
		this.dataJSONCases = dataJSONCases;
	}

	/**
	 * @return the expectedJSONCases
	 */
	public List<String> getExpectedJSONCases() {
		return expectedJSONCases;
	}

	/**
	 * @param expectedJSONCases the expectedJSONCases to set
	 */
	public void setExpectedJSONCases(List<String> expectedJSONCases) {
		this.expectedJSONCases = expectedJSONCases;
	}

	/**
	 * @return the expectedDatastoreCases
	 */
	public List<String> getExecutionLogJSONCases() {
		return executionLogJSONCases;
	}

	/**
	 * @param expectedDatastoreCases the expectedDatastoreCases to set
	 */
	public void setExecutionLogJSONCases(List<String> expectedDatastoreCases) {
		this.executionLogJSONCases = expectedDatastoreCases;
	}

	/**
	 * @return the statusJSONList
	 */
	public Map<String, JSONObject> getStatusJSONList() {
		return statusJSONList;
	}

	/**
	 * @param statusJSONList the statusJSONList to set
	 */
	public void setStatusJSONList(Map<String, JSONObject> statusJSONList) {
		this.statusJSONList = statusJSONList;
	}

	/**
	 * @return the statusJSONCases
	 */
	public List<String> getStatusJSONCases() {
		return statusJSONCases;
	}

	/**
	 * @param statusJSONCases the statusJSONCases to set
	 */
	public void setStatusJSONCases(List<String> statusJSONCases) {
		this.statusJSONCases = statusJSONCases;
	}

	/**
	 * @return the requestHeaderJSONList
	 */
	public Map<String, JSONObject> getRequestHeaderJSONList() {
		return requestHeaderJSONList;
	}

	/**
	 * @param requestHeaderJSONList the requestHeaderJSONList to set
	 */
	public void setRequestHeaderJSONList(Map<String, JSONObject> requestHeaderJSONList) {
		this.requestHeaderJSONList = requestHeaderJSONList;
	}

	/**
	 * @return the operationLogJSONList
	 */
	public Map<String, JSONObject> getOperationLogJSONList() {
		return operationLogJSONList;
	}

	/**
	 * @param operationLogJSONList the operationLogJSONList to set
	 */
	public void setOperationLogJSONList(Map<String, JSONObject> operationLogJSONList) {
		this.operationLogJSONList = operationLogJSONList;
	}

	/**
	 * @return the requestHeaderJSONCases
	 */
	public List<String> getRequestHeaderJSONCases() {
		return requestHeaderJSONCases;
	}

	/**
	 * @param requestHeaderJSONCases the requestHeaderJSONCases to set
	 */
	public void setRequestHeaderJSONCases(List<String> requestHeaderJSONCases) {
		this.requestHeaderJSONCases = requestHeaderJSONCases;
	}

	/**
	 * @return the operationLogJSONCases
	 */
	public List<String> getOperationLogJSONCases() {
		return operationLogJSONCases;
	}

	/**
	 * @param operationLogJSONCases the operationLogJSONCases to set
	 */
	public void setOperationLogJSONCases(List<String> operationLogJSONCases) {
		this.operationLogJSONCases = operationLogJSONCases;
	}

	/**
	 * @return the urlParameterJSONList
	 */
	public Map<String, JSONObject> getUrlParameterJSONList() {
		return urlParameterJSONList;
	}

	/**
	 * @param urlParameterJSONList the urlParameterJSONList to set
	 */
	public void setUrlParameterJSONList(Map<String, JSONObject> urlParameterJSONList) {
		this.urlParameterJSONList = urlParameterJSONList;
	}

	/**
	 * @return the urlParameterJSONCases
	 */
	public List<String> getUrlParameterJSONCases() {
		return urlParameterJSONCases;
	}

	/**
	 * @param urlParameterJSONCases the urlParameterJSONCases to set
	 */
	public void setUrlParameterJSONCases(List<String> urlParameterJSONCases) {
		this.urlParameterJSONCases = urlParameterJSONCases;
	}

	/**
	 * @return the taskQueueJSONList
	 */
	public Map<String, JSONObject> getTaskQueueJSONList() {
		return taskQueueJSONList;
	}

	/**
	 * @param taskQueueJSONList the taskQueueJSONList to set
	 */
	public void setTaskQueueJSONList(Map<String, JSONObject> taskQueueJSONList) {
		this.taskQueueJSONList = taskQueueJSONList;
	}

	/**
	 * @return the dsOpLogJSONList
	 */
	public Map<String, JSONObject> getDsOpLogJSONList() {
		return dsOpLogJSONList;
	}

	/**
	 * @param dsOpLogJSONList the dsOpLogJSONList to set
	 */
	public void setDsOpLogJSONList(Map<String, JSONObject> dsOpLogJSONList) {
		this.dsOpLogJSONList = dsOpLogJSONList;
	}

	/**
	 * @return the taskQueueJSONCases
	 */
	public List<String> getTaskQueueJSONCases() {
		return taskQueueJSONCases;
	}

	/**
	 * @param taskQueueJSONCases the taskQueueJSONCases to set
	 */
	public void setTaskQueueJSONCases(List<String> taskQueueJSONCases) {
		this.taskQueueJSONCases = taskQueueJSONCases;
	}

	/**
	 * @return the dsOpLogJSONCases
	 */
	public List<String> getDsOpLogJSONCases() {
		return dsOpLogJSONCases;
	}

	/**
	 * @param dsOpLogJSONCases the dsOpLogJSONCases to set
	 */
	public void setDsOpLogJSONCases(List<String> dsOpLogJSONCases) {
		this.dsOpLogJSONCases = dsOpLogJSONCases;
	}

	public void getJSONData(HSSFSheet sheet, JSONModel jsonModel) {
		ParamaterModel params = null;
		String testCase = null;
		Map<String, Object> jsonMap = null;
		// loop for getting all the json from excel depending on the specified sheet
		for (int i = 0; i < sheet.getLastRowNum(); i++) {
			try {
				jsonMap = new HashMap<String, Object>();
				testCase = sheet.getRow(i).getCell(0, HSSFRow.CREATE_NULL_AS_BLANK).toString();
				if (UtilConstants.TEST_CASE.equals(testCase.toLowerCase())) {
					params = new ParamaterModel();
					params.setStartRow(i + 1);
					params.setLastCol(sheet.getRow(i).getLastCellNum());
					HSSFCell cell = sheet.getRow(i).getCell(1, HSSFRow.CREATE_NULL_AS_BLANK);
					testCase = sheet.getRow(i).getCell(1, HSSFRow.CREATE_NULL_AS_BLANK).toString();

					// checks if the cell is a formula,
					// then validates the cell value of the formula
					if (HSSFCell.CELL_TYPE_FORMULA == cell.getCellType()) {
						FormulaEvaluator evaluator = testSource.getCreationHelper()
								.createFormulaEvaluator();
						testCase = evaluator.evaluate(cell).formatAsString().replace("\"", "");
					} else {
						testCase = cell.toString();
					}
					jsonModel.getTestCaseList().add(testCase);

					jsonMap = generateJSON(params, sheet);
					jsonMap.remove(UtilConstants.DEPTH);
					jsonModel.getJsonDataList().put(testCase, new JSONObject(jsonMap));
				}
			} catch (Exception e) {
				if (null == jsonMap) {
					System.err.println("[" + sheet.getSheetName() + ": " + testCase + "]: "
							+ "Invalid json format, please check input file");
//					break;
				}
			}
		}
	}
}
