/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) RococoGlobal Technologies Corporation - All Rights Reserved 2013
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.util;

/**
 * This class holds the constants.
 * 
 * @author Eldon Ivan
 * @since 12/03/2013
 * @version 1
 */
public interface UtilConstants {

	/**
	 * Loader functionality target.
	 */
	public static final String SCRIPT = "script";
	
	/**
	 * Loader functionality target.
	 */
	public static final String DESIGN = "design";
	
	/**
	 * File location of the Prepare Script.
	 */
	public static final String LOAD_FILE = "./scripts/Prepare.sql";
	
	/**
	 * File location of the Clean Script.
	 */
	public static final String UNLOAD_FILE = "./scripts/Clean.sql";

	// SQL RESERVE VALUES
	public static final String SQL_INSERT = "INSERT INTO ";
	public static final String SQL_DELETE = "DELETE FROM ";
	public static final String SQL_VALUES = " VALUES ";
	public static final String SQL_WHERE = " WHERE ";
	public static final String SQL_EQUALS = " = ";
	public static final String SQL_AND = " AND ";
	public static final String SQL_OR = " OR ";
	public static final String SQL_COMMENT = "--";
	public static final String SQL_SELECT = "SELECT ";
	public static final String SQL_FROM = " FROM ";
	public static final String SQL_TRUNCATE = "TRUNCATE ";
	public static final String SQL_DISABLE_FOREIGN_KEY_CHECK = "SET FOREIGN_KEY_CHECKS=0";
	public static final String SQL_ENABLE_FOREIGN_KEY_CHECK = "SET FOREIGN_KEY_CHECKS=1";
	
	// GENERAL RESERVE VALUES
	public static final String SYSDATE = "SYSDATE";
	public static final String NULL = " NULL";
	public static final String IS = " IS ";
	public static final String NOT = " NOT ";
	public static final String OPEN_PAR = " ( ";
	public static final String CLOSE_PAR = " ) ";
	public static final String SINGLE_QUOTE = "'";
	public static final String DOUBLE_QUOTE = "\"";
	public static final String COMMA = " , ";
	public static final String COLON = ";";

	public static final String CONSTRAINT_PK = "PK";
	public static final String CONSTRAINT_FK = "FK";
	
	public static final String SELECT = "SELECT";
	public static final String UPDATE = "UPDATE";
	public static final String DELETE = "DELETE";
	
	public static final String SELECT_ALL_COLUMNS = "*";
	
	/**
	 * Next line.
	 */
	public static final String NEXTLINE = "\n";
	
	// DATA TYPES
	
	public static final String NULL_VALUE = "null";
	/**
	 * Integer data type.
	 */
	public static final String INTEGER = "int";
	public static final String VARCHAR = "varchar";
	
	/**
	 * Double data type.
	 */
	public static final String DOUBLE = "DOUBLE";
	
	/**
	 * Float data type.
	 */
	public static final String FLOAT = "FLOAT";
	
	/**
	 * Date data type.
	 */
	public static final String DATE = "DATE";
	
	/**
	 * Timestamp data type.
	 */
	public static final String TIMESTAMP = "TIMESTAMP";
	
	/**
	 * String data type.
	 */
	public static final String STRING = "STRING";
	public static final String VARBINARY = "varbinary";
	
	/**
	 * Char data type.
	 */
	public static final String CHAR = "CHAR";
	
	// DEFAULT DATA TYPE VALUES
	public static final String DEFAULT_INTEGER = "0";
	public static final String DEFAULT_DOUBLE = "0.0";
	public static final String DEFAULT_FLOAT = "0.0";
	public static final String DEFAULT_DATE = "SYSDATE";
	public static final String DEFAULT_TIMESTAMP = "SYSDATE";
	public static final String DEFAULT_VARCHAR = "NULL";
	public static final String DEFAULT_CHAR = "NULL";
	
	/**
	 * Number of header rows in the Excel file.
	 */
	public static final String TARGET_SHEET = "Data";
	public static final int HEADER_COUNT_TABLE = 5;
	public static final int HEADER_COUNT_OPERATION = 1;
	
	/* UI */
	public static final String MENU_FILE ="File";
	public static final String MENU_FILE_DB_SETUP="Database setup";
	public static final String LABEL_FILE = "Filename:";
	public static final String LABEL_SHEET="Sheet:";
	public static final String LABEL_SCENARIO="Scenario:";
	public static final String BUTTON_LOAD_SCENARIO ="Load";
	public static final String BUTTON_CHECK_SCENARIO ="Check";
	public static final String BUTTON_BROWSE ="Browse";
	public static final String BUTTON_LOAD ="Load";
	public static final String BUTTON_RELOAD ="Reload";
	public static final String BUTTON_VALIDATE ="Validate";
	public static final String BUTTON_UPLOAD ="Upload";
	public static final String BUTTON_GENERATE_SCRIPT="Generate Script";
	public static final String BUTTON_GENERATE="Generate";
	public static final String BUTTON_GENERATE_JSON="Generate JSON";
	public static final String LABEL_DETAILS="Details";
	public static final String LABEL_GENERATE="Generate";
	public static final String LABEL_INSERT="Insert";
	public static final String LABEL_DELETE="Delete";
	public static final String BUTTON_INSERT_UP="^";
	public static final String BUTTON_INSERT_DOWN="v";
	public static final String BUTTON_DELETE_UP="UP";
	public static final String BUTTON_DELETE_DOWN="DOWN";
	public static final String LABEL_GENERATE_FILE_PATH="Generate File Path: ";
	public static final String LABEL_GENERATE_INSERT_FILE="Insert File Name: ";
	public static final String LABEL_GENERATE_DELETE_FILE="Delete File Name: ";
	public static final String LABEL_GENERATE_FILE_NAME="File Name: ";
	
	/*	CONSTANTS FOR READING THE EXCEL FILE */
	/**
	 * Index for the scenario title.</br>
	 * Column (A) of excel file 
	 */
	public static final int COLUMN_SCENARIO_INDEX=0;
	public static final int COLUMN_INDEX_0=0;
	
	/**
	 * Column (B) of excel file
	 */
	public static final int COLUMN_B=1;
	
	/**
	 * Index for defining the database read start.</br>
	 * Column (C) of excel file
	 */
	public static final int COLUMN_DATABASE_INDEX=2;
	public static final int COLUMN_C=2;
	
	/**
	 * Index for the database table names.</br>
	 * Column (D) of excel file
	 */
	public static final int COLUMN_TABLE_NAMES_INDEX=3;
	public static final int COLUMN_D=3;
	
	/**
	 * Index for the database column names.</br>
	 * Column (E) of excel file
	 */
	public static final int COLUMN_COLUMN_NAMES_INDEX=4;
	
	/**
	 * Index for the database column data.</br>
	 * Column (E) of excel file
	 */
	public static final int COLUMN_COLUMN_DATA_INDEX=4;
	
	public static final String QUERY_ALL_TABLES="SHOW TABLES";
	public static final String QUERY_ALL_COLUMNS="SHOW COLUMNS FROM `?`";
	
	public static final String BEFORE="BEFORE";
	public static final String AFTER="AFTER";
	
	public static final String LABEL_DATABASE_NAME = "Database Name: ";
	public static final String LABEL_URL = "URL: ";
	public static final String LABEL_USERNAME = "Username: ";
	public static final String LABEL_PASSWORD = "Password: ";

	public static final String NULL_TAG = "-";
	public static final String DYNAMIC_TAG = "*D";
	public static final String EMPTY_TAG = "*EMP";
	
	public static final String SCENARIO_NAME = "TEST CASE: ";
	public static final String DATABASE_READ_BEFORE_START = "DATABASE ENTRIES START - BEFORE";
	public static final String DATABASE_TABLE_NAME = "TABLE NAME: ";
	public static final String DATABASE_READ_BEFORE_END = "DATABASE ENTRIES END - BEFORE";
	public static final String DATABASE_READ_AFTER_START = "DATABASE ENTRIES START - AFTER";
	public static final String DATABASE_READ_AFTER_END = "DATABASE ENTRIES END - AFTER";
	public static final String DATABASE_EMPTY_STRING = "''";
	public static final String SCENARIO_END = "TEST CASE END";
	public static final String C_TAG = "**C";
	public static final String E_TAG = "**E";
	public static final String EE_TAG = "**EE";
	public static final String ET_TAG = "**ET";
	public static final String SUFFIX_SQL =".sql";
	public static final String SUFFIX_TXT=".txt";
	public static final String SUFFIX_CSV=".csv";
	
	//JSONExelUtil constants
	public static final int BOLD_FONT = 700;
	public static final int REGULAR_FONT = 400;
	public static final String EMPTY_STRING = "";
	public static final String EMPTY_LIST = "[]";
	public static final String DASH = "-";
	public static final String LIST_DELIMITER = "**";
	public static final String TEST_CASE = "test case";
	public static final String DATA_SHEET = "JSON_Data";
//	public static final String EXPECTED_SHEET = "JSON_Expected";
	public static final String EXPECTED_SHEET = "Expected_Output";
	public static final String DEPTH = "depth";
	public static final String OUTPUT_LIST = "outputList";
	
	//DB Settings
	public static final String TEXT_DATABASE_NAME = "sbc-test";
	public static final String TEXT_URL = "jdbc:mysql://localhost:3306";
	public static final String TEXT_USERNAME = "root";
	public static final String TEXT_PASSWORD = "root";
	
	//Alert Messages
	public static final String ALERT_CONNECTION_NOT_SET = "DB Connection not set. \nPlease set-up DB connection on the File menu.";
	public static final String ALERT_EXCEL_FILE_LOADED = "Data from excel file is loaded.";
	public static final String ALERT_EXCEL_FILE_INCORRECT = "Excel file format is incorrect.";
	public static final String ALERT_CONNECTION_SUCCESSFUL = "DB connection successful.";
	public static final String ALERT_CONNECTION_FAILED = "DB connection failed.";
	public static final String ALERT_PRESS_BUTTON_AGAIN = "\nYou may now press the Load button again.";
	public static final String ALERT_JSON_SUCCESSFUL = "JSON statements are successfully created.";
	
	//Properties file name
	public static final String SCRIPT_FILENAME = "scripts.properties";
	public static final String JSON_FILENAME = "json.properties";
	
	//DataLoader Scenario type
	public static final String INSERT_TYPE = "insert";
	public static final String SELECT_TYPE = "select";
	
	//File Path to property file.
	public static final String SCRIPT_FILE_PATH = "C:\\personal\\Programming\\RGTC\\2 Software Development\\01_Process\\02_SDLC\\04_Testing\\03_Data Loader\\DataLoaderV3\\sample\\scripts.properties";

	public static final String EXECUTION_LOG_SHEET = "Execution_Log";
	public static final String STATUS_SHEET = "JSON_Status";

	public static final String REQUEST_HEADER_SHEET = "Request_Header";
	public static final String OPERATION_LOG_SHEET = "Operation_Log";

	public static final String ENCRYPTION_KEY = "sbcPHKey0655";
	public static final String ENCRYPT_FLAG = "*";
	public static final String DATE_FLAG = "#";

	public static final String TINYINT = "TINYINT";
	public static final String BIT = "BIT";
	// Added for handling new database column types
	// Date: 2014 11 11 07:54
	// By: Bob
	public static final String TEXT = "text";
	public static final String DATETIME = "datetime";
	public static final String ENUM = "enum";
	// Date: 2015 02 05 10:00
	// By: Bob
	public static final String TIME = "TIME";
	public static final String SMALLINT = "SMALLINT";
	public static final String POINT = "POINT";
	public static final String DECIMAL = "DECIMAL";

	public static final String URL_PARAMETER_SHEET = "URL_Parameter";
	public static final String DATE_FORMAT_yyyyMMddHHmmss = "yyyyMMddHHmmss";

	public static final String EMPTY_OBJECT = "{}";

	public static final String TASK_QUEUE_SHEET = "Task_Queue";
	public static final String DS_OPERATION_LOG_SHEET = "DS_Operation_Log";

	// Nikkan constants
	public static final String FILENAME_CHECK_SHEET = "Filename_Check";
	public static final String FORMAT_CHECK_SHEET = "Format_Check";
	public static final String CSV_DATA_CHECK_SHEET = "CSVData_Check";
	public static final String PARAMETER_INPUT_SHEET = "Parameter_Input";
	public static final String MASTER_DB_DATA_SHEET = "Master_DB_Data";

	public static final String MASTER_TEST_CASE = "MASTER";
	public static final String NO_OUTPUT_CSV = "NO_OUTPUT_CSV";

	public static final String TEST_FILE_TAG = "Test File";
	public static final String TEST_CASE_END = "TEST CASE END";
	
	// Formats and reserved words
	public static final String TIME_FORMAT = "TIME_FORMAT";
	public static final String DATE_FORMAT = "DATE_FORMAT";
	public static final String LPAD = "LPAD";
	public static final String FIXED = "FIXED";
	public static final String RANGE = "RANGE";
	public static final String MATCH = "MATCH";
	
	// Common regex
	public static final String TIME_HHMMSS = "([01]?[0-9]|2[0-3])[0-5][0-9][0-5][0-9]";
	
	//parameter_input tags
	public static final String PARAM_INPUT_TESTCASENO = "Test Case No.";
	public static final String PARAM_INPUT_TESTCASEEND = "TEST CASE END";

	public static final String XMLDATA_CHECK_SHEET = "XMLData_Check";

	// HTTP Request Constants
	public static final int HTTP_GET_METHOD = 0;
	public static final int HTTP_POST_METHOD = 1;
	public static final int HTTP_PUT_METHOD = 2;
	public static final int HTTP_DELETE_METHOD = 3;
}
