/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * Utility for reading the simplified Keiba Basic Design document.
 * @author Joven 'Bob' Montero
 * @version 0.01
 * @since 0.01
 */
public class ExcelService {

	/**
	 * The excel file containing the simplified basic design specifications.
	 */
	private Workbook sourceFile;
	/**
	 * List containing the sheets from the source file.
	 */
	private static List<Sheet> sheets;

	/**
	 * Function for loading the source file.
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param file
	 * @return
	 * @since 0.01
	 */
	public String loadFile(File file) {
		String output = "";
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
			sourceFile = WorkbookFactory.create(fis);
		} catch(Exception e) {
			e.printStackTrace();
		} finally{
			if(null != fis){
				try {
					fis.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				fis = null;
			}
		}

		return output;
	}

	public void loadWorkbook(Workbook workbook) {
		sourceFile = workbook;
	}

	/**
	 * Function for getting all the sheets from an excel file
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @return
	 * @since 0.01
	 */
	public List<Sheet> getAllSheets() {
		List<Sheet> outputList = new ArrayList<Sheet>();

		System.out.println(getSourceFile().getNumberOfSheets());
		for (int i = 0; i < getSourceFile().getNumberOfSheets(); i++) {
			outputList.add(getSourceFile().getSheetAt(i));
		}

		return outputList;
	}

	/**
	 * Function for getting a specific sheet by its name.
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param sheetName
	 * @return
	 * @since 0.01
	 */
	public Sheet getSheet(String sheetName) {
		Sheet output = null;

		output = sourceFile.getSheet(sheetName);

		return output;
	}

	/**
	 * Function for getting a specific sheet by index.
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param sheetName
	 * @return
	 * @since 0.01
	 */
	public Sheet getSheet(int index) {
		Sheet output = null;

		try {
			output = sourceFile.getSheetAt(index);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return output;
	}

	/**
	 * @return the sourceFile
	 */
	public Workbook getSourceFile() {
		return this.sourceFile;
	}

	/**
	 * @param sourceFile the sourceFile to set
	 */
	public void setSourceFile(Workbook sourceFile) {
		this.sourceFile = sourceFile;
	}
	
	public void closeWorkbook(){
		sourceFile = null;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param sourceFile2
	 * @since 0.01
	 */
	public void loadXLSXFile(File file) throws Exception {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
			OPCPackage opcPackage = OPCPackage.open(fis);
//			XSSFWorkbook sourceXLSXFile = new XSSFWorkbook(opcPackage);
			XSSFReader r = new XSSFReader(opcPackage);
			SharedStringsTable sst = r.getSharedStringsTable();
			
			XMLReader parser = fetchSheetParser(sst);
			
			Iterator<InputStream> sheets = r.getSheetsData();
			while(sheets.hasNext()) {
				System.out.println("Processing new sheet:\n");
				InputStream sheet = sheets.next();
				InputSource sheetSource = new InputSource(sheet);
				parser.parse(sheetSource);
				sheet.close();
				System.out.println("");
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally{
			if(null != fis){
				try {
					fis.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				fis = null;
			}
		}
	}
	
	public XMLReader fetchSheetParser(SharedStringsTable sst) throws SAXException {
		XMLReader parser =
			XMLReaderFactory.createXMLReader(
					"org.apache.xerces.parsers.SAXParser"
			);
		ContentHandler handler = new SheetHandler(sst);
		parser.setContentHandler(handler);
		return parser;
	}
	
	/** 
	 * See org.xml.sax.helpers.DefaultHandler javadocs 
	 */
	private static class SheetHandler extends DefaultHandler {
		private SharedStringsTable sst;
		private String lastContents;
		private boolean nextIsString;
		
		private SheetHandler(SharedStringsTable sst) {
			this.sst = sst;
		}
		
		public void startElement(String uri, String localName, String name,
				Attributes attributes) throws SAXException {
			// c => cell
			if(name.equals("c")) {
				// Print the cell reference
				System.out.print(attributes.getValue("r") + " - ");
				// Figure out if the value is an index in the SST
				String cellType = attributes.getValue("t");
				if(cellType != null && cellType.equals("s")) {
					nextIsString = true;
				} else {
					nextIsString = false;
				}
			}
			// Clear contents cache
			lastContents = "";
		}
		
		public void endElement(String uri, String localName, String name)
				throws SAXException {
			// Process the last contents as required.
			// Do now, as characters() may be called more than once
			if(nextIsString) {
				int idx = Integer.parseInt(lastContents);
				lastContents = new XSSFRichTextString(sst.getEntryAt(idx)).toString();
            nextIsString = false;
			}

			// v => contents of a cell
			// Output after we've seen the string contents
			if(name.equals("v")) {
				System.out.println(lastContents);
			}
		}

		public void characters(char[] ch, int start, int length)
				throws SAXException {
			lastContents += new String(ch, start, length);
		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param failureList2 
	 * @since 0.01
	 */
	public static void generateAnalysis(List<List<JSONObject>> failureList, String outputPath, String test, int maxTC) {
		try {
			JSONArray analysisList = new JSONArray();
			for (List<JSONObject> list : failureList) {
				for (JSONObject failure : list) {
					boolean existFlag = false;
					JSONObject currentLine = new JSONObject();
					for (int i = 0; i < analysisList.length(); i++) {
						String line = analysisList.getJSONObject(i).get("line").toString();
						if (line.equals(failure.get("line").toString())) {
							currentLine = analysisList.getJSONObject(i);
							existFlag = true;
							break;
						}
					}
					if (!existFlag) {
						analysisList.put(new JSONObject("{'line': '" + failure.get("line").toString() + "', 'cases': []}"));
						currentLine = analysisList.getJSONObject(analysisList.length() - 1);
					}

					existFlag = false;
					for (int i = 0; i < currentLine.getJSONArray("cases").length(); i++) {
						JSONObject caseJSON = currentLine.getJSONArray("cases").getJSONObject(i);
						if (caseJSON.getInt("colNo") == failure.getInt("colNo")) {
							existFlag = true;
							if (!caseJSON.getJSONArray("testCaseNames").toString().contains(failure.getString("testCaseName"))) {
								caseJSON.getJSONArray("testCaseNames").put(failure.getString("testCaseName"));
							}
							break;
						}
					}
					if (!existFlag) {
						JSONObject newCase = new JSONObject();
						newCase.put("col", failure.getString("col"));
						newCase.put("colNo", failure.getInt("colNo"));
						newCase.put("rowNo", failure.getInt("rowNo"));
						newCase.put("actualData", failure.getString("actualData"));
						newCase.put("expectedData", failure.getString("expectedData"));
						newCase.put("cause", failure.getString("cause"));
						newCase.put("testCaseNames", new JSONArray().put(failure.getString("testCaseName")));
						currentLine.getJSONArray("cases").put(newCase);
					}
				}
			}
			writeAnalysisFile(analysisList, outputPath, test, maxTC);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Emman Resmeros
	 * @version 0.01
	 * @param failureList2 
	 * @since 0.01
	 */
	public static void generateIndexFileAnalysis(List<List<JSONObject>> failureList, String outputPath, String test, int maxTC) {
		try {
			JSONArray analysisList = new JSONArray();
			for (List<JSONObject> list : failureList) {
				for (JSONObject failure : list) {
					boolean existFlag = false;
					JSONObject currentLine = new JSONObject();
					for (int i = 0; i < analysisList.length(); i++) {
						String line = analysisList.getJSONObject(i).get("tag").toString();
						if (line.equals(failure.get("tag").toString())) {
							currentLine = analysisList.getJSONObject(i);
							existFlag = true;
							break;
						}
					}

					if (!existFlag) {
						analysisList.put(new JSONObject("{'tag': " + failure.get("tag").toString() + ", 'cases': []}"));
						currentLine = analysisList.getJSONObject(analysisList.length() - 1);
					}

					existFlag = false;

					for (int i = 0; i < currentLine.getJSONArray("cases").length(); i++) {
						JSONObject caseJSON = currentLine.getJSONArray("cases").getJSONObject(i);

						if (!caseJSON.getJSONArray("testCaseNames").toString().contains(failure.getString("testCaseName"))) {
							System.out.println("added testcasename: " + failure.getString("testCaseName"));
							caseJSON.getJSONArray("testCaseNames").put(failure.getString("testCaseName"));
						}
					}

					if (!existFlag) {
						JSONObject newCase = new JSONObject();
						newCase.put("tag", failure.getString("tag"));
						newCase.put("actualData", failure.getString("actual"));
						newCase.put("expectedData", failure.getString("expected"));
						newCase.put("cause", failure.getString("cause"));
						newCase.put("testCaseNames", new JSONArray().put(failure.getString("testCaseName")));
						currentLine.getJSONArray("cases").put(newCase);
					}
				}
			}
			writeIndexAnalysisFile(analysisList, outputPath, test, maxTC);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param analysisList
	 * @param outputPath
	 * @since 0.01
	 */
	private static void writeAnalysisFile(JSONArray analysisList, String outputPath, String test, int maxTC) {
		Workbook outputWorkbook = new XSSFWorkbook();
		CreationHelper creationHelper = outputWorkbook.getCreationHelper();
		Sheet analysisSheet = outputWorkbook.createSheet("Analysis");
		analysisSheet.setColumnWidth(0, 3500);
//			analysisSheet.autoSizeColumn(0);
		// Create bold header style
		CellStyle headerStyle = outputWorkbook.createCellStyle();
		CellStyle resultStyle = outputWorkbook.createCellStyle();
		CellStyle caseStyle = outputWorkbook.createCellStyle();
		XSSFFont font = (XSSFFont) outputWorkbook.createFont();
		font.setBold(true);
		headerStyle.setFont(font);
		resultStyle.setFillBackgroundColor(IndexedColors.LIGHT_BLUE.getIndex());

		// Create a row and put some cells in it.
		Row actualDataRow = analysisSheet.createRow((short) 0);
		Row expectedDataRow = analysisSheet.createRow((short) 1);
		Row descRow = analysisSheet.createRow((short) 2);
		Cell actualHeaderCell = actualDataRow.createCell(0);
		Cell expectedHeaderCell = expectedDataRow.createCell(0);
		Cell descHeaderCell = descRow.createCell(0);
		actualDataRow.setHeight((short) 1000);
		expectedDataRow.setHeight((short) 1000);
		descRow.setHeight((short) 1500);
		actualHeaderCell.setCellValue("Actual");
		actualHeaderCell.setCellStyle(headerStyle);
		expectedHeaderCell.setCellValue("Expected");
		expectedHeaderCell.setCellStyle(headerStyle);
		descHeaderCell.setCellValue("Description");
		descHeaderCell.setCellStyle(headerStyle);

		DecimalFormat lpad = new DecimalFormat("000");
		int tcCnt = 1;
		// Creating rows for the test cases
		for (int i = 3; i < (maxTC + 3); i++) {
			Row testCaseRow = analysisSheet.createRow((short) i);
			Cell testCaseCell = testCaseRow.createCell(0);
			testCaseCell.setCellValue(test + "-" + lpad.format(tcCnt));
			tcCnt = tcCnt + 1;
		}

		font = (XSSFFont) outputWorkbook.createFont();
		font.setBold(false);
		headerStyle = outputWorkbook.createCellStyle();
		headerStyle.setFont(font);
		headerStyle.setWrapText(true);
		caseStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		caseStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

		for (int i = 0; i < analysisList.length(); i++) {
			JSONObject lineJSON = analysisList.getJSONObject(i);
			JSONArray casesJSON = lineJSON.getJSONArray("cases");
			String line = lineJSON.getString("line");

			for (int j = 0; j < casesJSON.length(); j++) {
				JSONObject caseJSON = casesJSON.getJSONObject(j);
				int nextCol = actualDataRow.getLastCellNum();
				String desc = "Line: " + line + "\n"
						+ "Item: " + caseJSON.getString("col") + "\n"
						+ "Cause:\n" + caseJSON.getString("cause") + "\n";
				actualDataRow.createCell(nextCol).setCellValue(caseJSON.getString("actualData"));
				expectedDataRow.createCell(nextCol).setCellValue(caseJSON.getString("expectedData"));
				descRow.createCell(nextCol).setCellValue(desc);
				actualDataRow.getCell(nextCol).setCellStyle(headerStyle);
				expectedDataRow.getCell(nextCol).setCellStyle(headerStyle);
				descRow.getCell(nextCol).setCellStyle(headerStyle);
				analysisSheet.setColumnWidth(nextCol, 5000);

				JSONArray testCaseNames = caseJSON.getJSONArray("testCaseNames");
				for (int k = 0; k < testCaseNames.length(); k++) {
					try {
						Row testCaseRow = getTestCaseRow(analysisSheet, testCaseNames.getString(k));
						Cell testCaseCell = testCaseRow.createCell(nextCol);
						testCaseCell.setCellStyle(caseStyle);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

		FileOutputStream fileOut;
		boolean createFileFlag = false;
		int retry = 0;
		while (!createFileFlag) {
			try {
				if (0 == retry) {
					fileOut = new FileOutputStream(new File(outputPath));
				} else {
					fileOut = new FileOutputStream(new File(outputPath.replace(".xls", " (" + retry + ").xls")));
				}
				createFileFlag = true;

				outputWorkbook.write(fileOut);
				fileOut.close();
			} catch (FileNotFoundException e) {
				retry = retry + 1;
				createFileFlag = false;
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param analysisList
	 * @param outputPath
	 * @since 0.01
	 */
	private static void writeIndexAnalysisFile(JSONArray analysisList, String outputPath, String test, int maxTC) {
		Workbook outputWorkbook = new XSSFWorkbook();
		CreationHelper creationHelper = outputWorkbook.getCreationHelper();
		Sheet analysisSheet = outputWorkbook.createSheet("Index File Analysis");
		analysisSheet.setColumnWidth(0, 3500);
//			analysisSheet.autoSizeColumn(0);
		// Create bold header style
		CellStyle headerStyle = outputWorkbook.createCellStyle();
		CellStyle resultStyle = outputWorkbook.createCellStyle();
		CellStyle caseStyle = outputWorkbook.createCellStyle();
		XSSFFont font = (XSSFFont) outputWorkbook.createFont();
		font.setBold(true);
		headerStyle.setFont(font);
		resultStyle.setFillBackgroundColor(IndexedColors.LIGHT_BLUE.getIndex());

		// Create a row and put some cells in it.
		Row actualDataRow = analysisSheet.createRow((short) 0);
		Row expectedDataRow = analysisSheet.createRow((short) 1);
		Row descRow = analysisSheet.createRow((short) 2);
		Cell actualHeaderCell = actualDataRow.createCell(0);
		Cell expectedHeaderCell = expectedDataRow.createCell(0);
		Cell descHeaderCell = descRow.createCell(0);
		actualDataRow.setHeight((short) 1000);
		expectedDataRow.setHeight((short) 1000);
		descRow.setHeight((short) 1500);
		actualHeaderCell.setCellValue("Actual");
		actualHeaderCell.setCellStyle(headerStyle);
		expectedHeaderCell.setCellValue("Expected");
		expectedHeaderCell.setCellStyle(headerStyle);
		descHeaderCell.setCellValue("Description");
		descHeaderCell.setCellStyle(headerStyle);

		DecimalFormat lpad = new DecimalFormat("000");
		int tcCnt = 1;
		// Creating rows for the test cases
		for (int i = 3; i < (maxTC + 3); i++) {
			Row testCaseRow = analysisSheet.createRow((short) i);
			Cell testCaseCell = testCaseRow.createCell(0);
			testCaseCell.setCellValue(test + "-" + lpad.format(tcCnt));
			tcCnt = tcCnt + 1;
		}

		font = (XSSFFont) outputWorkbook.createFont();
		font.setBold(false);
		headerStyle = outputWorkbook.createCellStyle();
		headerStyle.setFont(font);
		headerStyle.setWrapText(true);
		caseStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		caseStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

		for (int i = 0; i < analysisList.length(); i++) {
			JSONObject lineJSON = analysisList.getJSONObject(i);
			JSONArray casesJSON = lineJSON.getJSONArray("cases");
			String line = lineJSON.getString("tag");

			for (int j = 0; j < casesJSON.length(); j++) {
				JSONObject caseJSON = casesJSON.getJSONObject(j);
				int nextCol = actualDataRow.getLastCellNum();
				String desc = "Line: " + line + "\n"
						+ "Cause:\n" + caseJSON.getString("cause") + "\n";
				actualDataRow.createCell(nextCol).setCellValue(caseJSON.getString("actualData"));
				expectedDataRow.createCell(nextCol).setCellValue(caseJSON.getString("expectedData"));
				descRow.createCell(nextCol).setCellValue(desc);
				actualDataRow.getCell(nextCol).setCellStyle(headerStyle);
				expectedDataRow.getCell(nextCol).setCellStyle(headerStyle);
				descRow.getCell(nextCol).setCellStyle(headerStyle);
				analysisSheet.setColumnWidth(nextCol, 5000);

				JSONArray testCaseNames = caseJSON.getJSONArray("testCaseNames");
				for (int k = 0; k < testCaseNames.length(); k++) {
					try {
						Row testCaseRow = getTestCaseRow(analysisSheet, testCaseNames.getString(k));
						Cell testCaseCell = testCaseRow.createCell(nextCol);
						testCaseCell.setCellStyle(caseStyle);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

		FileOutputStream fileOut;
		boolean createFileFlag = false;
		int retry = 0;
		while (!createFileFlag) {
			try {
				if (0 == retry) {
					fileOut = new FileOutputStream(new File(outputPath));
				} else {
					fileOut = new FileOutputStream(new File(outputPath.replace(".xls", " (" + retry + ").xls")));
				}
				createFileFlag = true;

				outputWorkbook.write(fileOut);
				fileOut.close();
			} catch (FileNotFoundException e) {
				retry = retry + 1;
				createFileFlag = false;
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param analysisSheet
	 * @param testCase
	 * @return
	 * @since 0.01
	 */
	private static Row getTestCaseRow(Sheet analysisSheet, String testCase) {
		try {
			for (int i = 3; i <= analysisSheet.getLastRowNum(); i++) {
				String testCaseRow = analysisSheet.getRow(i).getCell(0).getStringCellValue();
				if (testCase.equals(testCaseRow)) {
					return analysisSheet.getRow(i);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}
}
