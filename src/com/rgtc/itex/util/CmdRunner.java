/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class for running a command in the shell.
 * @author Joven 'Bob' Montero
 * @version 0.01
 * @since 0.01
 */
public class CmdRunner {

	/**
	 * Function for running the command in a command shell.
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param root The directory where the command will be executed.
	 * @since 0.01
	 */
	public static void runCommand(String root, String command) {
		try {
    		String commandString = "cd " + root
    				+ "&& "
    				+ command;

    		System.out.println("Running command:\n" + commandString);
			ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", commandString);
			builder.redirectErrorStream(true);
			Process p = builder.start();
			BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			while (true) {
			    line = r.readLine();
			    if (line == null) { break; }
			    System.out.println(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
