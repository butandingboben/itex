/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Base64;

/**
 * <div class="jp">
 * </div>
 * <p>
 * <div class="en" style="padding:0em 0.5em" >
 * Utility class that handles encryption-related functionalities.
 * </div>
 *
 * @author Richard Go
 * @version 0.01
 * @since 0.01
 */
public class Encryption {

    /**
     * Generate randoom string.
     * @maker Richard Go
     * @version 0.01
     * @param length length of the random string
     * @return random string
     * @since 0.01
     */
    public static String random(int length) {
        byte[] salt = new byte[length];
        new SecureRandom().nextBytes(salt);
        return hex(salt);
    }

    /**
     * Convert array of bytes to string containing lexical representation of base 64 binary.
     * @maker Richard Go
     * @version 0.01
     * @param bytes array of bytes to convert
     * @return A string containing a lexical representation of base64 binary
     * @since 0.01
     */
    public static String base64(byte[] bytes) {
        return DatatypeConverter.printBase64Binary(bytes);
    }

    /**
     * Convert string containing lexical representation of base64 binary to array of bytes.
     * @maker Richard Go
     * @version 0.01
     * @param str A string containing lexical representation of base64 binary
     * @return array of bytes
     * @since 0.01
     */
    public static byte[] base64(String str) {
        return DatatypeConverter.parseBase64Binary(str);
    }

    /**
     * Convert array of bytes to string containing lexical representation of hex binary.
     * @maker Richard Go
     * @version 0.01
     * @param bytes array of bytes to convert
     * @return string containing lexical representation of hex binary
     * @since 0.01
     */
    public static String hex(byte[] bytes) {
        return DatatypeConverter.printHexBinary(bytes);
    }

    /**
     * Convert string containing lexical representation of hex binary to array of bytes.
     * @maker Richard Go
     * @version 0.01
     * @param str string containing lexical representation of hex binary
     * @return array of bytes
     * @since 0.01
     */
    public static byte[] hex(String str) {
        return DatatypeConverter.parseHexBinary(str);
    }

    /**
     * <div class="jp">
     * </div>
     * <p>
     * <div class="en" style="padding:0em 0.5em" >
     * Hashes the given message using HMAC SHA-256.
     * Expected length of string is 64.
     * </div>
     * @param secretKey the secret key to be used in the hash method.
     * @param message the message to be hashed.
     * @return hashed message
     * @throws NoSuchAlgorithmException the exception for no such algorithm.
     * @throws InvalidKeyException the exception for invalid key.
     * @throws UnsupportedEncodingException when the algorithm is not supported.
     * @throws IllegalStateException when the encoding is unable to execute.
     */
    public static String hash(String secretKey, String message)
            throws NoSuchAlgorithmException, InvalidKeyException,
                    IllegalStateException, UnsupportedEncodingException {
        final int filter = 0xff;
        final int offset = 0x100;
        final int base = 16;

        Mac sha256 = Mac.getInstance("HmacSHA256");
        final SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(), "HmacSHA256");
        sha256.init(secretKeySpec);
        byte[] hexHash = sha256.doFinal(message.getBytes("UTF-8"));

        String hex = null;
        StringBuffer strHash = new StringBuffer();
        for (int idx=0; idx<hexHash.length; idx++) {
            hex = Integer.toString((hexHash[idx] & filter) + offset, base).substring(1);
            strHash.append(hex);
        }
        return strHash.toString();
    }

    /**
     * <div class="jp">
     * </div>
     * <p>
     * <div class="en" style="padding:0em 0.5em" >
     * Hashes the given message using HMAC SHA-256, then encodes the string using base64 encoding.
     * Expected length of string is 88.
     * </div>
     * @param secretKey the secret key to be used in the hash method.
     * @param message the message to be hashed.
     * @return hashed message
     * @throws NoSuchAlgorithmException the exception for no such alogorithm.
     * @throws InvalidKeyException the exception for invalid key.
     * @throws UnsupportedEncodingException when the algorithm is not supported.
     * @throws IllegalStateException when the encoding is unable to execute.
     */
    public static String encrpytedHash(String secretKey, String message)
            throws NoSuchAlgorithmException, InvalidKeyException,
                    IllegalStateException, UnsupportedEncodingException {
        return Base64.encodeBase64String(hash(secretKey, message).getBytes());
    }
}
