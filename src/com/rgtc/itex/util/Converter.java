/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) RococoGlobal Technologies Corporation - All Rights Reserved 2013
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.util;

import static com.rgtc.itex.util.UtilConstants.COMMA;
import static com.rgtc.itex.util.UtilConstants.DATABASE_EMPTY_STRING;
import static com.rgtc.itex.util.UtilConstants.DYNAMIC_TAG;
import static com.rgtc.itex.util.UtilConstants.EMPTY_TAG;
import static com.rgtc.itex.util.UtilConstants.IS;
import static com.rgtc.itex.util.UtilConstants.NOT;
import static com.rgtc.itex.util.UtilConstants.NULL;
import static com.rgtc.itex.util.UtilConstants.NULL_TAG;
import static com.rgtc.itex.util.UtilConstants.DOUBLE_QUOTE;
import static com.rgtc.itex.util.UtilConstants.SINGLE_QUOTE;
import static com.rgtc.itex.util.UtilConstants.SQL_AND;
import static com.rgtc.itex.util.UtilConstants.SQL_EQUALS;
import static com.rgtc.itex.util.UtilConstants.STRING;
import static com.rgtc.itex.util.UtilConstants.EMPTY_STRING;
import static com.rgtc.itex.util.UtilConstants.ENCRYPT_FLAG;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * This class handles [CONVERSION] functions.
 * 
 * @author Eldon Ivan
 * @since 12/03/2013
 * @version 1
 */
public class Converter {
	
	/**
	 * Convert a list of strings to a singular comma separated string.
	 * @param list
	 * @return String
	 */
	public static String listToCommaDelimitedString(List<String> list) {
		return StringUtils.join(list, COMMA);
	}
	
	public static String getInsertValues(List<String> data, List<String> types){
		StringBuffer sb = null;
//		System.out.println("Data size: " + data.size());
//		System.out.println("types size: " + types.size());
		if(data.size() == types.size()){
			sb = new StringBuffer();;
			for(int i =0; i<data.size(); i++){
				
				if(NULL_TAG.equalsIgnoreCase(data.get(i))){
					sb.append(NULL);
				} else if (STRING.equalsIgnoreCase(types.get(i)) || data.get(i).matches("(.*)--(\\-)+(.*)")){
					sb.append(toInsertString(data.get(i)));
				} else if (EMPTY_TAG.equalsIgnoreCase(data.get(i))){
					// also include  quotes for empty tag -eldon 20140731
					sb.append(toInsertString(EMPTY_STRING));
				} else if ((NULL_TAG + NULL_TAG).equalsIgnoreCase(data.get(i))){
					// Support for single-dash as data value
					sb.append(toInsertString(NULL_TAG));
				} else if("point".equalsIgnoreCase(types.get(i))) {
					if (data.get(i).contains("point")) {
						sb.append(data.get(i));
					} else {
						sb.append("POINT(" + data.get(i) + ")");
					}
				} else {
					sb.append(data.get(i));
				}
				
				if(i != data.size()-1){
					sb.append(COMMA);
				}
			}
		}
		return sb.toString();
	}
	
	public static String getSelectWhereValues(List<String> columnNames, List<String> data, List<String> types){
		StringBuffer sb = null;
		if(data.size() == types.size()){
			sb = new StringBuffer();;
			for(int i =0; i<data.size(); i++){
				sb.append(columnNames.get(i));
				// added by: Joven Montero
				// for handling dynamic values and empty strings
				if (DYNAMIC_TAG.equalsIgnoreCase(data.get(i))) {
					sb.append(IS);
					sb.append(NOT);
					sb.append(NULL);
				}else if (StringUtils.contains(data.get(i), EMPTY_TAG)
						&& ENCRYPT_FLAG == types.get(i)) {
					sb.append(SQL_EQUALS);
					sb.append(data.get(i).replace(EMPTY_TAG, EMPTY_STRING));
				}else if (StringUtils.contains(data.get(i), "'" + NULL_TAG + "'")
						&& ENCRYPT_FLAG == types.get(i)) {
					sb.append(IS);
					sb.append(NULL);
				}else if (EMPTY_TAG.equalsIgnoreCase(data.get(i))) {
					sb.append(SQL_EQUALS);
					sb.append(DATABASE_EMPTY_STRING);
				}else if(NULL_TAG.equalsIgnoreCase(data.get(i))){
					sb.append(IS);
					sb.append(NULL);
				}else if(STRING.equalsIgnoreCase(types.get(i))){
					sb.append(SQL_EQUALS);
					sb.append(toInsertString(data.get(i)));
				}else{
					sb.append(SQL_EQUALS);
					sb.append(data.get(i));
				}
				
				if(i != data.size()-1){
					sb.append(SQL_AND);
				}
			}
		}
		return sb.toString();
	}
	
	/**
	 * Append single quotes
	 * @param s
	 * @return String
	 */
	public static String toInsertString(String s) {
		if (s.contains("'")) {
			s = s.replace("'", "\\'");
		}
		//if (s.contains("(.*)--(\\-)(.*)")) {
		//	return DOUBLE_QUOTE + s + DOUBLE_QUOTE;
		//}
		return DOUBLE_QUOTE + s + DOUBLE_QUOTE;
	}
	
	/**
	 * Convert Date to String.
	 * @param d
	 * @return String
	 */
	public static String dateToString(Date d) { 
		return toInsertString(d.toString());
	}
	
	/**
	 * Convert Date to Timestamp String.
	 * @param d
	 * @return String
	 */
	public static String dateToTimestampString(Date d) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(d);
		return toInsertString(new Timestamp(cal.getTimeInMillis()).toString());
	}
	
	public static String cleanString(String string){
		String s = string;
		if("'".equals(string.charAt(0))){
			s = s.replace("'", "");
		}
//		else if('-' == string.charAt(0)&& string.length() == 1){
//			s = s.replace("-", "");
//		}
		return s;
	}
	
	public static String cleanColumnType(String colType){
		return colType.substring(0, colType.indexOf("("));
	}
	
	public static int getColumnLenght(String data){
		data.indexOf("(");
		data.indexOf(")");
		String tmp = data.substring(data.indexOf("(")+1, data.indexOf(")"));
		int length = 0;
		if (data.contains("DECIMAL".toLowerCase())) {
			tmp = tmp.split(",")[0];
			length = Integer.parseInt(tmp) + 1;
		} else {
			length = Integer.parseInt(tmp);
		}
		return length;
	}
}
