/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.conn.HttpHostConnectException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.appengine.api.datastore.Entity;
import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import com.rgtc.itex.constants.Constants;
import com.rgtc.itex.constants.Setup;
import com.rgtc.itex.main.DataLoaderUtil;
import com.rgtc.itex.main.ITEx;
import com.rgtc.itex.models.AssertionOutputModel;
import com.rgtc.itex.models.ResponseModel;
import com.rgtc.itex.models.TestCase;
import com.rgtc.itex.service.AesService;
import com.rgtc.itex.service.ApiService;
import com.rgtc.itex.service.DatastoreService;
import com.rgtc.itex.service.FileService;
import com.rgtc.itex.service.TaskQueueService;
import com.rgtc.itex.util.JSONExcelUtil;

/**
 * <div class="jp">
 * 
 * </div>
 * <p>
 * <div class="en" style="padding:0em 0.5em" >
 * 
 * </div>
 *
 * @author Joven 'Bob' Montero
 * @version 0.01
 *		<div class="jp">
 *			
 *		</div>
 *		<div class="en" style="padding:0em 0.5em">
 *			
 *		</div>
 * @since 0.01
 */
public class MainWindow {

	/**
	 * UI component declarations.
	 */
	private JFrame mainFrame;
	private JButton saveCredBtn;
	private JButton browseBtn;
	private JButton runTestBtn;
	private JTable testSourceTbl;
	private JTable testCaseTbl;
	private JTextField urlTxFld;
	private JTextField schemaTxFld;
	private JTextField usernameTxFld;
	private JTextField passwordTxFld;
	private JTextField testDirTxFld;
	private JTextPane summaryTxPn;
	private JTextPane testTxPn;
	private JProgressBar progressBar;

	/**
	 * Global variable declarations.
	 */
	private static String[] testSourceHeaders = {"", "File Name"};
	private static String[] testCaseHeaders = {"Test", "N/A", "Case"};
	private static String[] fileSuffix = {"xls"};

	private static Object[][] testSourceTblLst = {};
	private static Object[][] testCaseTblLst= {};

	private static String testDirectory = "";
	private static String url = "jdbc:mysql://127.0.0.1";
	private static String schema = "mypage";
	private static String userName = "root";
	private static String password = "root";

	private static int testCount = 0;
	private static int successCount = 0;
	private static int failCount = 0;
	private static int invalidCount = 0;
	private static int totalTestCount = 0;
	private static int partialTestCount = 0;

	private static List<Integer>  moduleList = new ArrayList<Integer>();
	private static List<String> filenameList = new ArrayList<String>();
	private static List<String> filePrefixList = new ArrayList<String>();
	private static List<String> failedTestList = new ArrayList<String>();
	private static List<String> failedDbTestList = new ArrayList<String>();
	private static List<String> failedResponseTestList = new ArrayList<String>();
	private static List<String> failedStatusTestList = new ArrayList<String>();
	private static List<String> failedOpLogTestList = new ArrayList<String>();
	private static List<String> failedExecLogTestList = new ArrayList<String>();
	private static List<String> ignoredTestList = new ArrayList<String>();
	private static List<String> naTestList = new ArrayList<String>();
	private static List<String> exceptionTestList = new ArrayList<String>();

	private static Map<String, Object[][]> testList = new HashMap<String, Object[][]>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
						if ("Nimbus".equals(info.getName())) {
							UIManager.setLookAndFeel(info.getClassName());
							break;
						}
					}
					MainWindow window = new MainWindow();
					window.mainFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		mainFrame = new JFrame();
		mainFrame.setTitle("IT Executor");
		mainFrame.getContentPane().setBackground(SystemColor.controlHighlight);
		mainFrame.setFont(new Font("Verdana", Font.PLAIN, 11));
		mainFrame.setMinimumSize(new Dimension(950, 500));
		mainFrame.setBounds(100, 100, 960, 650);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setLocationRelativeTo(null);
		
		JPanel setupPanel = new JPanel();
		setupPanel.setBackground(SystemColor.control);
		
		JPanel resultPanel = new JPanel();
		resultPanel.setBackground(SystemColor.control);
		GroupLayout groupLayout = new GroupLayout(mainFrame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(setupPanel, GroupLayout.PREFERRED_SIZE, 378, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(resultPanel, GroupLayout.DEFAULT_SIZE, 560, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(setupPanel, GroupLayout.DEFAULT_SIZE, 461, Short.MAX_VALUE)
				.addComponent(resultPanel, GroupLayout.DEFAULT_SIZE, 461, Short.MAX_VALUE)
		);
		
		JPanel credentialPanel = new JPanel();
		credentialPanel.setBackground(SystemColor.controlHighlight);
		
		JLabel credentialsHeader = new JLabel("Credentials");
		credentialsHeader.setMinimumSize(new Dimension(55, 15));
		credentialsHeader.setFont(new Font("Arial", Font.BOLD, 11));
		credentialsHeader.setBackground(SystemColor.control);
		
		JLabel lblUrl = new JLabel("URL:");
		lblUrl.setPreferredSize(new Dimension(35, 25));
		lblUrl.setMinimumSize(new Dimension(20, 25));
		lblUrl.setFont(new Font("Arial", Font.PLAIN, 11));
		lblUrl.setBackground(SystemColor.menu);
		
		JLabel lblSchema = new JLabel("Schema:");
		lblSchema.setPreferredSize(new Dimension(35, 25));
		lblSchema.setMinimumSize(new Dimension(20, 25));
		lblSchema.setFont(new Font("Arial", Font.PLAIN, 11));
		lblSchema.setBackground(SystemColor.menu);
		
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setPreferredSize(new Dimension(35, 25));
		lblUsername.setMinimumSize(new Dimension(20, 25));
		lblUsername.setFont(new Font("Arial", Font.PLAIN, 11));
		lblUsername.setBackground(SystemColor.menu);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setPreferredSize(new Dimension(35, 25));
		lblPassword.setMinimumSize(new Dimension(20, 25));
		lblPassword.setFont(new Font("Arial", Font.PLAIN, 11));
		lblPassword.setBackground(SystemColor.menu);
		
		saveCredBtn = new JButton("Save");
		saveCredBtn.addActionListener(new CustomAction());
		
		JPanel testSourcePanel = new JPanel();
		testSourcePanel.setBackground(SystemColor.control);
		testSourcePanel.setMaximumSize(new Dimension(32767, 100));
		
		JPanel testCasePanel = new JPanel();
		testCasePanel.setBackground(SystemColor.control);
		
		JScrollPane pane2 = new JScrollPane();
		GroupLayout gl_testCasePanel = new GroupLayout(testCasePanel);
		gl_testCasePanel.setHorizontalGroup(
			gl_testCasePanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_testCasePanel.createSequentialGroup()
					.addComponent(pane2, GroupLayout.DEFAULT_SIZE, 366, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_testCasePanel.setVerticalGroup(
			gl_testCasePanel.createParallelGroup(Alignment.LEADING)
				.addComponent(pane2, GroupLayout.DEFAULT_SIZE, 209, Short.MAX_VALUE)
		);
		
		testCaseTbl = new JTable(generateTableModel(testCaseTblLst, testCaseHeaders));
		testCaseTbl.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		testCaseTbl.setName(Constants.TEST_CASE_TABLE);
		testCaseTbl.addMouseListener(new CustomMouseAdapter());
		pane2.setViewportView(testCaseTbl);
		testCasePanel.setLayout(gl_testCasePanel);
		GroupLayout gl_setupPanel = new GroupLayout(setupPanel);
		gl_setupPanel.setHorizontalGroup(
			gl_setupPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_setupPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_setupPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(testSourcePanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(gl_setupPanel.createParallelGroup(Alignment.LEADING, false)
							.addComponent(credentialPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(testCasePanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
		);
		gl_setupPanel.setVerticalGroup(
			gl_setupPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_setupPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(credentialPanel, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(testSourcePanel, GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(testCasePanel, GroupLayout.DEFAULT_SIZE, 209, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		JLabel lblTestDirectory = new JLabel("Test Directory:");
		lblTestDirectory.setPreferredSize(new Dimension(35, 25));
		lblTestDirectory.setMinimumSize(new Dimension(20, 25));
		lblTestDirectory.setFont(new Font("Arial", Font.BOLD, 11));
		lblTestDirectory.setBackground(SystemColor.menu);
		
		testDirTxFld = new JTextField();
		testDirTxFld.setToolTipText("Paste test directory here");
		testDirTxFld.setPreferredSize(new Dimension(12, 25));
		testDirTxFld.setMinimumSize(new Dimension(12, 25));
		testDirTxFld.setFont(new Font("Arial", Font.PLAIN, 11));
		testDirTxFld.setColumns(10);
		
		browseBtn = new JButton("Browse");
		browseBtn.addActionListener(new CustomAction());
		
		JScrollPane pane1 = new JScrollPane();
		GroupLayout gl_testSourcePanel = new GroupLayout(testSourcePanel);
		gl_testSourcePanel.setHorizontalGroup(
			gl_testSourcePanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_testSourcePanel.createSequentialGroup()
					.addGroup(gl_testSourcePanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_testSourcePanel.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblTestDirectory, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(testDirTxFld, GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(browseBtn))
						.addComponent(pane1, GroupLayout.DEFAULT_SIZE, 366, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_testSourcePanel.setVerticalGroup(
			gl_testSourcePanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_testSourcePanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_testSourcePanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTestDirectory, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(testDirTxFld, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
						.addComponent(browseBtn))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(pane1, GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE))
		);
		
		testSourceTbl = new JTable(generateTableModel(testSourceTblLst, testSourceHeaders));
		testSourceTbl.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		testSourceTbl.setName(Constants.TEST_SOURCE_TABLE);
		testSourceTbl.addMouseListener(new CustomMouseAdapter());
		pane1.setViewportView(testSourceTbl);
		testSourcePanel.setLayout(gl_testSourcePanel);
		
		urlTxFld = new JTextField();
		urlTxFld.setText("jdbc:mysql://127.0.0.1");
		urlTxFld.setPreferredSize(new Dimension(12, 25));
		urlTxFld.setMinimumSize(new Dimension(12, 25));
		urlTxFld.setFont(new Font("Arial", Font.PLAIN, 11));
		urlTxFld.setColumns(10);
		
		schemaTxFld = new JTextField();
		schemaTxFld.setText("mypage");
		schemaTxFld.setPreferredSize(new Dimension(12, 25));
		schemaTxFld.setMinimumSize(new Dimension(12, 25));
		schemaTxFld.setFont(new Font("Arial", Font.PLAIN, 11));
		schemaTxFld.setColumns(10);
		
		usernameTxFld = new JTextField();
		usernameTxFld.setText("root");
		usernameTxFld.setPreferredSize(new Dimension(12, 25));
		usernameTxFld.setMinimumSize(new Dimension(12, 25));
		usernameTxFld.setFont(new Font("Arial", Font.PLAIN, 11));
		usernameTxFld.setColumns(10);
		
		passwordTxFld = new JTextField();
		passwordTxFld.setText("root");
		passwordTxFld.setPreferredSize(new Dimension(12, 25));
		passwordTxFld.setMinimumSize(new Dimension(12, 25));
		passwordTxFld.setFont(new Font("Arial", Font.PLAIN, 11));
		passwordTxFld.setColumns(10);
		GroupLayout gl_credentialPanel = new GroupLayout(credentialPanel);
		gl_credentialPanel.setHorizontalGroup(
			gl_credentialPanel.createParallelGroup(Alignment.TRAILING)
				.addComponent(credentialsHeader, GroupLayout.PREFERRED_SIZE, 366, GroupLayout.PREFERRED_SIZE)
				.addGroup(gl_credentialPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_credentialPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_credentialPanel.createSequentialGroup()
							.addComponent(lblUrl, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(urlTxFld, GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_credentialPanel.createSequentialGroup()
							.addComponent(lblSchema, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(schemaTxFld, GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_credentialPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_credentialPanel.createSequentialGroup()
							.addComponent(lblPassword, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(passwordTxFld, GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE))
						.addGroup(gl_credentialPanel.createSequentialGroup()
							.addComponent(lblUsername, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(usernameTxFld, GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)))
					.addContainerGap())
				.addGroup(gl_credentialPanel.createSequentialGroup()
					.addContainerGap(296, Short.MAX_VALUE)
					.addComponent(saveCredBtn, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_credentialPanel.setVerticalGroup(
			gl_credentialPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_credentialPanel.createSequentialGroup()
					.addComponent(credentialsHeader, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(6)
					.addGroup(gl_credentialPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblUrl, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(urlTxFld, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblUsername, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(usernameTxFld, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_credentialPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSchema, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
						.addComponent(schemaTxFld, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblPassword, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
						.addComponent(passwordTxFld, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(saveCredBtn)
					.addContainerGap())
		);
		credentialPanel.setLayout(gl_credentialPanel);
		setupPanel.setLayout(gl_setupPanel);
		
		progressBar = new JProgressBar();
		progressBar.setToolTipText("Turns red when any of the test cases fails.");
		
		runTestBtn = new JButton("Run");
		runTestBtn.addActionListener(new CustomAction());
		
		JScrollPane scrollPane = new JScrollPane();
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setPreferredSize(new Dimension(6, 10));
		GroupLayout gl_resultPanel = new GroupLayout(resultPanel);
		gl_resultPanel.setHorizontalGroup(
			gl_resultPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_resultPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_resultPanel.createParallelGroup(Alignment.TRAILING)
						.addComponent(scrollPane, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 548, Short.MAX_VALUE)
						.addComponent(scrollPane_1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 548, Short.MAX_VALUE)
						.addGroup(gl_resultPanel.createSequentialGroup()
							.addComponent(progressBar, GroupLayout.DEFAULT_SIZE, 472, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(runTestBtn, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_resultPanel.setVerticalGroup(
			gl_resultPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_resultPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_resultPanel.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(progressBar, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(runTestBtn, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		summaryTxPn = new JTextPane();
		summaryTxPn.setEditable(false);
		summaryTxPn.setFont(new Font("Arial Unicode MS", Font.PLAIN, 11));
		summaryTxPn.setMaximumSize(new Dimension(2147483647, 10));
		scrollPane_1.setViewportView(summaryTxPn);
		
		testTxPn = new JTextPane();
		testTxPn.setEditable(false);
		testTxPn.setFont(new Font("Arial Unicode MS", Font.PLAIN, 11));
		scrollPane.setViewportView(testTxPn);
		resultPanel.setLayout(gl_resultPanel);
		mainFrame.getContentPane().setLayout(groupLayout);

		repaintTable(testSourceTbl);
		repaintTable(testCaseTbl);
	}

	public class CustomAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String source = e.getActionCommand();
			switch (source) {
			case Constants.SAVE_BUTTON:
				setCredentials();
				break;
			case Constants.BROWSE_BUTTON:
				browseDir();
				break;
			case Constants.RUN_BUTTON:
				testTxPn.setText("");
				summaryTxPn.setText("");
				Collections.sort(moduleList);
				CustomSwingWorker thread = new CustomSwingWorker();
				thread.execute();
				break;

			default:
				break;
			}
		}

		/**
		 * <div class="jp">
		 *
		 * </div>
		 * <p>
		 * <div class="en" style="padding:0em 0.5em" >
		 *
		 * </div>
		 * @maker Joven 'Bob' Montero
		 * @version 0.01
		 * @since 0.01
		 */
		private void browseDir() {
			JFileChooser fileChooser;
			FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel file", "xls");

			if (!testDirectory.equals("")) {
				fileChooser = new JFileChooser(testDirectory);
			} else if (!testDirTxFld.getText().equals("")) {
				fileChooser = new JFileChooser(testDirTxFld.getText());
			} else {
				fileChooser = new JFileChooser();
			}

			fileChooser.setFileFilter(filter);
			fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

			if (JFileChooser.APPROVE_OPTION == fileChooser.showOpenDialog(mainFrame)) {
				reset();
				
				testDirectory = fileChooser.getSelectedFile().toString();
//				testDirectory = "C:\\SBC\\Iteration 1\\02_SDLC\\04_Testing\\01_Test Specifications and Results\\01_IT Specifications";
				testDirTxFld.setText(testDirectory);

				getTestSources();
			}
		}

		/**
		 * <div class="jp">
		 *
		 * </div>
		 * <p>
		 * <div class="en" style="padding:0em 0.5em" >
		 *
		 * </div>
		 * @maker Joven 'Bob' Montero
		 * @version 0.01
		 * @since 0.01
		 */
		private void setCredentials() {
			if (!"".equals(urlTxFld.getText())) {
				url = urlTxFld.getText();
			} else {
				url = "jdbc:mysql://127.0.0.1";
			}
			if (!"".equals(schemaTxFld.getText())) {
				schema = schemaTxFld.getText();
			} else {
				schema = "mypage";
			}
			if (!"".equals(usernameTxFld.getText())) {
				userName = usernameTxFld.getText();
			} else {
				userName = "root";
			}
			if (!"".equals(passwordTxFld.getText())) {
				password = passwordTxFld.getText();
			} else {
				password = "root";
			}
			JOptionPane.showMessageDialog(mainFrame, "Credentials successfully saved.");
			System.out.println(url);
			System.out.println(schema);
			System.out.println(userName);
			System.out.println(password);
		}

	}

	public class CustomMouseAdapter extends MouseAdapter {

		@Override
		public void mouseClicked(MouseEvent e) {
			JTable table = (JTable) e.getComponent();
			String source = table.getName();
			int selectedRow = table.getSelectedRow();
			int selectedCol = table.getSelectedColumn();
			switch (source) {
			case Constants.TEST_SOURCE_TABLE:
				testCaseTbl.setEnabled(true);
				if (0 == selectedCol) {
					if ((boolean) testSourceTblLst[selectedRow][0]) {
						testSourceTblLst[selectedRow][0] = false;
						if (moduleList.contains(selectedRow)) {
							moduleList.remove(new Integer(selectedRow));
						}
					} else {
						testSourceTblLst[selectedRow][0] = true;
						if (!moduleList.contains(selectedRow)) {
							moduleList.add(new Integer(selectedRow));
						}
					}

					getNewTestCases(filenameList.get(selectedRow), (boolean) testSourceTblLst[selectedRow][0]);
				} else {
					if (null == testList.get(filenameList.get(selectedRow))) {
						getNewTestCases(filenameList.get(selectedRow), (boolean) testSourceTblLst[selectedRow][0]);
					} else {
						getExistingTestCases(filenameList.get(selectedRow));
					}
				}
				testList.put(filenameList.get(selectedRow), testCaseTblLst);

				testSourceTbl.setModel(generateTableModel(testSourceTblLst, testSourceHeaders));
				repaintTable(testSourceTbl);
				testSourceTbl.getSelectionModel().setSelectionInterval(selectedRow, selectedRow);
				break;
			case Constants.TEST_CASE_TABLE:
				if (0 == selectedCol) {
					int sourceRow = testSourceTbl.getSelectedRow();
					if ((boolean) testCaseTblLst[selectedRow][0]) {
						testCaseTblLst[selectedRow][0] = false;

						// Assess if all test are false.
						int falseCtr = 0;
						for (int i = 0; i < testCaseTblLst.length; i++) {
							if (!(boolean) testCaseTblLst[i][0]) {
								falseCtr++;
							}
						}

						// Set the test source if all tests becomes false
						if (falseCtr == testCaseTblLst.length) {
							testSourceTblLst[sourceRow][0] = false;
							if (moduleList.contains(sourceRow)) {
								moduleList.remove(new Integer(sourceRow));
							}
						}
					} else {
						testCaseTblLst[selectedRow][0] = true;
						if (!(boolean) testSourceTblLst[sourceRow][0]) {
							testSourceTblLst[sourceRow][0] = true;
							if (!moduleList.contains(sourceRow)) {
								moduleList.add(new Integer(sourceRow));
							}
						}
					}

					testSourceTbl.setModel(generateTableModel(testSourceTblLst, testSourceHeaders));
					repaintTable(testSourceTbl);
					testSourceTbl.getSelectionModel().setSelectionInterval(sourceRow, sourceRow);

					testCaseTbl.setModel(generateTableModel(testCaseTblLst, testCaseHeaders));
					repaintTable(testCaseTbl);
					testCaseTbl.getSelectionModel().setSelectionInterval(selectedRow, selectedRow);
				} else if (1 == selectedCol) {
					int sourceRow = testSourceTbl.getSelectedRow();
					if ((boolean) testCaseTblLst[selectedRow][1]) {
						testCaseTblLst[selectedRow][1] = false;
					} else {
						testCaseTblLst[selectedRow][1] = true;
					}

					testSourceTbl.setModel(generateTableModel(testSourceTblLst, testSourceHeaders));
					repaintTable(testSourceTbl);
					testSourceTbl.getSelectionModel().setSelectionInterval(sourceRow, sourceRow);

					testCaseTbl.setModel(generateTableModel(testCaseTblLst, testCaseHeaders));
					repaintTable(testCaseTbl);
					testCaseTbl.getSelectionModel().setSelectionInterval(selectedRow, selectedRow);
				}

				break;

			default:
				break;
			}
		}

	}

	public class CustomSwingWorker extends SwingWorker {

		/* (non-Javadoc)
		 * @see javax.swing.SwingWorker#doInBackground()
		 */
		@Override
		protected Object doInBackground() throws Exception {
			progressBar.setValue(0);
			runTestBtn.setEnabled(false);
			testSourceTbl.setEnabled(false);
			testCaseTbl.setEnabled(false);

			// Execute all tests.
			runITEx();
			return null;
		}
		protected void done() {
			if (testTxPn.getText().contains("Test Result:	NG")) {
				UIManager.put("nimbusOrange",new Color(0xBB5548));
			} else {
				UIManager.put("nimbusOrange",new Color(0x69B076));
			}
			progressBar.setValue(100);
			runTestBtn.setEnabled(true);
			testSourceTbl.setEnabled(true);
			testCaseTbl.setEnabled(true);
		};
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param testSourceTblLst
	 * @param testSourceHeaders
	 * @return
	 * @since 0.01
	 */
	private TableModel generateTableModel(Object[][] testSourceTblLst,
			String[] testSourceHeaders) {
		DefaultTableModel tableModel = new DefaultTableModel(testSourceTblLst, testSourceHeaders) {
			private static final long serialVersionUID = 3564497240160134166L;

			public boolean isCellEditable(int row, int column) {
				return false;
			}

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				switch (columnIndex) {
				case 0:
                    return Boolean.class;
				case 1:
					if ("File Name".equals(getColumnName(columnIndex))) {
	                    return String.class;
					}
                    return Boolean.class;

				default:
					return super.getColumnClass(columnIndex);
				}
			}
		};
		return tableModel;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @since 0.01
	 */
	public void runITEx() {
		List<File> testFiles = getModulesToExecute();
		List<String> testCases = getTestsToExecute();
		List<String> invalidTestCases = getInvalidTests();
//		System.out.println(testFiles);
//		System.out.println(testCases);
//		System.out.println(invalidTestCases);
		totalTestCount = testCases.size();
		partialTestCount = 0;

		File dumpDir = new File(testDirectory + "\\" + Constants.DUMP_DIR);
		// Deleting all sql dump file inside the Dumps folder.
    	if (dumpDir.exists()) {
			for (File dumpFile : dumpDir.listFiles()) {
				dumpFile.delete();
			}
		} else {
			dumpDir.mkdir();
		}

    	StringBuffer finalOutput = new StringBuffer();
    	for (File testSource : testFiles) {
        	try {
				StringBuffer output = new StringBuffer();
				StringBuffer header = new StringBuffer();
				finalOutput = new StringBuffer();
				header.append("-----------------------------------------------------------------------------------------------------------------");
				header.append("\n");
				header.append("Test Module:\t" + testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, ""));
				header.append("\n");
				header.append("-----------------------------------------------------------------------------------------------------------------");
				output.append(header.toString());
				output.append("\n");
				output.append("\n");
				finalOutput.append(testTxPn.getText());
				finalOutput.append(output.toString());
				testTxPn.setText(finalOutput.toString());

				// Write to file
				FileService.writeToFile(
						dumpDir.getAbsolutePath() + "\\"
							+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
							+ " IT Execution Result.txt",
						output.toString());

				// Getting the start time of the test.
				Long startRunTime = ITEx.getTime();

				resetTest();

				// Execute test
				// Executing the test.
		    	if (!testSource.getName().contains(Constants.CRON_LEVEL)) {
					// Execute api test
					runTest(testSource, testCases, invalidTestCases);
				} else {
					// Execute cron test
					runCronTest(testSource, testCases, invalidTestCases);
				}

				// Getting the end time of the test.
				Long endRunTime = ITEx.getTime();

				// Calculating the total run time.
				float totalRunTime =  ((float) (endRunTime - startRunTime)) / 1000;
				int totalHours = 0;
				int totalMinutes = 0;
				int totalSeconds = 0;

				totalHours = (int) ((totalRunTime / 60) / 60);
				totalMinutes = (int) ((totalRunTime / 60) - (totalHours * 60));
				totalSeconds = (int) totalRunTime - (totalMinutes * 60) - (totalHours * 3600);

				finalOutput = new StringBuffer("");
				output = new StringBuffer("");
				output.append("\n");
				output.append("Test execution finished.");
				output.append("\n");
				output.append("Total Time (sec): ");
				if (0 < totalHours) {
					output.append(totalHours + "hr ");
				}
				if (0 < totalMinutes) {
					output.append(totalMinutes + "min ");
				}
				if (0 < totalSeconds) {
					output.append(totalSeconds + "s");
				}
				output.append("\n");
				output.append("Test Summary:");
				output.append("\n");
				output.append("\tNo. of Tests:\t" + testCount);
				output.append("\n");
				output.append("\t\tSuccess:\t" + successCount);
				output.append("\n");
				output.append("\t\tFailure:\t" + failCount);
				output.append("\n");
				if (0 < ignoredTestList.size()) {
					output.append("\t\tIgnored:\t" + ignoredTestList.size());
					output.append("\n");
				}
				if (0 < invalidCount) {
					output.append("\t\tNA:\t\t\t" + invalidCount);
					output.append("\n");
				}
				if (0 < exceptionTestList.size()) {
					output.append("\t\tError:\t" + exceptionTestList.size());
					output.append("\n");
				}
				output.append("\n");
				if (0 < failCount) {
					output.append("\tFailed Tests:\t");
					output.append("\n");
					output.append(failedTestSummary());
				}
				if (0 < ignoredTestList.size()) {
					output.append("\tIgnored Tests:\t");
					output.append("\n");
					for (String testCase : ignoredTestList) {
						output.append("\t\t" + testCase);
						output.append("\n");
					}
				}
				if (0 < invalidCount) {
					output.append("\tNA Tests:\t");
					output.append("\n");
					for (String testCase : naTestList) {
						output.append("\t\t" + testCase);
						output.append("\n");
					}
				}
				if (0 < exceptionTestList.size()) {
					output.append("\tException Occurred:\t");
					output.append("\n");
					for (String testCase : exceptionTestList) {
						output.append("\t\t" + testCase);
						output.append("\n");
					}
				}
				output.append("\n");

				// Write to file
				FileService.writeToFile(
						dumpDir.getAbsolutePath() + "\\"
							+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
							+ " IT Execution Result.txt",
						output.toString());

				finalOutput.append(summaryTxPn.getText());
				finalOutput.append(header.toString());
				finalOutput.append(output.toString());
				summaryTxPn.setText(finalOutput.toString());
				summaryTxPn.setCaretPosition(summaryTxPn.getText().length());
				summaryTxPn.repaint();
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println(testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "") + ": "
						+ "Something went wrong. Unable to run test.");
			}
		}
    	JOptionPane.showMessageDialog(mainFrame, "Test execution finished.");
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @return
	 * @since 0.01
	 */
	private List<String> getInvalidTests() {
		List<String> output = new ArrayList<String>();
		for (Integer index : moduleList) {
			String module = (String) testSourceTblLst[index.intValue()][1] + ".xls";
			Object[][] testCaseList = testList.get(module);
			for (Object[] test : testCaseList) {
				if ((boolean) test[1]) {
					output.add((String) test[2]);
				}
			}
		}

		return output;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param testSource
	 * @param testCases 
	 * @param invalidTestCases
	 * @throws Exception 
	 * @since 0.01
	 */
	private void runTest(File testSource, List<String> testCases, List<String> invalidTestCases) throws Exception {
		StringBuffer finalOutput = new StringBuffer();
		/** DataLoaderUtil instance. **/
		DataLoaderUtil dataloader = null;
		/** JSONExcelUtil instance **/
		JSONExcelUtil jsonExcelUtil = new JSONExcelUtil();
		String startTimestamp = "";
		String endTimestamp = "";

		// Initializes the DataLoaderUtil instance.
		dataloader = new DataLoaderUtil(
				schema,
				url,
				userName,
				password);

		// Loads the test source file.
		dataloader.loadFile(testSource.getAbsolutePath());

		// Loads the sheet containing the data to inserted into the database.
		dataloader.loadSheet(Constants.DB_DATA_SHEET);

		// Loads the test source file for getting the JSON inputs.
		jsonExcelUtil.loadTestSource(testSource);

		for (String testCaseName : testCases) {
			TestCase test = new TestCase();
			test.setName(testCaseName);
			// Skipping of non-existent test case number in the current file.
			if (!dataloader.getScenarioNames().contains(test.getName())) {
				continue;
			}

			// Generating the initial timestamp.
			startTimestamp = ITEx.generateTimestamp();
			try {
				testCount++;

				// Removing all operation logs and execution logs;
				ITEx.emptyLogs("MypageOperationLog");
				ITEx.emptyLogs("MypageBackendTaskLog");

				// Validation the format of the output from the excel file.
				String validationMessage = dataloader.validateScenario(test.getName());
				if (0 > validationMessage.length()) {
					System.err.println("[" + test.getName() + "]\n"
							+ "Dataloader error: "
							+ validationMessage + "\n");
				} else {
					test.setInputJSON(jsonExcelUtil.getDataJSONList().get(testCaseName));
					test.setExpectedJSON(jsonExcelUtil.getExpectedJSONList().get(testCaseName));
					test.setRequestHeaderJSON(jsonExcelUtil.getRequestHeaderJSONList().get(testCaseName));
					test.setUrlParameterJSON(jsonExcelUtil.getUrlParameterJSONList().get(testCaseName));
					test.setOperationLogJSON(jsonExcelUtil.getOperationLogJSONList().get(testCaseName));

					if (null != test.getExpectedJSON()) {
						// Get expected response code from the expectedJSON object then
						// remove it from the json object.
						test.setExpectedStatusCd(test.getExpectedJSON().getInt("status"));
						test.getExpectedJSON().remove("status");
					} else {
						endTimestamp = ITEx.generateTimestamp();
						failedTestList.add(test.getName());
						failedDbTestList.add(test.getName());
						failedResponseTestList.add(test.getName());
						failedStatusTestList.add(test.getName());
						failedOpLogTestList.add(test.getName());
						failCount++;

						// Generating the output format.
						StringBuffer output = new StringBuffer();
						output.append("Test Case No.:\t" + testCaseName + "\n");
						output.append("Test Result:\t" + "Test case not found\n");
						output.append("Start Time:\t\t" + startTimestamp + "\n");
						output.append("End Time:\t\t" + endTimestamp + "\n");
						output.append("Input:" + "\n");
						output.append("\tURL Parameter: " + "None\n");
						output.append("\tRequest Header: " + "\n");
						output.append("\t" + "{}" + "\n");
						output.append("\tJSON Data: " + "\n");
						output.append("\t" + "{}" + "\n");
						output.append("\n");
						output.append("Output: " + "\n");
						output.append("\tRequest URL:\t\t\t\t" + "None\n");
						output.append("\tRequest Type:\t\t\t\t" + "None\n");
						output.append("\tHTTP Status Result:\t\t\t" + "NG\n");
						output.append("\t\tExpected HTTP Status:\t" + "None\n");
						output.append("\t\tActual HTTP Status:\t\t" + "None\n");
						output.append("\tJSON Result:\t\t\t\t" + "NG\n");
						output.append("\t\tExpected JSON Output:" + "\n");
						output.append("\t\t" + "{}" + "\n");
						output.append("\t\tActual JSON Output:" + "\n");
						output.append("\t\t" + "{}\n");
						output.append("\tDB Result:\t" + "NG\n");
						output.append("\n");

						finalOutput = new StringBuffer("");
						finalOutput.append(testTxPn.getText());
						finalOutput.append(output.toString());
						testTxPn.setText(finalOutput.toString());

						// Write to file
						FileService.writeToFile(
								testDirectory + "\\" + Constants.DUMP_DIR
									+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
									+ " IT Execution Result.txt",
								output.toString());

						// Update the progress bar.
						partialTestCount++;
						float currentProgress = ((float) partialTestCount / (float) totalTestCount) * 100;
						progressBar.setValue((int) currentProgress);
						UIManager.put("nimbusOrange",new Color(0xBB5548));
						testTxPn.setCaretPosition(testTxPn.getText().length());
						
						continue;
					}

					if (invalidTestCases.contains(test.getName())) {
						endTimestamp = ITEx.generateTimestamp();
						naTestList.add(test.getName());
						invalidCount++;

						// Generating the output format.
						StringBuffer output = new StringBuffer();
						output.append("Test Case No.:\t" + testCaseName + "\n");
						output.append("Test Result:\t" + "NA\n");
						output.append("Start Time:\t\t" + startTimestamp + "\n");
						output.append("End Time:\t\t" + endTimestamp + "\n");
						output.append("Input:" + "\n");
						if (null != test.getUrlParameterJSON()) {
							output.append("\tURL Parameter: ");
							output.append(test.getUrlParameterJSON().getString(Constants.URL_PARAMETER) + "\n");
						} else {
							output.append("\tURL Parameter: " + "None\n");
						}
						output.append("\tRequest Header: " + "\n");
						output.append("\t" + test.getRequestHeaderJSON().toString(5).replace("\n", "\n\t") + "\n");
						output.append("\tJSON Data: " + "\n");
						if (null != test.getInputJSON()) {
							output.append("\t" + test.getInputJSON().toString(5).replace("\n", "\n\t") + "\n");
						} else {
							output.append("\t" + "{}" + "\n");
						}
						output.append("\n");
						output.append("Output: " + "\n");
						output.append("\tRequest URL:\t\t\t\t" + "Invalid\n");
						output.append("\tRequest Type:\t\t\t\t" + "None\n");
						output.append("\tHTTP Status Result:\t\t\t" + "NG\n");
						output.append("\t\tExpected HTTP Status:\t" + test.getExpectedStatusCd() + "\n");
						output.append("\t\tActual HTTP Status:\t\t" + "None\n");
						output.append("\tJSON Result:\t\t\t\t" + "NG\n");
						output.append("\t\tExpected JSON Output:" + "\n");
						output.append("\t\t" + test.getExpectedJSON().toString(5).replace("\n", "\n\t\t") + "\n");
						output.append("\t\tActual JSON Output:" + "\n");
						output.append("\t\t" + "{}\n");
						output.append("\tDB Result:\t" + "NG\n");
						output.append("\n");

						finalOutput = new StringBuffer("");
						finalOutput.append(testTxPn.getText());
						finalOutput.append(output.toString());
						testTxPn.setText(finalOutput.toString());

						// Write to file
						FileService.writeToFile(
								testDirectory + "\\" + Constants.DUMP_DIR
									+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
									+ " IT Execution Result.txt",
								output.toString());

						// Update the progress bar.
						partialTestCount++;
						float currentProgress = ((float) partialTestCount / (float) totalTestCount) * 100;
						progressBar.setValue((int) currentProgress);

						if (testTxPn.getText().contains("Test Result:	NG")) {
							UIManager.put("nimbusOrange",new Color(0xBB5548));
						} else {
							UIManager.put("nimbusOrange",new Color(0x69B076));
						}
						testTxPn.setCaretPosition(testTxPn.getText().length());
						
						continue;
					}

					// Function call for loading the needed data into the database.
					dataloader.loadScenario(testCaseName);

					ApiService apiService = new ApiService();
					ResponseModel outputResponse = apiService.executeRequest(
							Setup.getInstance().getAPINumber(testSource.getName()),
							test.getRequestHeaderJSON(),
							test.getInputJSON(),
							test.getUrlParameterJSON(),
							testSource);

					AssertionOutputModel responseValidation = new AssertionOutputModel();
					AssertionOutputModel dbValidation = new AssertionOutputModel();
					boolean assertFlag = true;
					boolean assertCodeFlag = true;		

					if(null != test.getExpectedJSON()
							&& 0 < outputResponse.getMessage().length()
							&& 0 != outputResponse.getStatusCode()){
						// Validating the actual json response with the expected json response.
						try {
							responseValidation =
									jsonExcelUtil.validateJsonResponse(
											new JSONObject(outputResponse.getMessage()), test.getExpectedJSON());
						} catch (Exception e) {
							outputResponse.setMessage(new JSONObject().toString());
							responseValidation =
									jsonExcelUtil.validateJsonResponse(
											new JSONObject(outputResponse.getMessage()), test.getExpectedJSON());
						}

						// Validating the response code with the expected value.
						if(test.getExpectedStatusCd() != outputResponse.getStatusCode()){
							assertFlag = false;
							assertCodeFlag = false;
							failedStatusTestList.add(test.getName());
						}

						// Checking if the response validation returned fail.
						if(responseValidation.isError()){
							assertFlag = false;
							failedResponseTestList.add(test.getName());
						}
					} else {
						assertFlag = false;
						outputResponse.setMessage("{}");
						failedResponseTestList.add(test.getName());

						// Validating the response code with the expected value.
						if(test.getExpectedStatusCd() != outputResponse.getStatusCode()){
							assertCodeFlag = false;
							failedStatusTestList.add(test.getName());
						}
					}

					try {
						// Validating the database after the api request has been executed.
						dbValidation = dataloader.checkScenario("", test.getName());
					} catch (CommunicationsException dbException) {
						// DataLoaderUtil instance is re-initialize after MySQL service connection stoppage.
						dataloader = new DataLoaderUtil(
								schema,
								url,
								userName,
								password);

						// Loads the test source file.
						dataloader.loadFile(testSource.getAbsolutePath());

						// Loads the sheet containing the data to inserted into the database.
						dataloader.loadSheet(Constants.DB_DATA_SHEET);

						// Validation the format of the output from the excel file.
						dataloader.validateScenario(test.getName());

						// Validating the database after the api request has been executed.
						dbValidation = dataloader.checkScenario("", test.getName());
					}

					// Checking if the database validation returned fail.
					if(dbValidation.isError()){
						assertFlag = false;
						failedDbTestList.add(test.getName());
					}

					// Executing the taskqueue process.
					ITEx.sleep(10000);
					apiService.queueProcess(null);

					String operationLogComparison = null;
					String operationContent = "";
					if(null != test.getOperationLogJSON()
							&& 404 != test.getExpectedStatusCd()
							&& 405 != test.getExpectedStatusCd()){
						// Temporary fix for operationContent.
						try {
							operationContent = test.getOperationLogJSON().get("*" + Constants.OPERATION_CONTENT).toString();
							if (0 < operationContent.length()) {
								test.getOperationLogJSON().put(
										Constants.OPERATION_CONTENT,
										new JSONObject(operationContent));
							} else {
								test.getOperationLogJSON().put(
										Constants.OPERATION_CONTENT,
										"");
							}
							test.getOperationLogJSON().remove("*operationContent");
						} catch (JSONException e) {}
						operationLogComparison = isSame("MypageOperationLog", null, test.getOperationLogJSON(), operationContent);
						if (null == operationLogComparison) {
							int count = 1;
							int maxRetry = 4;
							do {
								ITEx.sleep(2000);
								apiService.queueProcess(null);
								operationLogComparison = isSame(
										"MypageOperationLog", null,
										test.getOperationLogJSON(), operationContent);
								if (maxRetry > count) {
									count++;
								} else {
									break;
								}
							} while (null == operationLogComparison);
						}
						if(null != operationLogComparison
								&& !operationLogComparison.contains("Result:\tOK")){
							assertFlag = false;
							failedOpLogTestList.add(test.getName());
						} else if (null == operationLogComparison){
							operationLogComparison = "\t\tNo Operation Log found.\n";
							assertFlag = false;
							failedOpLogTestList.add(test.getName());
						}
					}

					endTimestamp = ITEx.generateTimestamp();

					// Generating the output format.
					StringBuffer output = new StringBuffer();
					output.append("Test Case No.:\t" + testCaseName + "\n");
					output.append("Test Result:\t");
					if (assertFlag) {
						output.append("OK\n");
						successCount++;
					} else {
						output.append("NG\n");
						failedTestList.add(test.getName());
						failCount++;
					}
					output.append("Start Time:\t\t" + startTimestamp + "\n");
					output.append("End Time:\t\t" + endTimestamp + "\n");
					output.append("Input:" + "\n");
					output.append("\tURL Parameter: ");
					if (null != test.getUrlParameterJSON()) {
						output.append(test.getUrlParameterJSON().getString(Constants.URL_PARAMETER) + "\n");
					} else {
						output.append("No data" + "\n");
					}
					output.append("\tRequest Header: " + "\n");
					if (null != test.getRequestHeaderJSON()) {
						output.append("\t" + test.getRequestHeaderJSON().toString(5).replace("\n", "\n\t") + "\n");
					} else {
						output.append("\t" + "No data." + "\n");
					}
					output.append("\tJSON Data: " + "\n");
					if (null != test.getInputJSON()) {
						output.append("\t" + test.getInputJSON().toString(5).replace("\n", "\n\t") + "\n");
					} else {
						output.append("\t" + "{}" + "\n");
					}
					output.append("\n");
					output.append("Output: " + "\n");
					output.append("\tRequest URL:\t\t\t\t" + outputResponse.getRequestURL() + "\n");
					output.append("\tRequest Type:\t\t\t\t" + outputResponse.getRequestType() + "\n");
					output.append("\tHTTP Status Result:\t\t\t");
					if (assertCodeFlag) {
						output.append("OK\n");
					} else {
						output.append("NG\n");
					}
					output.append("\t\tExpected HTTP Status:\t" + test.getExpectedStatusCd() + "\n");
					output.append("\t\tActual HTTP Status:\t\t" + outputResponse.getStatusCode() + "\n");
					output.append("\tJSON Result:\t\t\t\t");
					if (!responseValidation.isError()) {
						output.append("OK\n");
					} else {
						output.append("NG\n");
					}
					output.append("\t\tExpected JSON Output:" + "\n");
					output.append("\t\t" + test.getExpectedJSON().toString(5).replace("\n", "\n\t\t") + "\n");
					output.append("\t\tActual JSON Output:" + "\n");
					output.append("\t\t" + new JSONObject(outputResponse.getMessage()).toString(5).replace("\n", "\n\t\t") + "\n");
					if (responseValidation.isError()) {
						output.append("\t\tAssertion Report:" + "\n");
						output.append("\t\t\t" + responseValidation.getError().replace("\n", "\n\t\t\t") + "\n");

					}
					if(null != operationLogComparison){
						output.append(operationLogComparison);
					}
					output.append("\tDB Result:\t");
					if (!dbValidation.isError()) {
						output.append("OK\n");
					} else {
						output.append("NG\n");
					}
					if (null != dbValidation.getError()) {
						output.append("\t\t" + dbValidation.getError().replace("\n", "\n\t\t"));
					}

					output.append("\n");

					finalOutput = new StringBuffer("");
					finalOutput.append(testTxPn.getText());
					finalOutput.append(output.toString());
					testTxPn.setText(finalOutput.toString());

					// Write to file
					FileService.writeToFile(
							testDirectory + "\\" + Constants.DUMP_DIR
								+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
								+ " IT Execution Result.txt",
							output.toString());

					// Empty the tables used during the testing.
					dataloader.loadCurrentDatabase("", test.getName(),
							url, userName, password);

					// Update the progress bar.
					partialTestCount++;
					float currentProgress = ((float) partialTestCount / (float) totalTestCount) * 100;
					progressBar.setValue((int) currentProgress);

					if (testTxPn.getText().contains("Test Result:	NG")) {
						UIManager.put("nimbusOrange",new Color(0xBB5548));
					} else {
						UIManager.put("nimbusOrange",new Color(0x69B076));
					}
					testTxPn.setCaretPosition(testTxPn.getText().length());
				}
			} catch(HttpHostConnectException e) {
				System.err.println("[" + testCaseName + "]: Server not responding.");
				// Empty the tables used during the testing.
				dataloader.loadCurrentDatabase("", testCaseName,
						url, userName, password);
			} catch (Exception e) {
				exceptionTestList.add(test.getName());

				endTimestamp = ITEx.generateTimestamp();

				// Generating the output format.
				StringBuffer output = new StringBuffer();
				output.append("Test Case No.:\t" + testCaseName + "\n");
				output.append("Test Result:\t" + "NG (Due to exception)\n");
				output.append("Start Time:\t\t" + startTimestamp + "\n");
				output.append("End Time:\t\t" + endTimestamp + "\n");
				output.append("Cause:" + "\n\t");
				output.append(ExceptionUtils.getStackTrace(e).replace("\n", "\n\t"));
				output.append("\n");

				finalOutput = new StringBuffer("");
				finalOutput.append(testTxPn.getText());
				finalOutput.append(output.toString());
				testTxPn.setText(finalOutput.toString());

				// Write to file
				FileService.writeToFile(
						testDirectory + "\\" + Constants.DUMP_DIR
							+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
							+ " IT Execution Result.txt",
						output.toString());

				// Empty the tables used during the testing.
				dataloader.loadCurrentDatabase("", test.getName(),
						url, userName, password);
			} 
		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param testSource
	 * @param testCases
	 * @param invalidTestCases
	 * @throws Exception 
	 * @throws IOException 
	 * @throws SQLException 
	 * @since 0.01
	 */
	private void runCronTest(File testSource, List<String> testCases, List<String> invalidTestCases) throws Exception {
		StringBuffer finalOutput = new StringBuffer();
		/** DataLoaderUtil instance. **/
		DataLoaderUtil dataloader = null;
		/** JSONExcelUtil instance **/
		JSONExcelUtil jsonExcelUtil = new JSONExcelUtil();
		String startTimestamp = "";
		String endTimestamp = "";
		boolean isCronJob = false;

		// Initializes the DataLoaderUtil instance.
		dataloader = new DataLoaderUtil(
				schema,
				url,
				userName,
				password);

		// Loads the test source file.
		dataloader.loadFile(testSource.getAbsolutePath());

		// Loads the sheet containing the data to inserted into the database.
		dataloader.loadSheet(Constants.DB_DATA_SHEET);

		// Loads the test source file for getting the JSON inputs.
		jsonExcelUtil.loadTestSource(testSource);

		if (testSource.getName().contains("API_02_04_01_01_01")) {
			isCronJob = true;
		}

		for (String testCaseName : testCases) {
			TestCase test = new TestCase();
			test.setName(testCaseName);
			// Skipping of non-existent test case number in the current file.
			if (!dataloader.getScenarioNames().contains(test.getName())) {
				continue;
			}

			// Generating the initial timestamp.
			startTimestamp = ITEx.generateTimestamp();
			try {
				testCount++;

				// Removing all operation logs and execution logs;
				ITEx.emptyLogs("MypageOperationLog");
				ITEx.emptyLogs("MypageBackendTaskLog");

				// Validation the format of the output from the excel file.
				String validationMessage = dataloader.validateScenario(test.getName());
				if (0 > validationMessage.length()) {
					System.err.println("[" + test.getName() + "]\n"
							+ "Dataloader error: "
							+ validationMessage + "\n");
				} else {
					test.setInputJSON(jsonExcelUtil.getDataJSONList().get(testCaseName));
					test.setExpectedJSON(jsonExcelUtil.getExpectedJSONList().get(testCaseName));
					test.setRequestHeaderJSON(jsonExcelUtil.getRequestHeaderJSONList().get(testCaseName));
					test.setUrlParameterJSON(jsonExcelUtil.getUrlParameterJSONList().get(testCaseName));
					test.setOperationLogJSON(jsonExcelUtil.getOperationLogJSONList().get(testCaseName));
					test.setDsOperationLogJSON(jsonExcelUtil.getDsOpLogJSONList().get(testCaseName));
					test.setTaskQueueJSON(jsonExcelUtil.getTaskQueueJSONList().get(testCaseName));

					if (null != test.getDsOperationLogJSON()) {
						DatastoreService ds = new DatastoreService("localhost", 8888, "XXXX", "XXXX");
						ds.insertOperationLogs(test.getDsOperationLogJSON());
					}

					if (null != test.getTaskQueueJSON()) {
						TaskQueueService tq = new TaskQueueService("localhost", 8888, "XXXX", "XXXX");
						tq.pushOperationLogs(test.getTaskQueueJSON());
					}

					if (null != test.getExpectedJSON()) {
						// Get expected response code from the expectedJSON object then
						// remove it from the json object.
						test.setExpectedStatusCd(test.getExpectedJSON().getInt("status"));
						test.getExpectedJSON().remove("status");
					} else {
						endTimestamp = ITEx.generateTimestamp();
						failedTestList.add(test.getName());
						failedDbTestList.add(test.getName());
						failedResponseTestList.add(test.getName());
						failedStatusTestList.add(test.getName());
						failedOpLogTestList.add(test.getName());
						failCount++;

						// Generating the output format.
						StringBuffer output = new StringBuffer();
						output.append("Test Case No.:\t" + testCaseName + "\n");
						output.append("Test Result:\t" + "Test case not found\n");
						output.append("Start Time:\t\t" + startTimestamp + "\n");
						output.append("End Time:\t\t" + endTimestamp + "\n");
						output.append("Input:" + "\n");
						output.append("\tURL Parameter: " + "None\n");
						output.append("\tRequest Header: " + "\n");
						output.append("\t" + "{}" + "\n");
						output.append("\tJSON Data: " + "\n");
						output.append("\t" + "{}" + "\n");
						output.append("\n");
						output.append("Output: " + "\n");
						output.append("\tRequest URL:\t\t\t\t" + "None\n");
						output.append("\tRequest Type:\t\t\t\t" + "None\n");
						output.append("\tHTTP Status Result:\t\t\t" + "NG\n");
						output.append("\t\tExpected HTTP Status:\t" + "None\n");
						output.append("\t\tActual HTTP Status:\t\t" + "None\n");
						output.append("\tJSON Result:\t\t\t\t" + "NG\n");
						output.append("\t\tExpected JSON Output:" + "\n");
						output.append("\t\t" + "{}" + "\n");
						output.append("\t\tActual JSON Output:" + "\n");
						output.append("\t\t" + "{}\n");
						output.append("\tDB Result:\t" + "NG\n");
						output.append("\n");

						finalOutput = new StringBuffer("");
						finalOutput.append(testTxPn.getText());
						finalOutput.append(output.toString());
						testTxPn.setText(finalOutput.toString());

						// Write to file
						FileService.writeToFile(
								testDirectory + "\\" + Constants.DUMP_DIR
									+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
									+ " IT Execution Result.txt",
								output.toString());

						// Update the progress bar.
						partialTestCount++;
						float currentProgress = ((float) partialTestCount / (float) totalTestCount) * 100;
						progressBar.setValue((int) currentProgress);
						UIManager.put("nimbusOrange",new Color(0xBB5548));
						testTxPn.setCaretPosition(testTxPn.getText().length());
						
						continue;
					}

					if (invalidTestCases.contains(test.getName())) {
						endTimestamp = ITEx.generateTimestamp();
						naTestList.add(test.getName());
						invalidCount++;

						// Generating the output format.
						StringBuffer output = new StringBuffer();
						output.append("Test Case No.:\t" + testCaseName + "\n");
						output.append("Test Result:\t" + "NA\n");
						output.append("Start Time:\t\t" + startTimestamp + "\n");
						output.append("End Time:\t\t" + endTimestamp + "\n");
						output.append("Input:" + "\n");
						output.append("\tRequest Header: " + "\n");
						output.append("\t" + test.getRequestHeaderJSON().toString(5).replace("\n", "\n\t") + "\n");
						output.append("\tJSON Data: " + "\n");
						if (null != test.getInputJSON()) {
							output.append("\t" + test.getInputJSON().toString(5).replace("\n", "\n\t") + "\n");
						} else {
							output.append("\t" + "{}" + "\n");
						}
						output.append("\n");
						output.append("Output: " + "\n");
						output.append("\tJSON Result:\t" + "None\n");
						output.append("\tDB Result:\t" + "None\n");
						output.append("\n");

						finalOutput = new StringBuffer("");
						finalOutput.append(testTxPn.getText());
						finalOutput.append(output.toString());
						testTxPn.setText(finalOutput.toString());

						// Write to file
						FileService.writeToFile(
								testDirectory + "\\" + Constants.DUMP_DIR
									+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
									+ " IT Execution Result.txt",
								output.toString());

						// Update the progress bar.
						partialTestCount++;
						float currentProgress = ((float) partialTestCount / (float) totalTestCount) * 100;
						progressBar.setValue((int) currentProgress);

						if (testTxPn.getText().contains("Test Result:	NG")) {
							UIManager.put("nimbusOrange",new Color(0xBB5548));
						} else {
							UIManager.put("nimbusOrange",new Color(0x69B076));
						}
						testTxPn.setCaretPosition(testTxPn.getText().length());
						
						continue;
					}

					// Function call for loading the needed data into the database.
					dataloader.loadScenario(testCaseName);

					ResponseModel outputResponse = null;
					if (isCronJob) {
						final ApiService apiService = new ApiService();
						outputResponse = apiService.executeRequest(
								Setup.getInstance().getAPINumber(testSource.getName()),
								test.getRequestHeaderJSON(),
								test.getInputJSON(),
								test.getUrlParameterJSON(),
								testSource);
					}

					TaskQueueService taskQueue = new TaskQueueService("localhost", 8888, "XXXX", "XXXX");
					if (!isCronJob) {
						// Insert task to task queue.
						taskQueue.pushTask(testSource.getName(), test.getInputJSON());
					}

					AssertionOutputModel responseValidation = new AssertionOutputModel();
					AssertionOutputModel dbValidation = new AssertionOutputModel();
					boolean assertFlag = true;
					boolean assertCodeFlag = true;		

					if (null != test.getExecutionLogJSON()
							&& test.getExecutionLogJSON().has("entryList")
							&& 1 < test.getExecutionLogJSON().getJSONArray("entryList").length()) {
							ITEx.sleep(2 * 60000);
					} else {
						ITEx.sleep(2 * 60000);
					}

					taskQueue.purgeTaskQueue("mypage-operation-log-entry");

					if (isCronJob) {
						if(null != test.getExpectedJSON()
								&& 0 != outputResponse.getStatusCode()){
							// Validating the actual json response with the expected json response.
							try {
								responseValidation =
										jsonExcelUtil.validateJsonResponse(
												new JSONObject(outputResponse.getMessage()), test.getExpectedJSON());
							} catch (Exception e) {
								outputResponse.setMessage(new JSONObject().toString());
								responseValidation =
										jsonExcelUtil.validateJsonResponse(
												new JSONObject(outputResponse.getMessage()), test.getExpectedJSON());
							}

							// Validating the response code with the expected value.
							if(test.getExpectedStatusCd() != outputResponse.getStatusCode()){
								assertFlag = false;
								assertCodeFlag = false;
								failedStatusTestList.add(test.getName());
							}

							// Checking if the response validation returned fail.
							if(responseValidation.isError()){
								assertFlag = false;
								failedResponseTestList.add(test.getName());
							}
						} else {
							assertFlag = false;
							outputResponse.setMessage("{}");
							failedResponseTestList.add(test.getName());

							// Validating the response code with the expected value.
							if(test.getExpectedStatusCd() != outputResponse.getStatusCode()){
								assertCodeFlag = false;
								failedStatusTestList.add(test.getName());
							}
						}
					}

					try {
						// Validating the database after the api request has been executed.
						dbValidation = dataloader.checkScenario("", test.getName());
					} catch (CommunicationsException dbException) {
						// DataLoaderUtil instance is re-initialize after MySQL service connection stoppage.
						dataloader = new DataLoaderUtil(
								schema,
								url,
								userName,
								password);

						// Loads the test source file.
						dataloader.loadFile(testSource.getAbsolutePath());

						// Loads the sheet containing the data to inserted into the database.
						dataloader.loadSheet(Constants.DB_DATA_SHEET);

						// Validation the format of the output from the excel file.
						dataloader.validateScenario(test.getName());

						// Validating the database after the api request has been executed.
						dbValidation = dataloader.checkScenario("", test.getName());
					}

					// Checking if the database validation returned fail.
					if(dbValidation.isError()){
						assertFlag = false;
						failedDbTestList.add(test.getName());
					}

					//check datastoreEntries
					String executionLogComparison = null;
					if (assertFlag && null != test.getExecutionLogJSON()) {
						executionLogComparison = isSame("MypageBackendTaskLog", null, test.getExecutionLogJSON(), "");
						if (null == executionLogComparison) {
							int count = 1;
							int maxRetry = 4;
							do {
								ITEx.sleep(2 * 1000);
								executionLogComparison = isSame(
										"MypageBackendTaskLog", null,
										test.getExecutionLogJSON(), "");
								if (maxRetry > count) {
									count++;
								} else {
									break;
								}
							} while (null == executionLogComparison);
						}
						if (null == executionLogComparison || !executionLogComparison.contains("Result:\tOK")) {
							failedExecLogTestList.add(test.getName());
							if (null == executionLogComparison) {
								executionLogComparison = "\tMypageBackendTaskLog Result:\tNG (No execution log found)\n";
							}
							assertFlag = false;
						}
					}

					endTimestamp = ITEx.generateTimestamp();

					// Generating the output format.
					StringBuffer output = new StringBuffer();
					output.append("Test Case No.:\t" + testCaseName + "\n");
					output.append("Test Result:\t");
					if (assertFlag) {
						output.append("OK\n");
						successCount++;
					} else {
						output.append("NG\n");
						failedTestList.add(test.getName());
						failCount++;
					}
					output.append("Start Time:\t\t" + startTimestamp + "\n");
					output.append("End Time:\t\t" + endTimestamp + "\n");
					output.append("Input:" + "\n");
					output.append("\tJSON Data: " + "\n");
					if (null != test.getInputJSON()) {
						output.append("\t" + test.getInputJSON().toString(5).replace("\n", "\n\t") + "\n");
					} else {
						output.append("\t" + "{}" + "\n");
					}
					output.append("\n");
					output.append("Output: " + "\n");
					if (isCronJob) {
						output.append("\tRequest URL:\t\t\t\t" + outputResponse.getRequestURL() + "\n");
						output.append("\tRequest Type:\t\t\t\t" + outputResponse.getRequestType() + "\n");
						output.append("\tHTTP Status Result:\t\t\t");
						if (assertCodeFlag) {
							output.append("OK\n");
						} else {
							output.append("NG\n");
						}
						output.append("\t\tExpected HTTP Status:\t" + test.getExpectedStatusCd() + "\n");
						output.append("\t\tActual HTTP Status:\t\t" + outputResponse.getStatusCode() + "\n");
						output.append("\tJSON Result:\t\t\t\t");
						if (!responseValidation.isError()) {
							output.append("OK\n");
						} else {
							output.append("NG\n");
						}
						output.append("\t\tExpected JSON Output:" + "\n");
						output.append("\t\t" + test.getExpectedJSON().toString(5).replace("\n", "\n\t\t") + "\n");
						output.append("\t\tActual JSON Output:" + "\n");
						output.append("\t\t" + new JSONObject(outputResponse.getMessage()).toString(5).replace("\n", "\n\t\t") + "\n");
						if (responseValidation.isError()) {
							output.append("\t\tAssertion Report:" + "\n");
							output.append("\t\t\t" + responseValidation.getError().replace("\n", "\n\t\t\t") + "\n");
						}
					}

					if(null != executionLogComparison){
						output.append(executionLogComparison);
					}

					output.append("\tDB Result:\t");
					if (!dbValidation.isError()) {
						output.append("OK\n");
					} else {
						output.append("NG\n");
					}
					if (null != dbValidation.getError()) {
						output.append("\t\t" + dbValidation.getError().replace("\n", "\n\t\t"));
					}

					output.append("\n");

					finalOutput = new StringBuffer("");
					finalOutput.append(testTxPn.getText());
					finalOutput.append(output.toString());
					testTxPn.setText(finalOutput.toString());

					// Write to file
					FileService.writeToFile(
							testDirectory + "\\" + Constants.DUMP_DIR
								+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
								+ " IT Execution Result.txt",
							output.toString());

					// Empty the tables used during the testing.
					dataloader.loadCurrentDatabase("", test.getName(),
							url, userName, password);

					// Update the progress bar.
					partialTestCount++;
					float currentProgress = ((float) partialTestCount / (float) totalTestCount) * 100;
					progressBar.setValue((int) currentProgress);

					if (testTxPn.getText().contains("Test Result:	NG")) {
						UIManager.put("nimbusOrange",new Color(0xBB5548));
					} else {
						UIManager.put("nimbusOrange",new Color(0x69B076));
					}
					testTxPn.setCaretPosition(testTxPn.getText().length());
				}
			} catch(HttpHostConnectException e) {
				System.err.println("[" + testCaseName + "]: Server not responding.");
				// Empty the tables used during the testing.
				dataloader.loadCurrentDatabase("", testCaseName,
						url, userName, password);
			} catch (Exception e) {
				exceptionTestList.add(test.getName());

				endTimestamp = ITEx.generateTimestamp();

				// Generating the output format.
				StringBuffer output = new StringBuffer();
				output.append("Test Case No.:\t" + testCaseName + "\n");
				output.append("Test Result:\t" + "NG (Due to exception)\n");
				output.append("Start Time:\t\t" + startTimestamp + "\n");
				output.append("End Time:\t\t" + endTimestamp + "\n");
				output.append("Cause:" + "\n\t");
				output.append(ExceptionUtils.getStackTrace(e).replace("\n", "\n\t"));
				output.append("\n");

				finalOutput = new StringBuffer("");
				finalOutput.append(testTxPn.getText());
				finalOutput.append(output.toString());
				testTxPn.setText(finalOutput.toString());

				// Write to file
				FileService.writeToFile(
						testDirectory + "\\" + Constants.DUMP_DIR
							+ testSource.getName().replace("." + Constants.EXCEL_FILE_FORMAT, "")
							+ " IT Execution Result.txt",
						output.toString());

				// Empty the tables used during the testing.
				dataloader.loadCurrentDatabase("", test.getName(),
						url, userName, password);
			} 
		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @since 0.01
	 */
	private void resetTest() {
		testCount = 0;
    	successCount = 0;
    	failCount = 0;
    	invalidCount = 0;
    	failedTestList = new ArrayList<String>();
    	failedDbTestList = new ArrayList<String>();
    	failedResponseTestList = new ArrayList<String>();
    	failedStatusTestList = new ArrayList<String>();
    	failedOpLogTestList = new ArrayList<String>();
    	ignoredTestList = new ArrayList<String>();
    	naTestList = new ArrayList<String>();
    	exceptionTestList = new ArrayList<String>();
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @since 0.01
	 */
	private List<File> getModulesToExecute() {
		List<File> output = new ArrayList<File>();
		for (Integer index : moduleList) {
			String module = (String) testSourceTblLst[index.intValue()][1] + ".xls";
			output.add(new File(testDirectory + "\\" + module));
		}

		return output;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @since 0.01
	 */
	private List<String> getTestsToExecute() {
		List<String> output = new ArrayList<String>();
		for (Integer index : moduleList) {
			String module = (String) testSourceTblLst[index.intValue()][1] + ".xls";
			Object[][] testCaseList = testList.get(module);
			for (Object[] test : testCaseList) {
				if ((boolean) test[0]) {
					output.add((String) test[2]);
				}
			}
		}

		return output;
	}


	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @return output
	 * @since 0.01
	 */
	public static String failedTestSummary() {
		StringBuffer output = new StringBuffer();

		output.append("\t\t----------------------------------------------------------------------------------------------" + "\n");
		output.append("\t\t| Failed Test Case          |    DB    | Response |  Status  | Operation Log | Execution Log |" + "\n");
		output.append("\t\t----------------------------------------------------------------------------------------------" + "\n");
		for (String testCase : failedTestList) {
			if (25 == testCase.length()) {
				output.append("\t\t| " + testCase + " |");
			} else {
				output.append("\t\t| " + testCase + "    |");
			}
			if (failedDbTestList.contains(testCase)) {
				output.append("   " + "Fail" + "   |");
			} else {
				output.append("   " + "    " + "   |");
			}
			if (failedResponseTestList.contains(testCase)) {
				output.append("   " + "Fail" + "   |");
			} else {
				output.append("   " + "    " + "   |");
			}
			if (failedStatusTestList.contains(testCase)) {
				output.append("   " + "Fail" + "   |");
			} else {
				output.append("   " + "    " + "   |");
			}
			if (failedOpLogTestList.contains(testCase)) {
				output.append("     " + "Fail" + "      |");
			} else {
				output.append("     " + "    " + "      |");
			}
			if (failedExecLogTestList.contains(testCase)) {
				output.append("     " + "Fail" + "      |");
			} else {
				output.append("     " + "    " + "      |");
			}
			output.append("\n");
		}
		output.append("\t\t----------------------------------------------------------------------------------------------" + "\n");

		return output.toString();
	}


	/**
	 * <div class="jp">
	 * Validated the expected and actual json
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param kind
	 * @param name
	 * @param expected
	 * @param operationContent
	 * @return
	 * @since 0.01
	 */
	public static String isSame(final String kind, final String name, JSONObject expected, String operationContent){
		JSONExcelUtil jsonExcelUtil = new JSONExcelUtil();
		DatastoreService ds = new DatastoreService("localhost", 8888, "XXXX", "XXXX");
		List<Entity> entities = ds.getAllEntities(kind, name);
		if (0 == entities.size()) {
			return null;
		}
		AssertionOutputModel aom = null;
		boolean isValid = false;
		JSONObject actualExecutionLogJSON = null;
		StringBuffer mismatch = new StringBuffer();
		boolean listFlag = false;
		JSONArray entryList = null;
		try {
			entryList = expected.getJSONArray("entryList");
			listFlag = true;
		} catch (Exception e) {
			listFlag = false;
		}
		if (!listFlag) {
			for (Entity e : entities) {
				HashMap<String, Object> props =  new HashMap<String, Object>(e.getProperties());
				Map<String, Object> cleanMap =  new HashMap<String, Object>();
				Iterator it = props.entrySet().iterator();
				while(it.hasNext()){
					Map.Entry<String, Object> pair = (Map.Entry)it.next();
					if(String.valueOf(pair.getValue()).equalsIgnoreCase("null")){
						//props.remove(pair.getKey());
						cleanMap.put(pair.getKey(), "");
					}else{
						cleanMap.put(pair.getKey(), pair.getValue());
					}
					it.remove();
				}
				actualExecutionLogJSON = new JSONObject(cleanMap);

				//Support for TEXT Object of operationContent.
				if (actualExecutionLogJSON.has("operationContent")
					/*&& actualExecutionLogJSON.getJSONObject("operationContent").has("value")*/) {

					JSONObject j = actualExecutionLogJSON.optJSONObject("operationContent");
					if(null != j && j.has("value")){
							String value = actualExecutionLogJSON.getJSONObject("operationContent").getString("value");
							actualExecutionLogJSON.remove("operationContent");
							actualExecutionLogJSON.put("operationContent", value);
					}


				}

				if (0 < operationContent.length()) {
					String actualOperationContent = AesService.decrypt(actualExecutionLogJSON
							.get(Constants.OPERATION_CONTENT).toString());
					actualExecutionLogJSON.put(Constants.OPERATION_CONTENT,
							getOperationContentJSON(actualOperationContent));
				}
				aom = jsonExcelUtil.validateJsonResponse(
						actualExecutionLogJSON, expected);
				mismatch.append("\t\t"
						+ actualExecutionLogJSON.toString(5).replace("\n",
								"\n\t\t") + "\n");
				if (!aom.isError()) {
					isValid = true;
					break;
				} else {
					mismatch.append("\t\t" + "Assertion Report:" + "\n");
					mismatch.append("\t\t\t" + aom.getError().replace("\n",
							"\n\t\t\t") + "\n");
				}
			}
			if (0 < operationContent.length()) {
				expected.put(Constants.OPERATION_CONTENT, new JSONObject(operationContent));
			}
		} else {

			for (Entity e : entities) {
				actualExecutionLogJSON = new JSONObject(e.getProperties());

				try {
					String content = actualExecutionLogJSON.get(Constants.CONTENT).toString();
					actualExecutionLogJSON.put(Constants.CONTENT,
							new JSONObject(content));
				} catch(JSONException err) {}

				mismatch.append("\t\t"
					+ actualExecutionLogJSON.toString(5).replace(
							"\n", "\n\t\t") + "\n");
			}

			for (int i = 0; i < entryList.length(); i++) {
				try {
					String content = entryList.getJSONObject(i).getString(Constants.CONTENT);
					entryList.getJSONObject(i).put(Constants.CONTENT,
							new JSONObject(content));
				} catch(JSONException err) {
					err.printStackTrace();
				}

				for (Entity e : entities) {

					actualExecutionLogJSON = new JSONObject(e.getProperties());
					try {
						actualExecutionLogJSON.put(Constants.CONTENT,
								new JSONObject(actualExecutionLogJSON.get(Constants.CONTENT).toString()));
					} catch(JSONException err) {}

					aom = jsonExcelUtil.validateJsonResponse(
							actualExecutionLogJSON, entryList.getJSONObject(i));
					if (!aom.isError()) {
						isValid = true;
						break;
					} else {
						System.err.println(aom.getError());
						isValid = false;
					}
				}

				if (!isValid) {
					break;
				}

			}
		}
		StringBuffer sb = new StringBuffer();
		if(isValid){
			sb.append("\t"+kind+" Result:\tOK\n");
		}else{
			sb.append("\t"+kind+" Result:\tNG\n");
		}
		sb.append("\t\tExpected "+kind+":\n");
		sb.append("\t\t"+expected.toString(5).replace("\n", "\n\t\t") + "\n");
		sb.append("\t\tActual "+kind+"(s):\n");
		sb.append(mismatch.toString());

		//clean up
		if (0 < entities.size()) {
			ds.removeEntry(kind);
		}

		return sb.toString();
	}



	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @source http://www.dreamincode.net/forums/topic/172977-how-to-make-a-program-wait-for-a-timer-to-stop-before-continuing/
	 * @version 0.01
	 * @param actualOperationContent
	 * @return
	 * @since 0.01
	 */
	private static JSONObject getOperationContentJSON(
			String actualOperationContent) {
		JSONObject output = new JSONObject();
		String[] data = actualOperationContent.split(",");
		for (int i = 0; i < data.length; i++) {
			try {
				output.put(data[i].split("=")[0], data[i].split("=")[1].replace("\\\\", "\\"));
			} catch (ArrayIndexOutOfBoundsException e) {
				output.put(data[i].split("=")[0], "");
			}
		}

		return output;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param filename
	 * @since 0.01
	 */
	public void getExistingTestCases(String filename) {
		try {
			testCaseTblLst = testList.get(filename);
			testCaseTbl.setModel(generateTableModel(testCaseTblLst, testCaseHeaders));
			repaintTable(testCaseTbl);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param filename
	 * @param testFlag 
	 * @since 0.01
	 */
	public void getNewTestCases(String filename, boolean testFlag) {
		try {
			DataLoaderUtil dataloader = new DataLoaderUtil(schema, url, userName, password);
			dataloader.loadFile(testDirectory + "\\" + filename);
			dataloader.loadSheet(Constants.DB_DATA_SHEET);
			testCaseTblLst = new Object[dataloader.getScenarioNames().size()][3];

			for (int i = 0; i < dataloader.getScenarioNames().size(); i++) {
				testCaseTblLst[i][0] = testFlag;
				testCaseTblLst[i][1] = false;
				testCaseTblLst[i][2] = dataloader.getScenarioNames().get(i);
			}
			testList.put(filename, testCaseTblLst);

			testCaseTbl.setModel(generateTableModel(testCaseTblLst, testCaseHeaders));
			repaintTable(testCaseTbl);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @since 0.01
	 */
	public void getTestSources() {
		FileService fileService = new FileService();
		filenameList = new ArrayList<String>();
		filePrefixList.add("");
		filenameList = fileService.getListOfFiles(new File(testDirectory), filePrefixList, fileSuffix, "");
		testSourceTblLst = new Object[filenameList.size()][2];

		for (int i = 0; i < filenameList.size(); i++) {
			testSourceTblLst[i][0] = false;
			testSourceTblLst[i][1] = filenameList.get(i).replace(".xls", "");
		}

		testSourceTbl.setModel(generateTableModel(testSourceTblLst, testSourceHeaders));
		repaintTable(testSourceTbl);
	}

	/**
	 * <div class="jp">
	 * 
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 * 
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param table 
	 * @since 0.01
	 */
	private void repaintTable(JTable table) {
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.getColumnModel().getColumn(0).setPreferredWidth(40);
		if (2 == table.getColumnCount()) {
			table.getColumnModel().getColumn(1).setPreferredWidth(305);
		} else {
			table.getColumnModel().getColumn(1).setPreferredWidth(40);
			table.getColumnModel().getColumn(2).setPreferredWidth(265);
		}
		table.getTableHeader().setReorderingAllowed(false);
		table.getTableHeader().setResizingAllowed(false);
		table.repaint();
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @since 0.01
	 */
	public void reset() {
		testSourceTblLst = null;
		testCaseTblLst = null;
		moduleList = new ArrayList<Integer>();
		testList = new HashMap<String, Object[][]>();
		totalTestCount = 0;
		partialTestCount = 0;

		testTxPn.setText("");
		summaryTxPn.setText("");
		testSourceTbl.setModel(generateTableModel(testSourceTblLst, testSourceHeaders));
		repaintTable(testSourceTbl);
		testCaseTbl.setModel(generateTableModel(testCaseTblLst, testSourceHeaders));
		repaintTable(testCaseTbl);
		testCaseTbl.setEnabled(false);
		UIManager.put("nimbusOrange",new Color(0x69B076));
	}
}
