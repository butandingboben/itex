/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Co., Ltd. All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.ui;

import java.io.File;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.commons.io.FileExistsException;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.ComparisonFailure;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.rgtc.itex.constants.Constants;
import com.rgtc.itex.main.DataLoaderUtil;
import com.rgtc.itex.main.ITEx;
import com.rgtc.itex.models.AssertionOutputModel;
import com.rgtc.itex.models.JSONModel;
import com.rgtc.itex.service.CustomWebDriverListener;
import com.rgtc.itex.util.JSONExcelUtil;

/**
 * <div class="jp">
 * 
 * </div>
 * <p>
 * <div class="en" style="padding:0em 0.5em" >
 * 
 * </div>
 *
 * @author Joven 'Bob' Montero
 * @version 0.01
 *		<div class="jp">
 *			
 *		</div>
 *		<div class="en" style="padding:0em 0.5em">
 *			
 *		</div>
 * @since 0.01
 */
public class ClientTestTemp {
	
	private static String baseURL = "http://localhost:8888";
	private static EventFiringWebDriver firingDriver;
	private static final String ORDER = "order";
	private static final String IDENTIFIER = "identifier";
	private static final String TYPE = "type";
	private static final String DOM = "dom";
	private static final String VALUE = "value";
	private static final String ROWS = "rows";
	private static final String ID_TYPE = "id";
	private static final String NAME_TYPE = "name";
	private static final String CSS_TYPE = "css";
	private static final String XPATH_TYPE = "xpath";
	private static final String CLASS_TYPE = "class";
	private static final String TEXT_DOM_TYPE = "text";
	private static final String SELECT_DOM_TYPE = "select";
	private static final String RADIO_DOM_TYPE = "radio";
	private static final String CHECKBOX_DOM_TYPE = "checkbox";
	private static final String TABLE_DOM_TYPE = "table";
	private static final String FIELD_DOM_TYPE = "field";
	private static final String BUTTON_DOM_TYPE = "button";
	private static final String FILE_DOM_TYPE = "file";
	private static final String PASSWORD_DOM_TYPE = "password";
	private static final String EMAIL_DOM_TYPE = "email";

	public static void main(String args[]) throws JSONException {

//		JSONExcelUtil jsonExcelUtil = new JSONExcelUtil();
//		jsonExcelUtil.loadTestSource(new File("C://Users//butandingboben//Desktop//excel//API_02_04_02_05_01_マイページ会員仮登録_Template v2 (Client).xls"));
//		JSONArray elementsJSON = jsonExcelUtil.getDataJSONList().get(testCaseName).getJSONArray("elements");
//
////        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        // And now use this to visit Google
////        driver.get(baseURL + "/user/edit?userId=&mode=add");
//        firingDriver.get(baseURL + "/user/edit?userId=&mode=add");
//
//		for (int i = 0; i < elementsJSON.length(); i++) {
//			JSONObject element = elementsJSON.getJSONObject(i);
//			try {
//				setInput(element);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//
//        ITEx.sleep(10 * 1000);
//
//		// Assert output here
//        JSONArray expectedJSON = jsonExcelUtil.getExpectedJSONList().get(testCaseName).getJSONArray("elements");
//
//		for (int i = 0; i < expectedJSON.length(); i++) {
//			JSONObject element = expectedJSON.getJSONObject(i);
//			try {
//				assertOutput(element);
//			} catch (NoSuchElementException e) {
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}

//        Getting table values
//        WebElement updateTable = driver.findElement(By.id("update_history_table"));
//        List<WebElement> tr_collection = updateTable.findElements(By.xpath("id('update_history_table')/tbody/tr"));
//        for (WebElement trElement : tr_collection) {
//        	List<WebElement> td_collection = trElement.findElements(By.xpath("td"));
//            for(WebElement tdElement : td_collection)
//            {
//                System.out.println(tdElement.getAttribute("id") + ": " + tdElement.getText());
//            }
//            System.out.println();
//		}

		try {
//			JSONExcelUtil jsonExcelUtil = new JSONExcelUtil();
//			JSONModel output = jsonExcelUtil.getAllJSON(testFile, "JSON_Data");
			DataLoaderUtil dataloader = new DataLoaderUtil("otta", "jdbc:mysql://127.0.0.1", "root", "root");

		    File testFile = new File("C://Users//butandingboben//Desktop//excel//UW-0201.xls");
		    String testCaseName = "UW-0201" + "-001";
			File dbTables = new File("C://Rococo Files//Projects//Codes//Pre-SBC//itex 20141027//database tables.txt");
			dataloader.loadFile(testFile.getAbsolutePath());
			dataloader.loadSheet(Constants.DB_DATA_SHEET);
			dataloader.validateScenario(testCaseName);

			// Function call for loading the required data for testing.
			dataloader.loadScenario(testCaseName, dbTables);

			// Browser
		    System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		    WebDriver driver = new ChromeDriver();
		    firingDriver = new EventFiringWebDriver(driver);
		    CustomWebDriverListener driverEventListener = new CustomWebDriverListener();
		    firingDriver.register(driverEventListener);
		    firingDriver.get("http://127.0.0.1:8080/login");
		    firingDriver.findElement(By.name("username")).clear();
		    firingDriver.findElement(By.name("username")).sendKeys("jdl@yahoo.com");
		    firingDriver.findElement(By.name("password")).clear();
		    firingDriver.findElement(By.name("password")).sendKeys("test123456");
		    firingDriver.findElement(By.cssSelector("input.btn-custom-blue.regist-button")).click();
		    firingDriver.findElement(By.linkText("追加")).click();
		    firingDriver.findElement(By.name("name")).clear();
		    firingDriver.findElement(By.name("name")).sendKeys("New Device");
		    firingDriver.findElement(By.name("serialNumber")).clear();
		    firingDriver.findElement(By.name("serialNumber")).sendKeys("B1234567");
		    firingDriver.findElement(By.xpath("(//input[@name='serialNumber'])[2]")).sendKeys("7654321");
		    firingDriver.findElement(By.cssSelector("input.btn-custom-blue.new-device-btn")).click();

			JOptionPane.showMessageDialog(null, "Proceed to checking the database");

			// Database checking
			AssertionOutputModel output = dataloader.checkScenario("", testCaseName);
			System.out.println("Checking database result:");
			if(output.isError()){
				System.out.println(output.getError().replace("\n", "\n\t"));
			} else {
				System.out.println("\tSuccess");
			}

			JOptionPane.showMessageDialog(null, "Proceed to database clean-up");
	        firingDriver.quit();

			// Function call for cleaning the database tables used during testing.
			// Note: Remove/Comment-out this line if you want to retain the data.
			dataloader.loadCurrentDatabase(testCaseName, dbTables);
		} catch (Exception e) {
			e.printStackTrace();
	        firingDriver.quit();
		}

//        ITEx.sleep(10 * 1000);
//        firingDriver.quit();
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param element
	 * @since 0.01
	 */
	private static void assertOutput(JSONObject element) throws Exception {
		WebElement domElement = null;
		String identifier = "";
		String identifierType = "";
		String domType = "";
		String value = "";
		try {
			identifier = element.getString(IDENTIFIER);
			identifierType = element.getString(TYPE);
			domType = element.getString(DOM);
			value = element.getString(VALUE);
		} catch (JSONException e) {}

		switch (domType) {
		case FIELD_DOM_TYPE:
			domElement = getDomElement(identifier, identifierType);
			try {
				System.out.println("Expected Value: " + value);
				System.out.println("Actual Value: " + domElement.getAttribute("value"));
				Assert.assertEquals(domElement.getAttribute("value"), value);
			} catch (ComparisonFailure e) {
				System.out.println("Exception occurred");
				e.printStackTrace();
			}
			break;
		case TEXT_DOM_TYPE:
			domElement = getDomElement(identifier, identifierType);
			try {
				System.out.println("Expected Value: " + value);
				System.out.println("Actual Value: " + domElement.getText());
				Assert.assertEquals(domElement.getText(), value);
			} catch (ComparisonFailure e) {
				System.out.println("Exception occurred");
				e.printStackTrace();
			}
			break;
		case SELECT_DOM_TYPE:
			Select selectDom = new Select(getDomElement(identifier, identifierType));
			try {
				Assert.assertEquals(selectDom.getFirstSelectedOption().getAttribute("value"), value);
			} catch (ComparisonFailure e) {
				System.out.println("Exception occurred");
				e.printStackTrace();
			}
			break;
		case RADIO_DOM_TYPE:
			List<WebElement> radioButtons = getDomElements(identifier, identifierType);
			for (int i = 0; i < radioButtons.size(); i++) {
	        	WebElement radio = radioButtons.get(i);
	        	String radioValue = radio.getAttribute("value");
	        	if (String.valueOf(value).equals(radioValue)) {
	        		if (!radio.isSelected()) {
						throw new AssertionError("Incorrect value");
					} else {}
	        		break;
				}
			}
			break;
		case CHECKBOX_DOM_TYPE:
			break;
		case TABLE_DOM_TYPE:
			break;
		case BUTTON_DOM_TYPE:
			domElement = getDomElement(identifier, identifierType);
			domElement.click();
			break;
		default:
			// No dom specified
			break;
		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param element
	 * @throws Exception 
	 * @since 0.01
	 */
	private static void setInput(JSONObject element) throws Exception {
		WebElement domElement = null;
		String identifier = "";
		String identifierType = "";
		String domType = "";
		String value = "";
		try {
			identifier = element.getString(IDENTIFIER);
			identifierType = element.getString(TYPE);
			domType = element.getString(DOM);
			value = element.getString(VALUE);
		} catch (JSONException e) {}

		switch (domType) {
		case FIELD_DOM_TYPE:
		case PASSWORD_DOM_TYPE:
		case TEXT_DOM_TYPE:
			domElement = getDomElement(identifier, identifierType);
			domElement.sendKeys(String.valueOf(value));
			break;
		case SELECT_DOM_TYPE:
			Select selectDom = new Select(getDomElement(identifier, identifierType));
			selectDom.selectByValue(String.valueOf(value));
			break;
		case RADIO_DOM_TYPE:
			List<WebElement> radioButtons = getDomElements(identifier, identifierType);
			for (int i = 0; i < radioButtons.size(); i++) {
	        	WebElement radio = radioButtons.get(i);
	        	String radioValue = radio.getAttribute("value");
	        	if (String.valueOf(value).equals(radioValue)) {
	        		radio.click();
	        		break;
				}
			}
			break;
		case CHECKBOX_DOM_TYPE:
			domElement = getDomElement(identifier, identifierType);
			if (Boolean.parseBoolean(value)) {
				if (!domElement.isSelected()){
					domElement.click();
				}
			} else {
				if (domElement.isSelected()){
					domElement.click();
				}
			}
			break;
		case TABLE_DOM_TYPE:
			break;
		case BUTTON_DOM_TYPE:
			domElement = getDomElement(identifier, identifierType);
			domElement.click();
			break;
		case FILE_DOM_TYPE:
			domElement = getDomElement(identifier, identifierType);
			File fileInput = new File(value);
			if (!fileInput.exists()) {
				throw new FileExistsException("The file is not existing.");
			} else {
				domElement.sendKeys(fileInput.getAbsolutePath());
			}
			break;
		case EMAIL_DOM_TYPE:
			domElement = getDomElement(identifier, identifierType);
			
			// TODO Email format verification
			domElement.sendKeys(String.valueOf(value));
			break;
			
		default:
			// No dom specified
			break;
		}
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param string
	 * @param string2
	 * @return
	 * @since 0.01
	 */
	private static WebElement getDomElement(String identifier, String identifierType) {
		WebElement domElement = null;

		switch (identifierType) {
		case ID_TYPE:
			domElement = firingDriver.findElement(By.id(identifier));
			break;
		case NAME_TYPE:
			domElement = firingDriver.findElement(By.name(identifier));
			break;
		case CSS_TYPE:
			domElement = firingDriver.findElement(By.cssSelector(identifier));
			break;
		case XPATH_TYPE:
			domElement = firingDriver.findElement(By.xpath(identifier));
			break;
		case CLASS_TYPE:
			domElement = firingDriver.findElement(By.className(identifier));
			break;

		default:
			break;
		}
		
		return domElement;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Joven 'Bob' Montero
	 * @version 0.01
	 * @param string
	 * @param string2
	 * @return
	 * @since 0.01
	 */
	private static List<WebElement> getDomElements(String identifier, String identifierType) {
		List<WebElement> domElements = null;

		switch (identifierType) {
		case ID_TYPE:
			domElements = firingDriver.findElements(By.id(identifier));
			break;
		case NAME_TYPE:
			domElements = firingDriver.findElements(By.name(identifier));
			break;
		case CSS_TYPE:
			domElements = firingDriver.findElements(By.cssSelector(identifier));
			break;
		case XPATH_TYPE:
			domElements = firingDriver.findElements(By.xpath(identifier));
			break;
		case CLASS_TYPE:
			domElements = firingDriver.findElements(By.className(identifier));
			break;

		default:
			break;
		}
		
		return domElements;
	}
}
