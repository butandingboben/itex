/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Global Technologies Corporation All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.rgtc.itex.models.TableColumnModel;
import com.rgtc.itex.util.Converter;
import com.rgtc.itex.util.UtilConstants;
import com.rgtc.itex.util.TransactionManager;

/**
 * <div class="jp">
 *
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 *
 * </div>
 *
 * @author Eldon Leuterio
 * @version 0.01
 * @since 0.01
 */
public class DatabaseReader {

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 * Returns a Map of database column names and data type where key is the column name
	 * and value is the data type.
	 * </div>
	 * @maker Eldon Leuterio
	 * @version 0.01
	 * @param transactionManager The transaction manager.
	 * @param tableName The name of the table.
	 * @return Map<String, String>
	 * @throws SQLException The type of exception to be thrown.
	 * @since 0.01
	 */
	public static Map<String, TableColumnModel> getDatabaseTableMap(
			TransactionManager transactionManager,
			String tableName) throws SQLException {
		Map<String, TableColumnModel> databaseTableMap = new HashMap<String, TableColumnModel>();
		String query = UtilConstants.QUERY_ALL_COLUMNS.replace("?", tableName);
//		String[] name = tableName.split("\\.");
//		if (name.length == 2) {
//			tableName = "`" + name[0] + "`.`" + name[1] + "`";
//		}
		//defaults to quoted tablename which causes an error :(
		ResultSet rs = transactionManager.retrieve(query, null);
		while(rs.next()){
			TableColumnModel tcm = new TableColumnModel();
			String colName = rs.getString(1);
			String colDetails = rs.getString(2);
			if(colDetails.contains(UtilConstants.VARCHAR)
					|| colDetails.contains(UtilConstants.TEXT)
					|| colDetails.contains(UtilConstants.DATETIME)
					|| colDetails.contains(UtilConstants.ENUM)
					|| colDetails.contains(UtilConstants.DATE.toLowerCase())
					|| colDetails.contains(UtilConstants.TIME.toLowerCase())
					|| colDetails.contains(UtilConstants.CHAR.toLowerCase())){
				tcm.setType(UtilConstants.STRING);
//				colType = Constants.STRING;
			} else if(colDetails.contains(UtilConstants.POINT.toLowerCase())) {
				tcm.setType(UtilConstants.POINT.toLowerCase());
			} else if(colDetails.contains(UtilConstants.INTEGER)
					|| colDetails.contains(UtilConstants.SMALLINT.toLowerCase())){
				tcm.setType(UtilConstants.INTEGER);
//				colType = Constants.INTEGER;
			} else if(colDetails.contains(UtilConstants.BIT.toLowerCase())){
				tcm.setType(UtilConstants.BIT.toLowerCase());
//				colType = Constants.INTEGER;
			} else if(colDetails.contains(UtilConstants.ENUM)) {
				tcm.setType(UtilConstants.ENUM);
			} else if(colDetails.contains(UtilConstants.DOUBLE.toLowerCase())) {
				tcm.setType(UtilConstants.DOUBLE.toLowerCase());
			} else if(colDetails.contains(UtilConstants.DECIMAL.toLowerCase())) {
				tcm.setType(UtilConstants.DECIMAL.toLowerCase());
			} else if(colDetails.contains(UtilConstants.FLOAT.toLowerCase())) {
				tcm.setType(UtilConstants.FLOAT.toLowerCase());
			}
			if (colDetails.contains(UtilConstants.DATETIME)
					|| colDetails.contains(UtilConstants.TEXT)
					|| colDetails.contains(UtilConstants.ENUM)
					|| colDetails.contains(UtilConstants.DOUBLE.toLowerCase())
					|| colDetails.contains(UtilConstants.DATE.toLowerCase())
					|| colDetails.contains(UtilConstants.POINT.toLowerCase())
					|| colDetails.contains(UtilConstants.TIME.toLowerCase())
					|| colDetails.contains(UtilConstants.FLOAT.toLowerCase())) {
				tcm.setLength(255);
			} else {
				tcm.setLength(Converter.getColumnLenght(colDetails));
			}
			
			databaseTableMap.put(colName.toUpperCase(), tcm);
		}
		rs.close();
		return databaseTableMap;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker
	 * @version 0.01
	 * @param transactionManager The transaction manager.
	 * @param sqlStatement The sql statement.
	 * @return ResultSet
	 * @throws SQLException The type of exception to be thrown.
	 * @since 0.01
	 */
	public static ResultSet retrieveFromDB(TransactionManager transactionManager,
			String sqlStatement) throws SQLException{
		return transactionManager.retrieve(sqlStatement, null);
	}
}
