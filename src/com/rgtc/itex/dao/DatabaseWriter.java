/* --------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (c) Rococo Global Technologies Corporation All Rights Reserved 2014.
 * -------------------------------------------------------------------------------- */
package com.rgtc.itex.dao;

import java.sql.SQLException;
import java.util.List;

import com.rgtc.itex.util.TransactionManager;

/**
 * <div class="jp">
 *
 * </div>
 * <p>
 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
 *
 * </div>
 *
 * @author Eldon Leuterio
 * @version 0.01
 * @since 0.01
 */
public class DatabaseWriter {


	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker
	 * @version 0.01
	 * @param transactionManager The transaction manager.
	 * @param statements The statement.
	 * @return int
	 * @throws SQLException The type of exception to be thrown.
	 * @since 0.01
	 */
	public static int insertToDB(TransactionManager transactionManager,
			List<String> statements) throws SQLException {
		transactionManager.beginTransactional();
		int cnt=0;
		for(String s: statements){
			try {
				cnt+=transactionManager.create(s, null);
			} catch (Exception e) {
				System.out.println("[Incorrect SQL] " + s);
				throw new SQLException(
						"DB Table: " + s.split(" ")[2] + "\n"
						+ "Message: " + e.getMessage());
			}
		}
		return cnt;
	}

	/**
	 * <div class="jp">
	 *
	 * </div>
	 * <p>
	 * <div class="en" style="border:1px solid gray; padding:0em 0.5em" >
	 *
	 * </div>
	 * @maker Emman Resmeros
	 * @version 0.01
	 * @param transactionManager The transaction manager.
	 * @param statements The statement.
	 * @return int
	 * @throws SQLException The type of exception to be thrown.
	 * @since 0.01
	 */
	public static int deleteFromDB(TransactionManager transactionManager,
			List<String> statements) throws SQLException {
		transactionManager.beginTransactional();
		int count = 0;

		for (String s: statements) {
			count += transactionManager.delete(s, null);
		}
		return count;
	}
	
	public static void configureDatabase(TransactionManager transactionManager,
			List<String> statements) throws SQLException {
		transactionManager.beginTransactional();

		for (String s: statements) {
			transactionManager.configureDb(s, null);
		}
	}
}
